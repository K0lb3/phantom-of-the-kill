import os
import re
PATH = os.path.dirname(os.path.realpath(__file__))

SRC = os.path.join(PATH,'assembly','MasterDataTable')
DST = os.path.join(PATH,"lib","masterdata")

# 1 - name, 2 - args
rePARSE = re.compile(r'return new ([^\n]+?)\(\)\s*\{\s*(.+?)\s*\};', re.S)
# 1 - name, 2 - func, 3 - arg
reARG = re.compile(r'\s*(.+?) = reader.(.+?)\((.*?)?\),')


def extract_parser(fp):
    with open(fp, "rb") as f:
        text = f.read().decode("utf8")
    correct_name = os.path.basename(fp[:-3])
    args = None
    for match in rePARSE.finditer(text):
        if match[1] == correct_name:
            args = match[2]
            break
    if not args:
        raise EnvironmentError
    parser = []  # key, func, arg
    for match in reARG.finditer(args+","):
        parser.append([
            match[1], match[2], (True if match[3] ==
                                 'true' else False) if match[3] else None
        ])
    return parser

def generate_converter(parser):
    return """
from .master_data import MasterDataReader, MasterDataWriter

def load(data):
    reader = MasterDataReader(data)
    return [
        {
            %s
        }
        for i in range(reader.length)
    ]

def save(values):
    writer = MasterDataWriter()
    for value in values:
        %s
    return writer.bytes

"""%(
    # load
    ",\n            ".join(
        "{key} : reader.{func}({arg})".format(
            key=v[0],
            func=v[1],
            arg = "" if v[2] is None else v[2]
        ) for v in parser
    ),
    #save
    "        ".join(
        "writer.{func}(value[{key}]{args})\n".format(
            key=v[0],
            func=v[1].replace("Read","Write"),
            args = "" if v[2] is None else ",{}".format(v[2])
        ) for v in parser
    )
)


for fp in os.listdir(SRC):
    fp = os.path.join(SRC, fp)
    parser = extract_parser(fp)
    txt = generate_converter(parser)
    print()
