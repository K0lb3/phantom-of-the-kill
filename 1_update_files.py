import os
import UnityPy
from lib.paths import Paths, PATH
from lib.api import Enviroment
from lib import AssetBatchConverter
from multiprocessing import Pool, cpu_count

SRC = os.path.join(PATH, *["Data", "cache"])
DST = os.path.join(PATH, "extracted")


def update_assetbundle(args):
    env, fpath, item = args
    sfp = os.path.join(SRC, *fpath.split("/"))
    os.makedirs(os.path.dirname(sfp), exist_ok=True)

    data = env.download_asset("ab", item["FileName"])
    open(sfp, "wb").write(data)

    dfp = os.path.join(DST, *fpath.split("/"))
    uenv = UnityPy.load(data)
    objects = uenv.objects

    extracted = []
    for obj in objects:
        if obj.path_id not in extracted:
            extracted.extend(AssetBatchConverter.export_obj(obj, dfp, len(objects) > 2))
    return fpath, len(extracted)


def update_streamingasset(args):
    env, fpath, item = args
    dfp = os.path.join(DST, "StreamingAssets", *fpath.split("/")) + item["Extension"]
    os.makedirs(os.path.dirname(dfp), exist_ok=True)
    data = env.download_asset("sa", item["FileName"])
    with open(dfp, "wb") as f:
        f.write(data)
    return fpath


def main():
    AssetBatchConverter.DST = DST

    paths = Paths()
    env = Enviroment(True)

    os.makedirs(SRC, exist_ok=True)
    os.makedirs(DST, exist_ok=True)
    # AssetBundle
    TODO = []
    for fpath, item in paths["AssetBundle"].items():
        sfp = os.path.join(SRC, *fpath.split("/"))
        if not os.path.exists(sfp) or os.path.getsize(sfp) != item["FileSize"]:
            TODO.append((fpath, item))

    pool = Pool(processes=cpu_count())
    for i, (fpath, ext) in enumerate(
        pool.imap_unordered(
            update_assetbundle, ((env, fpath, item) for fpath, item in TODO)
        )
    ):
        print(f"{i}/{len(TODO)} - {fpath} - extracted {ext} assets")

    # StreamingAssets
    TODO = []
    for fpath, item in paths["StreamingAssets"].items():
        # cut the crap, no need to save as "raw" file, since no extraction is required
        dfp = (
            os.path.join(DST, "StreamingAssets", *fpath.split("/")) + item["Extension"]
        )
        if not os.path.exists(dfp) or os.path.getsize(dfp) != item["FileSize"]:
            TODO.append((dfp, item))

    for i, (fpath, ext) in enumerate(
        pool.imap_unordered(
            update_assetbundle, ((env, fpath, item) for fpath, item in TODO)
        )
    ):
        print(f"{i}/{len(TODO)} - {fpath}")


if __name__ == "__main__":
    main()
