import json
import os
import shutil
from lib.paths import Paths, PATH
from lib.api import Enviroment

SRC = os.path.join(PATH, *['Data', 'cache'])
DST = os.path.join(PATH, 'extracted')

paths = Paths()
env = Enviroment(True)


os.makedirs(SRC, exist_ok = True)
os.makedirs(DST, exist_ok = True)

# StreamingAssets
for fpath, item in paths['StreamingAssets'].items():
    # cut the crap, no need to save as "raw" file, since no extraction is required
    if item["Extension"] != ".png":
        continue
    dfp = os.path.join(DST, "StreamingAssets", *fpath.split('/'))+item['Extension']
    os.makedirs(os.path.dirname(dfp), exist_ok=True)
    if not os.path.exists(dfp) or os.path.getsize(dfp) != item['FileSize']:
        # download file
        print(dfp)
        open(dfp, 'wb').write(env.download_asset('sa', item['FileName']))
