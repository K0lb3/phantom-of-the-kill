﻿// Decompiled with JetBrains decompiler
// Type: QuestStageEntryInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class QuestStageEntryInfo : MonoBehaviour
{
  [SerializeField]
  private GameObject normal;
  [SerializeField]
  private GameObject hurd;
  [SerializeField]
  private UILabel textNormal;
  [SerializeField]
  private UILabel textHurd;

  public void Awake()
  {
    this.IsDisplay = false;
  }

  public string TextNormal
  {
    set
    {
      this.textNormal.SetTextLocalize(value);
    }
  }

  public string TextHurd
  {
    set
    {
      this.textHurd.SetTextLocalize(value);
    }
  }

  public bool Normal
  {
    get
    {
      return this.normal.activeSelf;
    }
    set
    {
      this.normal.SetActive(value);
      if (!value)
        return;
      this.IsDisplay = value;
    }
  }

  public bool Hurd
  {
    get
    {
      return this.hurd.activeSelf;
    }
    set
    {
      this.hurd.SetActive(value);
      if (!value)
        return;
      this.IsDisplay = value;
    }
  }

  public bool IsDisplay
  {
    set
    {
      this.gameObject.SetActive(value);
    }
  }
}
