﻿// Decompiled with JetBrains decompiler
// Type: Unit0044ReisouScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;

public class Unit0044ReisouScene : NGSceneBase
{
  public Unit0044ReisouMenu menu;

  public override IEnumerator onInitSceneAsync()
  {
    Unit0044ReisouScene unit0044ReisouScene = this;
    if (Singleton<CommonRoot>.GetInstance().headerType == CommonRoot.HeaderType.Tower)
    {
      unit0044ReisouScene.bgmFile = TowerUtil.BgmFile;
      unit0044ReisouScene.bgmName = TowerUtil.BgmName;
    }
    else if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      IEnumerator e = unit0044ReisouScene.SetSeaBgm();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public static void ChangeScene(bool stack, ItemInfo gearInfo, ItemInfo reisouInfo)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_4_reisou", (stack ? 1 : 0) != 0, (object) gearInfo, (object) reisouInfo);
  }

  public virtual IEnumerator onStartSceneAsync(ItemInfo gearInfo, ItemInfo reisouInfo)
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    this.menu.GearInfo = gearInfo;
    this.menu.ReisouInfo = reisouInfo;
    IEnumerator e = this.menu.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public virtual void onStartScene(ItemInfo gearInfo, ItemInfo reisouInfo)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().isActiveHeader = true;
    Singleton<CommonRoot>.GetInstance().isActiveFooter = true;
  }

  public override void onEndScene()
  {
    Persist.sortOrder.Flush();
    this.menu.onEndScene();
    ItemIcon.ClearCache();
  }

  private IEnumerator SetSeaBgm()
  {
    Unit0044ReisouScene unit0044ReisouScene = this;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeaHomeMap seaHomeMap = ((IEnumerable<SeaHomeMap>) MasterData.SeaHomeMapList).ActiveSeaHomeMap(ServerTime.NowAppTimeAddDelta());
    if (seaHomeMap != null && !string.IsNullOrEmpty(seaHomeMap.bgm_cuesheet_name) && !string.IsNullOrEmpty(seaHomeMap.bgm_cue_name))
    {
      unit0044ReisouScene.bgmFile = seaHomeMap.bgm_cuesheet_name;
      unit0044ReisouScene.bgmName = seaHomeMap.bgm_cue_name;
    }
  }
}
