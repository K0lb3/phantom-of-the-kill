﻿// Decompiled with JetBrains decompiler
// Type: GotoHelp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using UnityEngine;

public class GotoHelp : MonoBehaviour
{
  [SerializeField]
  [Tooltip("ヘルプ遷移時にポップアップを全て閉じる")]
  private bool closeAllPopup_ = true;
  [SerializeField]
  [Tooltip("親メニュー")]
  private NGMenuBase rootMenu_;
  [SerializeField]
  [Tooltip("ヘルプタイトル")]
  private string help_;

  public Func<bool> onClickedButton { get; set; }

  private NGMenuBase root
  {
    get
    {
      return !((UnityEngine.Object) this.rootMenu_ != (UnityEngine.Object) null) ? (this.rootMenu_ = NGUITools.FindInParents<NGMenuBase>(this.gameObject)) : this.rootMenu_;
    }
  }

  public void onClickedHelp()
  {
    NGMenuBase root = this.root;
    if ((UnityEngine.Object) root != (UnityEngine.Object) null && root.IsPushAndSet())
      return;
    if (this.onClickedButton == null)
      this.onClickedButton = new Func<bool>(this.defaultClickedButton);
    if (!this.onClickedButton())
    {
      if (!((UnityEngine.Object) root != (UnityEngine.Object) null))
        return;
      root.IsPush = false;
    }
    else
    {
      if (!this.closeAllPopup_ || !Singleton<PopupManager>.GetInstance().isOpen)
        return;
      Singleton<PopupManager>.GetInstance().closeAll(false);
    }
  }

  private bool defaultClickedButton()
  {
    if (string.IsNullOrEmpty(this.help_))
      return false;
    HelpCategory helpCategory = Array.Find<HelpCategory>(MasterData.HelpCategoryList, (Predicate<HelpCategory>) (x => x.name == this.help_));
    if (helpCategory == null)
      return false;
    Help0152Scene.ChangeScene(true, helpCategory);
    return true;
  }
}
