﻿// Decompiled with JetBrains decompiler
// Type: MypageMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class MypageMenu : MypageMenuBase
{
  private static readonly string _affiliation_icon_sprite_name_format = "{0}__GUI__unit_affiliation__unit_affiliation_prefab";
  [NonSerialized]
  public GameObject _full_screen_effect;
  [SerializeField]
  private UILabel _chara_name;
  [SerializeField]
  private UISprite _affiliation_icon;
  [SerializeField]
  private UILabel _affiliation_name;
  [SerializeField]
  private UILabel _lbl_total_power;

  protected override void JogDialSetting()
  {
  }

  public override void SetJogDialActive(bool active)
  {
  }

  protected override IEnumerator CreateCharcterAnimation()
  {
    MypageMenu mypageMenu = this;
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = mypageMenu.\u003C\u003En__0();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> prefabF = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/mypage/FullScreenEffect_anim", 1f);
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    mypageMenu._full_screen_effect = prefabF.Result.Clone(mypageMenu.gameObject.transform);
    // ISSUE: reference to a compiler-generated method
    mypageMenu.CharaAnimObj.GetComponent<CharaAnimationConst>().setFinishAction(new System.Action(mypageMenu.\u003CCreateCharcterAnimation\u003Eb__7_0));
  }

  public override void CharaAnimProc()
  {
    base.CharaAnimProc();
    if (!this.CharaAnimation)
      return;
    Animator component = this._full_screen_effect.GetComponent<Animator>();
    if (!(bool) (UnityEngine.Object) component)
      return;
    component.SetTrigger(this.tweenName);
  }

  protected override IEnumerator CharcterAnimationSetting(bool isCloudAnim, int tweenID)
  {
    MypageMenu mypageMenu = this;
    // ISSUE: reference to a compiler-generated method
    IEnumerator e = mypageMenu.\u003C\u003En__1(isCloudAnim, tweenID);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerUnit displayPlayerUnit = mypageMenu.getDisplayPlayerUnit();
    if ((UnityEngine.Object) mypageMenu._chara_name != (UnityEngine.Object) null)
      mypageMenu._chara_name.SetText(displayPlayerUnit.unit.english_name);
    if ((UnityEngine.Object) mypageMenu._chara_anim_english_name != (UnityEngine.Object) null)
      mypageMenu._chara_anim_english_name.SetText(displayPlayerUnit.unit.english_name);
    UnitAffiliationIcon affiliationIcon = displayPlayerUnit.unit.getAffiliationIcon();
    if ((bool) (UnityEngine.Object) mypageMenu._affiliation_name)
      mypageMenu._affiliation_name.SetText(affiliationIcon.english_name);
    if ((bool) (UnityEngine.Object) mypageMenu._affiliation_icon)
      mypageMenu._affiliation_icon.spriteName = string.Format(MypageMenu._affiliation_icon_sprite_name_format, (object) affiliationIcon.file_name);
    if ((bool) (UnityEngine.Object) mypageMenu._lbl_total_power)
    {
      Judgement.NonBattleParameter nonBattleParameter = Judgement.NonBattleParameter.FromPlayerUnit(displayPlayerUnit, false);
      mypageMenu._lbl_total_power.SetText(string.Format("{0}", (object) nonBattleParameter.Combat));
    }
  }
}
