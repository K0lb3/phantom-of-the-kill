﻿// Decompiled with JetBrains decompiler
// Type: Startup000121NewsScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Startup000121NewsScrollParts : Startup000121ScrollParts
{
  [SerializeField]
  protected GameObject newsSprite;
  [SerializeField]
  protected GameObject bugSprite;

  public override void SetData(OfficialInformationArticle info, NGMenuBase menu)
  {
    if (info.sub_category_id == 1)
    {
      this.bugSprite.SetActive(true);
      this.newsSprite.SetActive(false);
    }
    else
    {
      this.newsSprite.SetActive(true);
      this.bugSprite.SetActive(false);
    }
  }
}
