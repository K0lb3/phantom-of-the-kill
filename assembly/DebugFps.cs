﻿// Decompiled with JetBrains decompiler
// Type: DebugFps
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class DebugFps : Singleton<DebugFps>
{
  public float fpsCalcTime = 0.1f;
  public float fps;
  private float time;
  private int fpsCount;

  protected override void Initialize()
  {
  }

  private void Start()
  {
    this.time = 0.0f;
    this.fps = 0.0f;
    this.fpsCount = 0;
  }

  private void LateUpdate()
  {
    this.time += Time.deltaTime;
    ++this.fpsCount;
    if ((double) this.time <= (double) this.fpsCalcTime)
      return;
    this.fps = (float) this.fpsCount / this.time;
    this.time = 0.0f;
    this.fpsCount = 0;
  }
}
