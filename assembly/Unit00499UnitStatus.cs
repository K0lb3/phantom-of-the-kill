﻿// Decompiled with JetBrains decompiler
// Type: Unit00499UnitStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Unit00499UnitStatus : MonoBehaviour
{
  [SerializeField]
  private UISprite slcUnitbaseAfter;
  [SerializeField]
  private GameObject dirAwake;
  [SerializeField]
  private UILabel TxtHp;
  [SerializeField]
  private UILabel TxtJobname;
  [SerializeField]
  private UILabel TxtLucky;
  [SerializeField]
  private UILabel TxtLv;
  [SerializeField]
  private UILabel TxtLvmax;
  [SerializeField]
  private UILabel TxtMagic;
  [SerializeField]
  private UILabel TxtPower;
  [SerializeField]
  private UILabel TxtPrincesstype;
  [SerializeField]
  private UILabel TxtProtect;
  [SerializeField]
  private UILabel TxtSpeed;
  [SerializeField]
  private UILabel TxtSpirit;
  [SerializeField]
  private UILabel TxtTechnique;
  [SerializeField]
  private UILabel TxtCost;
  [SerializeField]
  private GameObject hpStatusMaxStar;
  [SerializeField]
  private GameObject powerStatusMaxStar;
  [SerializeField]
  private GameObject magicStatusMaxStar;
  [SerializeField]
  private GameObject protectStatusMaxStar;
  [SerializeField]
  private GameObject spiritStatusMaxStar;
  [SerializeField]
  private GameObject speedStatusMaxStar;
  [SerializeField]
  private GameObject techniqueStatusMaxStar;
  [SerializeField]
  private GameObject luckyStatusMaxStar;
  public Unit00499Scene.Mode mode;
  private const string SlcUnitBaseAfterSpriteName = "slc_awakening_Unitbase_After.png__GUI__004-9-9_sozai__004-9-9_sozai_prefab";
  public GameObject linkUnit;
  public GameObject[] TransUpParameter;

  protected void InitNumber(int v1, bool isNormalUnit1, UILabel source)
  {
    ((System.Action<string, UILabel>) ((v, label) =>
    {
      if ((UnityEngine.Object) label == (UnityEngine.Object) null)
        return;
      label.SetTextLocalize(v);
      label.gameObject.SetActive(true);
    }))(isNormalUnit1 ? v1.ToString() : "---", source);
    if (isNormalUnit1)
      return;
    source.color = Color.white;
  }

  protected void setStatusMaxStar(GameObject go, bool isDisp)
  {
    if (!((UnityEngine.Object) go != (UnityEngine.Object) null))
      return;
    go.SetActive(isDisp);
  }

  public void SetStatusTextEarth(PlayerUnit playerUnit)
  {
    this.InitNumber(playerUnit.self_total_hp, true, this.TxtHp);
    this.InitNumber(playerUnit.self_total_strength, true, this.TxtPower);
    this.InitNumber(playerUnit.self_total_intelligence, true, this.TxtMagic);
    this.InitNumber(playerUnit.self_total_vitality, true, this.TxtProtect);
    this.InitNumber(playerUnit.self_total_mind, true, this.TxtSpirit);
    this.InitNumber(playerUnit.self_total_agility, true, this.TxtSpeed);
    this.InitNumber(playerUnit.self_total_dexterity, true, this.TxtTechnique);
    this.InitNumber(playerUnit.self_total_lucky, true, this.TxtLucky);
    this.InitNumber(playerUnit.level, true, this.TxtLv);
    this.InitNumber(playerUnit.max_level, true, this.TxtLvmax);
    this.setStatusMaxStar(this.hpStatusMaxStar, playerUnit.hp.is_max);
    this.setStatusMaxStar(this.powerStatusMaxStar, playerUnit.strength.is_max);
    this.setStatusMaxStar(this.magicStatusMaxStar, playerUnit.intelligence.is_max);
    this.setStatusMaxStar(this.protectStatusMaxStar, playerUnit.vitality.is_max);
    this.setStatusMaxStar(this.spiritStatusMaxStar, playerUnit.mind.is_max);
    this.setStatusMaxStar(this.speedStatusMaxStar, playerUnit.agility.is_max);
    this.setStatusMaxStar(this.techniqueStatusMaxStar, playerUnit.dexterity.is_max);
    this.setStatusMaxStar(this.luckyStatusMaxStar, playerUnit.lucky.is_max);
    this.TxtJobname.SetTextLocalize(playerUnit.getJobData().name);
  }

  public void SetStatusText(PlayerUnit playerUnit, bool enableAwake = false)
  {
    this.InitNumber(playerUnit.self_total_hp, playerUnit.unit.IsNormalUnit, this.TxtHp);
    this.InitNumber(playerUnit.self_total_strength, playerUnit.unit.IsNormalUnit, this.TxtPower);
    this.InitNumber(playerUnit.self_total_intelligence, playerUnit.unit.IsNormalUnit, this.TxtMagic);
    this.InitNumber(playerUnit.self_total_vitality, playerUnit.unit.IsNormalUnit, this.TxtProtect);
    this.InitNumber(playerUnit.self_total_mind, playerUnit.unit.IsNormalUnit, this.TxtSpirit);
    this.InitNumber(playerUnit.self_total_agility, playerUnit.unit.IsNormalUnit, this.TxtSpeed);
    this.InitNumber(playerUnit.self_total_dexterity, playerUnit.unit.IsNormalUnit, this.TxtTechnique);
    this.InitNumber(playerUnit.self_total_lucky, playerUnit.unit.IsNormalUnit, this.TxtLucky);
    this.InitNumber(playerUnit.level, playerUnit.unit.IsNormalUnit, this.TxtLv);
    this.InitNumber(playerUnit.max_level, playerUnit.unit.IsNormalUnit, this.TxtLvmax);
    if (playerUnit.unit.IsNormalUnit)
    {
      this.setStatusMaxStar(this.hpStatusMaxStar, playerUnit.hp.is_max);
      this.setStatusMaxStar(this.powerStatusMaxStar, playerUnit.strength.is_max);
      this.setStatusMaxStar(this.magicStatusMaxStar, playerUnit.intelligence.is_max);
      this.setStatusMaxStar(this.protectStatusMaxStar, playerUnit.vitality.is_max);
      this.setStatusMaxStar(this.spiritStatusMaxStar, playerUnit.mind.is_max);
      this.setStatusMaxStar(this.speedStatusMaxStar, playerUnit.agility.is_max);
      this.setStatusMaxStar(this.techniqueStatusMaxStar, playerUnit.dexterity.is_max);
      this.setStatusMaxStar(this.luckyStatusMaxStar, playerUnit.lucky.is_max);
    }
    else
    {
      this.setStatusMaxStar(this.hpStatusMaxStar, false);
      this.setStatusMaxStar(this.powerStatusMaxStar, false);
      this.setStatusMaxStar(this.magicStatusMaxStar, false);
      this.setStatusMaxStar(this.protectStatusMaxStar, false);
      this.setStatusMaxStar(this.spiritStatusMaxStar, false);
      this.setStatusMaxStar(this.speedStatusMaxStar, false);
      this.setStatusMaxStar(this.techniqueStatusMaxStar, false);
      this.setStatusMaxStar(this.luckyStatusMaxStar, false);
    }
    this.TxtPrincesstype.SetTextLocalize(playerUnit.unit_type.name);
    this.TxtCost.SetTextLocalize(playerUnit.cost);
    this.TxtJobname.SetTextLocalize(playerUnit.getJobData().name);
    if (!(playerUnit.unit.awake_unit_flag & enableAwake) || !((UnityEngine.Object) this.dirAwake != (UnityEngine.Object) null) || !((UnityEngine.Object) this.slcUnitbaseAfter != (UnityEngine.Object) null))
      return;
    this.dirAwake.SetActive(true);
    this.slcUnitbaseAfter.spriteName = "slc_awakening_Unitbase_After.png__GUI__004-9-9_sozai__004-9-9_sozai_prefab";
    this.TxtLv.color = Color.white;
  }

  public void SetStatusTextMemory(PlayerUnit playerUnit)
  {
    this.InitNumber(playerUnit.memory_hp, playerUnit.unit.IsNormalUnit, this.TxtHp);
    this.InitNumber(playerUnit.memory_strength, playerUnit.unit.IsNormalUnit, this.TxtPower);
    this.InitNumber(playerUnit.memory_intelligence, playerUnit.unit.IsNormalUnit, this.TxtMagic);
    this.InitNumber(playerUnit.memory_vitality, playerUnit.unit.IsNormalUnit, this.TxtProtect);
    this.InitNumber(playerUnit.memory_mind, playerUnit.unit.IsNormalUnit, this.TxtSpirit);
    this.InitNumber(playerUnit.memory_agility, playerUnit.unit.IsNormalUnit, this.TxtSpeed);
    this.InitNumber(playerUnit.memory_dexterity, playerUnit.unit.IsNormalUnit, this.TxtTechnique);
    this.InitNumber(playerUnit.memory_lucky, playerUnit.unit.IsNormalUnit, this.TxtLucky);
    this.InitNumber(playerUnit.memory_level, playerUnit.unit.IsNormalUnit, this.TxtLv);
    this.InitNumber(playerUnit.max_level, playerUnit.unit.IsNormalUnit, this.TxtLvmax);
    if (playerUnit.unit.IsNormalUnit)
    {
      this.setStatusMaxStar(this.hpStatusMaxStar, playerUnit.is_memory_hp_max);
      this.setStatusMaxStar(this.powerStatusMaxStar, playerUnit.is_memory_strength_max);
      this.setStatusMaxStar(this.magicStatusMaxStar, playerUnit.is_memory_intelligence_max);
      this.setStatusMaxStar(this.protectStatusMaxStar, playerUnit.is_memory_vitality_max);
      this.setStatusMaxStar(this.spiritStatusMaxStar, playerUnit.is_memory_mind_max);
      this.setStatusMaxStar(this.speedStatusMaxStar, playerUnit.is_memory_agility_max);
      this.setStatusMaxStar(this.techniqueStatusMaxStar, playerUnit.is_memory_dexterity_max);
      this.setStatusMaxStar(this.luckyStatusMaxStar, playerUnit.is_memory_lucky_max);
    }
    else
    {
      this.setStatusMaxStar(this.hpStatusMaxStar, false);
      this.setStatusMaxStar(this.powerStatusMaxStar, false);
      this.setStatusMaxStar(this.magicStatusMaxStar, false);
      this.setStatusMaxStar(this.protectStatusMaxStar, false);
      this.setStatusMaxStar(this.spiritStatusMaxStar, false);
      this.setStatusMaxStar(this.speedStatusMaxStar, false);
      this.setStatusMaxStar(this.techniqueStatusMaxStar, false);
      this.setStatusMaxStar(this.luckyStatusMaxStar, false);
    }
    this.TxtPrincesstype.SetTextLocalize(playerUnit.unit_type.name);
    this.TxtCost.SetTextLocalize(playerUnit.cost);
    this.TxtJobname.SetTextLocalize(playerUnit.getJobData().name);
  }
}
