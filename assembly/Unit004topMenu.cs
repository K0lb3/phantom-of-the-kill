﻿// Decompiled with JetBrains decompiler
// Type: Unit004topMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004topMenu : BackButtonMenuBase
{
  [SerializeField]
  private NGxScroll ScrollContainer;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UIButton Ibtn_PakuPaku;
  [SerializeField]
  private List<GameObject> campaignIcons;

  public IEnumerator Init()
  {
    Unit004topMenu unit004topMenu = this;
    unit004topMenu.gameObject.SetActive(false);
    unit004topMenu.TxtTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().TitleUnitBuguEdit, (IDictionary) null));
    unit004topMenu.Ibtn_PakuPaku.transform.parent.gameObject.SetActive(Player.Current.IsUniteReinfoce());
    unit004topMenu.InitBoost(unit004topMenu.campaignIcons);
    unit004topMenu.ScrollContainer.ResolvePosition();
    yield return (object) new WaitForSeconds(0.1f);
    unit004topMenu.gameObject.SetActive(true);
    if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
      Singleton<TutorialRoot>.GetInstance().CurrentAdvise();
  }

  private void InitBoost(List<GameObject> campaignIcons)
  {
    for (int index = 0; index < campaignIcons.Count; ++index)
      campaignIcons[index].SetActive(false);
    NGGameDataManager.Boost boostInfo = Singleton<NGGameDataManager>.GetInstance().BoostInfo;
    if (boostInfo == null)
      return;
    int[] boostListNow = boostInfo.TypeIdList;
    if (boostListNow.Length == 0)
      return;
    BoostCampaignTypeName[] campaignTypeNameList = MasterData.BoostCampaignTypeNameList;
    Array values = Enum.GetValues(typeof (BoostIconPosition));
    Func<BoostCampaignTypeName, bool> predicate = (Func<BoostCampaignTypeName, bool>) (w => ((IEnumerable<int>) boostListNow).Contains<int>(w.ID));
    IEnumerable<BoostCampaignTypeName> source = ((IEnumerable<BoostCampaignTypeName>) campaignTypeNameList).Where<BoostCampaignTypeName>(predicate);
    foreach (object obj in values)
    {
      object e = obj;
      BoostCampaignTypeName campaignTypeName = source.FirstOrDefault<BoostCampaignTypeName>((Func<BoostCampaignTypeName, bool>) (fd => fd.position_BoostIconPosition == (int) e));
      if (campaignTypeName != null)
      {
        string imgName = campaignTypeName.img_name;
        int boostIconPosition = campaignTypeName.position_BoostIconPosition;
        GameObject campaignIcon = campaignIcons[boostIconPosition];
        campaignIcon.SetActive(true);
        campaignIcon.GetComponent<SpriteSelectDirect>().SetSpriteName<string>(imgName, true);
      }
    }
  }

  public void onStartScene()
  {
    this.gameObject.SetActive(true);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public virtual void IbtnFormation_AnimList1()
  {
    Unit0046Scene.changeScene(true, (QuestLimitationBase[]) null, (string) null, false);
  }

  public virtual void IbtnCpmposite_AnimList2()
  {
    Unit00468Scene.changeScene0048(true);
  }

  public virtual void IbtnReinforce()
  {
    Unit00468Scene.changeScene00481Reinforce(true);
  }

  public virtual void IbtnEvolution_AnimList3()
  {
    Unit00468Scene.changeScene00491Evolution(true);
  }

  public virtual void IbtnTransmigration_AnimLIst4()
  {
    Unit00468Scene.changeScene00491Trans(true);
  }

  public virtual void IbtnOverview_AnimList5()
  {
    Unit00468Scene.changeScene00411(true);
  }

  public virtual void IbtnEquip_AnimList6()
  {
    Unit00468Scene.changeScene00412(true);
  }

  public virtual void IbtnMaterailOverview_AnimList7()
  {
    Unit00468Scene.changeScene00414(true);
  }

  public virtual void IbtnExtraSkill_AnimList8()
  {
    Unit004ExtraskillListScene.changeScene(true);
  }

  public virtual void IbtnPrincessType()
  {
    Unit004ReincarnationTypeTicketSelectionScene.changeScene(true);
  }

  public virtual void IbtnJobChange()
  {
    Unit004JobChangeUnitSelectScene.changeScene(true);
  }

  public virtual void IbtnOverview()
  {
    Bugu0052Scene.ChangeScene(true);
  }

  public virtual void IbtnComposite()
  {
    Bugu0053Scene.changeScene(true);
  }

  public virtual void IbtnPakuPaku()
  {
    Bugu00522Scene.ChangeScene(true);
  }

  public virtual void IbtnDrilling()
  {
    Bugu00526Scene.ChangeScene(true);
  }

  public virtual void IbtnRepair()
  {
    Bugu00524Scene.ChangeScene(true);
  }

  public virtual void IbtnSell()
  {
    Bugu00525Scene.ChangeScene(true, Bugu00525Scene.Mode.Weapon);
  }

  public virtual void IbtnRecipe()
  {
    Bugu00510Scene.changeSceneRecipe(true);
  }

  public virtual void IbtnMaterialList()
  {
    Bugu005MaterialListScene.ChangeScene(true);
  }

  public virtual void IbtnSupplyList()
  {
    Bugu005SupplyListScene.ChangeScene(true);
  }

  public virtual void IbtnReisouList()
  {
    Bugu005ReisouListScene.ChangeScene(true);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  private enum IconPosition
  {
    UnitComposite,
    UnitEvolution,
    UnitStrengthen,
    UnitTransmigration,
    WeaponCompsite,
    WeaponDrilling,
    WeaponRepair,
  }
}
