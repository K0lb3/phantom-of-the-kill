﻿// Decompiled with JetBrains decompiler
// Type: Revision
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public static class Revision
{
  public static string ApplicationVersion = "2020.01.24-10.26.15";

  public static string DLCVersion
  {
    get
    {
      return ResourceManager.DLCVersion;
    }
  }
}
