﻿// Decompiled with JetBrains decompiler
// Type: Raid032BattleMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeckOrganization;
using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Raid032BattleMenu : BackButtonMenuBase, IBattlePreparationPopup
{
  [SerializeField]
  private RaidBattleStatus mBattleStatus;
  [SerializeField]
  private Raid032BattleBossInfo mBossInfo;
  [SerializeField]
  private UIButton mMainBattleSelectBtn;
  [SerializeField]
  private UIButton mSimulatedBattleSelectBtn;
  [SerializeField]
  private UIButton mMainBattleChallengeBtn;
  [SerializeField]
  private UIButton mSimulatedBattleChallengeBtn;
  [SerializeField]
  private GameObject mSimulatedBattleAttentions;
  [SerializeField]
  private UILabel mRankingAllLbl;
  [SerializeField]
  private UILabel mRankingGuildLbl;
  [SerializeField]
  private Transform mPopupAnchor;
  [SerializeField]
  private SpriteRenderer mBackground;
  private WebAPI.Response.GuildraidBattleDetail mResponse;
  private GuildRaid mMasterData;
  private GameObject mMapInfoPopupPrefab;
  private GameObject mEnemyInfoPopupPrefab;
  private GameObject mRaidPreparationPopupPrefab;
  private RaidBattlePreparationPopup mPreparationPopup;
  private int loop_count;
  private Raid032BattleMenu.BattleMode mMode;
  private string[] mUsedHelpers;
  private bool fromBattle;

  public bool isInitializeSucceeded { get; private set; }

  public IEnumerator initAsync()
  {
    if ((UnityEngine.Object) this.mRaidPreparationPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> loader = new ResourceObject("Prefabs/raid032_battle/dir_raid_battle_attack_target").Load<GameObject>();
      IEnumerator e = loader.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.mRaidPreparationPopupPrefab = loader.Result;
      loader = (Future<GameObject>) null;
    }
  }

  public IEnumerator onStartSceneAsync(
    int loopCount,
    int raid_id,
    bool isSimulation = false,
    bool fromBattle = false)
  {
    Raid032BattleMenu raid032BattleMenu = this;
    raid032BattleMenu.fromBattle = fromBattle;
    raid032BattleMenu.loop_count = loopCount;
    Future<WebAPI.Response.GuildraidBattleDetail> ft = WebAPI.GuildraidBattleDetail(raid032BattleMenu.loop_count, raid_id, new System.Action<WebAPI.Response.UserError>(raid032BattleMenu.webErrorCallback));
    IEnumerator e = ft.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (ft.Result != null)
    {
      raid032BattleMenu.mResponse = ft.Result;
      GuildUtil.rp = raid032BattleMenu.mResponse.rp;
      GuildUtil.rp_max = ((IEnumerable<GuildRaidSettings>) MasterData.GuildRaidSettingsList).FirstOrDefault<GuildRaidSettings>((Func<GuildRaidSettings, bool>) (x => x.key == "RP_BASE_MAX")).value;
      if (!MasterData.GuildRaid.TryGetValue(raid_id, out raid032BattleMenu.mMasterData))
      {
        Debug.LogError((object) ("There is no MasterData in local [ID:" + (object) raid_id + "]"));
      }
      else
      {
        GuildUtil.RaidUsedUnitIds = raid032BattleMenu.mResponse.used_player_unit_ids;
        GuildUtil.UpdateRaidDeckInfo();
        raid032BattleMenu.updateRankingInfo();
        string path = "Prefabs/BackGround/101_plain_daytime";
        GuildRaidPeriod guildRaidPeriod;
        if (MasterData.GuildRaidPeriod.TryGetValue(raid032BattleMenu.mMasterData.period_id, out guildRaidPeriod) && !string.IsNullOrEmpty(guildRaidPeriod.bg_path))
          path = guildRaidPeriod.bg_path;
        Future<UnityEngine.Sprite> prefabBgF = new ResourceObject(path).Load<UnityEngine.Sprite>();
        e = prefabBgF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        UnityEngine.Sprite result = prefabBgF.Result;
        raid032BattleMenu.mBackground.sprite = result;
        yield return (object) raid032BattleMenu.mBossInfo.InitAsync(raid032BattleMenu.mMasterData, raid032BattleMenu.mResponse);
        yield return (object) raid032BattleMenu.mBattleStatus.InitAsync();
        if (isSimulation)
          raid032BattleMenu.changeMode(Raid032BattleMenu.BattleMode.Simulated);
        else
          raid032BattleMenu.changeMode(Raid032BattleMenu.BattleMode.Main);
        raid032BattleMenu.isInitializeSucceeded = true;
      }
    }
  }

  private void updateRankingInfo()
  {
    if (this.mResponse.damage_rank_in_all.HasValue)
      this.mRankingAllLbl.SetTextLocalize(this.mResponse.damage_rank_in_all.Value);
    else
      this.mRankingAllLbl.SetTextLocalize("---");
    if (this.mResponse.damage_rank_in_guild.HasValue)
      this.mRankingGuildLbl.SetTextLocalize(this.mResponse.damage_rank_in_guild.Value);
    else
      this.mRankingGuildLbl.SetTextLocalize("---");
  }

  public IEnumerator onBackSceneAsync()
  {
    this.mBossInfo.Reload(this.mMode == Raid032BattleMenu.BattleMode.Main);
    GuildUtil.UpdateRaidDeckInfo();
    if ((UnityEngine.Object) this.mPreparationPopup != (UnityEngine.Object) null)
      yield return (object) this.mPreparationPopup.ReloadAsync();
  }

  public void onCharangeButton()
  {
    PlayerUnit[] canRaidUnits = this.GetCanRaidUnits();
    if (this.mMode == Raid032BattleMenu.BattleMode.Simulated || GuildUtil.rp > 0)
    {
      if (canRaidUnits == null || ((IEnumerable<PlayerUnit>) canRaidUnits).Count<PlayerUnit>() <= 0)
        ModalWindow.Show(Consts.GetInstance().GUILD_RAID_CAN_NOT_CHALLENGE, Consts.GetInstance().GUILD_RAID_NO_USED_UNIT, (System.Action) (() => {}));
      else
        this.StartCoroutine(this.openBattlePreparationPopup());
    }
    else
    {
      Consts instance = Consts.GetInstance();
      this.StartCoroutine(PopupCommon.Show(instance.GUILD_RAID_RP_LACK_TITLE, instance.GUILD_RAID_RP_LACK_MESSAGE, (System.Action) null));
    }
  }

  public void onCancelButton()
  {
    this.onBackButton();
  }

  public void onMainBattleButton()
  {
    this.changeMode(Raid032BattleMenu.BattleMode.Main);
  }

  public void onSimulatedBattleButton()
  {
    this.changeMode(Raid032BattleMenu.BattleMode.Simulated);
  }

  public void onDamageRankingButton()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Raid032MyRankingScene.changeScene(this.mMasterData, true);
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (this.fromBattle)
    {
      Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
      RaidTopScene.ChangeSceneBattleFinish(true);
    }
    else
      this.backScene();
  }

  private void changeMode(Raid032BattleMenu.BattleMode mode)
  {
    if (mode != Raid032BattleMenu.BattleMode.Main)
    {
      if (mode != Raid032BattleMenu.BattleMode.Simulated)
        return;
      this.mMainBattleSelectBtn.isEnabled = true;
      this.mMainBattleChallengeBtn.gameObject.SetActive(false);
      this.mSimulatedBattleSelectBtn.isEnabled = false;
      this.mSimulatedBattleChallengeBtn.gameObject.SetActive(true);
      this.mSimulatedBattleAttentions.SetActive(true);
      this.mBossInfo.SetRewardsEnable(false);
      this.mBattleStatus.DisableAllLamp();
      this.mMode = Raid032BattleMenu.BattleMode.Simulated;
    }
    else
    {
      this.mMainBattleSelectBtn.isEnabled = false;
      this.mMainBattleChallengeBtn.gameObject.SetActive(true);
      this.mSimulatedBattleSelectBtn.isEnabled = true;
      this.mSimulatedBattleChallengeBtn.gameObject.SetActive(false);
      this.mSimulatedBattleAttentions.SetActive(false);
      this.mBossInfo.SetRewardsEnable(true);
      this.mBattleStatus.EnableAllLamp();
      this.mMode = Raid032BattleMenu.BattleMode.Main;
    }
  }

  private IEnumerator openBattlePreparationPopup()
  {
    Raid032BattleMenu raid032BattleMenu = this;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, false);
    Future<WebAPI.Response.GuildraidBattleHelper> ft = WebAPI.GuildraidBattleHelper(RaidBattleGuestSelectPopup.GetSelectedCategoryId(), new System.Action<WebAPI.Response.UserError>(raid032BattleMenu.webErrorCallback));
    IEnumerator e = ft.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (ft.Result != null)
    {
      WebAPI.Response.GuildraidBattleHelper result = ft.Result;
      raid032BattleMenu.mUsedHelpers = result.used_helpers;
      raid032BattleMenu.mPreparationPopup = raid032BattleMenu.mRaidPreparationPopupPrefab.CloneAndGetComponent<RaidBattlePreparationPopup>(raid032BattleMenu.mPopupAnchor);
      raid032BattleMenu.mPreparationPopup.gameObject.SetActive(false);
      yield return (object) null;
      yield return (object) raid032BattleMenu.mPreparationPopup.InitializeAsync((IBattlePreparationPopup) raid032BattleMenu, RaidBattlePreparationPopup.MODE.Sortie, result.helpers, raid032BattleMenu.mUsedHelpers, raid032BattleMenu.mResponse.recommend_strength);
      raid032BattleMenu.mPreparationPopup.gameObject.SetActive(true);
      Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
    }
  }

  public PlayerUnit[] GetPopupDeck()
  {
    return GuildUtil.RaidDeck;
  }

  public PlayerUnit GetPopupFriend()
  {
    if (GuildUtil.RaidFriend == null)
      return (PlayerUnit) null;
    if (!((IEnumerable<string>) this.mUsedHelpers).Contains<string>(GuildUtil.RaidFriend.player_id))
      return GuildUtil.RaidFriend.player_unit;
    GuildUtil.RaidFriend = (GvgCandidate) null;
    return (PlayerUnit) null;
  }

  public PlayerItem[] GetPopupSupply()
  {
    return SMManager.Get<PlayerItem[]>().AllRaidSupplies();
  }

  public int[] GetPopupGrayUnitIds()
  {
    return GuildUtil.RaidUsedUnitIds;
  }

  public void OnPopupSortie()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.sortieBattle());
  }

  public void OnPopupClose()
  {
    if (!((UnityEngine.Object) this.mPreparationPopup != (UnityEngine.Object) null))
      return;
    UnityEngine.Object.Destroy((UnityEngine.Object) this.mPreparationPopup.gameObject);
    this.mPreparationPopup = (RaidBattlePreparationPopup) null;
  }

  public void OnPopupUnitDetailOpen(PlayerUnit unit, PlayerUnit[] units, bool isFriend)
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (isFriend)
      Unit0042Scene.changeSceneFriendUnit(true, unit.player_id, unit.id);
    else
      Unit0042Scene.changeScene(true, unit, units, false, false);
  }

  public void OnPopupAutoDeckEdit()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    this.StartCoroutine(this.AutoDeckEditAsync());
  }

  private PlayerUnit[] GetCanRaidUnits()
  {
    PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
    int[] usedUnitIds = this.GetPopupGrayUnitIds();
    Func<PlayerUnit, bool> predicate = (Func<PlayerUnit, bool>) (x => !((IEnumerable<int>) usedUnitIds).Contains<int>(x.id) && !x.IsBrokenEquippedGear);
    return ((IEnumerable<PlayerUnit>) playerUnitArray).Where<PlayerUnit>(predicate).ToArray<PlayerUnit>();
  }

  private IEnumerator AutoDeckEditAsync()
  {
    Raid032BattleMenu raid032BattleMenu = this;
    Creator deckCreator_ = new Creator((PlayerUnit[]) null, raid032BattleMenu.GetCanRaidUnits(), (List<Filter>) null, 1, 5, SMManager.Get<Player>().max_cost);
    IEnumerator e = deckCreator_.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GuildUtil.RaidDeck = deckCreator_.result_ == null ? new PlayerUnit[0] : deckCreator_.result_.ToArray();
    yield return (object) raid032BattleMenu.mPreparationPopup.ReloadAsync();
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    raid032BattleMenu.IsPush = false;
  }

  public void OnPopupDeckEditOpen(PlayerUnit[] deck)
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Unit00468Scene.changeScene00468Raid(true, this.mMode == Raid032BattleMenu.BattleMode.Simulated);
  }

  public void OnPopupGearEquipOpen()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Unit00468Scene.changeScene00412Raid(true);
  }

  public void OnPopupGearRepairOpen()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Bugu00524Scene.ChangeScene(true);
  }

  public void OnPopupSupplyEquipOpen()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Quest00210Scene.changeScene(true, Quest00210Scene.Mode.Raid);
  }

  public void OnPopupBattleConfigOpen()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().monitorCoroutine(this.openPopupBattleSetting());
  }

  private IEnumerator openPopupBattleSetting()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Raid032BattleMenu raid032BattleMenu = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      raid032BattleMenu.IsPush = false;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) Quest0028PopupBattleSetting.show();
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  public void onMapInfoButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openMapInfoPopup());
  }

  private IEnumerator openMapInfoPopup()
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    if ((UnityEngine.Object) this.mMapInfoPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> f = new ResourceObject("Prefabs/popup/popup_029_tower_stage_status__anim_popup01").Load<GameObject>();
      IEnumerator e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.mMapInfoPopupPrefab = f.Result;
      f = (Future<GameObject>) null;
    }
    Tower029MapcheckPopup popup = this.mMapInfoPopupPrefab.CloneAndGetComponent<Tower029MapcheckPopup>((GameObject) null);
    popup.gameObject.SetActive(false);
    yield return (object) popup.InitializeAsync(this.mMasterData.stage_id);
    popup.gameObject.SetActive(true);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<PopupManager>.GetInstance().open(popup.gameObject, false, false, true, true, false, false, "SE_1006");
  }

  public void onEnemyInfoButton()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.openEnemyInfoPopup());
  }

  private IEnumerator openEnemyInfoPopup()
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    if ((UnityEngine.Object) this.mEnemyInfoPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> f = new ResourceObject("Prefabs/popup/popup_032_raid_enemy_status__anim_popup01").Load<GameObject>();
      IEnumerator e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.mEnemyInfoPopupPrefab = f.Result;
      f = (Future<GameObject>) null;
    }
    RaidBattleEnemyInfoPopup popup = this.mEnemyInfoPopupPrefab.CloneAndGetComponent<RaidBattleEnemyInfoPopup>((GameObject) null);
    popup.gameObject.SetActive(false);
    yield return (object) popup.InitializeAsync(this.mMasterData, this.mResponse.boss_total_damage, this.loop_count);
    popup.gameObject.SetActive(true);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<PopupManager>.GetInstance().open(popup.gameObject, false, false, true, true, false, false, "SE_1006");
  }

  private IEnumerator sortieBattle()
  {
    Raid032BattleMenu raid032BattleMenu = this;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    bool is_simulation = raid032BattleMenu.mMode == Raid032BattleMenu.BattleMode.Simulated;
    // ISSUE: explicit non-virtual call
    int[] array = ((IEnumerable<PlayerUnit>) __nonvirtual (raid032BattleMenu.GetPopupDeck())).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
    // ISSUE: explicit non-virtual call
    // ISSUE: explicit non-virtual call
    string support_player_id = __nonvirtual (raid032BattleMenu.GetPopupFriend()) != (PlayerUnit) null ? __nonvirtual (raid032BattleMenu.GetPopupFriend()).player_id : string.Empty;
    // ISSUE: explicit non-virtual call
    // ISSUE: explicit non-virtual call
    Future<WebAPI.Response.GuildraidBattleStart> ft = WebAPI.GuildraidBattleStart(is_simulation, raid032BattleMenu.loop_count, array, raid032BattleMenu.mMasterData.ID, support_player_id, __nonvirtual (raid032BattleMenu.GetPopupFriend()) != (PlayerUnit) null ? __nonvirtual (raid032BattleMenu.GetPopupFriend()).id : 0, new System.Action<WebAPI.Response.UserError>(raid032BattleMenu.webErrorCallback));
    IEnumerator e = ft.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (ft.Result != null)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      Singleton<CommonRoot>.GetInstance().loadingMode = 4;
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      yield return (object) null;
      yield return (object) null;
      Persist.battleEnvironment.Delete();
      Persist.pvpSuspend.Delete();
      Singleton<NGBattleManager>.GetInstance().startBattle(BattleInfo.MakeRaidBattleInfo(ft.Result), 0);
    }
  }

  private void webErrorCallback(WebAPI.Response.UserError error)
  {
    string empty1 = string.Empty;
    string empty2 = string.Empty;
    string title;
    string message;
    if (error.Code == "GRB016")
    {
      title = Consts.GetInstance().GUILD_RAID_BOSS_WAS_ALREADY_DIED_TITLE;
      message = Consts.GetInstance().GUILD_RAID_BOSS_WAS_ALREADY_DIED_MESSAGE;
    }
    else
    {
      title = error.Code;
      message = error.Reason;
    }
    ModalWindow.Show(title, message, (System.Action) (() =>
    {
      this.IsPush = false;
      NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
      if (error.Code == "GRB016")
      {
        this.onBackButton();
      }
      else
      {
        instance.clearStack();
        instance.destroyCurrentScene();
        Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, false);
      }
    }));
  }

  private enum BattleMode
  {
    Main,
    Simulated,
  }
}
