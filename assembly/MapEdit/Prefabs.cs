﻿// Decompiled with JetBrains decompiler
// Type: MapEdit.Prefabs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MapEdit
{
  public static class Prefabs
  {
    public static ResourceObject movement_mode
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_movement_mode");
      }
    }

    public static ResourceObject facility_cost
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_facility_cost");
      }
    }

    public static ResourceObject facility_info
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_facility_info");
      }
    }

    public static ResourceObject facility_storage
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_facility_storage");
      }
    }

    public static ResourceObject facility_list
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_facility_list");
      }
    }

    public static ResourceObject base_map_select
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_base_map_select");
      }
    }

    public static ResourceObject base_map_select_list
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_base_map_select_list");
      }
    }

    public static ResourceObject popup_base_map_detail
    {
      get
      {
        return new ResourceObject("Prefabs/popup/popup_031_map_detail__anim_popup01");
      }
    }

    public static ResourceObject popup_confirm_exchange_map
    {
      get
      {
        return new ResourceObject("Prefabs/popup/popup_base_map_select__anim_popup01");
      }
    }

    public static ResourceObject popup_confirm_save_slot
    {
      get
      {
        return new ResourceObject("Prefabs/popup/popup_map_edit_save_confirm__anim_popup01");
      }
    }

    public static ResourceObject map_save_slot_select
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_map_save_slot_select");
      }
    }

    public static ResourceObject map_slot_list
    {
      get
      {
        return new ResourceObject("Prefabs/map_edit031/dir_map_slot_list");
      }
    }

    public static ResourceObject facility_thumb
    {
      get
      {
        return new ResourceObject("Prefabs/FacilityIcon/dir_facility_thumb");
      }
    }
  }
}
