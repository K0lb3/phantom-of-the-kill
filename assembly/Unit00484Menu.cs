﻿// Decompiled with JetBrains decompiler
// Type: Unit00484Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitStatusInformation;
using UnityEngine;

public class Unit00484Menu : BackButtonMenuBase
{
  private const int UNSET_SKILL = 0;
  public const int MAX_CELL_COUNT = 30;
  [Header("ベース姫情報(dir_Status01)")]
  public UISprite[] princessType;
  public GameObject dyn_thum;
  public GameObject ibtn_Change;
  public GameObject limitBreak;
  public GameObject[] limitBreakIcon;
  public GameObject slc_Limitbreak;
  public UISprite[] limitBreakBlue;
  public UISprite[] limitBreakGreen;
  [SerializeField]
  protected UILabel TxtLv;
  [SerializeField]
  protected UILabel TxtUnity;
  [SerializeField]
  protected GameObject dirDearDegree;
  [SerializeField]
  protected UILabel txtDear;
  [SerializeField]
  protected UILabel txtDearDegreeAmountNumer;
  [SerializeField]
  protected UILabel txtDearDegreeAmountDenor;
  [SerializeField]
  public GameObject[] dir_skill;
  private GameObject[] skillAttention;
  public GameObject[] skillObject;
  public GameObject[] skillUpObject;
  [SerializeField]
  private GameObject[] StatusGaugeBase;
  [Header("素材ユニット情報(dir_Status02)")]
  [SerializeField]
  private GameObject ibtn_change_UpperParameter;
  [SerializeField]
  private UIDragScrollView uiDragScrollView;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private GameObject scrollBar;
  [SerializeField]
  private UIGrid grid;
  [SerializeField]
  private GameObject dir_attention;
  [Header("画面下部(Bottom)")]
  [SerializeField]
  private UILabel TxtZeny;
  [SerializeField]
  private UIButton ibtnCombine;
  [SerializeField]
  private GameObject floatingSkillDialog;
  private bool isSetfirstGridPosition;
  private Vector3 firstGridPosition;
  private GameObject unitIconPrefab;
  private GameObject upperParameterPrefab;
  private GameObject upperParameterLabelPrefab;
  private GameObject skillTypeIconPrefab;
  private GameObject statusGaugePrefab;
  private GameObject DirSozaiItemPrefab;
  private GameObject prefabUnityDetail;
  private GameObject prefabStageItem;
  private PlayerUnit baseUnit;
  private PlayerUnit[] selectUnits;
  private PlayerUnit[] duplicationSelectUnits;
  private GameObject skillDetailDialogPrefab;
  private PopupSkillDetails.Param[] skillParams;
  private Battle0171111Event floatingSkillDialogObject;
  private System.Action<BattleskillSkill> showSkillDialog;
  private System.Action<BattleskillSkill> showUnitSkillDialog;
  private System.Action<int, int> showSkillLevel;
  private System.Action popupUnityDetail;
  public float trust;
  public float maxTrust;
  private bool isMaterialOverUsed;
  [SerializeField]
  public Unit004813Menu combineResultMenu;
  [SerializeField]
  public GameObject mainPanel00484;
  private GameObject bgPrefabForCombineResult;
  public System.Action exceptionBackScene;

  public IEnumerator Init(PlayerUnit basePlayerUnit, PlayerUnit[] materialPlayerUnits)
  {
    Unit00484Menu unit00484Menu1 = this;
    yield return (object) unit00484Menu1.StartCoroutine(unit00484Menu1.LoadBgForCombineResult());
    unit00484Menu1.baseUnit = basePlayerUnit;
    UnitUnit base_unit = basePlayerUnit.unit;
    unit00484Menu1.selectUnits = ((IEnumerable<PlayerUnit>) materialPlayerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null)).ToArray<PlayerUnit>();
    List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
    foreach (PlayerUnit selectUnit in unit00484Menu1.selectUnits)
    {
      for (int index = 0; index < selectUnit.UnitIconInfo.SelectedCount; ++index)
        playerUnitList.Add(selectUnit);
    }
    unit00484Menu1.duplicationSelectUnits = playerUnitList.ToArray();
    IEnumerator e = unit00484Menu1.LoadResource();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    WebAPI.Response.UnitPreviewInheritancePreview_inheritance previewInheritance = (WebAPI.Response.UnitPreviewInheritancePreview_inheritance) null;
    if (unit00484Menu1.baseUnit.unit.IsEvolutioned)
    {
      Unit00484Menu unit00484Menu = unit00484Menu1;
      int baseRarity = unit00484Menu1.baseUnit.unit.rarity.index;
      List<int> list = ((IEnumerable<PlayerUnit>) unit00484Menu1.selectUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x =>
      {
        UnitUnit unit = x.unit;
        return unit00484Menu.baseUnit.unit.same_character_id == unit.same_character_id && baseRarity <= unit.rarity.index && Unit00484Menu.checkAnyInheritance(x);
      })).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (u => u.id)).OrderBy<int, int>((Func<int, int>) (i => i)).ToList<int>();
      if (list.Any<int>())
      {
        NGGameDataManager gdm = Singleton<NGGameDataManager>.GetInstance();
        string ikey = gdm.makeKeyPreviewInheritance(unit00484Menu1.baseUnit.id, list);
        if (!gdm.dicPreviewInheritance.TryGetValue(ikey, out previewInheritance))
        {
          Future<WebAPI.Response.UnitPreviewInheritance> future = WebAPI.UnitPreviewInheritance(unit00484Menu1.baseUnit.id, list.ToArray(), (System.Action<WebAPI.Response.UserError>) (error =>
          {
            WebAPI.DefaultUserErrorCallback(error);
            Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
          }));
          e = future.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          if (future.Result != null)
            previewInheritance = future.Result.preview_inheritance;
          if (previewInheritance == null)
          {
            yield break;
          }
          else
          {
            gdm.dicPreviewInheritance.Add(ikey, previewInheritance);
            future = (Future<WebAPI.Response.UnitPreviewInheritance>) null;
          }
        }
        gdm = (NGGameDataManager) null;
        ikey = (string) null;
      }
    }
    yield return (object) unit00484Menu1.Show_dir_Status01(previewInheritance, unit00484Menu1.getTrustUpValue(basePlayerUnit, unit00484Menu1.duplicationSelectUnits));
    foreach (Component component in unit00484Menu1.grid.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    int limitBreakCount = 0;
    if (unit00484Menu1.selectUnits.Length == 0 || ((IEnumerable<PlayerUnit>) unit00484Menu1.selectUnits).Count<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.UnitIconInfo.SelectedCount > 0)) <= 0)
    {
      unit00484Menu1.uiDragScrollView.enabled = false;
      unit00484Menu1.dir_attention.SetActive(true);
      unit00484Menu1.scrollBar.SetActive(false);
      unit00484Menu1.ibtn_change_UpperParameter.SetActive(false);
      for (int index = 0; index < 30; ++index)
        unit00484Menu1.DirSozaiItemPrefab.CloneAndGetComponent<SozaiItem>(unit00484Menu1.grid.transform).SetOnlyWLine();
    }
    else
    {
      unit00484Menu1.uiDragScrollView.enabled = true;
      unit00484Menu1.dir_attention.SetActive(false);
      unit00484Menu1.scrollBar.SetActive(true);
      unit00484Menu1.ibtn_change_UpperParameter.SetActive(true);
      for (int i = 0; i < 30; ++i)
      {
        SozaiItem component1 = unit00484Menu1.DirSozaiItemPrefab.CloneAndGetComponent<SozaiItem>(unit00484Menu1.grid.transform);
        if (i >= unit00484Menu1.selectUnits.Length)
        {
          component1.SetOnlyWLine();
        }
        else
        {
          PlayerUnit materialPlayerUnit = unit00484Menu1.selectUnits[i];
          UnitUnit material_unit = materialPlayerUnit.unit;
          if (materialPlayerUnit.UnitIconInfo.SelectedCount <= 0)
          {
            component1.SetOnlyWLine();
          }
          else
          {
            unit00484Menu1.upperParameterPrefab.CloneAndGetComponent<UpperParameter>(component1.DirSozaiBase.transform).Init(materialPlayerUnit);
            unit00484Menu1.upperParameterLabelPrefab.CloneAndGetComponent<UpperParameterLabel>(component1.DirSozaiBase2.transform).Init(basePlayerUnit, materialPlayerUnit);
            UnitIcon component2 = unit00484Menu1.unitIconPrefab.CloneAndGetComponent<UnitIcon>(component1.LinkSozaiBase.transform);
            component1.UnitIcon = component2;
            // ISSUE: reference to a compiler-generated method
            component2.onClick = new System.Action<UnitIconBase>(unit00484Menu1.\u003CInit\u003Eb__57_7);
            component2.SelectByCheckAndNumber(materialPlayerUnit.UnitIconInfo);
            component2.setBottom(materialPlayerUnit);
            component2.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
            if (material_unit.IsMaterialUnit)
            {
              PlayerMaterialUnit playerMaterialUnit = ((IEnumerable<PlayerMaterialUnit>) SMManager.Get<PlayerMaterialUnit[]>()).FirstOrDefault<PlayerMaterialUnit>((Func<PlayerMaterialUnit, bool>) (x => x._unit == material_unit.ID));
              if (playerMaterialUnit != null)
              {
                component1.TxtPossessionNum.gameObject.SetActive(true);
                component1.TxtPossessionNum.SetTextLocalize(Consts.Format(Consts.GetInstance().unit_004_9_9_possession_text, (IDictionary) new Hashtable()
                {
                  {
                    (object) "Count",
                    (object) playerMaterialUnit.quantity
                  }
                }));
              }
              e = component2.SetMaterialUnit(materialPlayerUnit, false, materialPlayerUnits);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
            }
            else
            {
              component1.TxtPossessionNum.gameObject.SetActive(false);
              e = component2.SetPlayerUnitEvolution(materialPlayerUnit, materialPlayerUnits, basePlayerUnit, true, false);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
            }
            if ((material_unit.same_character_id != base_unit.same_character_id ? 0 : (material_unit.rarity.index >= base_unit.rarity.index ? 1 : 0)) != 0 || material_unit.IsBreakThrough)
              limitBreakCount += !material_unit.IsBreakThrough ? (base_unit.rarity.index > material_unit.rarity.index ? 1 : materialPlayerUnit.breakthrough_count + 1) : materialPlayerUnit.UnitIconInfo.SelectedCount;
            int num = base_unit.breakthrough_limit - basePlayerUnit.breakthrough_count;
            if (limitBreakCount > num)
              limitBreakCount = num;
            materialPlayerUnit = (PlayerUnit) null;
          }
        }
      }
    }
    if (!unit00484Menu1.isSetfirstGridPosition)
    {
      unit00484Menu1.isSetfirstGridPosition = true;
      unit00484Menu1.firstGridPosition = unit00484Menu1.grid.transform.localPosition;
    }
    unit00484Menu1.grid.transform.localPosition = unit00484Menu1.firstGridPosition;
    unit00484Menu1.grid.repositionNow = true;
    unit00484Menu1.SetWidthLine();
    unit00484Menu1.onIbtnChangeUpperParameter();
    unit00484Menu1.TxtLv.SetTextLocalize((unit00484Menu1.baseUnit.max_level + limitBreakCount * unit00484Menu1.baseUnit.unit._level_per_breakthrough).ToString());
    unit00484Menu1.setLimitBreak(basePlayerUnit, limitBreakCount);
    e = unit00484Menu1.setSkillIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit00484Menu1.setUnityValue(basePlayerUnit, unit00484Menu1.duplicationSelectUnits);
    unit00484Menu1.SetPrice(unit00484Menu1.baseUnit, unit00484Menu1.duplicationSelectUnits);
    unit00484Menu1.ibtnCombine.isEnabled = unit00484Menu1.duplicationSelectUnits.Length != 0 && OverkillersUtil.checkDelete(((IEnumerable<PlayerUnit>) unit00484Menu1.selectUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsNormalUnit)).ToArray<PlayerUnit>());
    NGTween.playTweens(NGTween.findTweenersAll(unit00484Menu1.gameObject, false), 0, false);
  }

  public void ResetScrollViewPosition()
  {
    this.scrollView.ResetPosition();
  }

  private IEnumerator LoadResource()
  {
    Future<GameObject> unitIconPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) this.unitIconPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.unitIconPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.upperParameterPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = new ResourceObject("Prefabs/UnitIcon/UpperParameter").Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.upperParameterPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.upperParameterLabelPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = new ResourceObject("Prefabs/UnitIcon/UpperParameterLabel").Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.upperParameterLabelPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.skillTypeIconPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.BattleSkillIcon._battleSkillIcon.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.skillTypeIconPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.statusGaugePrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = Res.Prefabs.unit004_8_3.StatusGauge.Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.statusGaugePrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.DirSozaiItemPrefab == (UnityEngine.Object) null)
    {
      unitIconPrefabF = new ResourceObject("Prefabs/unit004_8_4/dir_Sozai_Item").Load<GameObject>();
      e = unitIconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.DirSozaiItemPrefab = unitIconPrefabF.Result;
      unitIconPrefabF = (Future<GameObject>) null;
    }
    e = this.CreateSkillDetailDialog();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) this.prefabUnityDetail == (UnityEngine.Object) null)
    {
      Future<GameObject>[] loaders = PopupUnityValueDetail.createLoaders(false);
      yield return (object) loaders[0].Wait();
      this.prefabUnityDetail = loaders[0].Result;
      yield return (object) loaders[1].Wait();
      this.prefabStageItem = loaders[1].Result;
      loaders = (Future<GameObject>[]) null;
    }
  }

  private IEnumerator Show_dir_Status01(
    WebAPI.Response.UnitPreviewInheritancePreview_inheritance previewInheritance,
    int addedTrustValue)
  {
    Unit00484Menu unit00484Menu = this;
    foreach (Component component in unit00484Menu.princessType)
      component.gameObject.SetActive(false);
    unit00484Menu.princessType[unit00484Menu.baseUnit.unit_type.ID - 1].gameObject.SetActive(true);
    unit00484Menu.dyn_thum.transform.Clear();
    UnitIcon unitIconScript = unit00484Menu.unitIconPrefab.CloneAndGetComponent<UnitIcon>(unit00484Menu.dyn_thum);
    PlayerUnit[] playerUnits = new PlayerUnit[1]
    {
      unit00484Menu.baseUnit
    };
    IEnumerator e = unitIconScript.SetPlayerUnit(unit00484Menu.baseUnit, playerUnits, (PlayerUnit) null, true, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitIconScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    unitIconScript.setLevelText(unit00484Menu.baseUnit);
    // ISSUE: reference to a compiler-generated method
    ((IEnumerable<GameObject>) unit00484Menu.limitBreakIcon).ForEachIndex<GameObject>(new System.Action<GameObject, int>(unit00484Menu.\u003CShow_dir_Status01\u003Eb__60_0));
    unit00484Menu.setTrust(addedTrustValue);
    unit00484Menu.setStatusGauge(unit00484Menu.statusGaugePrefab, unit00484Menu.duplicationSelectUnits, previewInheritance);
  }

  private void SetWidthLine()
  {
    int num = 0;
    foreach (Transform transform in this.grid.transform)
    {
      if (num % this.grid.maxPerLine == 0)
        transform.GetComponent<SozaiItem>().WLine.SetActive(true);
      else
        transform.GetComponent<SozaiItem>().WLine.SetActive(false);
      ++num;
    }
  }

  private void setTrust(int addTrustValue)
  {
    this.dirDearDegree.SetActive(false);
    if (!this.baseUnit.unit.trust_target_flag)
      return;
    this.dirDearDegree.SetActive(true);
    if (this.baseUnit.unit.IsSea)
      this.txtDear.SetTextLocalize(Consts.GetInstance().TXT_DEAR);
    else if (this.baseUnit.unit.IsResonanceUnit)
      this.txtDear.SetTextLocalize(Consts.GetInstance().TXT_RELEVANCE);
    else
      this.txtDear.SetTextLocalize(Consts.GetInstance().TXT_DEAR);
    this.trust = this.baseUnit.trust_rate;
    float trustRateMax = (float) PlayerUnit.GetTrustRateMax();
    float? nullable1 = new float?();
    foreach (PlayerUnit selectUnit in this.selectUnits)
    {
      if (selectUnit.UnitIconInfo.SelectedCount > 0)
      {
        if (selectUnit.unit.IsTrustMaterial(this.baseUnit))
        {
          this.trust += selectUnit.unit.TrustMaterialUnit(this.baseUnit).increase_value * (float) selectUnit.UnitIconInfo.SelectedCount;
          this.trust += selectUnit.trust_rate;
          float increaseValue = selectUnit.unit.TrustMaterialUnit(this.baseUnit).increase_value;
          if (selectUnit.UnitIconInfo.SelectedCount == 1 && (double) selectUnit.trust_rate != (double) trustRateMax)
            increaseValue += selectUnit.trust_rate;
          nullable1 = nullable1.HasValue ? new float?(Mathf.Min(nullable1.Value, increaseValue)) : new float?(increaseValue);
        }
        else if (this.baseUnit.unit.same_character_id == selectUnit.unit.same_character_id)
        {
          this.trust += (float) PlayerUnit.GetTrustComposeRate();
          this.trust += selectUnit.trust_rate;
        }
        else if (this.baseUnit.unit.character.ID == selectUnit.unit.character.ID)
          this.trust += selectUnit.unit.rarity.trust_rate * (float) (selectUnit.unity_value + 1);
      }
    }
    Consts.GetInstance();
    this.maxTrust = this.baseUnit.trust_max_rate + (float) addTrustValue;
    if ((double) this.maxTrust > (double) trustRateMax)
      this.maxTrust = trustRateMax;
    if (nullable1.HasValue)
    {
      float trust = this.trust;
      float? nullable2 = nullable1;
      float? nullable3 = nullable2.HasValue ? new float?(trust - nullable2.GetValueOrDefault()) : new float?();
      float maxTrust = this.maxTrust;
      if ((double) nullable3.GetValueOrDefault() > (double) maxTrust & nullable3.HasValue)
      {
        this.isMaterialOverUsed = true;
        goto label_24;
      }
    }
    this.isMaterialOverUsed = false;
label_24:
    if ((double) this.trust > (double) this.maxTrust)
      this.trust = this.maxTrust;
    if ((double) this.trust != (double) this.baseUnit.trust_rate)
      this.txtDearDegreeAmountNumer.color = Color.yellow;
    else
      this.txtDearDegreeAmountNumer.color = Color.white;
    if (addTrustValue > 0 && (double) this.baseUnit.trust_max_rate < (double) trustRateMax)
      this.txtDearDegreeAmountDenor.color = Color.yellow;
    else
      this.txtDearDegreeAmountDenor.color = Color.white;
    this.txtDearDegreeAmountDenor.SetTextLocalize((Math.Round((double) this.maxTrust * 100.0) / 100.0).ToString());
    this.txtDearDegreeAmountNumer.SetTextLocalize((Math.Round((double) this.trust * 100.0) / 100.0).ToString());
  }

  private Dictionary<int, float> SkillLevelUpRatio(
    PlayerUnit baseUnit,
    PlayerUnit[] materialUnits,
    Dictionary<int, int> playerSkillDict)
  {
    Dictionary<int, float> data = new Dictionary<int, float>();
    foreach (BattleskillSkill battleSkill in baseUnit.GetBattleSkills())
      data[baseUnit.evolutionSkill(battleSkill).ID] = 0.0f;
    if (materialUnits.Length < 1)
      return data;
    List<PlayerUnitSkills> list = ((IEnumerable<PlayerUnitSkills>) baseUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.level < baseUnit.evolutionSkill(x.skill).upper_level)).ToList<PlayerUnitSkills>();
    foreach (PlayerUnit materialUnit in materialUnits)
    {
      PlayerUnit material = materialUnit;
      if (material.unit.same_character_id == baseUnit.unit.same_character_id)
      {
        Dictionary<int, int> materialSkillDict = material.GetAcquireSkillsDictionary();
        list.ForEach((System.Action<PlayerUnitSkills>) (x =>
        {
          int id = baseUnit.evolutionSkill(x.skill).ID;
          if (materialSkillDict.ContainsKey(id) && materialSkillDict[id] > 0)
          {
            data[id] = 100f;
          }
          else
          {
            if ((double) material.unit.rarity.skill_levelup_rate <= (double) data[x.skill_id])
              return;
            data[id] = (float) material.unit.rarity.skill_levelup_rate;
          }
        }));
      }
      else
        list.ForEach((System.Action<PlayerUnitSkills>) (x =>
        {
          BattleskillSkill battleskillSkill = baseUnit.evolutionSkill(x.skill);
          if (UnitDetailIcon.IsSkillUpMaterial(material.unit, baseUnit))
          {
            UnitSkillupSetting skillupSetting = ((IEnumerable<UnitSkillupSetting>) MasterData.UnitSkillupSettingList).FirstOrDefault<UnitSkillupSetting>((Func<UnitSkillupSetting, bool>) (y => y.material_unit_id == material.unit.ID));
            bool flag = battleskillSkill.skill_type == (BattleskillSkillType) material.unit.skillup_type || material.unit.skillup_type == UnitDetailIcon.SKILL_ID_ALL;
            if (skillupSetting != null && skillupSetting.skill_group.HasValue)
            {
              IEnumerable<int> source = ((IEnumerable<UnitSkillupSkillGroupSetting>) MasterData.UnitSkillupSkillGroupSettingList).Where<UnitSkillupSkillGroupSetting>((Func<UnitSkillupSkillGroupSetting, bool>) (y => y.group_id == skillupSetting.skill_group.Value)).Select<UnitSkillupSkillGroupSetting, int>((Func<UnitSkillupSkillGroupSetting, int>) (y => y.skill_id));
              flag = flag && source.Contains<int>(battleskillSkill.ID);
            }
            if (!flag || !data.ContainsKey(battleskillSkill.ID))
              return;
            float num = 20f;
            if (skillupSetting != null)
              num = skillupSetting.levelup_ratio * 100f;
            if ((double) num <= (double) data[battleskillSkill.ID])
              return;
            data[battleskillSkill.ID] = num;
          }
          else
          {
            if (material.skills == null)
              return;
            PlayerUnitSkills playerUnitSkills = ((IEnumerable<PlayerUnitSkills>) material.skills).FirstOrDefault<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (y => x.skill_id == y.skill_id));
            if (playerUnitSkills == null || !data.ContainsKey(battleskillSkill.ID))
              return;
            float num = UnitSkillLevelUpProbability.Probability(x.level, playerUnitSkills.level) * 100f;
            if ((double) num <= (double) data[battleskillSkill.ID])
              return;
            data[battleskillSkill.ID] = num;
          }
        }));
    }
    return data;
  }

  private IEnumerator CreateSkillDetailDialog()
  {
    Future<GameObject> loader = PopupSkillDetails.createPrefabLoader(false);
    IEnumerator e = loader.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.skillDetailDialogPrefab = loader.Result;
  }

  private void popupSkillDetail(BattleskillSkill skill)
  {
    PopupSkillDetails.Param[] skillParams = this.skillParams;
    int? nullable = skillParams != null ? ((IEnumerable<PopupSkillDetails.Param>) skillParams).FirstIndexOrNull<PopupSkillDetails.Param>((Func<PopupSkillDetails.Param, bool>) (x => x.skill == skill)) : new int?();
    if (!nullable.HasValue)
      return;
    PopupSkillDetails.show(this.skillDetailDialogPrefab, this.skillParams[nullable.Value], false, (System.Action) null, false);
  }

  protected void SetPrice(PlayerUnit basePlayerUnit, PlayerUnit[] select)
  {
    this.TxtZeny.SetTextLocalize(CalcUnitCompose.priceCompose(basePlayerUnit, select).ToString());
  }

  private void setUnityValue(PlayerUnit baseUnit, PlayerUnit[] materialUnits)
  {
    float unityValueCount = 0.0f;
    float buildupUnityCount = 0.0f;
    float MAX_UNITYVALUE = (float) PlayerUnit.GetUnityValueMax();
    UnitUnit unit1 = baseUnit.unit;
    if ((double) baseUnit.unity_value < (double) MAX_UNITYVALUE && materialUnits.Length != 0)
    {
      Func<float, float, float> func = (double) baseUnit.unityTotal < (double) MAX_UNITYVALUE ? (Func<float, float, float>) ((now, add) => Mathf.Min(now + add, MAX_UNITYVALUE)) : (Func<float, float, float>) ((a, b) => 0.0f);
      foreach (PlayerUnit materialUnit in materialUnits)
      {
        UnitUnit unit2 = materialUnit.unit;
        if (unit2.IsNormalUnit)
        {
          if (unit2.same_character_id == unit1.same_character_id)
          {
            unityValueCount = Mathf.Min((float) ((double) unityValueCount + (double) materialUnit.unity_value + 1.0), MAX_UNITYVALUE);
            buildupUnityCount = func(buildupUnityCount, materialUnit.buildup_unity_value_f);
          }
        }
        else if (unit2.is_unity_value_up)
        {
          UnitUnit unitUnit = unit2;
          UnitUnit target = unit1;
          Func<UnitFamily[]> funcGetFamilies = (Func<UnitFamily[]>) (() => baseUnit.Families);
          UnityValueUpPattern valueUpPattern;
          if ((valueUpPattern = unitUnit.FindValueUpPattern(target, funcGetFamilies)) != null)
            buildupUnityCount = func(buildupUnityCount, (float) valueUpPattern.up_value);
        }
      }
    }
    if ((double) unityValueCount == 0.0 && (double) buildupUnityCount == 0.0)
      this.TxtUnity.color = Color.white;
    else
      this.TxtUnity.color = Color.yellow;
    double unityTotal = (double) baseUnit.unityTotal;
    float num = Mathf.Min(baseUnit.unityTotal + unityValueCount + buildupUnityCount, MAX_UNITYVALUE);
    this.TxtUnity.SetTextLocalize((double) num < 99.0 ? num.ToString("f1") : num.ToString());
    this.popupUnityDetail = (System.Action) (() =>
    {
      if ((UnityEngine.Object) this.prefabUnityDetail == (UnityEngine.Object) null)
        return;
      if (!baseUnit.is_enemy && !baseUnit.is_gesut && Player.Current.id == baseUnit.player_id)
      {
        PlayerUnit playerUnit = Array.Find<PlayerUnit>(SMManager.Get<PlayerUnit[]>(), (Predicate<PlayerUnit>) (x => x.id == baseUnit.id));
        if ((object) playerUnit == null)
          playerUnit = baseUnit;
        baseUnit = playerUnit;
      }
      PopupUnityValueDetail.show(this.prefabUnityDetail, this.prefabStageItem, Mathf.Min((float) baseUnit.unity_value + unityValueCount, MAX_UNITYVALUE), Mathf.Min(baseUnit.buildup_unity_value_f + buildupUnityCount, MAX_UNITYVALUE), baseUnit.unit, (System.Action) (() => Singleton<NGGameDataManager>.GetInstance().OpenUnityPopup = this.popupUnityDetail));
    });
  }

  private int getTrustUpValue(PlayerUnit baseUnit, PlayerUnit[] materialUnits)
  {
    int num = 0;
    if (materialUnits.Length != 0)
    {
      UnitUnit unit1 = baseUnit.unit;
      foreach (PlayerUnit materialUnit in materialUnits)
      {
        UnitUnit unit2 = materialUnit.unit;
        if (unit2.IsNormalUnit && unit2.same_character_id == unit1.same_character_id)
          num += (int) materialUnit.trust_max_rate;
      }
    }
    int trustRateMax = PlayerUnit.GetTrustRateMax();
    if (num > trustRateMax)
      num = trustRateMax;
    return num;
  }

  private void setStatusGauge(
    GameObject prefabGauge,
    PlayerUnit[] materialPlayerUnits,
    WebAPI.Response.UnitPreviewInheritancePreview_inheritance previewInheritance)
  {
    CalcUnitCompose.ComposeType[] composeTypeArray = new CalcUnitCompose.ComposeType[8]
    {
      CalcUnitCompose.ComposeType.HP,
      CalcUnitCompose.ComposeType.STRENGTH,
      CalcUnitCompose.ComposeType.INTELLIGENCE,
      CalcUnitCompose.ComposeType.VITALITY,
      CalcUnitCompose.ComposeType.MIND,
      CalcUnitCompose.ComposeType.AGILITY,
      CalcUnitCompose.ComposeType.DEXTERITY,
      CalcUnitCompose.ComposeType.LUCKY
    };
    int[] numArray1 = new int[8]
    {
      this.baseUnit.hp.inheritance,
      this.baseUnit.strength.inheritance,
      this.baseUnit.intelligence.inheritance,
      this.baseUnit.vitality.inheritance,
      this.baseUnit.mind.inheritance,
      this.baseUnit.agility.inheritance,
      this.baseUnit.dexterity.inheritance,
      this.baseUnit.lucky.inheritance
    };
    int[] numArray2 = new int[8]
    {
      this.baseUnit.self_total_hp,
      this.baseUnit.self_total_strength,
      this.baseUnit.self_total_intelligence,
      this.baseUnit.self_total_vitality,
      this.baseUnit.self_total_mind,
      this.baseUnit.self_total_agility,
      this.baseUnit.self_total_dexterity,
      this.baseUnit.self_total_lucky
    };
    int[] numArray3 = new int[8]
    {
      this.baseUnit.hp.compose,
      this.baseUnit.strength.compose,
      this.baseUnit.intelligence.compose,
      this.baseUnit.vitality.compose,
      this.baseUnit.mind.compose,
      this.baseUnit.agility.compose,
      this.baseUnit.dexterity.compose,
      this.baseUnit.lucky.compose
    };
    int[] numArray4 = new int[8]
    {
      this.baseUnit.hp.buildup,
      this.baseUnit.strength.buildup,
      this.baseUnit.intelligence.buildup,
      this.baseUnit.vitality.buildup,
      this.baseUnit.mind.buildup,
      this.baseUnit.agility.buildup,
      this.baseUnit.dexterity.buildup,
      this.baseUnit.lucky.buildup
    };
    bool[] flagArray = new bool[8]
    {
      this.baseUnit.hp.is_max,
      this.baseUnit.strength.is_max,
      this.baseUnit.intelligence.is_max,
      this.baseUnit.vitality.is_max,
      this.baseUnit.mind.is_max,
      this.baseUnit.agility.is_max,
      this.baseUnit.dexterity.is_max,
      this.baseUnit.lucky.is_max
    };
    int[] previewInheritances = new int[8]
    {
      previewInheritance != null ? previewInheritance.hp : 0,
      previewInheritance != null ? previewInheritance.strength : 0,
      previewInheritance != null ? previewInheritance.intelligence : 0,
      previewInheritance != null ? previewInheritance.vitality : 0,
      previewInheritance != null ? previewInheritance.mind : 0,
      previewInheritance != null ? previewInheritance.agility : 0,
      previewInheritance != null ? previewInheritance.dexterity : 0,
      previewInheritance != null ? previewInheritance.lucky : 0
    };
    UnitTypeParameter unitTypeParameter = this.baseUnit.UnitTypeParameter;
    int gaugeMax = ((IEnumerable<int>) new int[8]
    {
      this.baseUnit.hp.initial + this.baseUnit.hp.inheritance + this.baseUnit.hp.level_up_max_status + unitTypeParameter.hp_compose_max,
      this.baseUnit.strength.initial + this.baseUnit.strength.inheritance + this.baseUnit.strength.level_up_max_status + unitTypeParameter.strength_compose_max,
      this.baseUnit.intelligence.initial + this.baseUnit.intelligence.inheritance + this.baseUnit.intelligence.level_up_max_status + unitTypeParameter.intelligence_compose_max,
      this.baseUnit.vitality.initial + this.baseUnit.vitality.inheritance + this.baseUnit.vitality.level_up_max_status + unitTypeParameter.vitality_compose_max,
      this.baseUnit.mind.initial + this.baseUnit.mind.inheritance + this.baseUnit.mind.level_up_max_status + unitTypeParameter.mind_compose_max,
      this.baseUnit.agility.initial + this.baseUnit.agility.inheritance + this.baseUnit.agility.level_up_max_status + unitTypeParameter.agility_compose_max,
      this.baseUnit.dexterity.initial + this.baseUnit.dexterity.inheritance + this.baseUnit.dexterity.level_up_max_status + unitTypeParameter.dexterity_compose_max,
      this.baseUnit.lucky.initial + this.baseUnit.lucky.inheritance + this.baseUnit.lucky.level_up_max_status + unitTypeParameter.lucky_compose_max
    }).Select<int, int>((Func<int, int, int>) ((v, i) => v + previewInheritances[i])).Max();
    int length = this.StatusGaugeBase.Length;
    for (int index = 0; index < length; ++index)
    {
      GameObject gameObject = this.StatusGaugeBase[index];
      gameObject.transform.Clear();
      prefabGauge.CloneAndGetComponent<Unit00483StatusGauge>(gameObject).Init(this.baseUnit, materialPlayerUnits, composeTypeArray[index], numArray1[index], previewInheritances[index], numArray2[index], numArray3[index], numArray4[index], flagArray[index], gaugeMax);
      NGTween.playTweens(NGTween.findTweenersAll(gameObject, false), 0, false);
    }
  }

  private void setLimitBreak(PlayerUnit basePlayerUnit, int limitBreakCount)
  {
    this.limitBreak.SetActive(true);
    foreach (Component component in this.limitBreakBlue)
      component.gameObject.SetActive(false);
    foreach (Component component in this.limitBreakGreen)
      component.gameObject.SetActive(false);
    for (int index = 0; index < basePlayerUnit.breakthrough_count; ++index)
      this.limitBreakBlue[index].gameObject.SetActive(true);
    for (int breakthroughCount = basePlayerUnit.breakthrough_count; breakthroughCount < basePlayerUnit.breakthrough_count + limitBreakCount; ++breakthroughCount)
    {
      UISprite uiSprite = this.limitBreakGreen[breakthroughCount];
      uiSprite.gameObject.SetActive(true);
      NGTween.playTweens(NGTween.findTweenersAll(uiSprite.gameObject, false), 0, false);
    }
    if (basePlayerUnit.unit.breakthrough_limit != 0)
      return;
    this.slc_Limitbreak.SetActive(false);
  }

  private IEnumerator CreateSkillIcon(
    BattleskillSkill sk,
    int idx,
    int unitSkillLv,
    int needLv)
  {
    BattleSkillIcon component = this.skillTypeIconPrefab.Clone(this.skillObject[idx].transform).GetComponent<BattleSkillIcon>();
    component.SetDepth(6);
    if (unitSkillLv == 0)
      component.EnableNeedLvIcon(needLv);
    IEnumerator e = component.Init(sk);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.skillObject[idx].GetComponent<UIButton>().onClick.Clear();
    EventDelegate.Add(this.skillObject[idx].GetComponent<UIButton>().onClick, (EventDelegate.Callback) (() => this.popupSkillDetail(sk)));
  }

  private void DisplaySkillLevelUpArrow(int idx, float rate)
  {
    if ((double) rate <= 0.0)
    {
      this.skillUpObject[idx].SetActive(false);
    }
    else
    {
      this.skillUpObject[idx].SetActive(true);
      string str = "slc_SkillUP";
      string n = (double) rate >= 40.0 ? ((double) rate >= 70.0 ? ((double) rate >= 100.0 ? str + "4" : str + "3") : str + "2") : str + "1";
      this.skillUpObject[idx].transform.GetChildren().ForEach<Transform>((System.Action<Transform>) (x => x.gameObject.SetActive(false)));
      Transform transform = this.skillUpObject[idx].transform.Find(n);
      if ((UnityEngine.Object) transform != (UnityEngine.Object) null)
        transform.gameObject.SetActive(true);
    }
    NGTween.playTweens(NGTween.findTweenersAll(this.skillUpObject[idx], false), 0, false);
  }

  private void resetDisplaySkillAttention()
  {
    if (this.skillAttention == null)
      return;
    foreach (GameObject gameObject in this.skillAttention)
      gameObject.SetActive(false);
  }

  private void displaySkillAttention(int index)
  {
    if (this.skillAttention == null || this.skillAttention.Length <= index)
      return;
    this.skillAttention[index].SetActive(true);
  }

  private IEnumerator setSkillIcon()
  {
    Unit00484Menu unit00484Menu = this;
    ((IEnumerable<GameObject>) unit00484Menu.skillObject).ForEach<GameObject>((System.Action<GameObject>) (x => x.transform.Clear()));
    unit00484Menu.resetDisplaySkillAttention();
    Dictionary<int, int> playerSkillDict = new Dictionary<int, int>();
    foreach (PlayerUnitSkills acquireSkill in unit00484Menu.baseUnit.GetAcquireSkills())
    {
      if (!playerSkillDict.ContainsKey(acquireSkill.skill_id))
        playerSkillDict.Add(acquireSkill.skill_id, acquireSkill.level);
    }
    IEnumerable<int> allSkillIDs = ((IEnumerable<UnitSkill>) unit00484Menu.baseUnit.GetSkills()).Select<UnitSkill, int>((Func<UnitSkill, int>) (x => x.skill_BattleskillSkill)).Concat<int>(((IEnumerable<UnitSkillCharacterQuest>) unit00484Menu.baseUnit.GetCharacterSkills()).Select<UnitSkillCharacterQuest, int>((Func<UnitSkillCharacterQuest, int>) (x => x.skill_BattleskillSkill))).Concat<int>(((IEnumerable<UnitSkillHarmonyQuest>) unit00484Menu.baseUnit.GetHarmonySkills()).Select<UnitSkillHarmonyQuest, int>((Func<UnitSkillHarmonyQuest, int>) (x => x.skill_BattleskillSkill))).Concat<int>(((IEnumerable<UnitSkillIntimate>) unit00484Menu.baseUnit.GetIntimateSkills()).Select<UnitSkillIntimate, int>((Func<UnitSkillIntimate, int>) (x => x.skill_BattleskillSkill))).Distinct<int>();
    List<int> intList1 = new List<int>();
    foreach (int index in allSkillIDs)
      intList1.Add(unit00484Menu.baseUnit.evolutionSkill(MasterData.BattleskillSkill[index]).ID);
    allSkillIDs = allSkillIDs.Concat<int>((IEnumerable<int>) intList1).Distinct<int>();
    List<int> intList2 = new List<int>();
    // ISSUE: reference to a compiler-generated method
    foreach (UnitSkillCharacterQuest skillCharacterQuest in ((IEnumerable<UnitSkillCharacterQuest>) MasterData.UnitSkillCharacterQuestList).Where<UnitSkillCharacterQuest>(new Func<UnitSkillCharacterQuest, bool>(unit00484Menu.\u003CsetSkillIcon\u003Eb__75_7)).ToArray<UnitSkillCharacterQuest>())
    {
      if (skillCharacterQuest.skill_after_evolution > 0)
        intList2.Add(skillCharacterQuest.skill_after_evolution);
    }
    allSkillIDs = allSkillIDs.Concat<int>((IEnumerable<int>) intList2).Distinct<int>();
    Dictionary<int, float> skillRatio = unit00484Menu.SkillLevelUpRatio(unit00484Menu.baseUnit, unit00484Menu.selectUnits, playerSkillDict);
    int skillIndex = 0;
    int len = unit00484Menu.skillObject.Length;
    IEnumerable<PlayerUnitSkills> skills = ((IEnumerable<PlayerUnitSkills>) unit00484Menu.baseUnit.skills).Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.skill.skill_type != BattleskillSkillType.magic && x.skill.skill_type != BattleskillSkillType.leader && !BattleskillSkill.InvestElementSkillIds.Contains(x.skill_id)));
    skills = (IEnumerable<PlayerUnitSkills>) skills.OrderBy<PlayerUnitSkills, int>((Func<PlayerUnitSkills, int>) (x => x.skill_id)).ToArray<PlayerUnitSkills>();
    List<PopupSkillDetails.Param> lstSkillParam = new List<PopupSkillDetails.Param>();
    PlayerUnitSkills unitSkill;
    BattleskillSkill skill;
    int currentLevel1;
    IEnumerator e;
    if (skillIndex < len - 1)
    {
      PlayerUnitSkills us = skills.FirstOrDefault<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.skill.skill_type == BattleskillSkillType.growth));
      if (us != null)
      {
        unitSkill = us;
        if (unitSkill != null && allSkillIDs.Any<int>((Func<int, bool>) (x => x == us.skill_id)))
        {
          skill = unit00484Menu.baseUnit.evolutionSkill(unitSkill.skill);
          if (skill != null)
          {
            if (playerSkillDict.ContainsKey(skill.ID))
            {
              currentLevel1 = Math.Min(playerSkillDict[skill.ID], skill.upper_level);
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, currentLevel1, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Growth, new int?(currentLevel1)));
              if (currentLevel1 < skill.upper_level)
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[skill.ID]);
              else
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, 0.0f);
            }
            else
            {
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, 0, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[unitSkill.skill_id]);
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Growth, new int?()));
            }
            ++skillIndex;
          }
          skill = (BattleskillSkill) null;
        }
        unitSkill = (PlayerUnitSkills) null;
      }
    }
    PlayerUnitSkills[] uss;
    int currentLevel2;
    if (skillIndex < len - 1)
    {
      uss = skills.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.skill.skill_type == BattleskillSkillType.duel)).ToArray<PlayerUnitSkills>();
      for (currentLevel1 = 0; currentLevel1 < uss.Length; ++currentLevel1)
      {
        PlayerUnitSkills us = uss[currentLevel1];
        unitSkill = us;
        if (us != null && allSkillIDs.Any<int>((Func<int, bool>) (x => x == us.skill_id)))
        {
          skill = unit00484Menu.baseUnit.evolutionSkill(unitSkill.skill);
          if (skill != null)
          {
            if (playerSkillDict.ContainsKey(skill.ID))
            {
              currentLevel2 = Math.Min(playerSkillDict[skill.ID], skill.upper_level);
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, currentLevel2, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Duel, new int?(currentLevel2)));
              if (currentLevel2 < skill.upper_level)
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[skill.ID]);
              else
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, 0.0f);
            }
            else
            {
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, 0, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Duel, new int?()));
              unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[unitSkill.skill_id]);
            }
            if (++skillIndex < len)
            {
              unitSkill = (PlayerUnitSkills) null;
              skill = (BattleskillSkill) null;
            }
            else
              break;
          }
        }
      }
      uss = (PlayerUnitSkills[]) null;
    }
    if (skillIndex < len - 1)
    {
      PlayerUnitSkills us = skills.FirstOrDefault<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.skill.skill_type == BattleskillSkillType.release));
      if (us != null)
      {
        unitSkill = us;
        if (unitSkill != null && allSkillIDs.Any<int>((Func<int, bool>) (x => x == us.skill_id)))
        {
          skill = unit00484Menu.baseUnit.evolutionSkill(unitSkill.skill);
          if (skill != null)
          {
            if (playerSkillDict.ContainsKey(skill.ID))
            {
              currentLevel1 = Math.Min(playerSkillDict[skill.ID], skill.upper_level);
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, currentLevel1, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Release, new int?(currentLevel1)));
              if (currentLevel1 < skill.upper_level)
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[skill.ID]);
              else
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, 0.0f);
            }
            else
            {
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, 0, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Release, new int?()));
              unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[unitSkill.skill_id]);
            }
            ++skillIndex;
          }
          skill = (BattleskillSkill) null;
        }
        unitSkill = (PlayerUnitSkills) null;
      }
    }
    if (skillIndex < len - 1)
    {
      uss = skills.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.skill.skill_type == BattleskillSkillType.command)).ToArray<PlayerUnitSkills>();
      for (currentLevel1 = 0; currentLevel1 < uss.Length; ++currentLevel1)
      {
        PlayerUnitSkills us = uss[currentLevel1];
        unitSkill = us;
        if (unitSkill != null && allSkillIDs.Any<int>((Func<int, bool>) (x => x == us.skill_id)))
        {
          skill = unit00484Menu.baseUnit.evolutionSkill(unitSkill.skill);
          if (skill != null)
          {
            if (playerSkillDict.ContainsKey(skill.ID))
            {
              currentLevel2 = Math.Min(playerSkillDict[skill.ID], skill.upper_level);
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, currentLevel2, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Command, new int?(currentLevel2)));
              if (currentLevel2 < skill.upper_level)
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[skill.ID]);
              else
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, 0.0f);
            }
            else
            {
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, 0, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Command, new int?()));
              unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[unitSkill.skill_id]);
            }
            if (++skillIndex < len)
            {
              unitSkill = (PlayerUnitSkills) null;
              skill = (BattleskillSkill) null;
            }
            else
              break;
          }
        }
      }
      uss = (PlayerUnitSkills[]) null;
    }
    if (skillIndex < len - 1)
    {
      uss = skills.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (x => x.skill.skill_type == BattleskillSkillType.passive)).ToArray<PlayerUnitSkills>();
      for (currentLevel1 = 0; currentLevel1 < uss.Length; ++currentLevel1)
      {
        PlayerUnitSkills us = uss[currentLevel1];
        unitSkill = us;
        if (unitSkill != null && allSkillIDs.Any<int>((Func<int, bool>) (x => x == us.skill_id)))
        {
          skill = unit00484Menu.baseUnit.evolutionSkill(unitSkill.skill);
          if (skill != null)
          {
            if (playerSkillDict.ContainsKey(skill.ID))
            {
              currentLevel2 = Math.Min(playerSkillDict[skill.ID], skill.upper_level);
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, currentLevel2, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Grant, new int?(currentLevel2)));
              if (currentLevel2 < skill.upper_level)
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[skill.ID]);
              else
                unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, 0.0f);
            }
            else
            {
              e = unit00484Menu.CreateSkillIcon(skill, skillIndex, 0, unitSkill.level);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              lstSkillParam.Add(new PopupSkillDetails.Param(skill, UnitParameter.SkillGroup.Grant, new int?()));
              unit00484Menu.DisplaySkillLevelUpArrow(skillIndex, skillRatio[unitSkill.skill_id]);
            }
            if (++skillIndex < len)
            {
              unitSkill = (PlayerUnitSkills) null;
              skill = (BattleskillSkill) null;
            }
            else
              break;
          }
        }
      }
      uss = (PlayerUnitSkills[]) null;
    }
    unit00484Menu.skillParams = lstSkillParam.ToArray();
    if (unit00484Menu.dir_skill != null)
    {
      for (int index = 0; index < unit00484Menu.dir_skill.Length; ++index)
        unit00484Menu.dir_skill[index].SetActive(index < skillIndex);
    }
  }

  private int calcComposeSuccessProb(PlayerUnit[] playerUnits)
  {
    int ret = 0;
    ((IEnumerable<PlayerUnit>) playerUnits).ForEach<PlayerUnit>((System.Action<PlayerUnit>) (pu =>
    {
      if (pu.unit.IsBuildup)
        ret += 100;
      else if (pu.unit.rarity.index == 0)
        ret += 5;
      else if (pu.unit.rarity.index == 1)
        ret += 10;
      else if (pu.unit.rarity.index == 2)
        ret += 50;
      else
        ret += 100;
    }));
    if (ret > 100)
      ret = 100;
    return ret;
  }

  public void OnChangeUnit00486Scene()
  {
    if (this.IsPushAndSet())
      return;
    foreach (Component component in this.grid.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Unit00486Scene.changeScene(false, this.baseUnit, this.selectUnits, this.maxTrust, this.exceptionBackScene);
  }

  private IEnumerator doCombine()
  {
    Unit00484Menu unit00484Menu = this;
    Unit00484Menu.endSequenceCombine();
    if (!Singleton<CommonRoot>.GetInstance().isLoading)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      List<int> intList1 = new List<int>();
      List<int> intList2 = new List<int>();
      List<int> intList3 = new List<int>();
      for (int index = 0; index < unit00484Menu.selectUnits.Length; ++index)
      {
        if (unit00484Menu.selectUnits[index].unit.IsNormalUnit)
        {
          intList1.Add(unit00484Menu.selectUnits[index].id);
        }
        else
        {
          intList2.Add(unit00484Menu.selectUnits[index].id);
          intList3.Add(unit00484Menu.selectUnits[index].UnitIconInfo.SelectedCount);
        }
      }
      Future<WebAPI.Response.UnitCompose> paramF = WebAPI.UnitCompose(unit00484Menu.baseUnit.id, intList2.ToArray(), intList3.ToArray(), intList1.ToArray(), (System.Action<WebAPI.Response.UserError>) (error =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e = paramF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      WebAPI.Response.UnitCompose result = paramF.Result;
      if (result == null)
      {
        yield break;
      }
      else
      {
        Singleton<NGGameDataManager>.GetInstance().clearPreviewInheritance(0);
        PlayerUnit playerUnit1 = (PlayerUnit) null;
        foreach (PlayerUnit playerUnit2 in SMManager.Get<PlayerUnit[]>())
        {
          if (playerUnit2.id == unit00484Menu.baseUnit.id)
          {
            playerUnit1 = playerUnit2;
            break;
          }
        }
        List<PlayerUnit> materialList = new List<PlayerUnit>();
        materialList.AddRange((IEnumerable<PlayerUnit>) unit00484Menu.selectUnits);
        List<PlayerUnit> resultList = new List<PlayerUnit>();
        resultList.Add(unit00484Menu.baseUnit);
        resultList.Add(playerUnit1);
        List<int> otherList = new List<int>();
        otherList.Add(Convert.ToInt32(result.is_success));
        otherList.Add(result.increment_medal);
        otherList.Add(result.gain_trust_result.is_equip_awake_skill_release ? 1 : 0);
        otherList.Add(result.gain_trust_result.has_new_player_awake_skill ? 1 : 0);
        Dictionary<string, object> showPopupData = unit00484Menu.setShowPopupData(resultList, result);
        unit00484Menu.StartCoroutine(unit00484Menu.ShowCombineResult(materialList, resultList, otherList, showPopupData));
        paramF = (Future<WebAPI.Response.UnitCompose>) null;
      }
    }
    unit00484Menu.IsPush = false;
  }

  private Dictionary<string, object> setShowPopupData(
    List<PlayerUnit> resultList,
    WebAPI.Response.UnitCompose param)
  {
    Dictionary<string, object> dictionary = new Dictionary<string, object>();
    dictionary["unlockQuests"] = (object) param.unlock_quests;
    Func<List<int>, List<int>, int> func = (Func<List<int>, List<int>, int>) ((list1, list2) =>
    {
      int num1 = 0;
      foreach (int num2 in list1)
      {
        if (!list2.Contains(num2))
          num1 = num2;
      }
      return num1;
    });
    List<int> list3 = ((IEnumerable<PlayerUnitSkills>) resultList[0].GetAcquireSkills()).Select<PlayerUnitSkills, int>((Func<PlayerUnitSkills, int>) (x => x.skill_id)).ToList<int>();
    List<int> list4 = ((IEnumerable<PlayerUnitSkills>) resultList[1].GetAcquireSkills()).Select<PlayerUnitSkills, int>((Func<PlayerUnitSkills, int>) (x => x.skill_id)).ToList<int>();
    dictionary["beforeSkillId"] = (object) func(list3, list4);
    dictionary["afterSkillId"] = (object) func(list4, list3);
    return dictionary;
  }

  private void changeSceneForCombine(
    List<PlayerUnit> materialList,
    List<PlayerUnit> resultList,
    List<int> otherList,
    Dictionary<string, object> showPopupData)
  {
    unit004812Scene.changeScene(false, materialList, resultList, otherList, showPopupData, Unit00468Scene.Mode.Unit0048);
  }

  private IEnumerator ShowCombineResult(
    List<PlayerUnit> materialList,
    List<PlayerUnit> resultList,
    List<int> otherList,
    Dictionary<string, object> showPopupData)
  {
    Singleton<CommonRoot>.GetInstance().setBackground(this.bgPrefabForCombineResult);
    this.combineResultMenu.mode = Unit00468Scene.Mode.Unit0048;
    this.combineResultMenu.showPopupData = showPopupData;
    IEnumerator e = this.combineResultMenu.setCharacter(resultList[0], resultList[1], otherList);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.mainPanel00484.SetActive(false);
    this.combineResultMenu.gameObject.SetActive(true);
    Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    Singleton<CommonRoot>.GetInstance().setDisableFooterColor(true);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (this.exceptionBackScene != null)
    {
      this.exceptionBackScene();
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().startScene = "unit004_6_8";
      Singleton<CommonRoot>.GetInstance().startSceneArgs = new object[1]
      {
        (object) Unit00468Scene.Mode.Unit0048
      };
      this.backScene();
    }
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnChange()
  {
    if (this.IsPushAndSet())
      return;
    if (this.exceptionBackScene != null)
      this.exceptionBackScene();
    else
      Unit00468Scene.changeScene0048(true);
  }

  public virtual void IbtnCombine()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.IbtnCombineAsync());
  }

  private IEnumerator IbtnCombineAsync()
  {
    Unit00484Menu unit00484Menu = this;
    Unit00484Menu.beginSequenceCombine();
    if (((IEnumerable<PlayerUnit>) unit00484Menu.selectUnits).Any<PlayerUnit>((Func<PlayerUnit, bool>) (unit => unit.tower_is_entry)))
    {
      bool isRejected = false;
      yield return (object) PopupManager.StartTowerEntryUnitWarningPopupProc((System.Action<bool>) (selected => isRejected = !selected), false);
      if (isRejected)
      {
        unit00484Menu.cancelCombineAsync();
        yield break;
      }
    }
    bool isRarity = ((IEnumerable<PlayerUnit>) unit00484Menu.selectUnits).Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.rarity.index >= 2));
    bool isMemoryAlert = false;
    if (PlayerTransmigrateMemoryPlayerUnitIds.Current != null)
    {
      int?[] memoryPlayerUnitIds = PlayerTransmigrateMemoryPlayerUnitIds.Current.transmigrate_memory_player_unit_ids;
      foreach (PlayerUnit selectUnit in unit00484Menu.selectUnits)
      {
        PlayerUnit unit = selectUnit;
        if (unit != (PlayerUnit) null && !isMemoryAlert)
        {
          isMemoryAlert = ((IEnumerable<int?>) memoryPlayerUnitIds).Any<int?>((Func<int?, bool>) (x =>
          {
            if (!x.HasValue)
              return false;
            int? nullable = x;
            int id = unit.id;
            return nullable.GetValueOrDefault() == id & nullable.HasValue;
          }));
          if (isMemoryAlert)
            break;
        }
      }
    }
    bool isAlertOverkillersSlot = ((IEnumerable<PlayerUnit>) unit00484Menu.selectUnits).Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.isReleasedOverkillersSlot(0)));
    Consts consts = Consts.GetInstance();
    if (unit00484Menu.isMaterialOverUsed)
    {
      bool bCancel = false;
      bool bWait = true;
      unit00484Menu.StartCoroutine(PopupCommon.Show(consts.POPUP_00484_ALERT_TITLE, consts.POPUP_00484_MATERIAL_OVER_USE, (System.Action) (() =>
      {
        bCancel = true;
        bWait = false;
      })));
      while (bWait)
        yield return (object) null;
      if (bCancel)
      {
        unit00484Menu.cancelCombineAsync();
        yield break;
      }
    }
    if ((double) unit00484Menu.baseUnit.trust_max_rate > 0.0 && ((IEnumerable<PlayerUnit>) unit00484Menu.selectUnits).Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => (double) x.trust_rate >= (double) consts.TRUST_RATE_LEVEL_SIZE)))
    {
      bool bCancel = false;
      bool bWait = true;
      int num1 = Mathf.FloorToInt(unit00484Menu.trust / consts.TRUST_RATE_LEVEL_SIZE);
      int num2 = 0 + Mathf.FloorToInt(unit00484Menu.baseUnit.trust_rate / consts.TRUST_RATE_LEVEL_SIZE);
      for (int index = 0; index < unit00484Menu.selectUnits.Length; ++index)
      {
        if (!unit00484Menu.selectUnits[index].unit.IsTrustMaterial(unit00484Menu.baseUnit))
          num2 += Mathf.FloorToInt(unit00484Menu.selectUnits[index].trust_rate / consts.TRUST_RATE_LEVEL_SIZE);
      }
      PopupCommonNoYes.Show(Consts.GetInstance().POPUP_00484_ALERT_TITLE, Consts.Format(Consts.GetInstance().POPUP_00484_SKILL_ACQUISITION_STATUS_AFTER_COMBINATION, (IDictionary) new Hashtable()
      {
        {
          (object) "count",
          (object) (num1 - num2)
        },
        {
          (object) "remaining",
          (object) (Mathf.CeilToInt(unit00484Menu.maxTrust / consts.TRUST_RATE_LEVEL_SIZE) - Mathf.FloorToInt(unit00484Menu.trust / consts.TRUST_RATE_LEVEL_SIZE))
        }
      }), (System.Action) (() => bWait = false), (System.Action) (() =>
      {
        bCancel = true;
        bWait = false;
      }), NGUIText.Alignment.Center, (string) null, NGUIText.Alignment.Left, false);
      while (bWait)
        yield return (object) null;
      if (bCancel)
      {
        unit00484Menu.cancelCombineAsync();
        yield break;
      }
    }
    if (isRarity | isMemoryAlert | isAlertOverkillersSlot)
    {
      Consts instance = Consts.GetInstance();
      NGUIText.Alignment alignment = NGUIText.Alignment.Center;
      string messageB = (string) null;
      NGUIText.Alignment alignmentB = NGUIText.Alignment.Left;
      string message;
      if (isRarity)
      {
        message = instance.POPUP_00484_ALERT_RARITY;
        messageB = isMemoryAlert ? "\n" + instance.POPUP_00484_ALERT_MEMORY : string.Empty;
        if (isAlertOverkillersSlot)
          messageB += instance.POPUP_00484_ALERT_OVERKILLERS_SLOTS;
      }
      else
      {
        message = isMemoryAlert ? instance.POPUP_00484_ALERT_MEMORY : string.Empty;
        alignment = NGUIText.Alignment.Left;
        if (isAlertOverkillersSlot)
          message += instance.POPUP_00484_ALERT_OVERKILLERS_SLOTS;
      }
      PopupCommonNoYes.Show(instance.POPUP_00484_ALERT_TITLE, message, new System.Action(unit00484Menu.combine), new System.Action(unit00484Menu.cancelCombineAsync), alignment, messageB, alignmentB, true);
    }
    else
      yield return (object) unit00484Menu.doCombine();
  }

  private void combine()
  {
    this.StartCoroutine(this.doCombine());
  }

  private static void beginSequenceCombine()
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, false, false, false, "SE_1006");
  }

  public static void endSequenceCombine()
  {
    if (!Singleton<PopupManager>.GetInstance().isOpenNoFinish)
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  private void cancelCombineAsync()
  {
    this.IsPush = false;
    Unit00484Menu.endSequenceCombine();
  }

  public void onIbtnChangeUpperParameter()
  {
    foreach (Component component1 in this.grid.transform)
    {
      SozaiItem component2 = component1.GetComponent<SozaiItem>();
      if (!((UnityEngine.Object) component2.UnitIcon == (UnityEngine.Object) null))
      {
        component2.UnitIcon.Dir_select_check.SetActive(component2.DirSozaiBase.activeSelf);
        component2.DirSozaiBase.SetActive(!component2.DirSozaiBase.activeSelf);
      }
    }
  }

  public static bool checkAnyInheritance(PlayerUnit target)
  {
    return target.dexterity.inheritance != 0 || target.agility.inheritance != 0 || (target.mind.inheritance != 0 || target.strength.inheritance != 0) || (target.vitality.inheritance != 0 || target.hp.inheritance != 0 || target.intelligence.inheritance != 0) || (uint) target.lucky.inheritance > 0U;
  }

  public void onClickedUnityValue()
  {
    System.Action popupUnityDetail = this.popupUnityDetail;
    if (popupUnityDetail == null)
      return;
    popupUnityDetail();
  }

  public IEnumerator LoadBgForCombineResult()
  {
    Future<GameObject> bgF = Res.Prefabs.BackGround.UnitBackground_60.Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.bgPrefabForCombineResult = bgF.Result;
  }
}
