﻿// Decompiled with JetBrains decompiler
// Type: Tower029ManualScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Tower029ManualScene : NGSceneBase
{
  [SerializeField]
  private Tower029ManualMenu menu;

  public static void ChangeScene(bool isStack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("tower029_manual", isStack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Tower029ManualScene tower029ManualScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    IEnumerator e = tower029ManualScene.menu.InitializeAsync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    tower029ManualScene.bgmFile = TowerUtil.BgmFile;
    tower029ManualScene.bgmName = TowerUtil.BgmName;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onEndScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    DetailController.Release();
    Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
  }
}
