﻿// Decompiled with JetBrains decompiler
// Type: DailyMissionController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class DailyMissionController : MonoBehaviour
{
  private GameObject windowPrefab;
  public GameObject panelPrefab;
  public GameObject missionPointRewardDetailPopupPrefab;
  public GameObject missionPointRewardDetailItemPrefab;
  public GameObject weeklyMissionPointRewardPopupPrefab;
  public GameObject getPointRewardEffectPopupPrefab;
  public GameObject challengeAgainConfirmationPopupPrefab;
  private DailyMissionWindow currentMissionWindow;

  private void Start()
  {
    this.StartCoroutine(this.LoadResource());
  }

  private IEnumerator LoadResource()
  {
    Future<GameObject> window = Res.Prefabs.dailymission027_2.Daily_Mission.Load<GameObject>();
    IEnumerator e = window.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.windowPrefab = window.Result;
    Future<GameObject> panel = Res.Prefabs.dailymission027_2.dir_Daily_Mission.Load<GameObject>();
    e = panel.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.panelPrefab = panel.Result;
    Future<GameObject> missionPointRewardPopupF = Res.Prefabs.dailymission027_2.MissionPointRewardPopup.Load<GameObject>();
    e = missionPointRewardPopupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.missionPointRewardDetailPopupPrefab = missionPointRewardPopupF.Result;
    this.missionPointRewardDetailPopupPrefab.GetComponent<UIWidget>().alpha = 0.0f;
    Future<GameObject> pointRewardDetailItemF = Res.Prefabs.dailymission027_2.MissionPointRewardDetailItem.Load<GameObject>();
    e = pointRewardDetailItemF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.missionPointRewardDetailItemPrefab = pointRewardDetailItemF.Result;
    Future<GameObject> weeklyF = Res.Prefabs.dailymission027_2.WeeklyMissionPointRewardPopup.Load<GameObject>();
    e = weeklyF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.weeklyMissionPointRewardPopupPrefab = weeklyF.Result;
    Future<GameObject> getPointRewardEffectF = Res.Prefabs.dailymission027_2.GetPointRewardEffectPopup.Load<GameObject>();
    e = getPointRewardEffectF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.getPointRewardEffectPopupPrefab = getPointRewardEffectF.Result;
    Future<GameObject> challengeAgainConfirmationPopupF = Res.Prefabs.dailymission027_2.ChallengeAgainConfirmationPopup.Load<GameObject>();
    e = challengeAgainConfirmationPopupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.challengeAgainConfirmationPopupPrefab = challengeAgainConfirmationPopupF.Result;
  }

  public void Show()
  {
    if ((Object) this.currentMissionWindow != (Object) null)
      return;
    this.StartCoroutine(this.ShowWindow());
  }

  public void Hide()
  {
    if ((Object) this.currentMissionWindow == (Object) null)
      return;
    Object.Destroy((Object) this.currentMissionWindow.gameObject);
    this.currentMissionWindow = (DailyMissionWindow) null;
  }

  public bool IsOpened
  {
    get
    {
      return (Object) this.currentMissionWindow != (Object) null;
    }
  }

  private IEnumerator ShowWindow()
  {
    DailyMissionController controller = this;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
    while ((Object) controller.windowPrefab == (Object) null || (Object) controller.panelPrefab == (Object) null)
      yield return (object) null;
    GameObject gameObject = controller.windowPrefab.Clone(controller.transform);
    controller.currentMissionWindow = gameObject.GetComponent<DailyMissionWindow>();
    IEnumerator e = controller.currentMissionWindow.Init(controller, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    while ((Object) controller.missionPointRewardDetailPopupPrefab == (Object) null || (Object) controller.missionPointRewardDetailItemPrefab == (Object) null || ((Object) controller.weeklyMissionPointRewardPopupPrefab == (Object) null || (Object) controller.getPointRewardEffectPopupPrefab == (Object) null) || (Object) controller.challengeAgainConfirmationPopupPrefab == (Object) null)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
  }
}
