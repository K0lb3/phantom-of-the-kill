﻿// Decompiled with JetBrains decompiler
// Type: PopupGearAttachedElementDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class PopupGearAttachedElementDetail : PopupAutoCloseOnAnyTap
{
  [SerializeField]
  [Tooltip("属性アイコン")]
  private UI2DSprite iconElement_;
  [SerializeField]
  [Tooltip("属性名")]
  private UILabel txtName_;
  [SerializeField]
  [Tooltip("説明文")]
  private UILabel txtDescription_;
  private BattleskillSkill skill_;

  public static Future<GameObject> createPrefabLoader()
  {
    return new ResourceObject("Prefabs/unit004_4_3/popup_ElementClass_detail").Load<GameObject>();
  }

  public static IEnumerator show(GameObject prefab, GearGear gear)
  {
    if (gear != null)
    {
      Singleton<PopupManager>.GetInstance().open(prefab, false, false, false, false, false, false, "SE_1006").GetComponent<PopupGearAttachedElementDetail>().initialize(gear);
      while (Singleton<PopupManager>.GetInstance().isOpen)
        yield return (object) null;
    }
  }

  private void initialize(GearGear gear)
  {
    this.setEventOnAnyTap((Collider) null);
    this.skill_ = gear.attachedElementSkill;
    this.txtName_.SetTextLocalize(this.skill_.name);
    this.txtDescription_.SetTextLocalize(this.skill_.description);
  }

  private IEnumerator Start()
  {
    Future<GameObject> loader = Res.Icons.CommonElementIcon.Load<GameObject>();
    yield return (object) loader.Wait();
    GameObject result = loader.Result;
    loader = (Future<GameObject>) null;
    this.iconElement_.sprite2D = result.GetComponent<CommonElementIcon>().getIcon(this.skill_.element);
  }
}
