﻿// Decompiled with JetBrains decompiler
// Type: Shop00720RewardPatternData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class Shop00720RewardPatternData
{
  private List<UnityEngine.Sprite> reelPattern = new List<UnityEngine.Sprite>();
  private List<Shop00720RewardData> rewardList = new List<Shop00720RewardData>();
  private bool isEnabled;
  private string description;

  public bool IsEnabled
  {
    get
    {
      return this.isEnabled;
    }
    set
    {
      this.isEnabled = value;
    }
  }

  public List<UnityEngine.Sprite> ReelPattern
  {
    get
    {
      return this.reelPattern;
    }
    set
    {
      this.reelPattern = value;
    }
  }

  public List<Shop00720RewardData> RewardList
  {
    get
    {
      return this.rewardList;
    }
    set
    {
      this.rewardList = value;
    }
  }

  public string Description
  {
    get
    {
      return this.description;
    }
    set
    {
      this.description = value;
    }
  }

  public Shop00720RewardPatternData(int[] reelIds)
  {
    this.isEnabled = ((IEnumerable<int>) reelIds).All<int>((Func<int, bool>) (a => (uint) a > 0U));
  }

  public IEnumerator SetSprite(int[] ids)
  {
    int[] numArray = ids;
    for (int index = 0; index < numArray.Length; ++index)
    {
      Future<UnityEngine.Sprite> spriteF = this.LoadSpriteThumbnail(numArray[index]);
      IEnumerator e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.reelPattern.Add(spriteF.Result);
      spriteF = (Future<UnityEngine.Sprite>) null;
    }
    numArray = (int[]) null;
  }

  private Future<UnityEngine.Sprite> LoadSpriteThumbnail(int id)
  {
    return Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("AssetBundle/Resources/Animations/slot/Texture/lilleImages/slot_icon_{0:D2}f", (object) id), 1f);
  }
}
