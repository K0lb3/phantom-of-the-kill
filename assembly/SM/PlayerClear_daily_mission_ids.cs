﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerClear_daily_mission_ids
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;

namespace SM
{
  [Serializable]
  public class PlayerClear_daily_mission_ids : KeyCompare
  {
    public string date;
    public int?[] mission_ids;

    public PlayerClear_daily_mission_ids()
    {
    }

    public PlayerClear_daily_mission_ids(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.date = (string) json[nameof (date)];
      this.mission_ids = ((IEnumerable<object>) json[nameof (mission_ids)]).Select<object, int?>((Func<object, int?>) (s =>
      {
        long? nullable = (long?) s;
        return !nullable.HasValue ? new int?() : new int?((int) nullable.GetValueOrDefault());
      })).ToArray<int?>();
    }
  }
}
