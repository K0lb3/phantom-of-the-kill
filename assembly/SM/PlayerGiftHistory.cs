﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGiftHistory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGiftHistory : KeyCompare
  {
    public int id;
    public int passed_days;
    public GiftBase gift;
    public int coin_id;
    public int gift_id;

    public PlayerGiftHistory()
    {
    }

    public PlayerGiftHistory(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.id = (int) (long) json[nameof (id)];
      this.passed_days = (int) (long) json[nameof (passed_days)];
      this.gift = json[nameof (gift)] == null ? (GiftBase) null : new GiftBase((Dictionary<string, object>) json[nameof (gift)]);
      this.coin_id = (int) (long) json[nameof (coin_id)];
      this.gift_id = (int) (long) json[nameof (gift_id)];
    }
  }
}
