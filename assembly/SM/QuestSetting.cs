﻿// Decompiled with JetBrains decompiler
// Type: SM.QuestSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class QuestSetting : KeyCompare
  {
    public int quest_m_id;
    public int quest_type_id;
    public int quest_s_id;

    public QuestSetting()
    {
    }

    public QuestSetting(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.quest_m_id = (int) (long) json[nameof (quest_m_id)];
      this.quest_type_id = (int) (long) json[nameof (quest_type_id)];
      this.quest_s_id = (int) (long) json[nameof (quest_s_id)];
    }
  }
}
