﻿// Decompiled with JetBrains decompiler
// Type: SM.SelectTicket
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class SelectTicket : KeyCompare
  {
    public DateTime start_at;
    public string description;
    public string name;
    public string detail;
    public DateTime end_at;
    public bool unit_type_selectable;
    public int cost;
    public bool exchange_limit;
    public int category_id;
    public int id;
    public SelectTicketChoices[] choices;

    public SelectTicket()
    {
    }

    public SelectTicket(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.start_at = DateTime.Parse((string) json[nameof (start_at)]);
      this.description = (string) json[nameof (description)];
      this.name = (string) json[nameof (name)];
      this.detail = (string) json[nameof (detail)];
      this.end_at = DateTime.Parse((string) json[nameof (end_at)]);
      this.unit_type_selectable = (bool) json[nameof (unit_type_selectable)];
      this.cost = (int) (long) json[nameof (cost)];
      this.exchange_limit = (bool) json[nameof (exchange_limit)];
      this.category_id = (int) (long) json[nameof (category_id)];
      this.id = (int) (long) json[nameof (id)];
      List<SelectTicketChoices> selectTicketChoicesList = new List<SelectTicketChoices>();
      foreach (object obj in (List<object>) json[nameof (choices)])
        selectTicketChoicesList.Add(obj == null ? (SelectTicketChoices) null : new SelectTicketChoices((Dictionary<string, object>) obj));
      this.choices = selectTicketChoicesList.ToArray();
    }
  }
}
