﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;

namespace SM
{
  [Serializable]
  public class PlayerItem : KeyCompare
  {
    public bool isEarthMode;
    private long? _playerUnitRevision;
    private bool _unitEquipped;
    private const string nameFormat = "{0}+{1}";
    private GearRankIncr _gearRankCache;
    private ReisouRankIncr _reisouRankCache;
    public const int DEFAULT_LIMIT = 5;
    public int gear_accessory_remaining_amount;
    public int entity_id;
    public PlayerGearBuildupParam gear_buildup_param;
    public bool for_battle;
    public int box_type_id;
    public int _entity_type;
    public int equipped_reisou_player_gear_id;
    public bool favorite;
    public int gear_exp_next;
    public bool is_new;
    public bool broken;
    public string player_id;
    public int gear_level_unlimit;
    public int gear_level;
    public int gear_level_limit_max;
    public int gear_total_exp;
    public int gear_exp;
    public int id;
    public int gear_level_limit;
    public int quantity;

    public PlayerItem(GearGear gear, MasterDataTable.CommonRewardType entity_type)
    {
      this._hasKey = false;
      this.gear_accessory_remaining_amount = 0;
      this.entity_id = gear.ID;
      this.gear_buildup_param = (PlayerGearBuildupParam) null;
      this.for_battle = false;
      this.box_type_id = 0;
      this._entity_type = (int) entity_type;
      this.equipped_reisou_player_gear_id = 0;
      this.favorite = false;
      this.gear_exp_next = 0;
      this.is_new = false;
      this.broken = false;
      this.player_id = "";
      this.gear_level_unlimit = 0;
      this.gear_level = 1;
      this.gear_level_limit_max = 1;
      this.gear_total_exp = 0;
      this.gear_exp = 0;
      this._key = (object) (this.id = 0);
      this.gear_level_limit = 0;
      this.quantity = 1;
    }

    public override bool Equals(object rhs)
    {
      return this.Equals(rhs as PlayerItem);
    }

    public override int GetHashCode()
    {
      return 0;
    }

    public bool Equals(PlayerItem rhs)
    {
      if ((object) rhs == null)
        return false;
      if ((object) this == (object) rhs)
        return true;
      return !(this.GetType() != rhs.GetType()) && this.id == rhs.id && this.entity_type == rhs.entity_type && this.player_id == rhs.player_id;
    }

    public static bool operator ==(PlayerItem lhs, PlayerItem rhs)
    {
      return (object) lhs == null ? (object) rhs == null : lhs.Equals(rhs);
    }

    public static bool operator !=(PlayerItem lhs, PlayerItem rhs)
    {
      return !(lhs == rhs);
    }

    private bool UnitEquipped
    {
      get
      {
        long num1 = SMManager.Revision<PlayerUnit[]>();
        if (this._playerUnitRevision.HasValue)
        {
          long? playerUnitRevision = this._playerUnitRevision;
          long num2 = num1;
          if (playerUnitRevision.GetValueOrDefault() == num2 & playerUnitRevision.HasValue)
            goto label_3;
        }
        this._playerUnitRevision = new long?(num1);
        PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
        this._unitEquipped = playerUnitArray != null && ((IEnumerable<PlayerUnit>) playerUnitArray).Any<PlayerUnit>((Func<PlayerUnit, bool>) (x => ((IEnumerable<int?>) x.equip_gear_ids).Contains<int?>(new int?(this.id))));
label_3:
        return this._unitEquipped;
      }
    }

    public bool ForBattle
    {
      get
      {
        if (this.entity_type == MasterDataTable.CommonRewardType.gear)
          return this.UnitEquipped;
        return this.entity_type == MasterDataTable.CommonRewardType.supply && this.box_type_id == 2;
      }
    }

    public bool ForTower
    {
      get
      {
        if (this.entity_type == MasterDataTable.CommonRewardType.gear)
          return this.UnitEquipped;
        return this.entity_type == MasterDataTable.CommonRewardType.supply && this.box_type_id == 3;
      }
    }

    public string name
    {
      get
      {
        if (this.gear == null)
          return this.supply.name;
        if (this.gear_level_unlimit <= 0)
          return this.gear.name;
        return "{0}+{1}".F((object) this.gear.name, (object) this.gear_level_unlimit);
      }
    }

    public MasterDataTable.CommonRewardType entity_type
    {
      get
      {
        return (MasterDataTable.CommonRewardType) this._entity_type;
      }
    }

    public GearGear gear
    {
      get
      {
        return this.entity_type == MasterDataTable.CommonRewardType.gear ? MasterData.GearGear[this.entity_id] : (GearGear) null;
      }
    }

    public SupplySupply supply
    {
      get
      {
        return this.entity_type == MasterDataTable.CommonRewardType.supply ? MasterData.SupplySupply[this.entity_id] : (SupplySupply) null;
      }
    }

    public PlayerItem equipReisou
    {
      get
      {
        return ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).FirstOrDefault<PlayerItem>((Func<PlayerItem, bool>) (x => x.id == this.equipped_reisou_player_gear_id));
      }
    }

    public bool isReisouSet
    {
      get
      {
        return (uint) this.equipped_reisou_player_gear_id > 0U;
      }
    }

    public GearRankIncr gearRankIncr
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return (GearRankIncr) null;
        if (this._gearRankCache == null || this._gearRankCache != null && this._gearRankCache.gear_kind != this.gear.kind && (this._gearRankCache.group_id != this.gear.rank_incr_group && this._gearRankCache.level != this.gear_level))
          this._gearRankCache = GearRankIncr.FromRank(this.gear.kind, this.gear.rank_incr_group, this.gear_level);
        return this._gearRankCache;
      }
    }

    public ReisouRankIncr reisouRankIncr
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return (ReisouRankIncr) null;
        if (this._reisouRankCache == null || this._reisouRankCache != null && this._reisouRankCache.gear_kind != this.gear.kind && (this._reisouRankCache.group_id != this.gear.rank_incr_group && this._reisouRankCache.level != this.gear_level))
          this._reisouRankCache = ReisouRankIncr.FromRank(this.gear.kind, this.gear.rank_incr_group, this.gear_level);
        return this._reisouRankCache;
      }
    }

    public int power
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.power + this.reisouRankIncr.power : this.gear.power + this.gearRankIncr.power;
      }
    }

    public int physical_defense
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.physical_defense + this.reisouRankIncr.physical_defense : this.gear.physical_defense + this.gearRankIncr.physical_defense;
      }
    }

    public int magic_defense
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.magic_defense + this.reisouRankIncr.magic_defense : this.gear.magic_defense + this.gearRankIncr.magic_defense;
      }
    }

    public int hit
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.hit + this.reisouRankIncr.hit : this.gear.hit + this.gearRankIncr.hit;
      }
    }

    public int critical
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.critical + this.reisouRankIncr.critical : this.gear.critical + this.gearRankIncr.critical;
      }
    }

    public int evasion
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.evasion + this.reisouRankIncr.evasion : this.gear.evasion + this.gearRankIncr.evasion;
      }
    }

    public int hp_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.hp_incremental + this.reisouRankIncr.hp_incremental : this.gear.hp_incremental + this.gear_buildup_param.hp_add + this.gearRankIncr.hp_incremental;
      }
    }

    public int strength_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.strength_incremental + this.reisouRankIncr.strength_incremental : this.gear.strength_incremental + this.gear_buildup_param.strength_add + this.gearRankIncr.strength_incremental;
      }
    }

    public int vitality_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.vitality_incremental + this.reisouRankIncr.vitality_incremental : this.gear.vitality_incremental + this.gear_buildup_param.vitality_add + this.gearRankIncr.vitality_incremental;
      }
    }

    public int intelligence_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.intelligence_incremental + this.reisouRankIncr.intelligence_incremental : this.gear.intelligence_incremental + this.gear_buildup_param.intelligence_add + this.gearRankIncr.intelligence_incremental;
      }
    }

    public int mind_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.mind_incremental + this.reisouRankIncr.mind_incremental : this.gear.mind_incremental + this.gear_buildup_param.mind_add + this.gearRankIncr.mind_incremental;
      }
    }

    public int agility_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.agility_incremental + this.reisouRankIncr.agility_incremental : this.gear.agility_incremental + this.gear_buildup_param.agility_add + this.gearRankIncr.agility_incremental;
      }
    }

    public int dexterity_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.dexterity_incremental + this.reisouRankIncr.dexterity_incremental : this.gear.dexterity_incremental + this.gear_buildup_param.dexterity_add + this.gearRankIncr.dexterity_incremental;
      }
    }

    public int lucky_incremental
    {
      get
      {
        if (this.entity_type != MasterDataTable.CommonRewardType.gear)
          return 0;
        return this.isReisou() ? this.gear.lucky_incremental + this.reisouRankIncr.lucky_incremental : this.gear.lucky_incremental + this.gear_buildup_param.lucky_add + this.gearRankIncr.lucky_incremental;
      }
    }

    public GearGearSkill[] skills
    {
      get
      {
        List<GearGearSkill> source1 = new List<GearGearSkill>();
        if (this.entity_type == MasterDataTable.CommonRewardType.gear)
        {
          List<GearGearSkill> list = ((IEnumerable<GearGearSkill>) MasterData.GearGearSkillList).Where<GearGearSkill>((Func<GearGearSkill, bool>) (x => x.gear.ID == this.entity_id && x.isReleased(this))).ToList<GearGearSkill>();
          if (list.Count > 0)
          {
            foreach (IGrouping<int, GearGearSkill> source2 in list.GroupBy<GearGearSkill, int>((Func<GearGearSkill, int>) (x => x.skill_group)))
              source1.Add(source2.OrderByDescending<GearGearSkill, int>((Func<GearGearSkill, int>) (x => x.release_rank)).First<GearGearSkill>());
          }
        }
        return source1.OrderBy<GearGearSkill, int>((Func<GearGearSkill, int>) (x => x.skill_group)).ToArray<GearGearSkill>();
      }
    }

    public static PlayerItem CreateForKey(int id)
    {
      PlayerItem playerItem = new PlayerItem();
      playerItem._hasKey = true;
      int num1;
      int num2 = num1 = id;
      playerItem.id = num1;
      playerItem._key = (object) num2;
      return playerItem;
    }

    public bool isWeapon()
    {
      return !this.isSupply() && !this.isCompse() && (!this.isExchangable() && !this.isReisou());
    }

    public bool isComposeManaSeed()
    {
      bool flag = false;
      if (!this.isSupply() && this.gear.isComposeManaSeed())
        flag = true;
      return flag;
    }

    public bool isSupply()
    {
      return this.entity_type == MasterDataTable.CommonRewardType.supply;
    }

    public bool isCompse()
    {
      return !this.isSupply() && this.gear.kind.Enum == GearKindEnum.smith && this.gear.compose_kind.kind.Enum != GearKindEnum.smith;
    }

    public bool isExchangable()
    {
      if (this.gear == null)
        return false;
      GearGear gear = this.gear;
      return gear.kind.Enum == GearKindEnum.smith && gear.compose_kind.kind.Enum == GearKindEnum.smith;
    }

    public bool isReisou()
    {
      return this.gear != null && this.gear.isReisou();
    }

    public CommonElement GetElement()
    {
      CommonElement commonElement = CommonElement.none;
      IEnumerable<GearGearSkill> source = ((IEnumerable<GearGearSkill>) this.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (x => ((IEnumerable<BattleskillEffect>) x.skill.Effects).Any<BattleskillEffect>((Func<BattleskillEffect, bool>) (ef => ef.effect_logic.Enum == BattleskillEffectLogicEnum.invest_element))));
      if (source.Any<GearGearSkill>())
        commonElement = source.First<GearGearSkill>().skill.element;
      return commonElement;
    }

    public PlayerItem Clone()
    {
      return (PlayerItem) this.MemberwiseClone();
    }

    public bool isLimitMax()
    {
      return this.gear_level_limit_max <= this.gear_level_limit;
    }

    public BL.Item toBLItem()
    {
      return new BL.Item()
      {
        playerItemId = this.id,
        itemId = this.supply.ID,
        amount = this.quantity,
        initialAmount = this.quantity
      };
    }

    public PlayerItem()
    {
    }

    public PlayerItem(Dictionary<string, object> json)
    {
      this._hasKey = true;
      this.gear_accessory_remaining_amount = (int) (long) json[nameof (gear_accessory_remaining_amount)];
      this.entity_id = (int) (long) json[nameof (entity_id)];
      this.gear_buildup_param = json[nameof (gear_buildup_param)] == null ? (PlayerGearBuildupParam) null : new PlayerGearBuildupParam((Dictionary<string, object>) json[nameof (gear_buildup_param)]);
      this.for_battle = (bool) json[nameof (for_battle)];
      this.box_type_id = (int) (long) json[nameof (box_type_id)];
      this._entity_type = (int) (long) json[nameof (_entity_type)];
      this.equipped_reisou_player_gear_id = (int) (long) json[nameof (equipped_reisou_player_gear_id)];
      this.favorite = (bool) json[nameof (favorite)];
      this.gear_exp_next = (int) (long) json[nameof (gear_exp_next)];
      this.is_new = (bool) json[nameof (is_new)];
      this.broken = (bool) json[nameof (broken)];
      this.player_id = (string) json[nameof (player_id)];
      this.gear_level_unlimit = (int) (long) json[nameof (gear_level_unlimit)];
      this.gear_level = (int) (long) json[nameof (gear_level)];
      this.gear_level_limit_max = (int) (long) json[nameof (gear_level_limit_max)];
      this.gear_total_exp = (int) (long) json[nameof (gear_total_exp)];
      this.gear_exp = (int) (long) json[nameof (gear_exp)];
      this._key = (object) (this.id = (int) (long) json[nameof (id)]);
      this.gear_level_limit = (int) (long) json[nameof (gear_level_limit)];
      this.quantity = (int) (long) json[nameof (quantity)];
    }
  }
}
