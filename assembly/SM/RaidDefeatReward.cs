﻿// Decompiled with JetBrains decompiler
// Type: SM.RaidDefeatReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class RaidDefeatReward : KeyCompare
  {
    public int reward_quantity;
    public string reward_message;
    public string reward_title;
    public int reward_id;
    public int reward_type_id;

    public RaidDefeatReward()
    {
    }

    public RaidDefeatReward(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.reward_quantity = (int) (long) json[nameof (reward_quantity)];
      this.reward_message = (string) json[nameof (reward_message)];
      this.reward_title = (string) json[nameof (reward_title)];
      this.reward_id = (int) (long) json[nameof (reward_id)];
      this.reward_type_id = (int) (long) json[nameof (reward_type_id)];
    }
  }
}
