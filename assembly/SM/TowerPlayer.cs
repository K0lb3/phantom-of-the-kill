﻿// Decompiled with JetBrains decompiler
// Type: SM.TowerPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class TowerPlayer : KeyCompare
  {
    public int tower_medal;

    public TowerPlayer()
    {
    }

    public TowerPlayer(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.tower_medal = (int) (long) json[nameof (tower_medal)];
    }
  }
}
