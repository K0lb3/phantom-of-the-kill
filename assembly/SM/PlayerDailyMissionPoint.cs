﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerDailyMissionPoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerDailyMissionPoint : KeyCompare
  {
    public PlayerDailyMissionPointDaily daily;
    public PlayerDailyMissionPointWeekly weekly;

    public PlayerDailyMissionPoint()
    {
    }

    public PlayerDailyMissionPoint(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.daily = json[nameof (daily)] == null ? (PlayerDailyMissionPointDaily) null : new PlayerDailyMissionPointDaily((Dictionary<string, object>) json[nameof (daily)]);
      this.weekly = json[nameof (weekly)] == null ? (PlayerDailyMissionPointWeekly) null : new PlayerDailyMissionPointWeekly((Dictionary<string, object>) json[nameof (weekly)]);
    }
  }
}
