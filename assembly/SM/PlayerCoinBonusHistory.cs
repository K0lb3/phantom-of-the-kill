﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerCoinBonusHistory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerCoinBonusHistory : KeyCompare
  {
    public int coinbonus_id;
    public int coinbonus_reward_id;
    public int id;

    public PlayerCoinBonusHistory()
    {
    }

    public PlayerCoinBonusHistory(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.coinbonus_id = (int) (long) json[nameof (coinbonus_id)];
      this.coinbonus_reward_id = (int) (long) json[nameof (coinbonus_reward_id)];
      this.id = (int) (long) json[nameof (id)];
    }
  }
}
