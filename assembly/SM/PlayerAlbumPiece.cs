﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerAlbumPiece
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerAlbumPiece : KeyCompare
  {
    public int count;
    public bool is_open;
    public int id;
    public int piece_id;
    public int album_id;

    public PlayerAlbumPiece()
    {
    }

    public PlayerAlbumPiece(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.count = (int) (long) json[nameof (count)];
      this.is_open = (bool) json[nameof (is_open)];
      this.id = (int) (long) json[nameof (id)];
      this.piece_id = (int) (long) json[nameof (piece_id)];
      this.album_id = (int) (long) json[nameof (album_id)];
    }
  }
}
