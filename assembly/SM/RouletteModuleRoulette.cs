﻿// Decompiled with JetBrains decompiler
// Type: SM.RouletteModuleRoulette
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class RouletteModuleRoulette : KeyCompare
  {
    public bool is_campaign;
    public int deck_id;
    public bool can_roulette;
    public int id;

    public RouletteModuleRoulette()
    {
    }

    public RouletteModuleRoulette(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.is_campaign = (bool) json[nameof (is_campaign)];
      this.deck_id = (int) (long) json[nameof (deck_id)];
      this.can_roulette = (bool) json[nameof (can_roulette)];
      this.id = (int) (long) json[nameof (id)];
    }
  }
}
