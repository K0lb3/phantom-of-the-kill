﻿// Decompiled with JetBrains decompiler
// Type: SM.GuildInvestScale
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class GuildInvestScale : KeyCompare
  {
    public int release_level;
    public int scale;

    public GuildInvestScale()
    {
    }

    public GuildInvestScale(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.release_level = (int) (long) json[nameof (release_level)];
      this.scale = (int) (long) json[nameof (scale)];
    }
  }
}
