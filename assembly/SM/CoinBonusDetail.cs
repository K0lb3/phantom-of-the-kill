﻿// Decompiled with JetBrains decompiler
// Type: SM.CoinBonusDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class CoinBonusDetail : KeyCompare
  {
    public string body;
    public int image_height;
    public string image_url;
    public int image_width;

    public CoinBonusDetail()
    {
    }

    public CoinBonusDetail(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.body = (string) json[nameof (body)];
      this.image_height = (int) (long) json[nameof (image_height)];
      this.image_url = (string) json[nameof (image_url)];
      this.image_width = (int) (long) json[nameof (image_width)];
    }
  }
}
