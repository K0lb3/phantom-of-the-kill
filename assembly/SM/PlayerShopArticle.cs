﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerShopArticle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerShopArticle : KeyCompare
  {
    public int _article;
    public DateTime? recharge_at;
    public int? limit;
    public string banner_url;
    public DateTime? end_at;

    public ShopArticle article
    {
      get
      {
        if (MasterData.ShopArticle.ContainsKey(this._article))
          return MasterData.ShopArticle[this._article];
        Debug.LogError((object) ("Key not Found: MasterData.ShopArticle[" + (object) this._article + "]"));
        return (ShopArticle) null;
      }
    }

    public PlayerShopArticle()
    {
    }

    public PlayerShopArticle(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this._article = (int) (long) json[nameof (article)];
      this.recharge_at = json[nameof (recharge_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (recharge_at)]));
      int? nullable1;
      if (json[nameof (limit)] != null)
      {
        long? nullable2 = (long?) json[nameof (limit)];
        nullable1 = nullable2.HasValue ? new int?((int) nullable2.GetValueOrDefault()) : new int?();
      }
      else
        nullable1 = new int?();
      this.limit = nullable1;
      this.banner_url = (string) json[nameof (banner_url)];
      this.end_at = json[nameof (end_at)] == null ? new DateTime?() : new DateTime?(DateTime.Parse((string) json[nameof (end_at)]));
    }
  }
}
