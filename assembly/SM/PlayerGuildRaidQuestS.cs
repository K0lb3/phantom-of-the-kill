﻿// Decompiled with JetBrains decompiler
// Type: SM.PlayerGuildRaidQuestS
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class PlayerGuildRaidQuestS : KeyCompare
  {
    public int loop_count;
    public int challenge_count;
    public int _quest_guildraid_s;

    public GuildRaid quest_guildraid_s
    {
      get
      {
        if (MasterData.GuildRaid.ContainsKey(this._quest_guildraid_s))
          return MasterData.GuildRaid[this._quest_guildraid_s];
        Debug.LogError((object) ("Key not Found: MasterData.GuildRaid[" + (object) this._quest_guildraid_s + "]"));
        return (GuildRaid) null;
      }
    }

    public PlayerGuildRaidQuestS()
    {
    }

    public PlayerGuildRaidQuestS(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.loop_count = (int) (long) json[nameof (loop_count)];
      this.challenge_count = (int) (long) json[nameof (challenge_count)];
      this._quest_guildraid_s = (int) (long) json[nameof (quest_guildraid_s)];
    }
  }
}
