﻿// Decompiled with JetBrains decompiler
// Type: SM.FixedEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace SM
{
  [Serializable]
  public class FixedEntity : KeyCompare
  {
    public int single_fix_count;
    public string fix_reward;
    public int gacha_id;

    public FixedEntity()
    {
    }

    public FixedEntity(Dictionary<string, object> json)
    {
      this._hasKey = false;
      this.single_fix_count = (int) (long) json[nameof (single_fix_count)];
      this.fix_reward = (string) json[nameof (fix_reward)];
      this.gacha_id = (int) (long) json[nameof (gacha_id)];
    }
  }
}
