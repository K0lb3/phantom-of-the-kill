﻿// Decompiled with JetBrains decompiler
// Type: Story009132ScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class Story009132ScrollParts : MonoBehaviour
{
  [SerializeField]
  private UISprite IbtnChapterSprite;
  [SerializeField]
  private UIButton IbtnChapter;
  [SerializeField]
  private UISprite SlcNew;
  [SerializeField]
  private UISprite SlcClear;
  [SerializeField]
  private UILabel TxtChapter;
  private StoryPlaybackSeaDetail story;
  private NGMenuBase menu;

  public void onClickEpisodeButton()
  {
    if (this.menu.IsPushAndSet())
      return;
    Story0093Scene.changeScene(true, this.story.script_id, new bool?(true), (System.Action) null);
  }

  public void Init(Story009132Menu menu, StoryPlaybackSeaDetail story)
  {
    this.menu = (NGMenuBase) menu;
    this.story = story;
    EventDelegate.Add(this.IbtnChapter.onClick, new EventDelegate.Callback(this.onClickEpisodeButton));
    this.SlcNew.gameObject.SetActive(false);
    this.SlcClear.gameObject.SetActive(false);
    this.TxtChapter.SetTextLocalize(story.name);
  }
}
