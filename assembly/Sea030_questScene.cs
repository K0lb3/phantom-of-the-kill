﻿// Decompiled with JetBrains decompiler
// Type: Sea030_questScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sea030_questScene : NGSceneBase
{
  protected static Vector3 scrollPos = new Vector3(0.0f, 0.0f, 0.0f);
  [SerializeField]
  private Sea030_questMenu menu;
  [SerializeField]
  private UIScrollView scrollView;

  public static void ChangeScene(bool isStack, bool scrollPosInitialize = true, bool forceInitialize = false)
  {
    if (scrollPosInitialize)
      Sea030_questScene.scrollPos.Set(0.0f, 0.0f, 0.0f);
    if (SMManager.Get<PlayerSeaQuestS[]>() == null)
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    Singleton<NGGameDataManager>.GetInstance().IsSea = true;
    Singleton<NGSceneManager>.GetInstance().changeScene("sea030_quest", (isStack ? 1 : 0) != 0, (object) forceInitialize);
  }

  public IEnumerator onStartSceneAsync(bool forceInitialize = false)
  {
    Sea030_questScene sea030QuestScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeaHomeMap seaHomeMap = ((IEnumerable<SeaHomeMap>) MasterData.SeaHomeMapList).ActiveSeaHomeMap(ServerTime.NowAppTimeAddDelta());
    if (seaHomeMap != null && !string.IsNullOrEmpty(seaHomeMap.bgm_cuesheet_name) && !string.IsNullOrEmpty(seaHomeMap.bgm_cue_name))
    {
      sea030QuestScene.bgmFile = seaHomeMap.bgm_cuesheet_name;
      sea030QuestScene.bgmName = seaHomeMap.bgm_cue_name;
    }
    e = new Future<NGGameDataManager.StartSceneProxyResult>(new Func<Promise<NGGameDataManager.StartSceneProxyResult>, IEnumerator>(Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxyImpl)).Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerSeaQuestS[] StoryData = SMManager.Get<PlayerSeaQuestS[]>();
    sea030QuestScene.tweens = (UITweener[]) null;
    e = sea030QuestScene.menu.Init(StoryData, forceInitialize);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    sea030QuestScene.scrollView.mLocalPosition = Sea030_questScene.scrollPos;
  }

  public void onStartScene(bool forceInitialize = false)
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override IEnumerator onEndSceneAsync()
  {
    Sea030_questScene sea030QuestScene = this;
    Sea030_questScene.scrollPos = sea030QuestScene.scrollView.mLocalPosition;
    float startTime = Time.time;
    while (!sea030QuestScene.isTweenFinished && (double) Time.time - (double) startTime < (double) sea030QuestScene.tweenTimeoutTime)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    sea030QuestScene.isTweenFinished = true;
    yield return (object) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) sea030QuestScene.\u003C\u003En__0();
  }
}
