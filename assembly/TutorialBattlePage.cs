﻿// Decompiled with JetBrains decompiler
// Type: TutorialBattlePage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UniLinq;
using UnityEngine;

public class TutorialBattlePage : TutorialPageBase
{
  private Dictionary<int, string> turnDict = new Dictionary<int, string>();
  [SerializeField]
  private TutorialBattlePage.MESSAGE_TYPE messageType;
  [SerializeField]
  private int stageId;
  [SerializeField]
  private int deckId;

  public override IEnumerator Show()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    TutorialBattlePage tutorialBattlePage = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated method
    tutorialBattlePage.StartCoroutine(tutorialBattlePage.\u003C\u003En__0());
    string message = string.Empty;
    if (tutorialBattlePage.messageType == TutorialBattlePage.MESSAGE_TYPE.BATTLE1)
      message = Consts.GetInstance().newchapter_battle1_tutorial;
    else if (tutorialBattlePage.messageType == TutorialBattlePage.MESSAGE_TYPE.BATTLE2)
      message = Consts.GetInstance().newchapter_battle2_tutorial;
    else
      Debug.LogWarning((object) "battle advice not found");
    tutorialBattlePage.turnDict = tutorialBattlePage.parseTurnMessage(message);
    tutorialBattlePage.StartCoroutine(tutorialBattlePage.startBattle());
    return false;
  }

  private IEnumerator startBattle()
  {
    TutorialBattlePage tutorialBattlePage = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    for (int i = 0; i < 10; ++i)
    {
      Debug.Log((object) ("Wait Battle start...... " + (object) i));
      yield return (object) null;
    }
    BattleInfo battleInfo = Singleton<NGBattleManager>.GetInstance().battleInfo ?? new BattleInfo();
    battleInfo.quest_type = CommonQuestType.Story;
    battleInfo.battleId = "";
    battleInfo.stageId = tutorialBattlePage.stageId;
    battleInfo.helper = (PlayerHelper) null;
    battleInfo.deckIndex = tutorialBattlePage.deckId;
    battleInfo.isAutoBattleEnable = false;
    battleInfo.isRetreatEnable = false;
    battleInfo.isStoryEnable = false;
    battleInfo.enemy_items = new Tuple<int, GameCore.Reward>[0];
    IEnumerator e = MasterData.LoadBattleStageEnemy(battleInfo.stage);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = MasterData.LoadBattleMapLandform(battleInfo.stage.map);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    battleInfo.enemy_ids = ((IEnumerable<BattleStageEnemy>) MasterData.BattleStageEnemyList).Where<BattleStageEnemy>(new Func<BattleStageEnemy, bool>(tutorialBattlePage.\u003CstartBattle\u003Eb__6_0)).Select<BattleStageEnemy, int>((Func<BattleStageEnemy, int>) (x => x.ID)).ToArray<int>();
    battleInfo.user_enemy_ids = new int[0];
    Singleton<NGBattleManager>.GetInstance().startBattle(battleInfo, 0);
  }

  public void OnPlayerTurnStart(int turn)
  {
    Debug.Log((object) ("turn callback " + (object) turn));
    string message;
    if (!this.turnDict.TryGetValue(turn, out message))
      return;
    this.advice.FinishCallback = (System.Action) (() => {});
    this.advice.SetMessage(message, (Dictionary<string, Func<Transform, UIButton>>) null);
  }

  public void OnBattleFinish()
  {
    this.StartCoroutine(this.finishLoop());
  }

  private IEnumerator finishLoop()
  {
    TutorialBattlePage tutorialBattlePage = this;
    NGSceneManager sceneManager = Singleton<NGSceneManager>.GetInstance();
    sceneManager.destroyLoadedScenes();
    sceneManager.changeScene("empty", false, (object[]) Array.Empty<object>());
    while (!sceneManager.isSceneInitialized)
      yield return (object) null;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    tutorialBattlePage.NextPage();
  }

  private Dictionary<int, string> parseTurnMessage(string message)
  {
    int index = -1;
    Dictionary<int, string> dictionary = new Dictionary<int, string>();
    List<string> self = new List<string>();
    Regex regex = new Regex("#turn (\\d+)");
    string str = message;
    char[] chArray = new char[1]{ '\n' };
    foreach (string input in str.Split(chArray))
    {
      if (input.StartsWith("#"))
      {
        Match match = regex.Match(input);
        if (match.Success)
        {
          if (index >= 0)
          {
            dictionary[index] = self.Join("\n");
            self.Clear();
          }
          index = int.Parse(match.Groups[1].Value);
        }
        else
          self.Add(input);
      }
      else
        self.Add(input);
    }
    dictionary[index] = self.Join("\n");
    foreach (KeyValuePair<int, string> keyValuePair in dictionary)
      Debug.Log((object) (keyValuePair.Key.ToString() + ": " + keyValuePair.Value));
    return dictionary;
  }

  private enum MESSAGE_TYPE
  {
    BATTLE1,
    BATTLE2,
  }
}
