﻿// Decompiled with JetBrains decompiler
// Type: FriendRequestPopupBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FriendRequestPopupBase : BackButtonMenuBase
{
  private List<string> target_friend_ids = new List<string>();
  [SerializeField]
  protected UILabel TxtFriendName;
  [SerializeField]
  protected UILabel TxtFriendRequest;
  [SerializeField]
  protected UILabel TxtLastPlay;
  [SerializeField]
  protected UILabel TxtPopuptitle26;
  [SerializeField]
  protected UILabel TxtRecommend;
  [SerializeField]
  protected UI2DSprite Emblem;
  private const float LINK_WIDTH = 136f;
  private const float LINK_DEFWIDTH = 136f;
  private const float scale = 1f;
  [SerializeField]
  private GameObject linkChar;
  private DateTime lastTime;
  private System.Action callback;
  [SerializeField]
  protected GameObject slc_Master;
  [SerializeField]
  protected GameObject slc_Friend;
  [SerializeField]
  protected GameObject slc_Guild;

  public void SetCallback(System.Action callback)
  {
    this.callback = callback;
  }

  public IEnumerator Init(PlayerHelper helper, int incr_friend_point)
  {
    this.SetGameObject(this.slc_Friend, false);
    this.SetGameObject(this.slc_Guild, false);
    this.SetGameObject(this.slc_Master, false);
    if (helper.is_friend)
      this.SetGameObject(this.slc_Friend, true);
    else if (helper.is_guild_member)
      this.SetGameObject(this.slc_Guild, true);
    else
      this.SetGameObject(this.slc_Master, true);
    this.lastTime = helper.target_player_last_signed_in_at;
    IEnumerator e = this.Init(helper.leader_unit, helper.leader_unit.level, helper.target_player_id, helper.target_player_name, incr_friend_point, helper.current_emblem_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator Init(Gladiator gladiator)
  {
    IEnumerator e = this.Init(PlayerUnit.create_by_unitunit(gladiator), gladiator.leader_unit_level, gladiator.player_id, gladiator.name, -1, gladiator.current_emblem_id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator Init(
    PlayerUnit playerUnit,
    int level,
    string playerID,
    string playerName,
    int point,
    int emblemId)
  {
    yield return (object) this._Init(playerUnit, playerUnit.unit, level, playerID, playerName, point, emblemId);
  }

  private IEnumerator Init(
    UnitUnit unit,
    int level,
    string playerID,
    string playerName,
    int point,
    int emblemId)
  {
    yield return (object) this._Init((PlayerUnit) null, unit, level, playerID, playerName, point, emblemId);
  }

  private IEnumerator _Init(
    PlayerUnit playerUnit,
    UnitUnit unit,
    int level,
    string playerID,
    string playerName,
    int point,
    int emblemId)
  {
    Future<GameObject> prefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = prefabF.Result.Clone(this.linkChar.transform);
    gameObject.transform.localScale = new Vector3(1f, 1f);
    UnitIcon unitScript = gameObject.GetComponent<UnitIcon>();
    if (playerUnit == (PlayerUnit) null)
      yield return (object) unitScript.SetUnit(unit, unit.GetElement(), false);
    else
      yield return (object) unitScript.SetUnit(playerUnit, unit.GetElement(), false);
    unitScript.BottomModeValue = unit.awake_unit_flag ? UnitIconBase.BottomMode.AwakeUnit : UnitIconBase.BottomMode.Normal;
    unitScript.setLevelText(level.ToLocalizeNumberText());
    unitScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    this.AddTargetFriendsIds(playerID);
    this.SetText(playerName, point);
    e = this.SetEmblem(emblemId);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator SetEmblem(int emblemId)
  {
    Future<UnityEngine.Sprite> sprF = EmblemUtility.LoadEmblemSprite(emblemId);
    IEnumerator e = sprF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.Emblem.sprite2D = sprF.Result;
  }

  public virtual void SetText(string name, int point)
  {
    this.TxtFriendName.SetTextLocalize(name);
    TimeSpan self = ServerTime.NowAppTime() - this.lastTime;
    if (self.Days < 3)
      this.TxtLastPlay.SetTextLocalize(Consts.Format(Consts.GetInstance().QUEST_00282_LAST_LOGIN, (IDictionary) new Hashtable()
      {
        {
          (object) "time",
          (object) self.DisplayString()
        }
      }));
    else
      this.TxtLastPlay.SetTextLocalize(Consts.GetInstance().QUEST_00282_FRIEND_MANAGER);
  }

  public virtual void IbtnPopupYes()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    Singleton<CommonRoot>.GetInstance().StartCoroutine(this.FriendApplyAsync());
  }

  public void IbtnPopupYesWithoutClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().StartCoroutine(this.FriendApplyAsync());
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    if (this.callback == null)
      return;
    this.callback();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  private IEnumerator FriendApplyAsync()
  {
    // ISSUE: object of a compiler-generated type is created
    // ISSUE: variable of a compiler-generated type
    FriendRequestPopupBase.\u003C\u003Ec__DisplayClass28_0 cDisplayClass280 = new FriendRequestPopupBase.\u003C\u003Ec__DisplayClass28_0();
    // ISSUE: reference to a compiler-generated field
    cDisplayClass280.\u003C\u003E4__this = this;
    // ISSUE: reference to a compiler-generated field
    cDisplayClass280.call = this.callback;
    if (SMManager.Get<Player>().friends_count >= SMManager.Get<Player>().max_friends)
    {
      // ISSUE: reference to a compiler-generated field
      Singleton<CommonRoot>.GetInstance().StartCoroutine(this.openPopup00813(cDisplayClass280.call));
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 1;
      // ISSUE: reference to a compiler-generated method
      Future<WebAPI.Response.FriendApply> future = WebAPI.FriendApply(this.target_friend_ids.ToArray(), new System.Action<WebAPI.Response.UserError>(cDisplayClass280.\u003CFriendApplyAsync\u003Eb__0));
      IEnumerator e = future.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (future.Result != null)
      {
        // ISSUE: reference to a compiler-generated field
        Singleton<CommonRoot>.GetInstance().StartCoroutine(this.openPopup00815(cDisplayClass280.call));
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      }
      else
      {
        this.callback();
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      }
    }
  }

  private IEnumerator openPopup00813(System.Action call)
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_008_13__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    EventDelegate.Add(Singleton<PopupManager>.GetInstance().openAlert(prefab.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<FriendApplicationResultPopup>().OK.onClick, (EventDelegate.Callback) (() => call()));
  }

  private IEnumerator openPopup00814(System.Action call)
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_008_14__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    EventDelegate.Add(Singleton<PopupManager>.GetInstance().openAlert(prefab.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<FriendApplicationResultPopup>().OK.onClick, (EventDelegate.Callback) (() => call()));
  }

  private IEnumerator openPopup00815(System.Action call)
  {
    Future<GameObject> prefab = Res.Prefabs.popup.popup_008_15__anim_popup01.Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    EventDelegate.Add(Singleton<PopupManager>.GetInstance().openAlert(prefab.Result, false, false, (EventDelegate) null, false, true, false, true).GetComponent<FriendApplicationResultPopup>().OK.onClick, (EventDelegate.Callback) (() => call()));
  }

  public void AddTargetFriendsIds(string id)
  {
    this.target_friend_ids.Add(id);
  }

  private void SetGameObject(GameObject obj, bool active)
  {
    if ((UnityEngine.Object) obj == (UnityEngine.Object) null)
      return;
    obj.SetActive(active);
  }
}
