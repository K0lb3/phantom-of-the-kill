﻿// Decompiled with JetBrains decompiler
// Type: Story0098Scroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Story0098Scroll : BannerBase
{
  public IEnumerator InitScroll(QuestExtraS extra)
  {
    Story0098Scroll story0098Scroll = this;
    Future<Texture2D> futureIdle = Singleton<ResourceManager>.GetInstance().Load<Texture2D>(extra.seek_type != QuestExtra.SeekType.L ? BannerBase.GetSpriteIdlePath(extra.quest_m_QuestExtraM, BannerBase.Type.quest, QuestExtra.SeekType.M, true, false) : BannerBase.GetSpriteIdlePath(extra.quest_l_QuestExtraL, BannerBase.Type.quest, QuestExtra.SeekType.L, true, false), 1f);
    IEnumerator e = futureIdle.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Texture2D result = futureIdle.Result;
    if ((Object) result != (Object) null)
    {
      UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      sprite.name = result.name;
      story0098Scroll.IdleSprite.sprite2D = sprite;
    }
  }

  public IEnumerator InitScroll(TowerPlaybackStory story)
  {
    Story0098Scroll story0098Scroll = this;
    Future<Texture2D> futureIdle = Singleton<ResourceManager>.GetInstance().Load<Texture2D>(BannerBase.GetSpriteIdlePath(story.banner_id, BannerBase.Type.tower, QuestExtra.SeekType.L, true, false), 1f);
    IEnumerator e = futureIdle.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Texture2D result = futureIdle.Result;
    if ((Object) result != (Object) null)
    {
      UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      sprite.name = result.name;
      story0098Scroll.IdleSprite.sprite2D = sprite;
    }
  }

  public IEnumerator InitScroll(RaidPlaybackStory story)
  {
    Story0098Scroll story0098Scroll = this;
    Future<Texture2D> futureIdle = Singleton<ResourceManager>.GetInstance().Load<Texture2D>(BannerBase.GetSpriteIdlePath(MasterData.GuildRaidPeriod[story.period_id].banner_id, BannerBase.Type.raid, QuestExtra.SeekType.L, true, false), 1f);
    IEnumerator e = futureIdle.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Texture2D result = futureIdle.Result;
    if ((Object) result != (Object) null)
    {
      UnityEngine.Sprite sprite = UnityEngine.Sprite.Create(result, new Rect(0.0f, 0.0f, (float) result.width, (float) result.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      sprite.name = result.name;
      story0098Scroll.IdleSprite.sprite2D = sprite;
    }
  }
}
