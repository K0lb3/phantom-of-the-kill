﻿// Decompiled with JetBrains decompiler
// Type: Unit05443Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using System.Collections;

public class Unit05443Menu : Unit00443Menu
{
  public override IEnumerator FavoriteAPI()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Unit05443Menu unit05443Menu = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    Singleton<EarthDataManager>.GetInstance().SetFavoriteGear(unit05443Menu.RetentionGear.itemID, unit05443Menu.nowFavorite.gameObject.activeSelf);
    return false;
  }

  public override void choiceUnitChangeScene()
  {
    Unit054431Scene.ChangeScene(true, this.sendParam);
  }
}
