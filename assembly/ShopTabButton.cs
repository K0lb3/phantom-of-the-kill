﻿// Decompiled with JetBrains decompiler
// Type: ShopTabButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ShopTabButton : SpreadColorButton
{
  [SerializeField]
  private ShopTabType shopTabType;
  private bool isDragOut;

  private void OnDragOut(GameObject draggedObject)
  {
    this.isDragOut = true;
  }

  protected override void OnPress(bool isPressed)
  {
    base.OnPress(isPressed);
    if (isPressed || !this.isDragOut || Shop0079Menu.CurrentTabType == this.shopTabType)
      return;
    this.SetSprite(this.disabledSprite);
    this.isDragOut = false;
  }

  protected override void SetState(UIButtonColor.State state, bool instant)
  {
  }
}
