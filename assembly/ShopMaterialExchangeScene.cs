﻿// Decompiled with JetBrains decompiler
// Type: ShopMaterialExchangeScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class ShopMaterialExchangeScene : NGSceneBase
{
  private static readonly string NAME_SCENE = "shop007_MaterialExchange";
  private ShopMaterialExchangeMenu mainMenu_;

  public static void changeScene(bool isStack = true)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(ShopMaterialExchangeScene.NAME_SCENE, isStack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    ShopMaterialExchangeScene materialExchangeScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    materialExchangeScene.mainMenu_ = materialExchangeScene.menuBase as ShopMaterialExchangeMenu;
    IEnumerator e1;
    if (!WebAPI.IsResponsedAtRecent("ShopStatus", 60.0))
    {
      Future<WebAPI.Response.ShopStatus> future = WebAPI.ShopStatus((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = future.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (future.Result == null)
        yield break;
      else
        future = (Future<WebAPI.Response.ShopStatus>) null;
    }
    PlayerSelectTicketSummary[] selectTicketSummaryArray = SMManager.Get<PlayerSelectTicketSummary[]>();
    if (selectTicketSummaryArray != null)
    {
      SM.SelectTicket[] selectTicketArray = SMManager.Get<SM.SelectTicket[]>();
      List<PlayerSelectTicketSummary> playerUnitTickets = ((IEnumerable<PlayerSelectTicketSummary>) selectTicketSummaryArray).Where<PlayerSelectTicketSummary>((Func<PlayerSelectTicketSummary, bool>) (x => x != null && x.quantity > 0)).ToList<PlayerSelectTicketSummary>();
      for (int i = playerUnitTickets.Count - 1; i >= 0; i--)
      {
        MasterDataTable.SelectTicket selectTicket = ((IEnumerable<MasterDataTable.SelectTicket>) MasterData.SelectTicketList).First<MasterDataTable.SelectTicket>((Func<MasterDataTable.SelectTicket, bool>) (x => x.ID == playerUnitTickets[i].ticket_id));
        if (selectTicket != null && selectTicket.category_id.ID != 2)
          playerUnitTickets.Remove(playerUnitTickets[i]);
      }
      int[] ticketIds = playerUnitTickets.Select<PlayerSelectTicketSummary, int>((Func<PlayerSelectTicketSummary, int>) (pt => pt.ticket_id)).ToArray<int>();
      IOrderedEnumerable<SM.SelectTicket> orderedEnumerable = ((IEnumerable<SM.SelectTicket>) selectTicketArray).Where<SM.SelectTicket>((Func<SM.SelectTicket, bool>) (t => ((IEnumerable<int>) ticketIds).Contains<int>(t.id))).OrderBy<SM.SelectTicket, DateTime>((Func<SM.SelectTicket, DateTime>) (x => x.end_at)).OrderBy<SM.SelectTicket, int>((Func<SM.SelectTicket, int>) (s => this.GetTicketsPriority(playerUnitTickets.FirstOrDefault<PlayerSelectTicketSummary>((Func<PlayerSelectTicketSummary, bool>) (x => x.ticket_id == s.id)))));
      e1 = materialExchangeScene.mainMenu_.coInitialize((IEnumerable<SM.SelectTicket>) orderedEnumerable, playerUnitTickets);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
    else
    {
      e1 = materialExchangeScene.coEndSceneErrorCouponDateTime();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
  }

  private int GetTicketsPriority(PlayerSelectTicketSummary playerUnitTicket)
  {
    return ((IEnumerable<MasterDataTable.SelectTicket>) MasterData.SelectTicketList).First<MasterDataTable.SelectTicket>((Func<MasterDataTable.SelectTicket, bool>) (x => x.ID == playerUnitTicket.ticket_id)).priority;
  }

  private IEnumerator coEndSceneErrorCouponDateTime()
  {
    bool berrorwait = true;
    Consts instance = Consts.GetInstance();
    ModalWindow.Show(instance.SHOP_00723_ERROR_DATE_TITLE, instance.SHOP_00723_ERROR_DATE_MESSAGE, (System.Action) (() => berrorwait = false));
    while (berrorwait)
      yield return (object) null;
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
  }

  public override void onEndScene()
  {
    base.onEndScene();
    this.mainMenu_.onEndMenu();
  }
}
