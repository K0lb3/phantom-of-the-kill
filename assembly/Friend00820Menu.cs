﻿// Decompiled with JetBrains decompiler
// Type: Friend00820Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Friend00820Menu : BackButtonMenuBase
{
  [SerializeField]
  private NGxScroll ScrollContainer;

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void ScrollContainerResolvePosition()
  {
    this.ScrollContainer.ResolvePosition();
  }
}
