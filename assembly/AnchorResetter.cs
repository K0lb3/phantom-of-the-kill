﻿// Decompiled with JetBrains decompiler
// Type: AnchorResetter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnchorResetter : MonoBehaviour
{
  private bool firstEnable = true;

  private void Awake()
  {
    this.firstEnable = true;
  }

  private void OnEnable()
  {
    if (!this.firstEnable)
      return;
    this.firstEnable = false;
    UIRect component = this.GetComponent<UIRect>();
    if (!((Object) component != (Object) null))
      return;
    component.ResetAnchors();
    component.Update();
  }
}
