﻿// Decompiled with JetBrains decompiler
// Type: MypageUnitUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public static class MypageUnitUtil
{
  public static int getUnitId()
  {
    return Persist.mypageUnitId.Data._unit_id;
  }

  public static void setUnit(int unit_id)
  {
    Persist.mypageUnitId.Data._unit_id = unit_id;
    Persist.mypageUnitId.Flush();
  }

  public static void setDefault()
  {
    MypageUnitUtil.setUnit(0);
  }

  public static void setDefaultUnitNotFound()
  {
    MypageUnitUtil.setDefault();
  }

  public static void sellUnit(int unit_id)
  {
    if (unit_id != MypageUnitUtil.getUnitId())
      return;
    MypageUnitUtil.setDefault();
  }
}
