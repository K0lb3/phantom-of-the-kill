﻿// Decompiled with JetBrains decompiler
// Type: Unit0544Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;

public class Unit0544Menu : Unit0044Menu
{
  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.bugu0544SortAndFilter;
  }

  protected override void ChangeDetailScene(ItemInfo item)
  {
    Unit05443Scene.changeSceneLimited(true, item);
  }

  protected override void BottomInfoUpdate()
  {
    int num = 0;
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (!playerItem.isSupply() && !playerItem.isReisou())
        ++num;
    }
    this.TxtNumberpossession.SetTextLocalize(string.Format("{0}", (object) num));
  }
}
