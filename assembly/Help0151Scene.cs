﻿// Decompiled with JetBrains decompiler
// Type: Help0151Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Help0151Scene : NGSceneBase
{
  public Help0151Menu menu;

  public static void ChangeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("help015_1", stack, (object[]) Array.Empty<object>());
  }

  public override IEnumerator onInitSceneAsync()
  {
    IEnumerator e = this.menu.InitHelp();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      yield return (object) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
  }

  public IEnumerator onStartSceneAsync()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    Help0151Scene help0151Scene = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
      help0151Scene.bgmName = "bgm104";
    return false;
  }
}
