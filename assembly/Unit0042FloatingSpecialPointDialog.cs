﻿// Decompiled with JetBrains decompiler
// Type: Unit0042FloatingSpecialPointDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Unit0042FloatingSpecialPointDialog : Unit0042FloatingDialogBase
{
  private static readonly string specialIconSpriteBaseName = "slc_icon_specific_effectiveness_{0}.png__GUI__unit_detail{1}__unit_detail{1}_prefab";
  [SerializeField]
  protected UILabel txt_Factor;
  [SerializeField]
  protected UILabel txt_EventName;
  [SerializeField]
  protected UISprite specialIcon;

  public new void Show()
  {
    if (this.DialogConteiner.activeInHierarchy && this.isShow)
      return;
    base.Show();
    this.dir_FamilyType.SetActive(false);
    this.dir_SpecialPoint.SetActive(true);
  }

  public void setData(string strFactor, string strEventName, string iconSpriteName)
  {
    this.txt_Factor.SetTextLocalize(strFactor);
    this.txt_EventName.SetTextLocalize(strEventName);
    this.specialIcon.spriteName = iconSpriteName;
  }
}
