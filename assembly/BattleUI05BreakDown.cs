﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05BreakDown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class BattleUI05BreakDown : MonoBehaviour
{
  [SerializeField]
  private UILabel Txt_Point;
  [SerializeField]
  private UILabel Txt_Title;

  public void SetPoint(string title, int point)
  {
    this.Txt_Title.SetTextLocalize(title);
    this.Txt_Point.SetTextLocalize(Consts.Format(Consts.GetInstance().RESULT_RANKING_MENU_POINT, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (point),
        (object) point
      }
    }));
  }

  public void SetSpecialRate(string specialRate)
  {
    this.Txt_Title.SetTextLocalize(Consts.GetInstance().RESULT_RANKING_MENU_RATE_TITLE);
    this.Txt_Point.SetTextLocalize(Consts.Format(Consts.GetInstance().RESULT_RANKING_MENU_RATE, (IDictionary) new Hashtable()
    {
      {
        (object) "rate",
        (object) specialRate
      }
    }));
  }
}
