﻿// Decompiled with JetBrains decompiler
// Type: Unit004OverkillersSlotUnitSelectScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Unit004OverkillersSlotUnitSelectScene : NGSceneBase
{
  public static readonly string DefaultName = "unit004_UnitEquip_List";

  public static void changeScene(
    PlayerUnit base_unit,
    int slot_no,
    PlayerUnit select_unit,
    System.Action<PlayerUnit> act_selected)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Unit004OverkillersSlotUnitSelectScene.DefaultName, true, (object) new Unit004OverkillersSlotUnitSelectScene.Param(base_unit, slot_no, select_unit, act_selected));
  }

  private Unit004OverkillersSlotUnitSelectMenu menu
  {
    get
    {
      return this.menuBase as Unit004OverkillersSlotUnitSelectMenu;
    }
  }

  public IEnumerator onStartSceneAsync(Unit004OverkillersSlotUnitSelectScene.Param param)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    yield return (object) this.menu.initialize(param);
  }

  public void onStartScene(Unit004OverkillersSlotUnitSelectScene.Param param)
  {
    this.StartCoroutine(this.doDelayLoadingOff());
  }

  private IEnumerator doDelayLoadingOff()
  {
    yield return (object) new WaitForSeconds(0.5f);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public IEnumerator onBackSceneAsync(Unit004OverkillersSlotUnitSelectScene.Param param)
  {
    yield break;
  }

  public void onBackScene(Unit004OverkillersSlotUnitSelectScene.Param param)
  {
  }

  public class Param
  {
    public PlayerUnit baseUnit { get; private set; }

    public int slotNo { get; private set; }

    public PlayerUnit selectUnit { get; private set; }

    public System.Action<PlayerUnit> actSelected { get; private set; }

    public Param(
      PlayerUnit base_unit,
      int slot_no,
      PlayerUnit select_unit,
      System.Action<PlayerUnit> act_selected)
    {
      this.baseUnit = base_unit;
      this.slotNo = slot_no;
      this.selectUnit = select_unit;
      this.actSelected = act_selected;
    }
  }
}
