﻿// Decompiled with JetBrains decompiler
// Type: Versus026MatchBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using Net;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Versus026MatchBase : BackButtonMenuBase
{
  public static readonly int PVP_TUTORIAL_FRIEND_END_PAGE = 100;
  [SerializeField]
  protected UILabel txtLeaderSkill;
  [SerializeField]
  protected UILabel txtPass;
  [SerializeField]
  protected UILabel txtTimeLimit;
  [SerializeField]
  protected UILabel txtTotalPower;
  [SerializeField]
  protected UILabel txtTitle;
  [SerializeField]
  protected UILabel txtBonus;
  [SerializeField]
  protected GameObject dirScrollText;
  [SerializeField]
  protected GameObject bgCharacter;
  [SerializeField]
  protected GameObject[] unitIcons;
  [SerializeField]
  protected GameObject dirBonus;
  [SerializeField]
  protected UIButton btnBreakRepair;
  [SerializeField]
  protected UIButton btnStartMatch;
  protected bool isFriendMatch;
  protected PvpMatchingTypeEnum type;
  protected WebAPI.Response.PvpBoot pvpInfo;
  private bool isMatched;
  protected bool isRepair;
  protected bool isCancel;
  private bool isCloseFriendMatchPopup;
  private BattleInfo battleInfo;
  private GameObject scrollTextPrefab;

  public virtual IEnumerator Init(
    PvpMatchingTypeEnum type,
    WebAPI.Response.PvpBoot pvpInfo)
  {
    this.pvpInfo = pvpInfo;
    this.isMatched = false;
    this.isRepair = false;
    this.type = type;
    this.isFriendMatch = type == PvpMatchingTypeEnum.friend || type == PvpMatchingTypeEnum.guest;
    this.SetBonusDisplay();
    IEnumerator e = this.SetLiderSkillInfo();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.SetBgCharacter();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.SetUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SetMatchButton();
  }

  public virtual void IbtnStartMatch()
  {
    if (this.IsPushAndSet())
      return;
    if (!this.isCompletedOverkillersDeck)
      this.errorMatchingOverkillers();
    else if (this.isMatched)
    {
      Debug.LogError((object) "Already connect matching");
    }
    else
    {
      if (this.GetBoolCostOver())
        return;
      this.StartCoroutine(this.PopupConfirmationMatching());
    }
  }

  protected virtual bool isCompletedOverkillersDeck
  {
    get
    {
      PlayerDeck deck = this.GetDeck();
      bool bCompleted = true;
      if (deck != null)
        OverkillersUtil.checkCompletedDeck(deck.player_units, out bCompleted, (HashSet<int>) null, (bool[]) null);
      return bCompleted;
    }
  }

  private void errorMatchingOverkillers()
  {
    Consts instance = Consts.GetInstance();
    this.StartCoroutine(PopupCommon.Show(instance.QUEST_0028_ERROR_TITLE_OVERKILLERS, instance.QUEST_0028_ERROR_MESSAGE_OVERKILLERS, (System.Action) null));
  }

  public virtual void IbtnOrganization()
  {
    if (this.IsPushAndSet())
      return;
    Unit0046Scene.changeSceneVersus(true);
  }

  public virtual void IbtnWarExperience()
  {
  }

  public void IbtnRepair()
  {
    if (this.IsPushAndSet())
      return;
    Bugu00524Scene.ChangeScene(true);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet() || this.isMatched)
      return;
    this.backScene();
  }

  protected override void backScene()
  {
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
    Singleton<NGSceneManager>.GetInstance().changeScene("versus026_1", false, (object[]) Array.Empty<object>());
  }

  protected void SetMatchButton()
  {
    this.btnBreakRepair.gameObject.SetActive(this.isRepair);
    this.btnStartMatch.gameObject.SetActive(!this.isRepair);
  }

  private void SetBonusDisplay()
  {
    if (this.pvpInfo.bonus != null && this.pvpInfo.bonus.Length != 0)
    {
      Bonus[] array = ((IEnumerable<Bonus>) this.pvpInfo.bonus).Where<Bonus>((Func<Bonus, bool>) (x => x.category != 12)).ToArray<Bonus>();
      if (array.Length == 0)
      {
        this.dirBonus.SetActive(false);
      }
      else
      {
        new BonusDisplay().Set(array, this.txtBonus, this.txtTimeLimit, false, true);
        this.dirBonus.SetActive(true);
      }
    }
    else
      this.dirBonus.SetActive(false);
  }

  private IEnumerator SetLiderSkillInfo()
  {
    GameObject gameObject;
    if ((UnityEngine.Object) this.scrollTextPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> h = Res.Prefabs.colosseum.colosseum023_4.ScrollText.Load<GameObject>();
      IEnumerator e = h.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.scrollTextPrefab = h.Result;
      gameObject = this.scrollTextPrefab.Clone(this.dirScrollText.transform);
      gameObject.name = "ScrollText";
      h = (Future<GameObject>) null;
    }
    else
      gameObject = this.dirScrollText.transform.GetChildInFind("ScrollText").gameObject;
    PlayerDeck deck = this.GetDeck();
    PlayerUnit leaderUnit = this.GetLeaderUnit(deck);
    if (leaderUnit != (PlayerUnit) null)
    {
      if (leaderUnit.leader_skill != null)
      {
        this.txtLeaderSkill.SetTextLocalize(leaderUnit.leader_skill.skill.name);
        string text = leaderUnit.leader_skill.skill.description.Replace("\r", "").Replace("\n", "");
        gameObject.GetComponent<Colosseum0234ScrollText>().StartScroll(text);
      }
      else
      {
        this.txtLeaderSkill.SetTextLocalize("なし");
        gameObject.GetComponent<Colosseum0234ScrollText>().StartScroll("リーダースキルがありません");
      }
    }
    else
    {
      this.txtLeaderSkill.SetTextLocalize("なし");
      gameObject.GetComponent<Colosseum0234ScrollText>().StartScroll("ユニットが編成されていません");
    }
    this.txtTotalPower.text = deck.total_combat.ToLocalizeNumberText();
  }

  private IEnumerator SetBgCharacter()
  {
    foreach (Component component in this.bgCharacter.transform)
      UnityEngine.Object.Destroy((UnityEngine.Object) component.gameObject);
    PlayerUnit leaderUnit = this.GetLeaderUnit(this.GetDeck());
    yield return (object) leaderUnit.unit.LoadQuestWithMask(leaderUnit.job_id, this.bgCharacter.transform, this.bgCharacter.GetComponent<UIWidget>().depth, Res.GUI._002_2_sozai.mask_chara.Load<Texture2D>());
  }

  private IEnumerator SetUnitIcon()
  {
    PlayerDeck deck = this.GetDeck();
    Future<GameObject> unitPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = unitPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject unitPrefab = unitPrefabF.Result;
    for (int i = 0; i < deck.player_units.Length; ++i)
    {
      foreach (Component component in this.unitIcons[i].transform)
        UnityEngine.Object.DestroyObject((UnityEngine.Object) component.gameObject);
      UnitIcon unitPlayer = unitPrefab.Clone(this.unitIcons[i].transform).GetComponent<UnitIcon>();
      PlayerUnit playerUnit = deck.player_units[i];
      if (i == 0)
        yield return (object) unitPlayer.setBottomUnit(playerUnit, deck.player_units);
      else
        yield return (object) unitPlayer.SetPlayerUnit(playerUnit, deck.player_units, (PlayerUnit) null, false, false);
      if (playerUnit != (PlayerUnit) null)
      {
        unitPlayer.setLevelText(playerUnit);
        unitPlayer.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
        if (i == 0)
          unitPlayer.BreakWeaponOnlyBottom = playerUnit.IsBrokenEquippedGear;
        else
          unitPlayer.BreakWeapon = playerUnit.IsBrokenEquippedGear;
        this.isRepair = playerUnit.IsBrokenEquippedGear || this.isRepair;
      }
      else
        unitPlayer.SetEmpty();
      unitPlayer.princessType.DispPrincessType(false);
      unitPlayer = (UnitIcon) null;
      playerUnit = (PlayerUnit) null;
    }
  }

  private PlayerDeck GetDeck()
  {
    PlayerDeck[] playerDeckArray = SMManager.Get<PlayerDeck[]>();
    if (((IEnumerable<PlayerUnit>) playerDeckArray[Persist.versusDeckOrganized.Data.number].player_units).FirstOrDefault<PlayerUnit>() == (PlayerUnit) null)
    {
      Persist.versusDeckOrganized.Data.number = 0;
      Persist.versusDeckOrganized.Flush();
    }
    return playerDeckArray[Persist.versusDeckOrganized.Data.number];
  }

  private PlayerUnit GetLeaderUnit(PlayerDeck deck)
  {
    PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) deck.player_units).FirstOrDefault<PlayerUnit>();
    if (playerUnit == (PlayerUnit) null)
      playerUnit = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerDeck[]>()[0].player_units).First<PlayerUnit>();
    return playerUnit;
  }

  private bool GetBoolCostOver()
  {
    PlayerDeck deck = this.GetDeck();
    if (deck.cost <= deck.cost_limit)
      return false;
    Consts instance = Consts.GetInstance();
    ModalWindow.Show(instance.VERSUS_00262POPUP_COSTOVER_TITLE, instance.VERSUS_00262POPUP_COSTOVER_DESCRIPTION, (System.Action) (() => this.CancelBattle()));
    return true;
  }

  private void StartMatch()
  {
    this.battleInfo = new BattleInfo();
    this.battleInfo.pvp = true;
    this.StartCoroutine(this.doMatchingAndStartBattle());
  }

  private IEnumerator doMatchingAndStartBattle()
  {
    Versus026MatchBase versus026MatchBase = this;
    if (!versus026MatchBase.isMatched)
    {
      versus026MatchBase.isMatched = true;
      Persist.battleEnvironment.Delete();
      PVNpcManager.destroyPVNpcManager();
      Singleton<CommonRoot>.GetInstance().loadingMode = 2;
      Future<WebAPI.Response.PvpBoot> futureF = WebAPI.PvpBoot((System.Action<WebAPI.Response.UserError>) (e =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        this.CancelBattle();
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e1 = futureF.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (futureF.Result != null)
      {
        versus026MatchBase.pvpInfo = futureF.Result;
        Singleton<CommonRoot>.GetInstance().loadingMode = 0;
        if (!versus026MatchBase.IsMatchingBeginCheck())
        {
          versus026MatchBase.CancelBattle();
          versus026MatchBase.IsPush = true;
          e1 = versus026MatchBase.ErrorMathcingBegin();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
        }
        else if (versus026MatchBase.pvpInfo.pvp_maintenance)
        {
          Singleton<PopupManager>.GetInstance().onDismiss();
          if (!versus026MatchBase.IsPushAndSet())
            yield return (object) PopupCommon.Show(versus026MatchBase.pvpInfo.pvp_maintenance_title, versus026MatchBase.pvpInfo.pvp_maintenance_message, (System.Action) (() =>
            {
              NGSceneManager instance = Singleton<NGSceneManager>.GetInstance();
              instance.clearStack();
              instance.destroyCurrentScene();
              instance.changeScene(Singleton<CommonRoot>.GetInstance().startScene, false, (object[]) Array.Empty<object>());
            }));
        }
        else if (versus026MatchBase.pvpInfo.is_battle)
        {
          versus026MatchBase.isCancel = false;
          versus026MatchBase.PopupReadyErrorMatching(new System.Action(versus026MatchBase.CancelBattle));
          yield return (object) new WaitWhile((Func<bool>) (() => Singleton<PopupManager>.GetInstance().isOpen || !this.isCancel));
          StartScript.Restart();
        }
        else if (!versus026MatchBase.pvpInfo.is_latest_client_version)
        {
          Future<GameObject> f = Res.Prefabs.popup.popup_026_1_1__anim_popup01.Load<GameObject>();
          e1 = f.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          Singleton<PopupManager>.GetInstance().open(f.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup02611Menu>().Init();
        }
        else
        {
          versus026MatchBase.isCancel = false;
          GameObject popup_obj = (GameObject) null;
          yield return (object) versus026MatchBase.PopupConnectMatching((System.Action<GameObject>) (obj => popup_obj = obj));
          PVPManager pvpManager = PVPManager.createPVPManager();
          PVPMatching mm = pvpManager.getMatchingBehaviour();
          pvpManager.matchingType = versus026MatchBase.pvpInfo.is_tutorial_battle_end || !versus026MatchBase.pvpInfo.is_tutorial ? versus026MatchBase.type : PvpMatchingTypeEnum.tutorial;
          string roomkey = versus026MatchBase.SetRoomKey(MasterData.PvpMatchingType[(int) versus026MatchBase.type].room_key);
          mm.setMatchingServer(versus026MatchBase.pvpInfo.matching_host, versus026MatchBase.pvpInfo.matching_port);
          Singleton<CommonRoot>.GetInstance().loadingMode = 4;
          Future<MatchingPeer.MatchedConfirmation> mF = (Future<MatchingPeer.MatchedConfirmation>) null;
          PvpClassKind pvpClassKind = (PvpClassKind) null;
          MasterData.PvpClassKind.TryGetValue(versus026MatchBase.pvpInfo.current_class, out pvpClassKind);
          if (versus026MatchBase.type == PvpMatchingTypeEnum.class_match && pvpClassKind.cpu_defeats_count.HasValue && pvpClassKind.cpu_defeats_count.Value <= versus026MatchBase.pvpInfo.pvp_class_record.pvp_record.current_consecutive_loss)
          {
            versus026MatchBase.battleInfo.pvp_vs_npc = true;
          }
          else
          {
            mF = mm.matchingPVP(roomkey, (MatchingDebugInfo) null);
            e1 = mF.Wait();
            while (e1.MoveNext())
            {
              mm.IsCancel = versus026MatchBase.isCancel;
              yield return e1.Current;
            }
            e1 = (IEnumerator) null;
            if (versus026MatchBase.isCancel)
            {
              versus026MatchBase.CancelBattle();
              yield break;
            }
            else if (mF.Exception != null)
            {
              Debug.LogError((object) (" === Matching error:" + (object) mF.Exception));
              versus026MatchBase.PopupTimeOutMatching(new System.Action(versus026MatchBase.CancelBattle));
              yield break;
            }
            else if (mF.Result.player_id.Equals("Start-NPC"))
              versus026MatchBase.battleInfo.pvp_vs_npc = true;
          }
          if ((bool) (UnityEngine.Object) popup_obj)
          {
            popup_obj.GetComponent<Popup02642SerchMatching>().DisableButton();
            yield return (object) null;
          }
          bool isSuccess = false;
          if (versus026MatchBase.battleInfo.pvp_vs_npc)
          {
            yield return (object) versus026MatchBase.prepareToStartPvNpc((System.Action) (() => isSuccess = true));
          }
          else
          {
            yield return (object) versus026MatchBase.GetEnemyInfo(mF.Result.player_id, (System.Action<WebAPI.Response.PvpFriend>) (res => Singleton<PVPManager>.GetInstance().enemyInfo = res));
            if (Singleton<PVPManager>.GetInstance().enemyInfo == null)
            {
              Singleton<CommonRoot>.GetInstance().loadingMode = 0;
              yield break;
            }
            else
              yield return (object) versus026MatchBase.prepareToStartPvP((System.Action) (() => isSuccess = true));
          }
          if (isSuccess)
            versus026MatchBase.startBattle();
        }
      }
    }
  }

  private IEnumerator prepareToStartPvP(System.Action successCallback)
  {
    Versus026MatchBase versus026MatchBase = this;
    if (versus026MatchBase.isFriendMatch)
    {
      Singleton<PopupManager>.GetInstance().onDismiss();
      versus026MatchBase.isCloseFriendMatchPopup = false;
      yield return (object) versus026MatchBase.PopupFindMatching();
      // ISSUE: reference to a compiler-generated method
      yield return (object) new WaitWhile(new Func<bool>(versus026MatchBase.\u003CprepareToStartPvP\u003Eb__42_0));
      if (versus026MatchBase.isCancel)
      {
        versus026MatchBase.PopupCancelBattle(new System.Action(versus026MatchBase.CancelBattle), true);
        yield break;
      }
    }
    PVPMatching mm = Singleton<PVPManager>.GetInstance().getMatchingBehaviour();
    Future<MatchingPeer.Matched> rF = mm.matchingReady(!versus026MatchBase.isCancel);
    IEnumerator e = rF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (rF.Exception != null)
    {
      Debug.LogError((object) (" === MatchingReady error:" + (object) rF.Exception));
      if (rF.Exception.ToString().IndexOf("Cancel") != -1)
        versus026MatchBase.PopupCancelBattle(new System.Action(versus026MatchBase.CancelBattle), false);
      else if (rF.Exception.ToString().IndexOf("createRoom[ok]") != -1)
      {
        versus026MatchBase.PopupTimeOutMatching(new System.Action(versus026MatchBase.CancelBattle));
      }
      else
      {
        versus026MatchBase.isCancel = false;
        versus026MatchBase.PopupReadyErrorMatching(new System.Action(versus026MatchBase.CancelBattle));
        // ISSUE: reference to a compiler-generated method
        yield return (object) new WaitWhile(new Func<bool>(versus026MatchBase.\u003CprepareToStartPvP\u003Eb__42_1));
        StartScript.Restart();
      }
    }
    else
    {
      versus026MatchBase.setMatched(rF.Result);
      mm.cleanupDestroy();
      successCallback();
    }
  }

  private IEnumerator prepareToStartPvNpc(System.Action successCallback)
  {
    Versus026MatchBase versus026MatchBase = this;
    int tmpLoadingMode = Singleton<CommonRoot>.GetInstance().loadingMode;
    Singleton<CommonRoot>.GetInstance().loadingMode = 2;
    PlayerDeck deck = versus026MatchBase.GetDeck();
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.PvpPlayerNpcStart> startF = WebAPI.PvpPlayerNpcStart(deck.deck_number, deck.deck_type_id, (int) versus026MatchBase.type, new System.Action<WebAPI.Response.UserError>(versus026MatchBase.\u003CprepareToStartPvNpc\u003Eb__43_0));
    IEnumerator e = startF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (startF.Result != null)
    {
      PVNpcManager pvNpcManager = PVNpcManager.createPVNpcManager();
      pvNpcManager.matchingType = Singleton<PVPManager>.GetInstance().matchingType;
      pvNpcManager.player = startF.Result.player;
      pvNpcManager.enemy = startF.Result.target_player;
      pvNpcManager.stage = startF.Result.stage;
      versus026MatchBase.battleInfo = BattleInfo.MakePvNpcBattleInfo(startF.Result);
      yield return (object) versus026MatchBase.GetEnemyInfo(pvNpcManager.enemy.id, (System.Action<WebAPI.Response.PvpFriend>) (res => Singleton<PVNpcManager>.GetInstance().enemyInfo = res));
      if (Singleton<PVNpcManager>.GetInstance().enemyInfo != null)
      {
        versus026MatchBase.setMatched((MatchingPeer.Matched) null);
        yield return (object) PVPManager.destroyPVPManager();
        Singleton<CommonRoot>.GetInstance().loadingMode = tmpLoadingMode;
        successCallback();
      }
    }
  }

  private void startBattle()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    Singleton<CommonRoot>.GetInstance().loadingMode = 4;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Singleton<NGBattleManager>.GetInstance().startBattle(this.battleInfo, 0);
  }

  private void setMatched(MatchingPeer.Matched matched)
  {
    if (matched != null)
    {
      this.battleInfo.host = matched.host;
      this.battleInfo.port = matched.port;
      this.battleInfo.battleToken = matched.battleToken;
    }
    else
    {
      this.battleInfo.host = (string) null;
      this.battleInfo.port = 0;
      this.battleInfo.battleToken = (string) null;
    }
  }

  private IEnumerator GetEnemyInfo(string id, System.Action<WebAPI.Response.PvpFriend> callback)
  {
    Versus026MatchBase versus026MatchBase = this;
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.PvpFriend> f = WebAPI.PvpFriend(id, new System.Action<WebAPI.Response.UserError>(versus026MatchBase.\u003CGetEnemyInfo\u003Eb__46_0));
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    callback(f.Result);
  }

  protected virtual string SetRoomKey(string key)
  {
    return key;
  }

  public void OkFriendMatch()
  {
    this.isCloseFriendMatchPopup = true;
  }

  public void CancelFriendMatch()
  {
    this.isCloseFriendMatchPopup = true;
    this.isCancel = true;
  }

  public void CancelBattle()
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<PopupManager>.GetInstance().onDismiss();
    PVPManager.createPVPManager().getMatchingBehaviour().cleanupDestroy();
    this.isMatched = false;
    this.IsPush = false;
  }

  private IEnumerator PopupConfirmationMatching()
  {
    Versus026MatchBase versus026MatchBase = this;
    if (versus026MatchBase.isMatched)
    {
      Debug.LogError((object) "Already connect matching");
    }
    else
    {
      Future<GameObject> popupF = Res.Prefabs.popup.popup_026_4_1__anim_popup01.Load<GameObject>();
      IEnumerator e = popupF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().open(popupF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup02641Menu>().Initialize(new System.Action(versus026MatchBase.StartMatch), new System.Action(versus026MatchBase.CancelBattle));
    }
  }

  private void PopupCancelBattle(System.Action OkAction, bool byMyself)
  {
    if (!this.isMatched)
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Consts instance = Consts.GetInstance();
    ModalWindow.Show(byMyself ? instance.VERSUS_0026CANCEL_MYSELF_TITLE : instance.VERSUS_0026CANCEL_TITLE, byMyself ? instance.VERSUS_0026CANCEL_MYSELF_DESCRIPTION : instance.VERSUS_0026CANCEL_DESCRIPTION, (System.Action) (() => OkAction()));
  }

  private void PopupTimeOutMatching(System.Action OkAction)
  {
    if (!this.isMatched)
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Consts instance = Consts.GetInstance();
    ModalWindow.Show(instance.VERSUS_002645POPUP_TITLE, instance.VERSUS_002645POPUP_DESCRIPTION, (System.Action) (() => OkAction()));
  }

  private void PopupReadyErrorMatching(System.Action OkAction)
  {
    if (!this.isMatched)
      return;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Consts instance = Consts.GetInstance();
    ModalWindow.Show(instance.VERSUS_0026_READY_ERROR_TITLE, instance.VERSUS_0026_READY_ERROR_MESSAGE, (System.Action) (() =>
    {
      OkAction();
      this.isCancel = true;
    }));
  }

  private IEnumerator PopupConnectMatching(System.Action<GameObject> obj)
  {
    Versus026MatchBase versus026MatchBase = this;
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_026_4_2__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006");
    // ISSUE: reference to a compiler-generated method
    gameObject.GetComponent<Popup02642SerchMatching>().Init(new System.Action(versus026MatchBase.\u003CPopupConnectMatching\u003Eb__55_0));
    if (obj != null)
      obj(gameObject);
  }

  private IEnumerator PopupFindMatching()
  {
    Versus026MatchBase versus026MatchBase = this;
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_026_4_6__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) Singleton<PopupManager>.GetInstance().open(prefabF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup02646Menu>().Init(new System.Action(versus026MatchBase.OkFriendMatch), new System.Action(versus026MatchBase.CancelFriendMatch), new System.Action(versus026MatchBase.\u003CPopupFindMatching\u003Eb__56_0), Singleton<PVPManager>.GetInstance().enemyInfo);
  }

  protected virtual bool IsMatchingBeginCheck()
  {
    return true;
  }

  protected virtual IEnumerator ErrorMathcingBegin()
  {
    yield break;
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
