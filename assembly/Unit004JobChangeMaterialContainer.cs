﻿// Decompiled with JetBrains decompiler
// Type: Unit004JobChangeMaterialContainer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using JobChangeData;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004JobChangeMaterialContainer : MonoBehaviour
{
  [SerializeField]
  private Unit004JobChangeMaterialContainer.MaterialControl[] controls_ = new Unit004JobChangeMaterialContainer.MaterialControl[DefValues.NUM_MATERIALSLOT];
  private Unit004JobChangeMenu menu_;
  private PlayerMaterialUnit[] materials_;

  public bool isEnabled
  {
    get
    {
      return this.gameObject.activeSelf;
    }
    set
    {
      this.gameObject.SetActive(value);
    }
  }

  public void setAlpha(float a)
  {
    UIWidget component = this.GetComponent<UIWidget>();
    if (!((UnityEngine.Object) component != (UnityEngine.Object) null))
      return;
    component.alpha = a;
  }

  public void initialize(Unit004JobChangeMenu menu, GameObject iconPrefab)
  {
    this.menu_ = menu;
    foreach (Unit004JobChangeMaterialContainer.MaterialControl control in this.controls_)
      control.icon_ = iconPrefab.Clone(control.lnkIcon_).GetComponent<UnitIcon>();
  }

  public IEnumerator doUpdateMaterials(
    PlayerMaterialUnit[] materials,
    bool bBase,
    bool bUnlocked,
    bool conditionsLocked)
  {
    this.materials_ = materials;
    PlayerMaterialUnit[] playerMaterials = SMManager.Get<PlayerMaterialUnit[]>();
    string nullQuantity = Consts.GetInstance().JOBCHANGE_NULL_QUANTITY;
    bool bDisable = bBase | bUnlocked | conditionsLocked;
    for (int n = 0; n < this.controls_.Length; ++n)
    {
      Unit004JobChangeMaterialContainer.MaterialControl mc = this.controls_[n];
      mc.objTop_.SetActive(true);
      if (materials.Length <= n)
      {
        mc.icon_.unit = (UnitUnit) null;
        mc.icon_.SetEmpty();
        mc.icon_.SetCounter(0, false, false);
        mc.icon_.Gray = bDisable;
        mc.icon_.Button.isEnabled = false;
        mc.txtQuantity_.SetTextLocalize(nullQuantity);
        mc.txtQuantity_.color = bDisable ? Color.gray : Color.white;
      }
      else
      {
        PlayerMaterialUnit mu = materials[n];
        if (bDisable)
          mc.txtQuantity_.SetTextLocalize(nullQuantity);
        else
          mc.txtQuantity_.SetTextLocalize(mu.quantity);
        PlayerMaterialUnit playerMaterialUnit = Array.Find<PlayerMaterialUnit>(playerMaterials, (Predicate<PlayerMaterialUnit>) (m => m._unit == mu._unit));
        int q = playerMaterialUnit != null ? playerMaterialUnit.quantity : 0;
        mc.txtQuantity_.color = bDisable ? Color.gray : (q >= mu.quantity ? Color.white : Color.red);
        UnitUnit unit = mu.unit;
        yield return (object) mc.icon_.SetUnit(unit, unit.GetElement(), false);
        mc.icon_.Gray = bDisable || q < mu.quantity;
        mc.icon_.BottomModeValue = UnitIconBase.BottomMode.Nothing;
        mc.icon_.SetCounter(q, false, true);
        this.setEventOnClickedMaterial(mc.icon_, mu);
        mc = (Unit004JobChangeMaterialContainer.MaterialControl) null;
      }
    }
  }

  private void setEventOnClickedMaterial(UnitIcon icon, PlayerMaterialUnit materialUnit)
  {
    icon.Button.isEnabled = true;
    PlayerUnit playerUnit = PlayerUnit.CreateByPlayerMaterialUnit(materialUnit, 0);
    icon.onClick = (System.Action<UnitIconBase>) (ui => this.onClickedMaterial(playerUnit));
  }

  private void onClickedMaterial(PlayerUnit selected)
  {
    if (this.menu_.isCustomPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Unit0042Scene.changeScene(true, selected, ((IEnumerable<PlayerMaterialUnit>) this.materials_).Select<PlayerMaterialUnit, PlayerUnit>((Func<PlayerMaterialUnit, PlayerUnit>) (m => PlayerUnit.CreateByPlayerMaterialUnit(m, 0))).ToArray<PlayerUnit>(), false, false);
  }

  [Serializable]
  private class MaterialControl
  {
    public GameObject objTop_;
    public Transform lnkIcon_;
    public UILabel txtQuantity_;
    [NonSerialized]
    public UnitIcon icon_;
  }
}
