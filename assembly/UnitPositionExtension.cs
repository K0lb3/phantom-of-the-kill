﻿// Decompiled with JetBrains decompiler
// Type: UnitPositionExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public static class UnitPositionExtension
{
  public static void startMoveRoute(
    this BL.UnitPosition up,
    List<BL.Panel> route,
    float speed,
    BE env)
  {
    BL.Unit unit = up.unit;
    if (!unit.isView)
      return;
    BE.UnitResource unitResource = env.unitResource[up.unit];
    if (!unit.isEnable || (Object) unitResource.gameObject == (Object) null || unit.hp <= 0)
      return;
    unitResource.unitParts_.startMoveRoute(route, speed);
  }

  public static void cancelMove(this BL.UnitPosition up, BE env)
  {
    BL.Unit unit = up.unit;
    if (!unit.isView)
      return;
    BE.UnitResource unitResource = env.unitResource[up.unit];
    if (!unit.isEnable || (Object) unitResource.gameObject == (Object) null || unit.hp <= 0)
      return;
    unitResource.unitParts_.cancelMove(Singleton<NGBattleManager>.GetInstance().defaultUnitSpeed * 3f);
  }

  public static bool isMoving(this BL.UnitPosition up, BE env)
  {
    BL.Unit unit = up.unit;
    if (!unit.isView)
      return false;
    BE.UnitResource unitResource = env.unitResource[up.unit];
    return unit.isEnable && !((Object) unitResource.gameObject == (Object) null) && unitResource.unitParts_.isMoving;
  }
}
