﻿// Decompiled with JetBrains decompiler
// Type: Quest00214aMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest00214aMenu : BackButtonMenuBase
{
  private List<PlayerQuestSConverter> QuestList = new List<PlayerQuestSConverter>();
  private List<Tuple<QuestSConverter, PlayerQuestSConverter>> tblEpisodeQuest_ = new List<Tuple<QuestSConverter, PlayerQuestSConverter>>();
  private List<QuestDisplayConditionConverter>[] tblDisplayConditions_ = new List<QuestDisplayConditionConverter>[0];
  private List<GameObject> StageObjects = new List<GameObject>();
  [SerializeField]
  protected UILabel TxtTitle;
  public NGHorizontalScrollParts indicator;
  public Quest00214DetailDisplay container;
  public Quest00214Menu menu;
  public UIScrollView ScrollView;
  public Transform Middle;
  public UIGrid grid;
  public GameObject mask;
  public GameObject release;
  public SpringPanel.OnFinished onFinished;
  public Texture2D maskTexture;
  private bool releaseConditionActiveFlag;
  private bool ButtonMove;
  private int QuestQuantity;
  private GameObject CenterObj;
  private int startTweenCount;
  private Quest00214aMenu.Mode mode;
  [HideInInspector]
  public bool SceneStart;
  [HideInInspector]
  public List<UITweener> StartTweeners;
  [HideInInspector]
  public List<UITweener> EndTweeners;
  [HideInInspector]
  public List<UITweener> BothTweeners;
  private GameObject compareCenterObj;
  [SerializeField]
  private GameObject ButtonMission;
  [SerializeField]
  private GameObject Character;
  [SerializeField]
  private GameObject CharacterCombi;
  [SerializeField]
  private GameObject CharacterCombiTarget;
  [SerializeField]
  private GameObject CharacterTrioLeft;
  [SerializeField]
  private GameObject CharacterTrioCenter;
  [SerializeField]
  private GameObject CharacterTrioRight;
  [SerializeField]
  private NGxMaskSpriteWithScale SubBG;
  private GameObject unitIconPrefab;
  private GameObject releaseConditionPrefab;
  private const float AnimationTime = 0.3f;
  private UICenterOnChild ctrlCenter_;
  private QuestDetailManager detailManager_;
  private bool isChangedQuestDetail_;
  private bool isResetToneCondition_;

  private UICenterOnChild ctrlCenter
  {
    get
    {
      if ((UnityEngine.Object) this.ctrlCenter_ == (UnityEngine.Object) null)
        this.ctrlCenter_ = this.grid.GetComponent<UICenterOnChild>();
      return this.ctrlCenter_;
    }
  }

  private QuestDetailManager detailManager
  {
    get
    {
      if (this.detailManager_ == null)
        this.detailManager_ = new QuestDetailManager();
      return this.detailManager_;
    }
  }

  public virtual void IbtnBack()
  {
    Debug.Log((object) "click default event IbtnBack");
  }

  protected override void Update()
  {
    if (!this.gameObject.activeSelf || !this.SceneStart || (this.StageObjects.Count == 0 || this.isChangedQuestDetail_))
      return;
    base.Update();
    bool flag1 = !this.ScrollView.isDragging && !this.ButtonMove;
    for (int index = 0; index < this.StageObjects.Count; ++index)
    {
      GameObject stageObject = this.StageObjects[index];
      Quest00214Hscroll component = stageObject.GetComponent<Quest00214Hscroll>();
      PlayerQuestSConverter playerQuestSconverter = this.tblEpisodeQuest_[index].Item2;
      bool flag2 = playerQuestSconverter != null && !playerQuestSconverter.is_clear;
      if (!flag1 || !flag2)
      {
        if ((double) Mathf.Abs(this.ScrollView.transform.localPosition.x + component.defaultPosition()) < (double) component.spaceValue())
          component.ChangeToneConditionJudge(Mathf.Abs(this.ScrollView.transform.localPosition.x) - Mathf.Abs(component.defaultPosition()));
        else
          component.ChangeToneConditionJudge(Mathf.Abs(this.ScrollView.transform.localPosition.x + component.defaultPosition()));
        if (flag2)
        {
          component.centerAnimation(false);
          component.NotTouch(true);
        }
      }
      else if ((UnityEngine.Object) stageObject == (UnityEngine.Object) this.ctrlCenter.centeredObject & flag1)
      {
        component.centerAnimation(true);
        component.NotTouch(false);
      }
      else if (this.isResetToneCondition_)
      {
        if ((double) Mathf.Abs(this.ScrollView.transform.localPosition.x + component.defaultPosition()) < (double) component.spaceValue())
          component.ChangeToneConditionJudge(Mathf.Abs(this.ScrollView.transform.localPosition.x) - Mathf.Abs(component.defaultPosition()));
        else
          component.ChangeToneConditionJudge(Mathf.Abs(this.ScrollView.transform.localPosition.x + component.defaultPosition()));
      }
      if ((UnityEngine.Object) stageObject == (UnityEngine.Object) this.ctrlCenter.centeredObject && (UnityEngine.Object) stageObject != (UnityEngine.Object) this.compareCenterObj)
      {
        this.compareCenterObj = stageObject;
        QuestSConverter StageData = this.tblEpisodeQuest_[index].Item1;
        this.container.InitDetailDisplay(StageData, this.tblEpisodeQuest_[index].Item2, component.stageNumber());
        if (this.tblDisplayConditions_[index] == null)
          this.tblDisplayConditions_[index] = this.getDisplayConditionList(StageData.ID);
        this.StartCoroutine(this.InitReleases(this.unitIconPrefab, this.releaseConditionPrefab, this.tblDisplayConditions_[index]));
      }
    }
    this.isResetToneCondition_ = false;
    if (!this.ScrollView.isDragging && !this.ButtonMove)
      return;
    this.TweenStart(true);
  }

  public override void onBackButton()
  {
    if (this.startTweenCount > 0)
      return;
    this.Ending();
  }

  private List<QuestDisplayConditionConverter> getDisplayConditionList(
    int questId)
  {
    IEnumerable<QuestDisplayConditionConverter> source = this.mode == Quest00214aMenu.Mode.Character ? ((IEnumerable<QuestCharacterDisplayCondition>) MasterData.QuestCharacterDisplayConditionList).Where<QuestCharacterDisplayCondition>((Func<QuestCharacterDisplayCondition, bool>) (q => q.quest_s_QuestCharacterS == questId)).Select<QuestCharacterDisplayCondition, QuestDisplayConditionConverter>((Func<QuestCharacterDisplayCondition, QuestDisplayConditionConverter>) (q => new QuestDisplayConditionConverter(q))) : (this.mode == Quest00214aMenu.Mode.Harmony ? ((IEnumerable<QuestHarmonyDisplayCondition>) MasterData.QuestHarmonyDisplayConditionList).Where<QuestHarmonyDisplayCondition>((Func<QuestHarmonyDisplayCondition, bool>) (q => q.quest_s_QuestHarmonyS == questId)).Select<QuestHarmonyDisplayCondition, QuestDisplayConditionConverter>((Func<QuestHarmonyDisplayCondition, QuestDisplayConditionConverter>) (q => new QuestDisplayConditionConverter(q))) : (IEnumerable<QuestDisplayConditionConverter>) null);
    return source == null ? new List<QuestDisplayConditionConverter>() : source.OrderBy<QuestDisplayConditionConverter, int>((Func<QuestDisplayConditionConverter, int>) (x => x.priority)).ToList<QuestDisplayConditionConverter>();
  }

  private void CommonInit(GameObject iconPrefab, GameObject conditionPrefab)
  {
    this.indicator.SeEnable = false;
    this.GetComponent<UIWidget>().alpha = 0.0f;
    this.unitIconPrefab = iconPrefab;
    this.releaseConditionPrefab = conditionPrefab;
    this.QuestList.Clear();
    this.tblEpisodeQuest_.Clear();
    this.StageObjects.Clear();
    this.indicator.destroyParts(true);
  }

  private IEnumerator LoadCharacterSprite(int id, GameObject locationObject)
  {
    locationObject.transform.Clear();
    IEnumerator e = MasterData.UnitUnit[id].LoadQuestWithMask(locationObject.transform, locationObject.GetComponent<UIWidget>().depth, Res.GUI._002_2_sozai.mask_chara.Load<Texture2D>());
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadCharacterStorySprite(int id, GameObject locationObject)
  {
    locationObject.transform.Clear();
    Future<GameObject> f = MasterData.UnitUnit[id].LoadStory();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject go = f.Result.Clone(locationObject.transform);
    MasterData.UnitUnit[id].SetStoryData(go, "normal", new int?());
    go.GetComponent<NGxMaskSpriteWithScale>().maskTexture = this.maskTexture;
  }

  private IEnumerator LoadMask()
  {
    Future<Texture2D> maskF = Res.GUI._009_3_sozai.mask_Chara_C.Load<Texture2D>();
    IEnumerator e = maskF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.maskTexture = maskF.Result;
  }

  private void SetTween()
  {
    this.StartTweeners = new List<UITweener>();
    this.EndTweeners = new List<UITweener>();
    this.BothTweeners = new List<UITweener>();
    UITweener[] componentsInChildren = this.GetComponentsInChildren<UITweener>();
    this.startTweenCount = 0;
    foreach (UITweener uiTweener in componentsInChildren)
    {
      if (uiTweener.tweenGroup == 22)
      {
        ++this.startTweenCount;
        this.StartTweeners.Add(uiTweener);
      }
      else if (uiTweener.tweenGroup == 33)
        this.EndTweeners.Add(uiTweener);
      else if (uiTweener.tweenGroup == 44)
        this.BothTweeners.Add(uiTweener);
    }
  }

  private IEnumerator SetBg(
    QuestSConverter questData,
    PlayerQuestSConverter playerData,
    int index)
  {
    Future<UnityEngine.Sprite> bgSprite = Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format(Consts.GetInstance().BACKGROUND_BASE_PATH, (object) questData.quest_m.background_image_name), 1f);
    IEnumerator e = bgSprite.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<Texture2D> loadMask = Res.GUI._002_2_sozai.bg_mask_quest_stage_select.Load<Texture2D>();
    e = loadMask.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.SubBG.MainUI2DSprite.sprite2D = bgSprite.Result;
    this.SubBG.maskTexture = loadMask.Result;
    this.SubBG.xOffsetPixel = (int) questData.quest_m.offset_x;
    this.SubBG.yOffsetPixel = (int) questData.quest_m.offset_y;
    this.SubBG.scale = questData.quest_m.scale;
    this.SubBG.FitMask();
    this.container.StartInit();
    this.container.InitDetailDisplay(questData, playerData, index + 1);
  }

  private void SetGrayQuestIcon(int questLength, GameObject character)
  {
    this.ctrlCenter.onFinished = (SpringPanel.OnFinished) (() => this.TweenStart(false));
    if (questLength != 0)
      return;
    character.transform.GetChild(0).GetComponent<UI2DSprite>().color = Color.Lerp(Color.white, Color.black, 0.5f);
  }

  public IEnumerator Init(
    int id,
    WebAPI.Response.QuestProgressCharacter apiResponse,
    GameObject iconPrefab,
    GameObject conditionPrefab)
  {
    this.mode = Quest00214aMenu.Mode.Character;
    this.CommonInit(iconPrefab, conditionPrefab);
    foreach (PlayerCharacterQuestS quest in SMManager.Get<PlayerCharacterQuestS[]>().SelectReleased())
    {
      if (quest.quest_character_s.unit_UnitUnit == id)
        this.QuestList.Add(new PlayerQuestSConverter(quest));
    }
    IEnumerator e1 = ServerTime.WaitSync();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    List<QuestSConverter> episodeList = ((IEnumerable<QuestCharacterS>) MasterData.QuestCharacterSList).Where<QuestCharacterS>((Func<QuestCharacterS, bool>) (w => w.unit_UnitUnit == id && QuestCharacterS.CheckIsReleased(w.start_at))).Select<QuestCharacterS, QuestSConverter>((Func<QuestCharacterS, QuestSConverter>) (s =>
    {
      WebAPI.Response.QuestProgressCharacterCharacter_quest_s_lost_aps characterQuestSLostAps = ((IEnumerable<WebAPI.Response.QuestProgressCharacterCharacter_quest_s_lost_aps>) apiResponse.character_quest_s_lost_aps).FirstOrDefault<WebAPI.Response.QuestProgressCharacterCharacter_quest_s_lost_aps>((Func<WebAPI.Response.QuestProgressCharacterCharacter_quest_s_lost_aps, bool>) (sd => sd.quest_s_id == s.ID));
      int lost_ap = characterQuestSLostAps != null ? characterQuestSLostAps.lost_ap : 0;
      return new QuestSConverter(s, lost_ap);
    })).OrderBy<QuestSConverter, int>((Func<QuestSConverter, int>) (x => x.priority)).ToList<QuestSConverter>();
    this.tblEpisodeQuest_ = episodeList.Select<QuestSConverter, Tuple<QuestSConverter, PlayerQuestSConverter>>((Func<QuestSConverter, Tuple<QuestSConverter, PlayerQuestSConverter>>) (e => Tuple.Create<QuestSConverter, PlayerQuestSConverter>(e, this.QuestList.FirstOrDefault<PlayerQuestSConverter>((Func<PlayerQuestSConverter, bool>) (q => q._quest_s_id == e.ID))))).ToList<Tuple<QuestSConverter, PlayerQuestSConverter>>();
    this.QuestList = this.tblEpisodeQuest_.Where<Tuple<QuestSConverter, PlayerQuestSConverter>>((Func<Tuple<QuestSConverter, PlayerQuestSConverter>, bool>) (tbl => tbl.Item2 != null)).Select<Tuple<QuestSConverter, PlayerQuestSConverter>, PlayerQuestSConverter>((Func<Tuple<QuestSConverter, PlayerQuestSConverter>, PlayerQuestSConverter>) (tbl => tbl.Item2)).ToList<PlayerQuestSConverter>();
    this.QuestQuantity = episodeList.Count;
    this.tblDisplayConditions_ = new List<QuestDisplayConditionConverter>[this.QuestQuantity];
    int initCenter = 0;
    if (this.QuestList.Count > 0)
    {
      for (int index = 0; index < this.tblEpisodeQuest_.Count; ++index)
      {
        if (this.tblEpisodeQuest_[index].Item2 != null && !this.tblEpisodeQuest_[index].Item2.is_clear)
        {
          initCenter = index;
          break;
        }
      }
    }
    e1 = this.LoadCharacterSprite(id, this.Character);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    this.Character.gameObject.SetActive(true);
    this.CharacterCombi.gameObject.SetActive(false);
    this.CharacterCombiTarget.gameObject.SetActive(false);
    this.CharacterTrioLeft.gameObject.SetActive(false);
    this.CharacterTrioCenter.gameObject.SetActive(false);
    this.CharacterTrioRight.gameObject.SetActive(false);
    this.SetGrayQuestIcon(this.QuestList.Count, this.Character);
    e1 = this.SetBg(episodeList[initCenter], this.tblEpisodeQuest_[initCenter].Item2, initCenter);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    this.releaseConditionActiveFlag = false;
    this.SetTween();
    this.tblDisplayConditions_[initCenter] = this.getDisplayConditionList(episodeList[initCenter].ID);
    e1 = this.InitReleases(iconPrefab, conditionPrefab, this.tblDisplayConditions_[initCenter]);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    e1 = this.InitHscroll(initCenter);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (episodeList.Count > 0)
      this.menu.SetTxtTitle = episodeList.First<QuestSConverter>().quest_m.name;
  }

  public void InitTweenFinish()
  {
    this.indicator.SeEnable = true;
  }

  public IEnumerator Init(
    int unitId,
    int[] targetUnitIds,
    WebAPI.Response.QuestProgressCharacter apiResponse,
    GameObject iconPrefab,
    GameObject conditionPrefab,
    bool isTrio)
  {
    this.mode = Quest00214aMenu.Mode.Harmony;
    this.CommonInit(iconPrefab, conditionPrefab);
    foreach (PlayerHarmonyQuestS quest in SMManager.Get<PlayerHarmonyQuestS[]>().SelectReleased())
    {
      if (quest.quest_harmony_s.unit_UnitUnit == unitId)
        this.QuestList.Add(new PlayerQuestSConverter(quest));
    }
    IEnumerator e1 = ServerTime.WaitSync();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    List<QuestSConverter> episodeList = ((IEnumerable<QuestHarmonyS>) MasterData.QuestHarmonySList).Where<QuestHarmonyS>((Func<QuestHarmonyS, bool>) (w => w.unit_UnitUnit == unitId && QuestCharacterS.CheckIsReleased(w.start_at))).Select<QuestHarmonyS, QuestSConverter>((Func<QuestHarmonyS, QuestSConverter>) (s =>
    {
      WebAPI.Response.QuestProgressCharacterHarmony_quest_s_lost_aps harmonyQuestSLostAps = ((IEnumerable<WebAPI.Response.QuestProgressCharacterHarmony_quest_s_lost_aps>) apiResponse.harmony_quest_s_lost_aps).FirstOrDefault<WebAPI.Response.QuestProgressCharacterHarmony_quest_s_lost_aps>((Func<WebAPI.Response.QuestProgressCharacterHarmony_quest_s_lost_aps, bool>) (sd => sd.quest_s_id == s.ID));
      int lost_ap = harmonyQuestSLostAps != null ? harmonyQuestSLostAps.lost_ap : 0;
      return new QuestSConverter(s, lost_ap);
    })).OrderBy<QuestSConverter, int>((Func<QuestSConverter, int>) (x => x.priority)).ToList<QuestSConverter>();
    this.tblEpisodeQuest_ = episodeList.Select<QuestSConverter, Tuple<QuestSConverter, PlayerQuestSConverter>>((Func<QuestSConverter, Tuple<QuestSConverter, PlayerQuestSConverter>>) (e => Tuple.Create<QuestSConverter, PlayerQuestSConverter>(e, this.QuestList.FirstOrDefault<PlayerQuestSConverter>((Func<PlayerQuestSConverter, bool>) (q => q._quest_s_id == e.ID))))).ToList<Tuple<QuestSConverter, PlayerQuestSConverter>>();
    this.QuestList = this.tblEpisodeQuest_.Where<Tuple<QuestSConverter, PlayerQuestSConverter>>((Func<Tuple<QuestSConverter, PlayerQuestSConverter>, bool>) (tbl => tbl.Item2 != null)).Select<Tuple<QuestSConverter, PlayerQuestSConverter>, PlayerQuestSConverter>((Func<Tuple<QuestSConverter, PlayerQuestSConverter>, PlayerQuestSConverter>) (tbl => tbl.Item2)).ToList<PlayerQuestSConverter>();
    this.QuestQuantity = episodeList.Count;
    this.tblDisplayConditions_ = new List<QuestDisplayConditionConverter>[this.QuestQuantity];
    int initCenter = 0;
    if (this.QuestList.Count > 0)
    {
      for (int index = 0; index < this.tblEpisodeQuest_.Count; ++index)
      {
        if (this.tblEpisodeQuest_[index].Item2 != null && !this.tblEpisodeQuest_[index].Item2.is_clear)
        {
          initCenter = index;
          break;
        }
      }
    }
    e1 = this.LoadMask();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (isTrio && targetUnitIds.Length > 1)
    {
      e1 = this.LoadCharacterStorySprite(unitId, this.CharacterTrioLeft);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      e1 = this.LoadCharacterStorySprite(targetUnitIds[0], this.CharacterTrioCenter);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      e1 = this.LoadCharacterStorySprite(targetUnitIds[1], this.CharacterTrioRight);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      this.Character.gameObject.SetActive(false);
      this.CharacterCombi.gameObject.SetActive(false);
      this.CharacterCombiTarget.gameObject.SetActive(false);
      this.CharacterTrioLeft.gameObject.SetActive(true);
      this.CharacterTrioCenter.gameObject.SetActive(true);
      this.CharacterTrioRight.gameObject.SetActive(true);
      this.SetGrayQuestIcon(this.QuestList.Count, this.CharacterTrioLeft);
      this.SetGrayQuestIcon(this.QuestList.Count, this.CharacterTrioCenter);
      this.SetGrayQuestIcon(this.QuestList.Count, this.CharacterTrioRight);
    }
    else
    {
      e1 = this.LoadCharacterStorySprite(unitId, this.CharacterCombi);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      e1 = this.LoadCharacterStorySprite(targetUnitIds[0], this.CharacterCombiTarget);
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      this.Character.gameObject.SetActive(false);
      this.CharacterCombi.gameObject.SetActive(true);
      this.CharacterCombiTarget.gameObject.SetActive(true);
      this.CharacterTrioLeft.gameObject.SetActive(false);
      this.CharacterTrioCenter.gameObject.SetActive(false);
      this.CharacterTrioRight.gameObject.SetActive(false);
      this.SetGrayQuestIcon(this.QuestList.Count, this.CharacterCombi);
      this.SetGrayQuestIcon(this.QuestList.Count, this.CharacterCombiTarget);
    }
    e1 = this.SetBg(episodeList[initCenter], this.tblEpisodeQuest_[initCenter].Item2, initCenter);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    this.releaseConditionActiveFlag = false;
    this.SetTween();
    this.tblDisplayConditions_[initCenter] = this.getDisplayConditionList(episodeList[initCenter].ID);
    e1 = this.InitReleases(iconPrefab, conditionPrefab, this.tblDisplayConditions_[initCenter]);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    e1 = this.InitHscroll(initCenter);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (episodeList.Count > 0)
      this.menu.SetTxtTitle = episodeList.First<QuestSConverter>().quest_m.name;
  }

  public IEnumerator InitReleases(
    GameObject iconPrefab,
    GameObject conditionPrefab,
    List<QuestDisplayConditionConverter> list)
  {
    if (list.Count > 0)
    {
      IEnumerator e = this.release.GetComponent<Quest00214ReleaseCondition>().InitRelease(list, iconPrefab, conditionPrefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
      this.release.gameObject.SetActive(false);
  }

  private IEnumerator InitHscroll(int iCenter)
  {
    Quest00214aMenu quest00214aMenu = this;
    Future<GameObject> Hscroll = Res.Prefabs.quest002_14.hscroll_640.Load<GameObject>();
    IEnumerator e = Hscroll.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject result = Hscroll.Result;
    for (int index = 0; index < quest00214aMenu.QuestQuantity; ++index)
    {
      GameObject gameObject = quest00214aMenu.indicator.instantiateParts(result, true);
      quest00214aMenu.StageObjects.Add(gameObject);
      Quest00214Hscroll component = gameObject.GetComponent<Quest00214Hscroll>();
      if (quest00214aMenu.tblEpisodeQuest_[index].Item2 != null)
        component.UnLockCondition();
      else
        component.LockCondition();
      quest00214aMenu.initHscroll(component, quest00214aMenu.tblEpisodeQuest_[index].Item1, quest00214aMenu.QuestList.ToArray(), index + 1, quest00214aMenu.grid.cellWidth, iCenter);
    }
    quest00214aMenu.indicator.resetScrollView();
    quest00214aMenu.indicator.setItemPositionQuick(iCenter);
    yield return (object) null;
    quest00214aMenu.ctrlCenter.CenterOn(quest00214aMenu.StageObjects[iCenter].transform);
    quest00214aMenu.compareCenterObj = quest00214aMenu.StageObjects[iCenter];
    foreach (GameObject stageObject in quest00214aMenu.StageObjects)
    {
      Quest00214Hscroll component = stageObject.GetComponent<Quest00214Hscroll>();
      if ((UnityEngine.Object) stageObject != (UnityEngine.Object) quest00214aMenu.compareCenterObj)
      {
        component.centerAnimation(false);
        component.NotTouch(true);
      }
      else
      {
        PlayerQuestSConverter playerQuestSconverter = quest00214aMenu.tblEpisodeQuest_[iCenter].Item2;
        bool flag = playerQuestSconverter != null && !playerQuestSconverter.is_clear;
        component.centerAnimation(flag);
        component.NotTouch(!flag);
      }
    }
    quest00214aMenu.isResetToneCondition_ = true;
    quest00214aMenu.StageObjects.ForEach((System.Action<GameObject>) (x => x.GetComponent<Quest00214Hscroll>().onSetValue()));
    quest00214aMenu.Starting();
    quest00214aMenu.ToneChangeStart();
    quest00214aMenu.Update();
  }

  private void initHscroll(
    Quest00214Hscroll hscroll,
    QuestSConverter StageData,
    PlayerQuestSConverter[] charaque,
    int num,
    float gridWidth,
    int center)
  {
    hscroll.Init(StageData, charaque, num, gridWidth, center, (EventDelegate.Callback) (() =>
    {
      if (this.IsPushAndSet())
        return;
      this.isChangedQuestDetail_ = true;
      Quest00221Scene.changeDetailScene(this.detailManager.getData(StageData.data_type == QuestSConverter.DataType.Character ? CommonQuestType.Character : CommonQuestType.Harmony, StageData.ID, false), true);
    }));
  }

  private void OnEnable()
  {
    if (!this.isChangedQuestDetail_)
      return;
    this.isChangedQuestDetail_ = false;
    this.ScrollView.Press(false);
  }

  public void Starting()
  {
    this.mask.SetActive(true);
    this.mask.GetComponent<TweenAlpha>().ResetToBeginning();
    this.mask.GetComponent<TweenAlpha>().PlayForward();
    this.GetComponent<UIWidget>().alpha = 1f;
    this.StartTweeners.ForEach((System.Action<UITweener>) (x =>
    {
      x.SetOnFinished((EventDelegate.Callback) (() => --this.startTweenCount));
      x.ResetToBeginning();
      x.PlayForward();
    }));
    this.BothTweeners.ForEach((System.Action<UITweener>) (x =>
    {
      x.ResetToBeginning();
      x.PlayForward();
    }));
    this.IsPush = false;
  }

  public void Ending()
  {
    if (this.IsPushAndSet())
      return;
    this.mask.GetComponent<TweenAlpha>().ResetToBeginning();
    this.mask.GetComponent<TweenAlpha>().PlayReverse();
    this.EndTweeners.ForEach((System.Action<UITweener>) (x =>
    {
      x.ResetToBeginning();
      x.PlayForward();
    }));
    this.BothTweeners.ForEach((System.Action<UITweener>) (x =>
    {
      x.ResetToBeginning();
      x.PlayReverse();
    }));
    if (this.releaseConditionActiveFlag)
      this.ibtnReleaseCondition();
    this.menu.isSelect = false;
    this.Invoke("Disable", 0.3f);
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  private void Disable()
  {
    this.mask.SetActive(false);
    this.gameObject.SetActive(false);
    if (this.mode == Quest00214aMenu.Mode.Character)
    {
      this.menu.IconEnable = !this.menu.isSelect;
      this.menu.SetTxtTitle = Consts.GetInstance().QUEST_00214_CHARACTER_TITLE;
    }
    if (this.mode != Quest00214aMenu.Mode.Harmony)
      return;
    this.menu.CellEnable = !this.menu.isSelect;
    this.menu.SetTxtTitle = Consts.GetInstance().QUEST_00214_COMBI_TITLE;
  }

  public void ToneChangeStart()
  {
    this.SceneStart = true;
  }

  public void changeScene(PlayerCharacterQuestS episode)
  {
    Quest0028Scene.changeScene(true, episode, false);
  }

  public void ibtnReleaseCondition()
  {
    this.releaseConditionActiveFlag = !this.releaseConditionActiveFlag;
    this.release.GetComponent<Quest00214ReleaseCondition>().StartTweenClick(this.releaseConditionActiveFlag);
  }

  public void TweenStart(bool flag)
  {
    if (this.ButtonMove != flag)
    {
      this.container.StartTween(flag);
      if (this.releaseConditionActiveFlag && this.release.activeSelf)
        this.release.GetComponent<Quest00214ReleaseCondition>().StartTween(flag);
    }
    this.ButtonMove = flag;
  }

  private enum Mode
  {
    Character,
    Harmony,
  }
}
