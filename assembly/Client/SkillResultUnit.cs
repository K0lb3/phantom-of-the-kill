﻿// Decompiled with JetBrains decompiler
// Type: Client.SkillResultUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;

namespace Client
{
  internal class SkillResultUnit : BL.SkillResultUnit
  {
    public BE _be;

    public SkillResultUnit(BL.UnitPosition up, BE be)
      : base(up, be.core)
    {
      this._be = be;
    }

    public override void respawnReinforcement()
    {
      base.respawnReinforcement();
      if (this._up_target is BL.AIUnit)
        return;
      this._be.unitResource[this._up_target.unit].unitParts_.setActive(true);
    }
  }
}
