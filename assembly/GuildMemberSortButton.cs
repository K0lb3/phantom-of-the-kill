﻿// Decompiled with JetBrains decompiler
// Type: GuildMemberSortButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuildMemberSortButton : SortAndFilterButton
{
  [SerializeField]
  private GuildMemberSort menu;
  [SerializeField]
  private GuildMemberSort.SORT_TYPES sortType;
  [SerializeField]
  private UISprite[] LabelSprite;

  public GuildMemberSort.SORT_TYPES SortType
  {
    get
    {
      return this.sortType;
    }
  }

  protected override void Awake()
  {
    base.Awake();
  }

  private void Update()
  {
    foreach (UIWidget uiWidget in this.LabelSprite)
      uiWidget.color = this.Sprite.color;
  }

  public void TextColorGray(bool flag)
  {
    Color color = Color.gray;
    if (flag)
      color = Color.white;
    foreach (UIWidget uiWidget in this.LabelSprite)
      uiWidget.color = color;
  }

  public override void PressButton()
  {
    this.menu.SetSortCategory(this.sortType);
  }
}
