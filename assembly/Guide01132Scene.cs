﻿// Decompiled with JetBrains decompiler
// Type: Guide01132Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Guide01132Scene : NGSceneBase
{
  public Guide01132Menu menu;
  public bool one;

  public override IEnumerator onInitSceneAsync()
  {
    Guide01132Scene guide01132Scene = this;
    Future<GameObject> bgF = new ResourceObject("Prefabs/BackGround/UnitBackground_anim").Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    guide01132Scene.backgroundPrefab = bgF.Result;
  }

  public IEnumerator onStartSceneAsync(UnitUnit unit)
  {
    if (!this.one)
    {
      IEnumerator e = this.menu.onStartSceneAsync(unit, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.one = true;
    }
  }

  public void onStartScene(UnitUnit unit)
  {
    this.StartCoroutine(this.HideTipsLoading());
  }

  private IEnumerator HideTipsLoading()
  {
    yield return (object) new WaitForSeconds(0.1f);
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
