﻿// Decompiled with JetBrains decompiler
// Type: MypageEventButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class MypageEventButton : MonoBehaviour
{
  [SerializeField]
  private GameObject mBadge;

  public abstract bool IsActive();

  public abstract bool IsBadge();

  public void SetActive(bool value)
  {
    this.gameObject.SetActive(value);
  }

  public void SetBadgeActive(bool value)
  {
    if (!((Object) this.mBadge != (Object) null))
      return;
    this.mBadge.SetActive(value);
  }
}
