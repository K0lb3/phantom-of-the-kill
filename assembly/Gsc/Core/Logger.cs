﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Core.Logger
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.Network;
using UnityEngine;

namespace Gsc.Core
{
  public class Logger
  {
    private static bool initialized;

    public static event Application.LogCallback Callback;

    public static void Init()
    {
      if (Logger.initialized)
        return;
      Application.logMessageReceived -= new Application.LogCallback(Logger._HandleLog);
      Application.logMessageReceived += new Application.LogCallback(Logger._HandleLog);
      Logger.initialized = true;
    }

    public static void HandleLog(string logMessage, string stackTrace, LogType logType)
    {
      switch (logType)
      {
        case LogType.Error:
        case LogType.Assert:
        case LogType.Exception:
          UnityErrorLogSender.Instance.Send(logMessage, stackTrace, logType);
          break;
      }
    }

    private static void _HandleLog(string logMessage, string stackTrace, LogType logType)
    {
      Logger.HandleLog(logMessage, stackTrace, logType);
      if (Logger.Callback == null)
        return;
      Logger.Callback(logMessage, stackTrace, logType);
    }
  }
}
