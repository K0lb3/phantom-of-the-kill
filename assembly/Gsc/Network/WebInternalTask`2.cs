﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.WebInternalTask`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace Gsc.Network
{
  public class WebInternalTask<TRequest, TResponse> : WebInternalTask
    where TRequest : Request<TRequest, TResponse>
    where TResponse : Gsc.Network.Response<TResponse>
  {
    private readonly TRequest _request;
    private TResponse _response;
    private IErrorResponse _error;

    public TResponse Response
    {
      get
      {
        return this._response;
      }
    }

    public IErrorResponse error
    {
      get
      {
        return this._error;
      }
    }

    public WebInternalTask(Request<TRequest, TResponse> request)
      : base(request.GetMethod(), request.GetUrl(), request.GetPayload(), request.CustomHeaders)
    {
      this._request = (TRequest) request;
    }

    protected override WebTaskResult ProcessResponse(WebInternalResponse response)
    {
      return WebTask<TRequest, TResponse>.TryGetResponse(this._request, response, out this._response, out this._error);
    }
  }
}
