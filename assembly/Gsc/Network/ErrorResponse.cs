﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.ErrorResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Gsc.DOM;
using Gsc.DOM.Json;

namespace Gsc.Network
{
  public class ErrorResponse : Response<ErrorResponse>, IErrorResponse, IResponse
  {
    public IDocument data { get; private set; }

    public string ErrorCode { get; private set; }

    public ErrorResponse(byte[] payload)
    {
      if (payload.Length == 0)
        return;
      Document document = Document.Parse(payload);
      this.data = (IDocument) document;
      this.ErrorCode = document.Root.GetValueByPointer("/error_code", (string) null);
    }
  }
}
