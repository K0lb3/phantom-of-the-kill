﻿// Decompiled with JetBrains decompiler
// Type: Story00911Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Story00911Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private NGxScroll ScrollContainer;
  [SerializeField]
  private UI2DSprite DynCharacter;
  [SerializeField]
  private UI2DSprite DynCharacter2;
  [SerializeField]
  private GameObject DirNoStory;

  public IEnumerator Init(int id, int id2)
  {
    Story00911Menu story00911Menu = this;
    story00911Menu.TxtTitle.SetTextLocalize(Consts.Format(Consts.GetInstance().COMBI_QUEST_TITLE, (IDictionary) new Hashtable()
    {
      {
        (object) "name",
        (object) MasterData.UnitUnit[id].name
      },
      {
        (object) "name2",
        (object) MasterData.UnitUnit[id2].name
      }
    }));
    IEnumerator e = story00911Menu.SetCharacterLargeImage(id, id2);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    story00911Menu.ScrollContainer.Clear();
    e = story00911Menu.CreateEpisodes(id, id2);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    story00911Menu.ScrollContainer.ResolvePosition();
    // ISSUE: reference to a compiler-generated method
    story00911Menu.ScrollContainer.GridReposition(new UIGrid.OnReposition(story00911Menu.\u003CInit\u003Eb__5_0), true);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_10", false, (object[]) Array.Empty<object>());
  }

  private IEnumerator SetCharacterLargeImage(int id, int id2)
  {
    IEnumerator e = this.SetCharacterLargeImage(this.DynCharacter.transform, id);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.SetCharacterLargeImage(this.DynCharacter2.transform, id2);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator SetCharacterLargeImage(Transform trans, int id)
  {
    trans.Clear();
    UnitUnit unit = MasterData.UnitUnit[id];
    Future<GameObject> goFuture = unit.LoadMypage();
    IEnumerator e = goFuture.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = unit.SetLargeSpriteSnap(goFuture.Result.Clone(trans), 5);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator CreateEpisodes(int id, int id2)
  {
    Future<GameObject> prefabEpisodeF = Res.Prefabs.story009_6.story0096DirEpisode.Load<GameObject>();
    IEnumerator e = prefabEpisodeF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefabEpisode = prefabEpisodeF.Result;
    ((IEnumerable<PlayerHarmonyQuestS>) SMManager.Get<PlayerHarmonyQuestS[]>()).Where<PlayerHarmonyQuestS>((Func<PlayerHarmonyQuestS, bool>) (x => x.is_clear)).OrderBy<PlayerHarmonyQuestS, int>((Func<PlayerHarmonyQuestS, int>) (x => x.quest_harmony_s.ID)).ForEach<PlayerHarmonyQuestS>((System.Action<PlayerHarmonyQuestS>) (qu =>
    {
      if (qu.quest_harmony_s.unit.ID != id && qu.quest_harmony_s.unit.ID != id2 || qu.quest_harmony_s.target_unit.ID != id && qu.quest_harmony_s.target_unit.ID != id2)
        return;
      ((IEnumerable<StoryPlaybackHarmonyDetail>) MasterData.StoryPlaybackHarmonyDetailList).Where<StoryPlaybackHarmonyDetail>((Func<StoryPlaybackHarmonyDetail, bool>) (x => x.quest.ID == qu.quest_harmony_s.ID && x.timing_StoryPlaybackTiming != 2)).OrderBy<StoryPlaybackHarmonyDetail, int>((Func<StoryPlaybackHarmonyDetail, int>) (x => x.ID)).ForEach<StoryPlaybackHarmonyDetail>((System.Action<StoryPlaybackHarmonyDetail>) (story =>
      {
        GameObject gameObject = prefabEpisode.Clone((Transform) null);
        this.ScrollContainer.Add(gameObject, false);
        gameObject.GetComponent<Story0096EpisodeParts>().setData(qu, story, (NGMenuBase) this);
      }));
    }));
    this.DirNoStory.SetActive(this.ScrollContainer.grid.transform.childCount <= 0);
  }
}
