﻿// Decompiled with JetBrains decompiler
// Type: Battle01TipEventWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections.Generic;
using UnityEngine;

public class Battle01TipEventWindow : NGBattleMenuBase
{
  [SerializeField]
  private Battle01TipEventBase unit_exp;
  [SerializeField]
  private Battle01TipEventBase hp;
  [SerializeField]
  private Battle01TipEventBase gear;
  [SerializeField]
  private Battle01TipEventBase supply;
  [SerializeField]
  private Battle01TipEventBase medal;
  [SerializeField]
  private Battle01TipEventBase money;
  [SerializeField]
  private Battle01TipEventBase player_exp;
  [SerializeField]
  private Battle01TipEventBase skill_exp;
  [SerializeField]
  private Battle01TipEventBase unit_unit;
  [SerializeField]
  private Battle01TipEventBase battleMedal;
  [SerializeField]
  private Battle01TipEventTowerMedal towerMedal;
  [SerializeField]
  private Battle01TipEventGuildMedal guildMedal;

  public void open(BL.DropData e, BL.Unit unit)
  {
    Singleton<NGBattleManager>.GetInstance().isBattleEnable = false;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = true;
    foreach (KeyValuePair<MasterDataTable.CommonRewardType, Battle01TipEventBase> keyValuePair in new Dictionary<MasterDataTable.CommonRewardType, Battle01TipEventBase>()
    {
      {
        MasterDataTable.CommonRewardType.unit_exp,
        this.unit_exp
      },
      {
        MasterDataTable.CommonRewardType.gear,
        this.gear
      },
      {
        MasterDataTable.CommonRewardType.material_gear,
        this.gear
      },
      {
        MasterDataTable.CommonRewardType.gear_body,
        this.gear
      },
      {
        MasterDataTable.CommonRewardType.supply,
        this.supply
      },
      {
        MasterDataTable.CommonRewardType.medal,
        this.medal
      },
      {
        MasterDataTable.CommonRewardType.money,
        this.money
      },
      {
        MasterDataTable.CommonRewardType.player_exp,
        this.player_exp
      },
      {
        MasterDataTable.CommonRewardType.gear_experience_point,
        this.skill_exp
      },
      {
        MasterDataTable.CommonRewardType.unit,
        this.unit_unit
      },
      {
        MasterDataTable.CommonRewardType.material_unit,
        this.unit_unit
      },
      {
        MasterDataTable.CommonRewardType.battle_medal,
        this.battleMedal
      },
      {
        MasterDataTable.CommonRewardType.tower_medal,
        (Battle01TipEventBase) this.towerMedal
      },
      {
        MasterDataTable.CommonRewardType.guild_medal,
        (Battle01TipEventBase) this.guildMedal
      }
    })
    {
      if (keyValuePair.Key == e.reward.Type)
      {
        keyValuePair.Value.setData(e, unit);
        keyValuePair.Value.gameObject.SetActive(true);
      }
      else
        keyValuePair.Value.gameObject.SetActive(false);
    }
    this.GetComponent<NGTweenParts>().isActive = true;
  }

  public void dismiss()
  {
    this.GetComponent<NGTweenParts>().isActive = false;
    this.battleManager.isBattleEnable = true;
    Singleton<CommonRoot>.GetInstance().isTouchBlock = false;
  }
}
