﻿// Decompiled with JetBrains decompiler
// Type: Unit0046Indicator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnitStatusInformation;
using UnityEngine;

public class Unit0046Indicator : MonoBehaviour
{
  [SerializeField]
  private UILabel TxtLeaderskillDescription;
  [SerializeField]
  private UILabel TxtLeaderskillName;
  [SerializeField]
  private GameObject objLeaderSkillZoom;
  private System.Action popupLeaderSkillDetail_;

  public IEnumerator InitPlayerDeck(Player player, PlayerDeck playerDeck)
  {
    Unit0046Indicator unit0046Indicator = this;
    PlayerUnit playerUnit = ((IEnumerable<PlayerUnit>) playerDeck.player_units).FirstOrDefault<PlayerUnit>();
    if (playerUnit != (PlayerUnit) null)
    {
      if (playerUnit.leader_skill != null)
      {
        BattleskillSkill s = playerUnit.leader_skill.skill;
        unit0046Indicator.TxtLeaderskillName.SetTextLocalize(s.name);
        unit0046Indicator.TxtLeaderskillDescription.SetTextLocalize(s.description);
        Future<GameObject> loader = PopupSkillDetails.createPrefabLoader(Singleton<NGGameDataManager>.GetInstance().IsSea);
        yield return (object) loader.Wait();
        unit0046Indicator.popupLeaderSkillDetail_ = (System.Action) (() => PopupSkillDetails.show(loader.Result, new PopupSkillDetails.Param(s, UnitParameter.SkillGroup.Leader, new int?()), false, (System.Action) null, false));
        unit0046Indicator.objLeaderSkillZoom.SetActive(true);
      }
      else
      {
        unit0046Indicator.TxtLeaderskillName.SetTextLocalize(Consts.GetInstance().UNIT_0046_INDICATOR_INIT_PLAYER_DECK_1);
        unit0046Indicator.TxtLeaderskillDescription.SetTextLocalize(Consts.GetInstance().UNIT_0046_INDICATOR_INIT_PLAYER_DECK_2);
        unit0046Indicator.objLeaderSkillZoom.SetActive(false);
      }
    }
    else
    {
      unit0046Indicator.TxtLeaderskillName.SetTextLocalize(Consts.GetInstance().UNIT_0046_INDICATOR_INIT_PLAYER_DECK_3);
      unit0046Indicator.TxtLeaderskillDescription.SetTextLocalize(Consts.GetInstance().UNIT_0046_INDICATOR_INIT_PLAYER_DECK_4);
      unit0046Indicator.objLeaderSkillZoom.SetActive(false);
    }
    IEnumerator e = unit0046Indicator.GetComponent<Unit0046Indicator_menu>().SetStatus(playerDeck);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onClickedSkillZoom()
  {
    System.Action leaderSkillDetail = this.popupLeaderSkillDetail_;
    if (leaderSkillDetail == null)
      return;
    leaderSkillDetail();
  }
}
