﻿// Decompiled with JetBrains decompiler
// Type: Bugu00524Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;

public class Bugu00524Scene : NGSceneBase
{
  public Bugu00524Menu menu;

  public override IEnumerator onInitSceneAsync()
  {
    yield break;
  }

  public static void ChangeScene(bool stack)
  {
    bool flag = false;
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_repair", (stack ? 1 : 0) != 0, (object) flag);
  }

  public static void ChangeSceneFromExplore(bool stack)
  {
    bool flag = true;
    Singleton<NGSceneManager>.GetInstance().changeScene("bugu005_repair", (stack ? 1 : 0) != 0, (object) flag);
  }

  public virtual IEnumerator onStartSceneAsync(bool isFromExplore)
  {
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    IEnumerator e = this.menu.Init();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.menu.isFromExplore = isFromExplore;
  }

  public virtual void onStartScene(bool isFromExplore)
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<CommonRoot>.GetInstance().isActiveHeader = true;
    Singleton<CommonRoot>.GetInstance().isActiveFooter = true;
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onEndScene()
  {
    Persist.sortOrder.Flush();
    this.menu.onEndScene();
    ItemIcon.ClearCache();
    this.GetComponentInChildren<NGxScroll2>().scrollView.Press(false);
  }
}
