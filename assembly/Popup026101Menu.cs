﻿// Decompiled with JetBrains decompiler
// Type: Popup026101Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Popup026101Menu : BackButtonMenuBase
{
  [SerializeField]
  private UILabel title;
  [SerializeField]
  private UILabel message;

  public virtual void IbtnBack()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public virtual void IbtnRecover()
  {
    this.StartCoroutine(this.ShowBPAlertPopup());
  }

  public void IbtnNo()
  {
    this.IbtnBack();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  private IEnumerator ShowBPAlertPopup()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    this.title.SetTextLocalize(Consts.GetInstance().PVP_CLASS_MATCH_POPUP_101_TITLE);
    this.message.SetTextLocalize(Consts.GetInstance().PVP_CLASS_MATCH_POPUP_101_MESSAGE);
    Future<GameObject> prefabf = Res.Prefabs.popup.popup_026_10_2__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefabf.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup026102Menu>().Init();
  }
}
