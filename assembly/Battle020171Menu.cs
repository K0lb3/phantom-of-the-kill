﻿// Decompiled with JetBrains decompiler
// Type: Battle020171Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Battle020171Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtExplanation24;
  [SerializeField]
  protected UILabel TxtLvAfter28;
  [SerializeField]
  protected UILabel TxtLvbefore28;
  [SerializeField]
  protected UILabel TxtPlayername30;
  private System.Action onCallback;

  private void Start()
  {
    Singleton<NGSoundManager>.GetInstance().playSE("SE_1022", false, 0.0f, -1);
    Singleton<NGSoundManager>.GetInstance().PlayBgm("bgm016", 1, 0.0f, 0.5f, 0.5f);
  }

  private void OnDestroy()
  {
    Singleton<NGSoundManager>.GetInstance().StopBgm(1, 0.5f);
  }

  public void SetLv(int before, int after)
  {
    this.TxtLvbefore28.SetTextLocalize(before);
    this.TxtLvAfter28.SetTextLocalize(after);
  }

  public void SetName(string name)
  {
    this.TxtPlayername30.SetTextLocalize(name);
  }

  public void SetExplanetion(string str)
  {
    this.TxtExplanation24.SetTextLocalize(str);
  }

  public void SetCallback(System.Action callback)
  {
    this.onCallback = callback;
  }

  public virtual void IbtnScreen()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    if (this.onCallback == null)
      return;
    this.onCallback();
  }

  public override void onBackButton()
  {
    this.showBackKeyToast();
  }
}
