﻿// Decompiled with JetBrains decompiler
// Type: ExploreTask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public abstract class ExploreTask
{
  private float mStartTime;
  private string mRandState;

  protected long ProcTime
  {
    get
    {
      return (double) this.mStartTime == 0.0 ? 0L : (long) (((double) Time.realtimeSinceStartup - (double) this.mStartTime) * 1000.0);
    }
  }

  public bool IsLoaded { get; protected set; }

  public bool IsFinished { get; protected set; }

  protected void SetStartTime()
  {
    this.mStartTime = Time.realtimeSinceStartup;
  }

  public void SetRandState()
  {
    this.mRandState = Singleton<ExploreLotteryCore>.GetInstance().GetRandomCount().ToString();
  }

  public string GetRandState()
  {
    return this.mRandState;
  }

  public abstract Explore.STATE State();

  public abstract IEnumerator LoadAsync();

  public abstract IEnumerator UpdateAsync();

  public abstract void PayOut();

  public abstract long GetReqiredTime();

  public abstract long GetRestReqiredTime();

  public abstract long GetTakeOverTime();

  public abstract void OnBackExplore();

  public abstract long OnBackGroundWork(long calcTime);

  public abstract void OnContinue(long restReqTime, long takeOverTime);
}
