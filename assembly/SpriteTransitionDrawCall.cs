﻿// Decompiled with JetBrains decompiler
// Type: SpriteTransitionDrawCall
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

[ExecuteInEditMode]
public class SpriteTransitionDrawCall : MonoBehaviour
{
  [Header("Dissolve Mask")]
  public Color color = Color.white;
  public float xScale = 1f;
  public float yScale = 1f;
  public float xScaleMain = 1f;
  public float yScaleMain = 1f;
  public UI2DSprite sprite;
  public UIDrawCall drawcall;
  public bool brighten;
  [Tooltip("Dissolve Mask")]
  public Texture2D mask;
  [Range(0.0f, 1f)]
  public float width;
  [Range(0.0f, 1f)]
  public float disolvePower;
  [Header("Alpha Mask")]
  [Tooltip("Alpha Mask")]
  public Texture2D alphaMask;
  public float xOffset;
  public float yOffset;
  public float xOffsetMain;
  public float yOffsetMain;

  private void Update()
  {
    this.RemoveIfNotUsed();
  }

  private void RemoveIfNotUsed()
  {
    if (!((Object) this.drawcall == (Object) null) && !((Object) this.drawcall != (Object) this.sprite.drawCall))
      return;
    Object.Destroy((Object) this);
  }

  private void OnWillRenderObject()
  {
    if ((Object) this.drawcall == (Object) null)
      return;
    if ((Object) this.drawcall.dynamicMaterial == (Object) null)
      Debug.Log((object) "Material is null.");
    this.drawcall.dynamicMaterial.SetColor("_MainColor", this.color);
    this.drawcall.dynamicMaterial.SetFloat("_Brighten", this.brighten ? 0.0f : 1f);
    this.drawcall.dynamicMaterial.SetTexture("_Mask", (Texture) this.mask);
    this.drawcall.dynamicMaterial.SetFloat("_Width", this.width);
    this.drawcall.dynamicMaterial.SetFloat("_DisolvePower", this.disolvePower);
    if ((Object) this.alphaMask != (Object) null)
      this.drawcall.dynamicMaterial.SetTexture("_AlphaMask", (Texture) this.alphaMask);
    this.drawcall.dynamicMaterial.SetFloat("_xScale", this.xScale);
    this.drawcall.dynamicMaterial.SetFloat("_yScale", this.yScale);
    this.drawcall.dynamicMaterial.SetFloat("_xOffset", this.xOffset);
    this.drawcall.dynamicMaterial.SetFloat("_yOffset", this.yOffset);
    this.drawcall.dynamicMaterial.SetFloat("_xScaleMain", this.xScaleMain);
    this.drawcall.dynamicMaterial.SetFloat("_yScaleMain", this.yScaleMain);
    this.drawcall.dynamicMaterial.SetFloat("_xOffsetMain", this.xOffsetMain);
    this.drawcall.dynamicMaterial.SetFloat("_yOffsetMain", this.yOffsetMain);
  }
}
