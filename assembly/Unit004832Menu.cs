﻿// Decompiled with JetBrains decompiler
// Type: Unit004832Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit004832Menu : BackButtonMenuBase
{
  [SerializeField]
  private Vector3 TextYPos1;
  [SerializeField]
  private Vector3 TextYPos2;
  [SerializeField]
  protected UILabel TxtDescription1;
  [SerializeField]
  protected UILabel TxtDescription2;
  private List<PlayerUnit> selectedBasePlayerUnits;
  private List<List<PlayerUnit>> selectedMaterialPlayerUnits;
  private PlayerUnit[] selectUnits;
  private PlayerUnit baseUnit;
  [NonSerialized]
  public System.Action callbackNo;
  private System.Action callbackYes;
  private Func<List<PlayerUnit>, WebAPI.Response.UnitBuildup, Dictionary<string, object>> resultFuncBuildup;

  public Unit00468Scene.Mode mode { get; set; }

  public void Init(bool isAlert, bool isMemoryAlert, System.Action actionYes)
  {
    this.callbackYes = actionYes;
    this.InitCommon(isAlert, isMemoryAlert);
  }

  public void Init(
    Func<List<PlayerUnit>, WebAPI.Response.UnitBuildup, Dictionary<string, object>> func,
    PlayerUnit basePlayerUnit,
    PlayerUnit[] materialUnit,
    bool isAlert,
    bool isMemoryAlert)
  {
    this.resultFuncBuildup = func;
    this.baseUnit = basePlayerUnit;
    this.selectUnits = materialUnit;
    this.InitCommon(isAlert, isMemoryAlert);
  }

  public void Init(
    List<PlayerUnit> selectedBasePlayerUnits,
    List<List<PlayerUnit>> selectedMaterialPlayerUnits,
    bool isAlert,
    bool isMemoryAlert)
  {
    this.selectedBasePlayerUnits = selectedBasePlayerUnits;
    this.selectedMaterialPlayerUnits = selectedMaterialPlayerUnits;
    this.Init((Func<List<PlayerUnit>, WebAPI.Response.UnitBuildup, Dictionary<string, object>>) null, (PlayerUnit) null, (PlayerUnit[]) null, isAlert, isMemoryAlert);
  }

  private void InitCommon(bool isAlert, bool isMemoryAlert)
  {
    if (isAlert & isMemoryAlert)
    {
      this.TxtDescription1.transform.localPosition = this.TextYPos1;
      this.TxtDescription1.gameObject.SetActive(true);
      this.TxtDescription2.transform.localPosition = this.TextYPos2;
      this.TxtDescription2.gameObject.SetActive(true);
    }
    else if (isAlert)
    {
      this.TxtDescription1.transform.localPosition = this.TextYPos1;
      this.TxtDescription1.gameObject.SetActive(true);
      this.TxtDescription2.gameObject.SetActive(false);
    }
    else if (isMemoryAlert)
    {
      this.TxtDescription2.transform.localPosition = this.TextYPos1;
      this.TxtDescription2.gameObject.SetActive(true);
      this.TxtDescription1.gameObject.SetActive(false);
    }
    else
    {
      this.TxtDescription1.transform.localPosition = this.TextYPos1;
      this.TxtDescription1.gameObject.SetActive(true);
      this.TxtDescription1.text = "選択した姫を統合します。\n本当に実行してよろしいですか？";
      this.TxtDescription2.gameObject.SetActive(false);
    }
  }

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    if (this.callbackNo != null)
    {
      this.callbackNo();
      this.callbackNo = (System.Action) null;
    }
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnPopupYes()
  {
    if (this.IsPushAndSet())
      return;
    if (this.callbackYes != null)
    {
      this.callbackYes();
    }
    else
    {
      switch (this.mode)
      {
        case Unit00468Scene.Mode.Unit00420:
          this.StartCoroutine(this.buildup());
          break;
        case Unit00468Scene.Mode.UnitLumpTouta:
          this.StartCoroutine(this.lumpcCombine());
          break;
        default:
          Debug.LogError((object) ("Unit004832Menu: 想定されたmodeでありません. mode is " + (object) this.mode));
          break;
      }
    }
  }

  private IEnumerator buildup()
  {
    Unit00484Menu.endSequenceCombine();
    if (!Singleton<CommonRoot>.GetInstance().isLoading)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      List<int> intList1 = new List<int>();
      List<int> intList2 = new List<int>();
      for (int index = 0; index < this.selectUnits.Length; ++index)
      {
        intList1.Add(this.selectUnits[index].id);
        intList2.Add(this.selectUnits[index].UnitIconInfo.SelectedCount);
      }
      Future<WebAPI.Response.UnitBuildup> paramF = WebAPI.UnitBuildup(this.baseUnit.id, intList1.ToArray(), intList2.ToArray(), (System.Action<WebAPI.Response.UserError>) (error =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e = paramF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      WebAPI.Response.UnitBuildup result = paramF.Result;
      if (result != null)
      {
        PlayerUnit playerUnit1 = (PlayerUnit) null;
        foreach (PlayerUnit playerUnit2 in SMManager.Get<PlayerUnit[]>())
        {
          if (playerUnit2.id == this.baseUnit.id)
          {
            playerUnit1 = playerUnit2;
            break;
          }
        }
        List<PlayerUnit> num_list = new List<PlayerUnit>();
        num_list.AddRange((IEnumerable<PlayerUnit>) this.selectUnits);
        List<PlayerUnit> result_list = new List<PlayerUnit>();
        result_list.Add(this.baseUnit);
        result_list.Add(playerUnit1);
        List<int> other_list = new List<int>();
        other_list.Add(1);
        other_list.Add(result.increment_medal);
        other_list.Add(0);
        other_list.Add(0);
        Dictionary<string, object> showPopupData = this.resultFuncBuildup(result_list, result);
        unit004812Scene.changeScene(false, num_list, result_list, other_list, showPopupData, this.mode);
        paramF = (Future<WebAPI.Response.UnitBuildup>) null;
      }
    }
  }

  private IEnumerator lumpcCombine()
  {
    Unit004832Menu unit004832Menu = this;
    Singleton<PopupManager>.GetInstance().closeAll(false);
    if (Singleton<CommonRoot>.GetInstance().isLoading)
    {
      unit004832Menu.IsPush = false;
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      List<PlayerUnit> requestSelectedBasePlayerUnits = new List<PlayerUnit>();
      List<List<PlayerUnit>> playerUnitListList = new List<List<PlayerUnit>>();
      for (int index = 0; index < unit004832Menu.selectedBasePlayerUnits.Count; ++index)
      {
        if (unit004832Menu.selectedMaterialPlayerUnits[index].Count > 0)
        {
          requestSelectedBasePlayerUnits.Add(unit004832Menu.selectedBasePlayerUnits[index]);
          playerUnitListList.Add(unit004832Menu.selectedMaterialPlayerUnits[index]);
        }
      }
      int[] mpu_ids0 = new int[0];
      if (1 <= playerUnitListList.Count)
        mpu_ids0 = playerUnitListList[0].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids1 = new int[0];
      if (2 <= playerUnitListList.Count)
        mpu_ids1 = playerUnitListList[1].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids2 = new int[0];
      if (3 <= playerUnitListList.Count)
        mpu_ids2 = playerUnitListList[2].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids3 = new int[0];
      if (4 <= playerUnitListList.Count)
        mpu_ids3 = playerUnitListList[3].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids4 = new int[0];
      if (5 <= playerUnitListList.Count)
        mpu_ids4 = playerUnitListList[4].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids5 = new int[0];
      if (6 <= playerUnitListList.Count)
        mpu_ids5 = playerUnitListList[5].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids6 = new int[0];
      if (7 <= playerUnitListList.Count)
        mpu_ids6 = playerUnitListList[6].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids7 = new int[0];
      if (8 <= playerUnitListList.Count)
        mpu_ids7 = playerUnitListList[7].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids8 = new int[0];
      if (9 <= playerUnitListList.Count)
        mpu_ids8 = playerUnitListList[8].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      int[] mpu_ids9 = new int[0];
      if (10 <= playerUnitListList.Count)
        mpu_ids9 = playerUnitListList[9].Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
      Future<WebAPI.Response.UnitLumpCompose> paramF = WebAPI.UnitLumpCompose(requestSelectedBasePlayerUnits.Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>(), mpu_ids0, mpu_ids1, mpu_ids2, mpu_ids3, mpu_ids4, mpu_ids5, mpu_ids6, mpu_ids7, mpu_ids8, mpu_ids9, (System.Action<WebAPI.Response.UserError>) null);
      IEnumerator e = paramF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      WebAPI.Response.UnitLumpCompose result = paramF.Result;
      if (result == null)
      {
        unit004832Menu.IsPush = false;
      }
      else
      {
        Singleton<NGGameDataManager>.GetInstance().clearPreviewInheritance(0);
        PlayerUnit[] playerUnitArray = SMManager.Get<PlayerUnit[]>();
        List<Unit004832Menu.ResultPlayerUnit> resultPlayerUnitList = new List<Unit004832Menu.ResultPlayerUnit>();
        foreach (PlayerUnit beforePlayerUnit in requestSelectedBasePlayerUnits)
        {
          foreach (PlayerUnit afterPlayerUnit in playerUnitArray)
          {
            if (afterPlayerUnit.id == beforePlayerUnit.id)
            {
              resultPlayerUnitList.Add(new Unit004832Menu.ResultPlayerUnit(beforePlayerUnit, afterPlayerUnit));
              break;
            }
          }
        }
        Singleton<NGSceneManager>.GetInstance().clearStack("unit004_LumpTouta");
        Singleton<NGSceneManager>.GetInstance().destroyScene("unit004_LumpTouta_Confirmation");
        Singleton<NGSceneManager>.GetInstance().changeScene("unit004_LumpTouta_Result", false, (object) resultPlayerUnitList, (object) result.increment_medal, (object) result.gain_trust_results, (object) result.unlock_quests);
        unit004832Menu.IsPush = false;
      }
    }
  }

  public class ResultPlayerUnit
  {
    public PlayerUnit beforePlayerUnit;
    public PlayerUnit afterPlayerUnit;

    public ResultPlayerUnit(PlayerUnit beforePlayerUnit, PlayerUnit afterPlayerUnit)
    {
      this.beforePlayerUnit = beforePlayerUnit;
      this.afterPlayerUnit = afterPlayerUnit;
    }
  }

  public class OhterInfo
  {
    public bool is_success;
    public int increment_medal;
    public GainTrustResult gain_trust_result;

    public OhterInfo(bool is_success, int increment_medal, GainTrustResult gain_trust_result)
    {
      this.is_success = is_success;
      this.increment_medal = increment_medal;
      this.gain_trust_result = gain_trust_result;
    }
  }
}
