﻿// Decompiled with JetBrains decompiler
// Type: StatusInBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class StatusInBattle
{
  private string loaded_dir_inbattle_effect_container = string.Empty;
  [SerializeField]
  private GameObject dir_inbattle_effect_container;
  [SerializeField]
  private string dir_inbattle_effect_container_prefab_name;
  public List<SpriteNumberSelectDirect> slc_Remain_hours;
  public List<SpriteNumberSelectDirect> slc_Remain_minutes;
  [SerializeField]
  private GuildStatus MyStatus;
  [SerializeField]
  private GuildStatus EnStatus;

  public IEnumerator ResourceLoad(GuildRegistration myData, GuildRegistration enData)
  {
    IEnumerator e = this.MyStatus.ResourceLoad(myData);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = this.EnStatus.ResourceLoad(enData);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject obj = this.dir_inbattle_effect_container;
    string objName = this.dir_inbattle_effect_container_prefab_name;
    string path = string.Format("Prefabs/guild028_2/{0}", (object) objName);
    if (!(this.loaded_dir_inbattle_effect_container == objName))
    {
      Future<GameObject> f = Singleton<ResourceManager>.GetInstance().Load<GameObject>(path, 1f);
      e = f.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.loaded_dir_inbattle_effect_container = objName;
      f.Result.Clone(obj.transform);
    }
  }

  public void SetStatus(GuildRegistration myData, GuildRegistration enData)
  {
    this.MyStatus.SetStatus(myData);
    this.EnStatus.SetStatus(enData);
  }

  public void UpdateStatus(GuildRegistration myData, GuildRegistration enData)
  {
    this.MyStatus.UpdateStatus(myData);
    this.EnStatus.UpdateStatus(enData);
  }
}
