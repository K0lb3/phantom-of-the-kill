﻿// Decompiled with JetBrains decompiler
// Type: Earth.EarthQuestKeyPlayData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections.Generic;

namespace Earth
{
  [Serializable]
  public class EarthQuestKeyPlayData : BL.ModelBase
  {
    private static readonly string serverDataFormat = "{{\"keyID\":{0},\"isOpen\":{1},\"playCount\":{2}}}";
    private int mKeyID;
    private bool mIsOpen;
    private int mPlayCount;

    public int ID
    {
      get
      {
        return this.mKeyID;
      }
      set
      {
        this.mKeyID = value;
      }
    }

    public bool Open
    {
      get
      {
        return this.mIsOpen;
      }
      set
      {
        this.mIsOpen = value;
      }
    }

    public int PlayCount
    {
      get
      {
        return this.mPlayCount;
      }
      set
      {
        this.mPlayCount = value;
      }
    }

    public string GetSeverString()
    {
      return string.Format(EarthQuestKeyPlayData.serverDataFormat, (object) this.mKeyID, (object) (this.mIsOpen ? 1 : 0), (object) this.mPlayCount);
    }

    public static EarthQuestKeyPlayData JsonLoad(
      Dictionary<string, object> json)
    {
      return new EarthQuestKeyPlayData()
      {
        mKeyID = (int) (long) json["keyID"],
        mIsOpen = (uint) (int) (long) json["isOpen"] > 0U,
        mPlayCount = (int) (long) json["playCount"]
      };
    }
  }
}
