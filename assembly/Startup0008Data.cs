﻿// Decompiled with JetBrains decompiler
// Type: Startup0008Data
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;

public class Startup0008Data
{
  public string titleAgreement;
  public string agreementHedder;
  public string agreement;
  public string titleDissent;
  public string dissent;

  public IEnumerator Initialize()
  {
    Future<WebAPI.Response.Agreement> request = WebAPI.Agreement((System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = request.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.titleAgreement = request.Result.agreement_title;
    this.agreementHedder = request.Result.agreement_header;
    this.agreement = request.Result.agreement;
    this.titleDissent = request.Result.not_agreement_title;
    this.dissent = request.Result.not_agreement;
  }

  public string I2Converter(string key, string defaultValue)
  {
    return defaultValue;
  }
}
