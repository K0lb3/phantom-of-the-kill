﻿// Decompiled with JetBrains decompiler
// Type: Gacha00613Icon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Gacha00613Icon : MonoBehaviour
{
  public Gacha00613Scene Scene;
  public int Number;
  public bool is_new;

  public void IbtnIcon()
  {
    if (!this.Scene.Menu.IsBtnAction)
      return;
    GachaResultData instance = GachaResultData.GetInstance();
    this.is_new = instance.GetData().GetResultData()[this.Number].is_new;
    CommonRewardType commonRewardType = new CommonRewardType(instance.GetData().GetResultData()[this.Number].reward_type_id, instance.GetData().GetResultData()[this.Number].reward_result_id, instance.GetData().GetResultData()[this.Number].reward_result_quantity, instance.GetData().GetResultData()[this.Number].is_new, instance.GetData().GetResultData()[this.Number].is_reserves);
    commonRewardType.ThenUnit((System.Action<PlayerUnit>) (unit => this.ChangeSceneUnit(unit)));
    commonRewardType.ThenMaterialUnit((System.Action<PlayerMaterialUnit>) (unit => this.ChangeSceneMaterialUnit(unit)));
    commonRewardType.ThenGear((System.Action<PlayerItem>) (gear => this.ChangeSceneGear(gear)));
    commonRewardType.ThenMaterialGear((System.Action<PlayerMaterialGear>) (materialGear => this.ChangeSceneGear(materialGear)));
  }

  public void ChangeSceneUnit(PlayerUnit PU)
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, false, false, "SE_1006");
    if (PU.unit.IsMaterialUnit)
      Unit00493Scene.changeScene(true, PU.unit, this.is_new, true);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_8", (GachaResultData.GetInstance().GetData() == null || GachaResultData.GetInstance().GetData().additionalItems == null || GachaResultData.GetInstance().GetData().additionalItems.Length == 0 ? 1 : 0) != 0, (object) PU, (object) this.is_new);
  }

  public void ChangeSceneMaterialUnit(PlayerMaterialUnit PU)
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, false, false, "SE_1006");
    if (PU.unit.IsMaterialUnit)
      Unit00493Scene.changeScene(true, PU.unit, this.is_new, true);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_8", (GachaResultData.GetInstance().GetData() == null || GachaResultData.GetInstance().GetData().additionalItems == null || GachaResultData.GetInstance().GetData().additionalItems.Length == 0 ? 1 : 0) != 0, (object) PU, (object) this.is_new);
  }

  public void ChangeSceneGear(PlayerItem PI)
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, false, false, "SE_1006");
    if (!PI.gear.kind.isEquip)
      Bugu00561Scene.changeScene(true, new GameCore.ItemInfo(PI), this.is_new, true, true);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_11", true, (object) this.is_new, (object) new GameCore.ItemInfo(PI));
  }

  public void ChangeSceneGear(PlayerMaterialGear PI)
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, false, false, "SE_1006");
    if (!PI.gear.kind.isEquip)
      Bugu00561Scene.changeScene(true, new GameCore.ItemInfo(PI, 0), this.is_new, true, true);
    else
      Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_11", true, (object) this.is_new, (object) new GameCore.ItemInfo(PI, 0));
  }
}
