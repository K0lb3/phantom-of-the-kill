﻿// Decompiled with JetBrains decompiler
// Type: ReisouSkillDetail_01
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReisouSkillDetail_01 : MonoBehaviour
{
  private bool? isSea_;
  [SerializeField]
  protected UIDragScrollView uiDragScrollView;
  [SerializeField]
  protected UIDragScrollView uiDragScrollViewDetail;
  [SerializeField]
  protected UI2DSprite iconSkill;
  [SerializeField]
  protected UI2DSprite iconGenreL;
  [SerializeField]
  protected UI2DSprite iconGenreR;
  [SerializeField]
  protected UILabel txtSkillName;
  [SerializeField]
  protected UILabel txtSkillLv;
  [SerializeField]
  protected UILabel txtSkillDescription;
  [SerializeField]
  protected UILabel txtReleaseRank;
  private PopupSkillDetails.Param[] skillParams_;
  private GearGearSkill skill_;
  private GameObject skillDialogPrefab;
  private bool isPush;

  private bool isSea
  {
    get
    {
      return this.isSea_ ?? (this.isSea_ = new bool?(Singleton<NGGameDataManager>.GetInstance().IsSea)).Value;
    }
  }

  public IEnumerator Init(
    UIScrollView scrollView,
    GearGearSkill skill,
    PopupSkillDetails.Param[] skillParams,
    bool is_release)
  {
    this.uiDragScrollView.scrollView = scrollView;
    this.uiDragScrollViewDetail.scrollView = scrollView;
    this.skill_ = skill;
    this.skillParams_ = skillParams;
    yield return (object) this.doLoadSkill(skill.skill);
    this.txtSkillName.SetTextLocalize(skill.skill.name);
    this.txtSkillLv.SetTextLocalize(skill.skill_level);
    this.txtSkillDescription.SetTextLocalize(skill.skill.description);
    if (is_release)
      this.txtReleaseRank.gameObject.SetActive(false);
    else
      this.txtReleaseRank.SetTextLocalize(Consts.GetInstance().UNIT_0044_REISOU_RELEASE_RANK.F((object) skill.release_rank));
  }

  private IEnumerator doLoadSkill(BattleskillSkill s)
  {
    Future<UnityEngine.Sprite> ld = s.LoadBattleSkillIcon((BattleFuncs.InvestSkill) null);
    yield return (object) ld.Wait();
    this.iconSkill.sprite2D = ld.Result;
    ld = (Future<UnityEngine.Sprite>) null;
    this.loadGenre(s.genre1, s.genre2);
  }

  private void loadGenre(BattleskillGenre? gL, BattleskillGenre? gR)
  {
    this.iconGenreL.sprite2D = gL.HasValue ? SkillGenreIcon.loadSprite(gL.Value, this.isSea) : (UnityEngine.Sprite) null;
    this.iconGenreR.sprite2D = gR.HasValue ? SkillGenreIcon.loadSprite(gR.Value, this.isSea) : (UnityEngine.Sprite) null;
  }

  public void onBtnDetail()
  {
    if (this.isPush)
      return;
    this.StartCoroutine(this.openSkillDaialog());
  }

  private IEnumerator openSkillDaialog()
  {
    ReisouSkillDetail_01 reisouSkillDetail01 = this;
    if ((UnityEngine.Object) reisouSkillDetail01.skillDialogPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> loader = PopupSkillDetails.createPrefabLoader(reisouSkillDetail01.isSea);
      yield return (object) loader.Wait();
      reisouSkillDetail01.skillDialogPrefab = loader.Result;
      loader = (Future<GameObject>) null;
    }
    // ISSUE: reference to a compiler-generated method
    PopupSkillDetails.show(reisouSkillDetail01.skillDialogPrefab, reisouSkillDetail01.skillParams_, ((IEnumerable<PopupSkillDetails.Param>) reisouSkillDetail01.skillParams_).FirstIndexOrNull<PopupSkillDetails.Param>(new Func<PopupSkillDetails.Param, bool>(reisouSkillDetail01.\u003CopenSkillDaialog\u003Eb__20_0)).Value, false, (System.Action) null, false);
    reisouSkillDetail01.isPush = false;
  }
}
