﻿// Decompiled with JetBrains decompiler
// Type: Shop00720Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Shop00720Scene : NGSceneBase
{
  [SerializeField]
  private Shop00720Menu Menu;

  public IEnumerator onStartSceneAsync()
  {
    Shop00720Scene scene = this;
    Debug.Log((object) "★★★ onInitSceneAsync ★★★");
    IEnumerator e = scene.onStartSceneAsync(scene);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(Shop00720Scene scene)
  {
    IEnumerator e1 = this.Menu.Initialize(scene);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (DateTime.Now > Singleton<NGGameDataManager>.GetInstance().lastSlotTime.AddMinutes(10.0) || DateTime.Now.Hour != Singleton<NGGameDataManager>.GetInstance().lastSlotTime.Hour)
    {
      Future<WebAPI.Response.Slot> future = WebAPI.Slot((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = future.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (future.Result != null)
      {
        Singleton<NGGameDataManager>.GetInstance().lastSlotTime = DateTime.Now;
        future = (Future<WebAPI.Response.Slot>) null;
      }
    }
  }

  public void onStartScene()
  {
    Debug.Log((object) "★★★ onStartScene ★★★");
    this.Menu.Ready();
  }

  public override void onSceneInitialized()
  {
    Debug.Log((object) "★★★ onSceneInitialized ★★★");
  }

  public override void onEndScene()
  {
    Debug.Log((object) "★★★ onEndScene ★★★");
  }
}
