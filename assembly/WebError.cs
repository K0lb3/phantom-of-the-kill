﻿// Decompiled with JetBrains decompiler
// Type: WebError
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

public class WebError
{
  public readonly WebResponse Response;
  public readonly WebRequest Request;
  public readonly int Status;

  public WebError(WebRequest request, WebResponse response)
  {
    this.Request = request;
    this.Response = response;
    this.Status = response.Status;
  }

  private WebError(WebRequest request, string message)
  {
    this.Request = request;
    this.Response = WebResponse.Zero();
    this.Response.Body = message;
    this.Status = this.Response.Status;
  }

  public bool HasResponse()
  {
    return this.Status > 0;
  }

  public bool IsClientError()
  {
    return this.Status >= 400 && this.Status < 500;
  }

  public bool IsServerError()
  {
    return this.Status >= 500 && this.Status < 600;
  }

  public string Show()
  {
    return string.Format("{0} {1} {2}", (object) this.Status, (object) this.Request.Path, (object) this.Response.Body);
  }

  public static WebError ClientError4xx(WebRequest request, WebResponse response)
  {
    return new WebError(request, response);
  }

  public static WebError ServerError5xx(WebRequest request, WebResponse response)
  {
    return new WebError(request, response);
  }

  public static WebError Timeout(WebRequest request)
  {
    return new WebError(request, nameof (Timeout));
  }

  public static WebError RetryOut(WebRequest request)
  {
    return new WebError(request, nameof (RetryOut));
  }

  public static WebError ResponseError(WebRequest request)
  {
    return new WebError(request, nameof (ResponseError));
  }

  public static WebError ResponseException(WebRequest request, Exception exception)
  {
    return new WebError(request, "InternalError");
  }
}
