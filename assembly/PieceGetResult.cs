﻿// Decompiled with JetBrains decompiler
// Type: PieceGetResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class PieceGetResult
{
  public string albumName;
  public string pieceName;
  public int count;
  public int same_character_id;

  public PieceGetResult(string album_name, string piece_name, int count, int same_character_id)
  {
    this.albumName = album_name;
    this.pieceName = piece_name;
    this.count = count;
    this.same_character_id = same_character_id;
  }
}
