﻿// Decompiled with JetBrains decompiler
// Type: ResourceObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;

public class ResourceObject
{
  private string path;

  public ResourceObject(string path)
  {
    this.path = path;
  }

  public Future<T> Load<T>() where T : UnityEngine.Object
  {
    return Singleton<ResourceManager>.GetInstance().Load<T>(this.path, 1f);
  }

  public Future<T> Load<T>(System.Action<T> callback) where T : UnityEngine.Object
  {
    return Singleton<ResourceManager>.GetInstance().Load<T>(this.path, callback);
  }
}
