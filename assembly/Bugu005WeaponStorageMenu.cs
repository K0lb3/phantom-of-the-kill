﻿// Decompiled with JetBrains decompiler
// Type: Bugu005WeaponStorageMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using UniLinq;

public class Bugu005WeaponStorageMenu : Bugu005ItemListMenuBase
{
  private bool needClearCache = true;

  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.bugu005WeaponMaterialListSortAndFilter;
  }

  protected override List<PlayerMaterialGear> GetMaterialList()
  {
    return ((IEnumerable<PlayerMaterialGear>) SMManager.Get<PlayerMaterialGear[]>()).Where<PlayerMaterialGear>((Func<PlayerMaterialGear, bool>) (x => x.isWeaponMaterial())).ToList<PlayerMaterialGear>();
  }

  protected virtual void OnEnable()
  {
    if (!this.scroll.scrollView.isDragging)
      return;
    this.scroll.scrollView.Press(false);
  }

  public void onBackScene()
  {
    if ((UnityEngine.Object) this.SortPopupPrefab != (UnityEngine.Object) null)
      this.SortPopupPrefab.GetComponent<ItemSortAndFilter>().Initialize((Bugu005ItemListMenuBase) this, false);
    this.Sort(this.SortCategory, this.OrderBuySort, this.isEquipFirst);
    this.needClearCache = true;
  }

  public override void onEndScene()
  {
    base.onEndScene();
    Persist.sortOrder.Flush();
    if (!this.needClearCache)
      return;
    ItemIcon.ClearCache();
  }

  public void IbtnWeaponList()
  {
    if (this.IsPushAndSet())
      return;
    Bugu0052Scene.ChangeScene(false);
  }

  public void IbtnSell()
  {
    if (this.IsPushAndSet())
      return;
    this.needClearCache = false;
    Bugu00525Scene.ChangeScene(true, Bugu00525Scene.Mode.WeaponMaterial);
  }

  public void IbtnConversion()
  {
    if (this.IsPushAndSet())
      return;
    this.needClearCache = false;
    Bugu005WeaponMaterialConversionScene.ChangeScene(true, Bugu005WeaponMaterialConversionScene.Mode.WeaponMaterial);
  }
}
