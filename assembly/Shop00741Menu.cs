﻿// Decompiled with JetBrains decompiler
// Type: Shop00741Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Shop00741Menu : ShopArticleListMenu
{
  [SerializeField]
  private UILabel TxtOwnnumber;

  public override IEnumerator Init(Future<GameObject> cellPrefab)
  {
    IEnumerator e = base.Init(cellPrefab);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.TxtOwnnumber.SetTextLocalize(SMManager.Get<Player>().medal);
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
