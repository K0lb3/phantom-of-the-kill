﻿// Decompiled with JetBrains decompiler
// Type: Menu0591Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Menu0591Menu : BackButtonMenuBase
{
  [SerializeField]
  private NGxScroll scrollContainer;

  public IEnumerator InitSceneAsync()
  {
    this.scrollContainer.ResolvePosition();
    yield break;
  }

  public IEnumerator StartSceneAsync()
  {
    yield break;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().clearStack();
    Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
    Mypage051Scene.ChangeScene(false);
  }

  public void onHelpClick()
  {
    if (this.IsPushAndSet())
      return;
    Help0151Scene.ChangeScene(true);
  }

  public void onDetaResetClick()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowDetaResetPopup());
  }

  public void onStoryClick()
  {
    if (this.IsPushAndSet())
      return;
    Story0592Scene.ChangeScene(true);
  }

  private IEnumerator ShowDetaResetPopup()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_051_10__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefabF.Result.Clone((Transform) null), false, false, true, true, false, false, "SE_1006");
  }
}
