﻿// Decompiled with JetBrains decompiler
// Type: Popup004131Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class Popup004131Menu : BackButtonMenuBase
{
  private Unit00499Menu menu;
  private bool isRecod;
  [SerializeField]
  private UILabel txt_Description1_left;

  public void Init(Unit00499Menu menu, bool isRecod = false)
  {
    this.menu = menu;
    this.isRecod = isRecod;
    if (isRecod)
      this.txt_Description1_left.SetTextLocalize(Consts.GetInstance().MEMORY_LOAD_DESCRIPTION);
    else
      this.txt_Description1_left.SetTextLocalize(Consts.GetInstance().TRANSMIGRATE_DESCRIPTION);
  }

  public void IbtnYes()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
    this.StartCoroutine(this.menu.Transmigration(this.isRecod));
  }

  public void IbtnNo()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }
}
