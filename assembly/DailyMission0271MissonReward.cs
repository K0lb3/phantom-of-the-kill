﻿// Decompiled with JetBrains decompiler
// Type: DailyMission0271MissonReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class DailyMission0271MissonReward : MonoBehaviour
{
  [SerializeField]
  private UILabel rewardName;
  [SerializeField]
  private CreateIconObject rewardThum;

  public IEnumerator Init(MasterDataTable.CommonRewardType type, int id, int quantity)
  {
    this.rewardName.SetTextLocalize(CommonRewardType.GetRewardName(type, id, quantity, false));
    IEnumerator e = this.rewardThum.CreateThumbnail(type, id, quantity, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
