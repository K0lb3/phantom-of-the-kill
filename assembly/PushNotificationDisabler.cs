﻿// Decompiled with JetBrains decompiler
// Type: PushNotificationDisabler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PushNotificationDisabler : MonoBehaviour
{
  [SerializeField]
  private GameObject m_pushNotificationConfig;
  [SerializeField]
  private GameObject m_masterNameChange;
  [SerializeField]
  private GameObject m_userCommentEdit;

  private void Start()
  {
    if (!Application.platform.Equals((object) RuntimePlatform.WindowsPlayer) && !Application.platform.Equals((object) RuntimePlatform.OSXPlayer))
      return;
    this.SortObjects();
    this.DisablePushNotification();
  }

  private void SortObjects()
  {
    this.m_masterNameChange.transform.position = this.m_pushNotificationConfig.transform.position;
    this.m_userCommentEdit.transform.position = this.m_masterNameChange.transform.position;
  }

  private void DisablePushNotification()
  {
    this.m_pushNotificationConfig.SetActive(false);
  }
}
