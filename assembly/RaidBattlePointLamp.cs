﻿// Decompiled with JetBrains decompiler
// Type: RaidBattlePointLamp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class RaidBattlePointLamp : MonoBehaviour
{
  [SerializeField]
  private GameObject mLamp;

  public void On()
  {
    this.mLamp.SetActive(true);
  }

  public void Off()
  {
    this.mLamp.SetActive(false);
  }

  public void Enable()
  {
    this.GetComponent<UIWidget>().color = Color.white;
    this.mLamp.GetComponent<UIWidget>().color = Color.white;
  }

  public void Disable()
  {
    this.GetComponent<UIWidget>().color = Color.gray;
    this.mLamp.GetComponent<UIWidget>().color = Color.gray;
  }
}
