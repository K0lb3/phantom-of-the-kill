﻿// Decompiled with JetBrains decompiler
// Type: TutorialGachaPage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class TutorialGachaPage : TutorialPageBase
{
  private const int REWARD_TYPE_UNIT = 1;

  public override IEnumerator Show()
  {
    DateTime nowTime = ServerTime.NowAppTimeAddDelta();
    GachaTutorialPeriod gachaPeriod = ((IEnumerable<GachaTutorialPeriod>) MasterData.GachaTutorialPeriodList).Where<GachaTutorialPeriod>((Func<GachaTutorialPeriod, bool>) (x =>
    {
      if (x.start_at.HasValue)
      {
        DateTime dateTime1 = nowTime;
        DateTime? nullable = x.start_at;
        if ((nullable.HasValue ? (dateTime1 >= nullable.GetValueOrDefault() ? 1 : 0) : 0) != 0 && x.end_at.HasValue)
        {
          DateTime dateTime2 = nowTime;
          nullable = x.end_at;
          return nullable.HasValue && dateTime2 < nullable.GetValueOrDefault();
        }
      }
      return false;
    })).FirstOrDefault<GachaTutorialPeriod>();
    if (gachaPeriod != null)
    {
      GachaTutorial gachaTutorial = ((IEnumerable<GachaTutorial>) MasterData.GachaTutorialList).Where<GachaTutorial>((Func<GachaTutorial, bool>) (x =>
      {
        int? gachaTutorialPeriod = x._period_id_GachaTutorialPeriod;
        int id = gachaPeriod.ID;
        return gachaTutorialPeriod.GetValueOrDefault() == id & gachaTutorialPeriod.HasValue;
      })).FirstOrDefault<GachaTutorial>();
      if (gachaTutorial != null)
      {
        GachaModule gachaModule = new GachaModule()
        {
          description = new GachaDescription(),
          newentity = new GachaModuleNewentity[0],
          decks = new GachaModuleDecks[0],
          number = 1,
          stepup = new GachaStepup(),
          period = new GachaPeriod()
        };
        gachaModule.period.start_at = new DateTime?(gachaPeriod.start_at.HasValue ? gachaPeriod.start_at.Value : new DateTime(2010, 1, 1));
        gachaModule.period.end_at = new DateTime?(gachaPeriod.end_at.HasValue ? gachaPeriod.end_at.Value : new DateTime(2040, 1, 1));
        gachaModule.period.display_count_down = false;
        gachaModule.front_image_url = MasterData.GachaTutorialBannerList[0].front_image_url;
        gachaModule.gacha = new GachaModuleGacha[1]
        {
          new GachaModuleGacha()
        };
        gachaModule.gacha[0].count = 0;
        gachaModule.gacha[0].payment_id = gachaTutorial.payment_id;
        gachaModule.gacha[0].deck_id = gachaTutorial._deck_id;
        gachaModule.gacha[0].start_at = gachaModule.period.start_at;
        gachaModule.gacha[0].description = gachaTutorial.description;
        gachaModule.gacha[0].roll_count = 0;
        gachaModule.gacha[0].max_roll_count = gachaTutorial.max_roll_count;
        gachaModule.gacha[0].end_at = gachaModule.period.end_at;
        gachaModule.gacha[0].daily_count = 0;
        gachaModule.gacha[0].payment_amount = gachaTutorial.payment_amount;
        gachaModule.gacha[0].limit = gachaTutorial._limit;
        gachaModule.gacha[0].details = gachaModule.description;
        gachaModule.gacha[0].remain_count_for_reward = new int?();
        gachaModule.gacha[0].daily_limit = gachaTutorial._daily_limit;
        gachaModule.gacha[0].payment_type_id = gachaTutorial.payment_type_id_CommonPayType;
        gachaModule.gacha[0].button_url = (string) null;
        gachaModule.gacha[0].id = gachaTutorial.ID;
        gachaModule.gacha[0].max_roll_count = gachaTutorial.max_roll_count;
        gachaModule.gacha[0].name = gachaTutorial.name;
        gachaModule.type = 4;
        gachaModule.title_banner_url = MasterData.GachaTutorialBannerList[0].title_image_url;
        gachaModule.name = gachaTutorial.name;
        SMManager.UpdateList<GachaModule>(new GachaModule[1]
        {
          gachaModule
        }, false);
        Singleton<TutorialRoot>.GetInstance().TutorialGachaAdvice();
        yield break;
      }
    }
  }
}
