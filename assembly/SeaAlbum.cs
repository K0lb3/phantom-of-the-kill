﻿// Decompiled with JetBrains decompiler
// Type: SeaAlbum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class SeaAlbum : MonoBehaviour
{
  [SerializeField]
  private UI2DSprite Illustration;
  [SerializeField]
  private GameObject[] CoverPanel;
  private int index;

  public int Index
  {
    get
    {
      return this.index;
    }
  }

  public void Init(UnityEngine.Sprite sprite, PlayerAlbum playerAlbum, int index)
  {
    this.index = index;
    for (int index1 = 0; index1 < this.CoverPanel.Length; ++index1)
    {
      bool flag = !playerAlbum.player_album_piece[index1].is_open;
      this.CoverPanel[index1].SetActive(flag);
    }
    this.Illustration.sprite2D = sprite;
  }
}
