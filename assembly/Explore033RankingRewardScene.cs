﻿// Decompiled with JetBrains decompiler
// Type: Explore033RankingRewardScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using UnityEngine;

public class Explore033RankingRewardScene : NGSceneBase
{
  [SerializeField]
  private Explore033RankingRewardMenu menu;

  public static void changeScene()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("explore033_RankingReward", true, (object[]) Array.Empty<object>());
  }

  public override IEnumerator onInitSceneAsync()
  {
    yield break;
  }

  public IEnumerator onStartSceneAsync()
  {
    Explore033RankingRewardScene rankingRewardScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Future<GameObject> bgF = new ResourceObject("Prefabs/BackGround/ExploreChallengeBackground").Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    rankingRewardScene.backgroundPrefab = bgF.Result;
    yield return (object) rankingRewardScene.menu.InitializeAsync();
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }
}
