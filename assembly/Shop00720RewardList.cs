﻿// Decompiled with JetBrains decompiler
// Type: Shop00720RewardList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00720RewardList : BackButtonMenuBase
{
  [SerializeField]
  private NGxScrollMasonry Scroll;

  public void IbtnNo()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public IEnumerator Init(Shop00720Prefabs prefabs, int deckID)
  {
    List<SlotS001MedalDeckEntity> DeckMasters = ((IEnumerable<SlotS001MedalDeckEntity>) MasterData.SlotS001MedalDeckEntityList).Where<SlotS001MedalDeckEntity>((Func<SlotS001MedalDeckEntity, bool>) (d => d.reward_type_id == MasterDataTable.CommonRewardType.deck && d.deck_id == deckID)).ToList<SlotS001MedalDeckEntity>();
    SlotS001MedalDeckEntity[] DeckEntityList = ((IEnumerable<SlotS001MedalDeckEntity>) MasterData.SlotS001MedalDeckEntityList).Where<SlotS001MedalDeckEntity>((Func<SlotS001MedalDeckEntity, bool>) (d => d.reward_type_id != MasterDataTable.CommonRewardType.deck && DeckMasters.Any<SlotS001MedalDeckEntity>((Func<SlotS001MedalDeckEntity, bool>) (dm => dm.reward_id == d.deck_id)))).ToArray<SlotS001MedalDeckEntity>();
    SlotS001MedalDeck[] DeckList = MasterData.SlotS001MedalDeckList;
    this.Scroll.Reset();
    IEnumerable<int> ints = ((IEnumerable<SlotS001MedalDeck>) DeckList).Select<SlotS001MedalDeck, int>((Func<SlotS001MedalDeck, int>) (s => s.ID));
    GameObject prefab = prefabs.DirSlotReward;
    this.Scroll.Scroll.gameObject.SetActive(false);
    foreach (int num in ints)
    {
      int id = num;
      SlotS001MedalDeck targetDeck = ((IEnumerable<SlotS001MedalDeck>) DeckList).SingleOrDefault<SlotS001MedalDeck>((Func<SlotS001MedalDeck, bool>) (w => w.ID == id));
      int[] reelIds = new int[3]
      {
        targetDeck.prize_first,
        targetDeck.prize_second,
        targetDeck.prize_third
      };
      Shop00720RewardPatternData rewardPatternData = new Shop00720RewardPatternData(reelIds);
      if (rewardPatternData.IsEnabled)
      {
        IEnumerator e = this.SetRewardPatternData(rewardPatternData, reelIds, id, targetDeck, DeckEntityList, true);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (rewardPatternData.RewardList.Count != 0)
        {
          GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(prefab);
          this.Scroll.Add(gameObject, false);
          e = gameObject.GetComponent<Shop00720RewardScroll>().Init(rewardPatternData, prefabs);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        else
          continue;
      }
      rewardPatternData = (Shop00720RewardPatternData) null;
    }
    this.Scroll.Scroll.gameObject.SetActive(true);
    this.Scroll.ResolvePosition();
  }

  private void OnEnable()
  {
    this.Scroll.ResolvePosition();
  }

  private IEnumerator SetRewardPatternData(
    Shop00720RewardPatternData rewardPatternData,
    int[] reelIds,
    int id,
    SlotS001MedalDeck targetDeck,
    SlotS001MedalDeckEntity[] DeckEntityList,
    bool checkEmpty)
  {
    foreach (SlotS001MedalDeckEntity reward in ((IEnumerable<SlotS001MedalDeckEntity>) DeckEntityList).Where<SlotS001MedalDeckEntity>((Func<SlotS001MedalDeckEntity, bool>) (w => w.deck_id == id)))
      this.SetRewardData(reward, rewardPatternData);
    if (!checkEmpty || rewardPatternData.RewardList.Count != 0)
    {
      IEnumerator e = rewardPatternData.SetSprite(reelIds);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      rewardPatternData.Description = targetDeck.description;
    }
  }

  private void SetRewardData(
    SlotS001MedalDeckEntity reward,
    Shop00720RewardPatternData rewardPatternData)
  {
    Shop00720RewardData shop00720RewardData = new Shop00720RewardData()
    {
      RewardId = reward.reward_id,
      RewardType = reward.reward_type_id,
      Quantity = reward.reward_quantity.HasValue ? reward.reward_quantity.Value : 0
    };
    shop00720RewardData.Description = CommonRewardType.GetRewardName(shop00720RewardData.RewardType, shop00720RewardData.RewardId, shop00720RewardData.Quantity, false);
    rewardPatternData.RewardList.Add(shop00720RewardData);
  }
}
