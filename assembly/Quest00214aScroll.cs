﻿// Decompiled with JetBrains decompiler
// Type: Quest00214aScroll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class Quest00214aScroll : MonoBehaviour
{
  public UILabel label;
  public GameObject charcter;

  public IEnumerator Init(GameObject iconPreafab, QuestDisplayConditionConverter data)
  {
    this.label.SetText(data.name);
    IEnumerator e = ColosseumUtility.CreateUnitIcon(iconPreafab, MasterData.UnitUnit[data.unit_UnitUnit], this.charcter.transform, true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
