﻿// Decompiled with JetBrains decompiler
// Type: NGGameDataManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using gu3;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class NGGameDataManager : Singleton<NGGameDataManager>
{
  public static bool SeaChangeFlag = false;
  public static int UrlSchemePresentId = -1;
  public static string UrlSchemeSerial = "";
  public bool isCallHomeUpdateAllData = true;
  public int lastReferenceUnitID = -1;
  public int lastReferenceUnitIndex = -1;
  public string[] favoriteFriends = new string[0];
  private Dictionary<int, int> getTablePieceSameCharacterIds = new Dictionary<int, int>();
  public bool successStartSceneAsyncProxyImpl = true;
  [SerializeField]
  private int webApiUpdateIntervalSeconds = 1800;
  private DateTime updatedTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
  public Dictionary<string, WebAPI.Response.UnitPreviewInheritancePreview_inheritance> dicPreviewInheritance = new Dictionary<string, WebAPI.Response.UnitPreviewInheritancePreview_inheritance>();
  private Dictionary<int, OverkillersSlotRelease.Conditions[]> dicOverkillersSlotReleaseConditions = new Dictionary<int, OverkillersSlotRelease.Conditions[]>();
  private Modified<Player> player;
  private Modified<SeaPlayer> seaPlayer;
  private Modified<PlayerUnit[]> unit;
  private Modified<NGGameDataManager.TimeCounter> timeCounter;
  private DateTime oldTime;
  private bool started;
  public List<PlayerLoginBonus> loginBonuses;
  public OfficialInformationArticle[] officialInfos;
  public int[] playbackEventIds;
  public List<LevelRewardSchemaMixin> playerLevelRewards;
  public DateTime signedInAt;
  public DateTime? gachaLatestStartTime;
  public DateTime? limitedShopLatestStartTime;
  public DateTime? limitedShopLatestEndTime;
  public DateTime? raidMedalShopLatestStartTime;
  public DateTime lastInfoTime;
  public DateTime lastGachaTime;
  public DateTime lastSlotTime;
  public bool infoThrough;
  public int challenge_point;
  public int challenge_point_max;
  public Dictionary<string, Texture2D> webImageCache;
  private GameObject[] otherBattleAtlas;
  private GameObject[] resideResources;
  public int[] clearedExtraQuestSIds;
  public int[] clearedTowerStageIds;
  public int[] clearedRaidQuestSIds;
  private int receivedFriendRequestCount;
  public bool isActiveTotalPaymentBonus;
  public bool hasReceivableTotalPaymentBonus;
  public bool newbiePacks;
  public bool receivableGift;
  public bool isOpenRoulette;
  public bool hasFillableLoginbonus;
  private NGGameDataManager.Boost m_BoostInfo;
  private bool isColosseum;
  private bool isEarth;
  private bool isSea;
  private CommonQuestType? questType;
  private bool isFromUnityPopupStageList;
  private bool isOpenPvpCampaign;
  private List<Tuple<int, int>> gachaTicketIDQList;
  public Color baseAmbientLight;
  public UnityEngine.Sprite loadingBgSprite;
  public NGGameDataManager.TimeCounter timeInstance;
  private bool mRefreshHomeHome;
  private bool mIsUpdating;
  private bool mIsBeforeUpdating;

  public int ReceivedFriendRequestCount
  {
    get
    {
      return this.receivedFriendRequestCount;
    }
    set
    {
      this.receivedFriendRequestCount = value;
    }
  }

  public Dictionary<int, int> GetTablePieceSameCharacterIds
  {
    get
    {
      return this.getTablePieceSameCharacterIds;
    }
    set
    {
      this.getTablePieceSameCharacterIds = value;
    }
  }

  public NGGameDataManager.Boost BoostInfo
  {
    get
    {
      return this.m_BoostInfo;
    }
  }

  public bool IsColosseum
  {
    get
    {
      return this.isColosseum;
    }
    set
    {
      this.isColosseum = value;
    }
  }

  public bool IsEarth
  {
    get
    {
      return this.isEarth;
    }
    set
    {
      this.isEarth = value;
    }
  }

  public bool IsSea
  {
    get
    {
      return this.isSea;
    }
    set
    {
      this.isSea = value;
      ++this.revisionIsSea;
    }
  }

  public int revisionIsSea { get; private set; }

  public CommonQuestType? QuestType
  {
    get
    {
      return this.questType;
    }
    set
    {
      this.questType = value;
    }
  }

  public bool IsFromUnityPopupStageList
  {
    get
    {
      return this.isFromUnityPopupStageList;
    }
    set
    {
      this.isFromUnityPopupStageList = value;
    }
  }

  public NGGameDataManager.FromUnityPopup fromUnityPopup { get; set; }

  public System.Action OpenUnityPopup { private get; set; }

  public bool HasOpenUnityPopup
  {
    get
    {
      return this.OpenUnityPopup != null;
    }
  }

  public void OnceOpenUnityPopup()
  {
    if (!this.HasOpenUnityPopup)
      return;
    this.OpenUnityPopup();
    this.OpenUnityPopup = (System.Action) null;
  }

  public bool IsOpenPvpCampaign
  {
    get
    {
      return this.isOpenPvpCampaign;
    }
    set
    {
      this.isOpenPvpCampaign = value;
    }
  }

  public bool IsResideResourceLoaded(string name)
  {
    if (this.resideResources != null)
    {
      bool flag = ((IEnumerable<GameObject>) this.resideResources).Any<GameObject>((Func<GameObject, bool>) (x => x.name == name));
      if (flag)
        return flag;
    }
    return this.otherBattleAtlas != null && ((IEnumerable<GameObject>) this.otherBattleAtlas).Any<GameObject>((Func<GameObject, bool>) (x => x.name == name));
  }

  public IEnumerator LoadResideResources()
  {
    if (PerformanceConfig.GetInstance().IsTuningCommonTextureLoaded)
    {
      this.UnLoadResideResources();
      ResourceObject[] resources = new ResourceObject[23]
      {
        new ResourceObject("GUI/button_text/button_text_prefab"),
        new ResourceObject("GUI/popup_base/popup_base_prefab"),
        new ResourceObject("GUI/popup_base_skill/popup_base_skill_prefab"),
        new ResourceObject("GUI/popup_base_other/popup_base_other_prefab"),
        new ResourceObject("GUI/unit_detail/unit_detail_prefab"),
        new ResourceObject("GUI/unit_sort_filter/unit_sort_filter_prefab"),
        new ResourceObject("GUI/unit_title_short/unit_title_short_prefab"),
        new ResourceObject("GUI/023-5_sozai/023-5_sozai_prefab"),
        new ResourceObject("GUI/004-8-3_sozai/004-8-3_sozai_prefab"),
        new ResourceObject("GUI/008-01_sozai/008-01_sozai_prefab"),
        new ResourceObject("GUI/004-9-9_sozai/004-9-9_sozai_prefab"),
        new ResourceObject("GUI/027-1_sozai/027-1_sozai_prefab"),
        new ResourceObject("GUI/facility_thumb/facility_thumb_prefab"),
        new ResourceObject("GUI/002-2_sozai/002-2_sozai_prefab"),
        new ResourceObject("GUI/002-8_sozai/002-8_sozai_prefab"),
        new ResourceObject("GUI/004-6_sozai/004-6_sozai_prefab"),
        new ResourceObject("GUI/002-17_sozai/002-17_sozai_prefab"),
        new ResourceObject("GUI/004-1_sozai/004-1_sozai_prefab"),
        new ResourceObject("GUI/battleUI/battleUI_prefab"),
        new ResourceObject("GUI/battleUI_common/battleUI_common_prefab"),
        new ResourceObject("GUI/battleUI_duel/battleUI_duel_prefab"),
        new ResourceObject("GUI/map_edit/map_edit_prefab"),
        new ResourceObject("GUI/sea_home/sea_home_prefab")
      };
      Singleton<ResourceManager>.GetInstance().ClearCache();
      int size = resources.Length;
      this.resideResources = new GameObject[size];
      for (int i = 0; i < size; ++i)
      {
        Future<GameObject> prefab = resources[i].Load<GameObject>();
        IEnumerator e = prefab.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.resideResources[i] = prefab.Result;
        prefab = (Future<GameObject>) null;
      }
    }
  }

  private void UnLoadResideResources()
  {
    if (this.resideResources == null)
      return;
    int length = this.resideResources.Length;
    for (int index = 0; index < length; ++index)
      this.resideResources[index] = (GameObject) null;
    this.resideResources = (GameObject[]) null;
  }

  public IEnumerator LoadOtherBattleAtlas()
  {
    if (this.otherBattleAtlas == null)
    {
      ResourceObject[] resources = new ResourceObject[2]
      {
        new ResourceObject("GUI/button_text/button_text_prefab"),
        new ResourceObject("GUI/popup_base/popup_base_prefab")
      };
      int size = resources.Length;
      this.otherBattleAtlas = new GameObject[size];
      for (int i = 0; i < size; ++i)
      {
        Future<GameObject> prefab = resources[i].Load<GameObject>();
        IEnumerator e = prefab.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.otherBattleAtlas[i] = prefab.Result;
        prefab = (Future<GameObject>) null;
      }
    }
  }

  public IEnumerator UnLoadOtherBattleAtlas()
  {
    if (this.otherBattleAtlas != null)
    {
      int length = this.otherBattleAtlas.Length;
      for (int index = 0; index < length; ++index)
        this.otherBattleAtlas[index] = (GameObject) null;
      this.otherBattleAtlas = (GameObject[]) null;
      yield break;
    }
  }

  public IEnumerator GetWebImage(string url, UI2DSprite sprite)
  {
    if (!string.IsNullOrEmpty(url))
    {
      IEnumerator e = this.LoadWebImage(url);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Texture2D texture = (Texture2D) null;
      Singleton<NGGameDataManager>.GetInstance().webImageCache.TryGetValue(url, out texture);
      if (!((UnityEngine.Object) texture == (UnityEngine.Object) null))
      {
        texture.wrapMode = TextureWrapMode.Clamp;
        float width = (float) texture.width;
        float height = (float) texture.height;
        sprite.sprite2D = UnityEngine.Sprite.Create(texture, new Rect(0.0f, 0.0f, width, height), new Vector2(0.0f, 0.0f), 100f, 0U, SpriteMeshType.FullRect);
        sprite.SetDimensions((int) width, (int) height);
        sprite.gameObject.SetActive(true);
      }
    }
  }

  public IEnumerator LoadWebImage(string url)
  {
    int errorCount = 0;
    if (url != "")
    {
      if (!Singleton<NGGameDataManager>.GetInstance().webImageCache.ContainsKey(url))
      {
        Singleton<NGGameDataManager>.GetInstance().webImageCache[url] = (Texture2D) null;
        while ((UnityEngine.Object) Singleton<NGGameDataManager>.GetInstance().webImageCache[url] == (UnityEngine.Object) null && errorCount < 3)
        {
          Dictionary<string, object> requestResult = new Dictionary<string, object>();
          yield return (object) WWWUtil.RequestAndCache(url, (System.Action<Dictionary<string, object>>) (result => requestResult = result));
          if (string.IsNullOrEmpty(((WWW) requestResult["www"]).error))
          {
            Singleton<NGGameDataManager>.GetInstance().webImageCache[url] = (Texture2D) requestResult["texture"];
            break;
          }
          ++errorCount;
        }
      }
      if ((UnityEngine.Object) Singleton<NGGameDataManager>.GetInstance().webImageCache[url] == (UnityEngine.Object) null)
        Debug.LogError((object) string.Format("missing download url: {0}", (object) url));
    }
  }

  public bool isInitialized
  {
    get
    {
      return this.player != null && this.unit != null && this.started;
    }
  }

  private IEnumerator SaveUserInfo()
  {
    if (!Persist.userInfo.Exists)
    {
      Player player;
      while (true)
      {
        player = SMManager.Get<Player>();
        if (player == null || string.IsNullOrEmpty(player.short_id))
          yield return (object) null;
        else
          break;
      }
      Persist.userInfo.Data.userId = player.short_id;
      Persist.userInfo.Flush();
    }
  }

  public void CacheClear()
  {
    this.webImageCache.Clear();
    this.loadingBgSprite = (UnityEngine.Sprite) null;
  }

  protected override void Initialize()
  {
    this.player = SMManager.Observe<Player>();
    this.seaPlayer = SMManager.Observe<SeaPlayer>();
    this.unit = SMManager.Observe<PlayerUnit[]>();
    this.timeCounter = SMManager.Observe<NGGameDataManager.TimeCounter>();
    this.oldTime = DateTime.Now;
    this.webImageCache = new Dictionary<string, Texture2D>();
    this.StartCoroutine(this.SaveUserInfo());
    Consts.Update(MasterData.ConstsConstsList);
  }

  private void Start()
  {
    this.timeInstance.Init(this.player, this.seaPlayer);
    SMManager.Change<NGGameDataManager.TimeCounter>(this.timeInstance);
    this.started = true;
  }

  private void Update()
  {
    if (!this.isInitialized)
      return;
    DateTime now = DateTime.Now;
    float totalSeconds = (float) (now - this.oldTime).TotalSeconds;
    this.oldTime = now;
    if (!this.timeCounter.Value.AddDeltaTime(totalSeconds))
      return;
    this.timeCounter.NotifyChanged();
  }

  private void OnDestroy()
  {
    this.UnLoadResideResources();
  }

  private void OnApplicationPause(bool pause)
  {
    if (!this.isInitialized)
      return;
    if (pause)
    {
      if (Persist.notification.Data.Ap)
      {
        int seconds = Mathf.FloorToInt(this.timeInstance.ApFullRecoverySeconds);
        if (seconds > 0)
          LocalNotification.ScheduleWithTimeInterval(new LocalNotification.Notification()
          {
            category = "AP_RECOVERY",
            message = Consts.GetInstance().AP_RECOVER_PUSHNOTIFICATION_TEXT
          }, seconds);
      }
      if (!Persist.notification.Data.Bp)
        return;
      int seconds1 = Mathf.FloorToInt(this.timeInstance.BpFullRecoverySeconds);
      if (seconds1 <= 0)
        return;
      LocalNotification.ScheduleWithTimeInterval(new LocalNotification.Notification()
      {
        category = "BP_RECOVERY",
        message = Consts.GetInstance().BP_RECOVER_PUSHNOTIFICATION_TEXT
      }, seconds1);
    }
    else
    {
      LocalNotification.CancelNotificationsWithCategory("AP_RECOVERY");
      LocalNotification.CancelNotificationsWithCategory("BP_RECOVERY");
    }
  }

  public bool refreshHomeHome
  {
    set
    {
      this.mRefreshHomeHome = value;
    }
  }

  public bool IsUpdating
  {
    get
    {
      return this.mIsUpdating || this.mIsBeforeUpdating;
    }
  }

  private IEnumerator CallHomeStartup()
  {
    IEnumerator e;
    if (this.isCallHomeUpdateAllData)
    {
      Future<WebAPI.Response.HomeStartUp2> handler = WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null);
      e = handler.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.loginBonuses = ((IEnumerable<PlayerLoginBonus>) handler.Result.player_loginbonuses).ToList<PlayerLoginBonus>();
      this.officialInfos = handler.Result.articles;
      this.playerLevelRewards = ((IEnumerable<LevelRewardSchemaMixin>) handler.Result.player_achieve_level_rewards).ToList<LevelRewardSchemaMixin>();
      this.signedInAt = handler.Result.last_signed_in_at;
      this.gachaLatestStartTime = handler.Result.gacha_latest_start_time;
      this.limitedShopLatestStartTime = handler.Result.limit_shop_start_at;
      this.limitedShopLatestEndTime = handler.Result.limit_shop_end_at;
      this.raidMedalShopLatestStartTime = handler.Result.raid_medal_shop_latest_start_time;
      this.challenge_point = handler.Result.challenge_point;
      this.receivedFriendRequestCount = handler.Result.received_friend_request_count;
      this.isOpenPvpCampaign = handler.Result.is_open_pvp_campaign;
      this.playbackEventIds = handler.Result.story_playback_event_ids;
      this.newbiePacks = handler.Result.has_buyable_newbie_packs;
      this.receivableGift = handler.Result.has_receivable_rewards;
      this.isActiveTotalPaymentBonus = handler.Result.is_active_paymentbonus;
      this.hasReceivableTotalPaymentBonus = handler.Result.has_receivable_paymentbonus;
      this.isOpenRoulette = handler.Result.is_open_roulette;
      this.hasFillableLoginbonus = handler.Result.has_fillable_loginbonus;
      this.favoriteFriends = handler.Result.favorite_friend_list;
      this.InitBoostInfo(handler.Result.active_boost_period_id_list, handler.Result.boost_type_id_list);
      try
      {
        this.lastInfoTime = Persist.lastInfoTime.Data.GetLastInfoTime();
        Persist.lastInfoTime.Data.SetLastInfoTime(this.signedInAt);
        Persist.lastInfoTime.Flush();
      }
      catch
      {
        Persist.lastInfoTime.Delete();
      }
      this.isCallHomeUpdateAllData = false;
      handler = (Future<WebAPI.Response.HomeStartUp2>) null;
    }
    else
    {
      Future<WebAPI.Response.HomeStartUp> handler = WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null);
      e = handler.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (handler.Result.player_loginbonuses.Length != 0)
      {
        if (this.loginBonuses == null)
          this.loginBonuses = ((IEnumerable<PlayerLoginBonus>) handler.Result.player_loginbonuses).ToList<PlayerLoginBonus>();
        else
          this.loginBonuses.AddRange((IEnumerable<PlayerLoginBonus>) handler.Result.player_loginbonuses);
      }
      if (handler.Result.player_achieve_level_rewards.Length != 0)
      {
        if (this.playerLevelRewards == null)
          this.playerLevelRewards = ((IEnumerable<LevelRewardSchemaMixin>) handler.Result.player_achieve_level_rewards).ToList<LevelRewardSchemaMixin>();
        else
          this.playerLevelRewards.AddRange((IEnumerable<LevelRewardSchemaMixin>) handler.Result.player_achieve_level_rewards);
      }
      this.officialInfos = handler.Result.articles;
      this.signedInAt = handler.Result.last_signed_in_at;
      this.gachaLatestStartTime = handler.Result.gacha_latest_start_time;
      this.limitedShopLatestStartTime = handler.Result.limit_shop_start_at;
      this.limitedShopLatestEndTime = handler.Result.limit_shop_end_at;
      this.raidMedalShopLatestStartTime = handler.Result.raid_medal_shop_latest_start_time;
      this.challenge_point = handler.Result.challenge_point;
      this.receivedFriendRequestCount = handler.Result.received_friend_request_count;
      this.isOpenPvpCampaign = handler.Result.is_open_pvp_campaign;
      this.playbackEventIds = handler.Result.story_playback_event_ids;
      this.newbiePacks = handler.Result.has_buyable_newbie_packs;
      this.receivableGift = handler.Result.has_receivable_rewards;
      this.isActiveTotalPaymentBonus = handler.Result.is_active_paymentbonus;
      this.hasReceivableTotalPaymentBonus = handler.Result.has_receivable_paymentbonus;
      this.isOpenRoulette = handler.Result.is_open_roulette;
      this.hasFillableLoginbonus = handler.Result.has_fillable_loginbonus;
      this.InitBoostInfo(handler.Result.active_boost_period_id_list, handler.Result.boost_type_id_list);
      try
      {
        this.lastInfoTime = Persist.lastInfoTime.Data.GetLastInfoTime();
        Persist.lastInfoTime.Data.SetLastInfoTime(this.signedInAt);
        Persist.lastInfoTime.Flush();
      }
      catch
      {
        Persist.lastInfoTime.Delete();
      }
      handler = (Future<WebAPI.Response.HomeStartUp>) null;
    }
  }

  public IEnumerator StartSceneAsyncProxyImpl(
    Promise<NGGameDataManager.StartSceneProxyResult> promise)
  {
    NGGameDataManager ngGameDataManager = this;
    while (ngGameDataManager.mIsUpdating)
      yield return (object) null;
    DateTime now = DateTime.UtcNow;
    TimeSpan timeSpan = now - ngGameDataManager.updatedTime;
    if (ngGameDataManager.mRefreshHomeHome || timeSpan.TotalSeconds > (double) ngGameDataManager.webApiUpdateIntervalSeconds)
    {
      ngGameDataManager.mIsUpdating = true;
      if (!PerformanceConfig.GetInstance().IsTuningTitleToHome)
      {
        while ((UnityEngine.Object) Singleton<TutorialRoot>.GetInstance() == (UnityEngine.Object) null)
          yield return (object) null;
      }
      bool isStartupSequence = ngGameDataManager.isCallHomeUpdateAllData;
      IEnumerator e;
      if ((UnityEngine.Object) Singleton<TutorialRoot>.GetInstance() != (UnityEngine.Object) null && Singleton<TutorialRoot>.GetInstance().IsTutorialFinish() || WebAPI.LastPlayerBoot.player_is_create)
      {
        if ((UnityEngine.Object) Singleton<TutorialRoot>.GetInstance() != (UnityEngine.Object) null && Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
          Singleton<TutorialRoot>.GetInstance().ReleaseResources();
        if (ngGameDataManager.isSea)
        {
          if (ngGameDataManager.isCallHomeUpdateAllData)
          {
            // ISSUE: reference to a compiler-generated method
            Future<WebAPI.Response.SeaStartUp2> handler = WebAPI.SeaStartUp2(new System.Action<WebAPI.Response.UserError>(ngGameDataManager.\u003CStartSceneAsyncProxyImpl\u003Eb__124_0));
            e = handler.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            if (handler.Result == null)
            {
              e = ngGameDataManager.CallHomeStartup();
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              ngGameDataManager.mIsUpdating = false;
              yield break;
            }
            else
            {
              ngGameDataManager.loginBonuses = ((IEnumerable<PlayerLoginBonus>) handler.Result.player_loginbonuses).ToList<PlayerLoginBonus>();
              ngGameDataManager.officialInfos = handler.Result.articles;
              ngGameDataManager.playerLevelRewards = ((IEnumerable<LevelRewardSchemaMixin>) handler.Result.player_achieve_level_rewards).ToList<LevelRewardSchemaMixin>();
              ngGameDataManager.signedInAt = handler.Result.last_signed_in_at;
              ngGameDataManager.gachaLatestStartTime = handler.Result.gacha_latest_start_time;
              ngGameDataManager.limitedShopLatestStartTime = handler.Result.limit_shop_start_at;
              ngGameDataManager.limitedShopLatestEndTime = handler.Result.limit_shop_end_at;
              ngGameDataManager.raidMedalShopLatestStartTime = handler.Result.raid_medal_shop_latest_start_time;
              ngGameDataManager.challenge_point = handler.Result.challenge_point;
              ngGameDataManager.receivedFriendRequestCount = handler.Result.received_friend_request_count;
              ngGameDataManager.playbackEventIds = handler.Result.story_playback_event_ids;
              ngGameDataManager.isActiveTotalPaymentBonus = handler.Result.is_active_paymentbonus;
              ngGameDataManager.hasReceivableTotalPaymentBonus = handler.Result.has_receivable_paymentbonus;
              ngGameDataManager.hasFillableLoginbonus = handler.Result.has_fillable_loginbonus;
              ngGameDataManager.favoriteFriends = handler.Result.favorite_friend_list;
              ngGameDataManager.InitBoostInfo(handler.Result.active_boost_period_id_list, handler.Result.boost_type_id_list);
              ngGameDataManager.SetTablePieceSameCharacterIds(handler.Result.gettable_piece_same_character_ids);
              try
              {
                ngGameDataManager.lastInfoTime = Persist.lastInfoTime.Data.GetLastInfoTime();
                Persist.lastInfoTime.Data.SetLastInfoTime(ngGameDataManager.signedInAt);
                Persist.lastInfoTime.Flush();
              }
              catch
              {
                Persist.lastInfoTime.Delete();
              }
              ngGameDataManager.isCallHomeUpdateAllData = false;
              handler = (Future<WebAPI.Response.SeaStartUp2>) null;
            }
          }
          else
          {
            ngGameDataManager.successStartSceneAsyncProxyImpl = false;
            // ISSUE: reference to a compiler-generated method
            Future<WebAPI.Response.SeaStartUp> handler = WebAPI.SeaStartUp(new System.Action<WebAPI.Response.UserError>(ngGameDataManager.\u003CStartSceneAsyncProxyImpl\u003Eb__124_1));
            e = handler.Wait();
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            if (handler.Result != null)
            {
              ngGameDataManager.successStartSceneAsyncProxyImpl = true;
              if (handler.Result.player_loginbonuses.Length != 0)
              {
                if (ngGameDataManager.loginBonuses == null)
                  ngGameDataManager.loginBonuses = ((IEnumerable<PlayerLoginBonus>) handler.Result.player_loginbonuses).ToList<PlayerLoginBonus>();
                else
                  ngGameDataManager.loginBonuses.AddRange((IEnumerable<PlayerLoginBonus>) handler.Result.player_loginbonuses);
              }
              if (handler.Result.player_achieve_level_rewards.Length != 0)
              {
                if (ngGameDataManager.playerLevelRewards == null)
                  ngGameDataManager.playerLevelRewards = ((IEnumerable<LevelRewardSchemaMixin>) handler.Result.player_achieve_level_rewards).ToList<LevelRewardSchemaMixin>();
                else
                  ngGameDataManager.playerLevelRewards.AddRange((IEnumerable<LevelRewardSchemaMixin>) handler.Result.player_achieve_level_rewards);
              }
              ngGameDataManager.signedInAt = handler.Result.last_signed_in_at;
              ngGameDataManager.SetTablePieceSameCharacterIds(handler.Result.gettable_piece_same_character_ids);
            }
            try
            {
              ngGameDataManager.lastInfoTime = Persist.lastInfoTime.Data.GetLastInfoTime();
              Persist.lastInfoTime.Data.SetLastInfoTime(ngGameDataManager.signedInAt);
              Persist.lastInfoTime.Flush();
            }
            catch
            {
              Persist.lastInfoTime.Delete();
            }
            handler = (Future<WebAPI.Response.SeaStartUp>) null;
          }
        }
        else
        {
          e = ngGameDataManager.CallHomeStartup();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
        }
        e = OnDemandDownload.WaitLoadHasUnitResource(false, isStartupSequence);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        e = WebAPI.TutorialTutorialRagnarokResume((System.Action<WebAPI.Response.UserError>) null).Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      ngGameDataManager.updatedTime = now;
      ngGameDataManager.mRefreshHomeHome = false;
      ngGameDataManager.mIsUpdating = false;
    }
    promise.Result = new NGGameDataManager.StartSceneProxyResult();
    ngGameDataManager.mIsBeforeUpdating = false;
  }

  public void Parse(WebAPI.Response.ShopStatus response)
  {
    if (response == null)
      return;
    this.challenge_point_max = response.challenge_point_max;
    this.challenge_point = response.challenge_point;
  }

  public void Parse(WebAPI.Response.ShopBuy response)
  {
    if (response == null)
      return;
    this.challenge_point = response.challenge_point;
  }

  public void SetFriendRequestCount()
  {
    PlayerFriend[] playerFriendArray = ((IEnumerable<PlayerFriend>) SMManager.Get<PlayerFriend[]>()).ReceivedFriendApplications();
    if (playerFriendArray != null)
      this.receivedFriendRequestCount = playerFriendArray.Length;
    else
      this.receivedFriendRequestCount = 0;
  }

  public void SetTablePieceSameCharacterIds(int[] characterIds)
  {
    if (characterIds == null || characterIds.Length == 0)
    {
      this.getTablePieceSameCharacterIds.Clear();
    }
    else
    {
      this.getTablePieceSameCharacterIds.Clear();
      foreach (int characterId in characterIds)
      {
        if (!this.getTablePieceSameCharacterIds.ContainsKey(characterId))
          this.getTablePieceSameCharacterIds.Add(characterId, -1);
      }
    }
  }

  private void InitBoostInfo(int[] periodIdList, int[] typeIdList)
  {
    if (this.m_BoostInfo == null)
      this.m_BoostInfo = new NGGameDataManager.Boost(periodIdList, typeIdList);
    else
      this.m_BoostInfo.Init(periodIdList, typeIdList);
  }

  public void StartSceneAsyncProxy(
    System.Action<NGGameDataManager.StartSceneProxyResult> callback = null)
  {
    new Future<NGGameDataManager.StartSceneProxyResult>(new Func<Promise<NGGameDataManager.StartSceneProxyResult>, IEnumerator>(this.StartSceneAsyncProxyImpl)).RunOn<NGGameDataManager.StartSceneProxyResult>((MonoBehaviour) this, callback);
  }

  public bool InfoOrLoginBonusJump()
  {
    bool flag = false;
    if (!this.infoThrough)
    {
      this.infoThrough = true;
      try
      {
        foreach (OfficialInformationArticle informationArticle in ((IEnumerable<OfficialInformationArticle>) this.officialInfos).Where<OfficialInformationArticle>((Func<OfficialInformationArticle, bool>) (x => !Persist.infoUnRead.Data.GetUnRead(x))))
        {
          if (this.lastInfoTime < informationArticle.published_at)
          {
            Singleton<NGSceneManager>.GetInstance().changeScene("startup000_12", false, (object) informationArticle);
            flag = true;
            break;
          }
        }
      }
      catch
      {
        Persist.infoUnRead.Delete();
      }
    }
    if (!flag)
    {
      foreach (PlayerLoginBonus loginBonuse in this.loginBonuses)
      {
        if (loginBonuse.loginbonus.draw_type != LoginbonusDrawType.popup)
        {
          Startup00014Scene.changeScene(false);
          flag = true;
          break;
        }
      }
    }
    return flag;
  }

  public void bootFirstScene(string scene)
  {
    bool playerIsCreate = WebAPI.LastPlayerBoot.player_is_create;
    if (playerIsCreate)
    {
      if (this.checkTutorialGachaState())
        return;
      if (!Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
        Singleton<TutorialRoot>.GetInstance().EndTutorial();
    }
    if (!playerIsCreate)
    {
      if (!Persist.newTutorial.Data.startRagnarokTutorial)
      {
        Persist.tutorial.Delete();
        Persist.tutorial.Clear();
        Persist.colosseumTutorial.Delete();
        Persist.colosseumTutorial.Clear();
        Persist.newTutorial.Data.startRagnarokTutorial = true;
        Persist.newTutorial.Flush();
      }
      Future<WebAPI.Response.TutorialTutorialRagnarokResume> future = WebAPI.TutorialTutorialRagnarokResume((System.Action<WebAPI.Response.UserError>) null);
      future.RunOn<WebAPI.Response.TutorialTutorialRagnarokResume>((MonoBehaviour) this, (System.Action<WebAPI.Response.TutorialTutorialRagnarokResume>) (_ =>
      {
        SMManager.Get<Player>().name = Persist.tutorial.Data.PlayerName;
        Singleton<TutorialRoot>.GetInstance().StartTutorial(future.Result);
      }));
    }
    else if (WebAPI.LastPlayerBoot.player_during_battle)
    {
      Singleton<NGSoundManager>.GetInstance().CheckInitialize(true);
      if (this.isCallHomeUpdateAllData)
        WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp2>) (_ =>
        {
          this.isCallHomeUpdateAllData = false;
          Singleton<NGBattleManager>.GetInstance().startBattle((BattleInfo) null, WebAPI.LastPlayerBoot.continue_count);
        }));
      else
        WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp>) (_ => Singleton<NGBattleManager>.GetInstance().startBattle((BattleInfo) null, WebAPI.LastPlayerBoot.continue_count)));
    }
    else if (WebAPI.LastPlayerBoot.player_during_sea_battle)
    {
      this.IsSea = true;
      Singleton<NGSoundManager>.GetInstance().CheckInitialize(true);
      if (this.isCallHomeUpdateAllData)
      {
        Future<WebAPI.Response.SeaStartUp2> handler = WebAPI.SeaStartUp2((System.Action<WebAPI.Response.UserError>) (e => this.StartCoroutine(PopupUtility.SeaErrorStartUp(e))));
        handler.RunOn<WebAPI.Response.SeaStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.SeaStartUp2>) (_ =>
        {
          if (handler.Result == null)
            return;
          this.SetTablePieceSameCharacterIds(handler.Result.gettable_piece_same_character_ids);
          this.isCallHomeUpdateAllData = false;
          Singleton<NGBattleManager>.GetInstance().startBattle((BattleInfo) null, WebAPI.LastPlayerBoot.continue_count);
        }));
      }
      else
      {
        Future<WebAPI.Response.SeaStartUp> homeF = WebAPI.SeaStartUp((System.Action<WebAPI.Response.UserError>) null);
        homeF.RunOn<WebAPI.Response.SeaStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.SeaStartUp>) (_ =>
        {
          this.SetTablePieceSameCharacterIds(homeF.Result.gettable_piece_same_character_ids);
          Singleton<NGBattleManager>.GetInstance().startBattle((BattleInfo) null, WebAPI.LastPlayerBoot.continue_count);
        }));
      }
    }
    else if (WebAPI.LastPlayerBoot.player_during_tower_battle)
    {
      BattleInfo bi = new BattleInfo();
      bi.isResume = true;
      bi.quest_type = CommonQuestType.Tower;
      Singleton<NGSoundManager>.GetInstance().CheckInitialize(true);
      if (this.isCallHomeUpdateAllData)
        WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp2>) (_ =>
        {
          this.isCallHomeUpdateAllData = false;
          Singleton<NGBattleManager>.GetInstance().startBattle(bi, WebAPI.LastPlayerBoot.continue_count);
        }));
      else
        WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp>) (_ => Singleton<NGBattleManager>.GetInstance().startBattle(bi, WebAPI.LastPlayerBoot.continue_count)));
    }
    else if (WebAPI.LastPlayerBoot.player_during_raid_battle)
    {
      BattleInfo bi = new BattleInfo();
      bi.isResume = true;
      bi.quest_type = CommonQuestType.GuildRaid;
      Singleton<NGSoundManager>.GetInstance().CheckInitialize(true);
      if (this.isCallHomeUpdateAllData)
        WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp2>) (_ =>
        {
          this.isCallHomeUpdateAllData = false;
          Singleton<NGBattleManager>.GetInstance().startBattle(bi, WebAPI.LastPlayerBoot.continue_count);
        }));
      else
        WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp>) (_ => Singleton<NGBattleManager>.GetInstance().startBattle(bi, WebAPI.LastPlayerBoot.continue_count)));
    }
    else if (WebAPI.LastPlayerBoot.player_during_pvp)
    {
      Singleton<NGSoundManager>.GetInstance().CheckInitialize(true);
      if (this.isCallHomeUpdateAllData)
        WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp2>) (_ =>
        {
          this.isCallHomeUpdateAllData = false;
          BattleInfo battleInfo = new BattleInfo();
          battleInfo.pvp = true;
          battleInfo.pvp_restart = true;
          battleInfo.port = WebAPI.LastPlayerBoot.game_server_port;
          battleInfo.host = WebAPI.LastPlayerBoot.game_server_host;
          battleInfo.battleToken = WebAPI.LastPlayerBoot.pvp_token;
          if (string.IsNullOrEmpty(battleInfo.battleToken))
            battleInfo.pvp_vs_npc = true;
          Singleton<NGBattleManager>.GetInstance().startBattle(battleInfo, 0);
        }));
      else
        WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp>) (_ =>
        {
          BattleInfo battleInfo = new BattleInfo();
          battleInfo.pvp = true;
          battleInfo.pvp_restart = true;
          battleInfo.port = WebAPI.LastPlayerBoot.game_server_port;
          battleInfo.host = WebAPI.LastPlayerBoot.game_server_host;
          battleInfo.battleToken = WebAPI.LastPlayerBoot.pvp_token;
          if (string.IsNullOrEmpty(battleInfo.battleToken))
            battleInfo.pvp_vs_npc = true;
          Singleton<NGBattleManager>.GetInstance().startBattle(battleInfo, 0);
        }));
    }
    else if (WebAPI.LastPlayerBoot.player_during_pvp_result)
    {
      if (this.isCallHomeUpdateAllData)
        WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp2>) (_ =>
        {
          this.isCallHomeUpdateAllData = false;
          Singleton<NGSceneManager>.GetInstance().changeScene("versus026_8", false, (object[]) Array.Empty<object>());
        }));
      else
        WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp>) (_ => Singleton<NGSceneManager>.GetInstance().changeScene("versus026_8", false, (object[]) Array.Empty<object>())));
    }
    else if (WebAPI.LastPlayerBoot.player_during_sea_date)
    {
      Singleton<NGGameDataManager>.GetInstance().IsSea = true;
      Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (data =>
      {
        if (data == null)
          return;
        Sea030HomeScene.ChangeScene(false, true);
      }));
    }
    else if (WebAPI.LastPlayerBoot.during_retry_gacha)
    {
      Singleton<NGSoundManager>.GetInstance().CheckInitialize(true);
      if (this.isCallHomeUpdateAllData)
        WebAPI.HomeStartUp2((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp2>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp2>) (_ =>
        {
          this.isCallHomeUpdateAllData = false;
          Gacha00613Scene.ChangeScene(false, true);
        }));
      else
        WebAPI.HomeStartUp((System.Action<WebAPI.Response.UserError>) null).RunOn<WebAPI.Response.HomeStartUp>((MonoBehaviour) this, (System.Action<WebAPI.Response.HomeStartUp>) (_ => Gacha00613Scene.ChangeScene(false, true)));
    }
    else
    {
      this.isSea = NGGameDataManager.SeaChangeFlag;
      if (PerformanceConfig.GetInstance().IsTuningTitleToHome)
      {
        if (this.isSea && scene == "mypage")
        {
          Singleton<CommonRoot>.GetInstance().isLoading = true;
          Sea030HomeScene.ChangeScene(false, false);
        }
        else if (scene == "mypage")
          MypageScene.ChangeScene(false, false, false);
        else
          Singleton<NGSceneManager>.GetInstance().changeScene(scene, false, (object[]) Array.Empty<object>());
      }
      else
        Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (data =>
        {
          if (this.isSea && scene == "mypage")
          {
            Singleton<CommonRoot>.GetInstance().isLoading = true;
            Sea030HomeScene.ChangeScene(false, false);
          }
          else if (scene == "mypage")
            MypageScene.ChangeScene(false, false, false);
          else
            Singleton<NGSceneManager>.GetInstance().changeScene(scene, false, (object[]) Array.Empty<object>());
        }));
    }
  }

  public void bootFirstSceneBefore()
  {
    if (!WebAPI.LastPlayerBoot.player_is_create || WebAPI.LastPlayerBoot.player_during_battle || (WebAPI.LastPlayerBoot.player_during_sea_battle || WebAPI.LastPlayerBoot.player_during_tower_battle) || (WebAPI.LastPlayerBoot.player_during_raid_battle || WebAPI.LastPlayerBoot.player_during_pvp || (WebAPI.LastPlayerBoot.player_during_pvp_result || WebAPI.LastPlayerBoot.player_during_sea_date)) || WebAPI.LastPlayerBoot.during_retry_gacha)
      return;
    this.mIsBeforeUpdating = true;
    this.isSea = NGGameDataManager.SeaChangeFlag;
    Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) null);
  }

  public bool isChangeHaveGachaTiket()
  {
    if (this.gachaTicketIDQList == null)
    {
      this.gachaTicketIDQList = ((IEnumerable<PlayerGachaTicket>) this.player.Value.gacha_tickets).Select<PlayerGachaTicket, Tuple<int, int>>((Func<PlayerGachaTicket, Tuple<int, int>>) (x => Tuple.Create<int, int>(x.ticket_id, x.quantity))).ToList<Tuple<int, int>>();
      return true;
    }
    if (!((IEnumerable<PlayerGachaTicket>) this.player.Value.gacha_tickets).Any<PlayerGachaTicket>((Func<PlayerGachaTicket, bool>) (x => !this.gachaTicketIDQList.Any<Tuple<int, int>>((Func<Tuple<int, int>, bool>) (tp => tp.Item1 == x.ticket_id && tp.Item2 == x.quantity)))))
      return false;
    this.gachaTicketIDQList = ((IEnumerable<PlayerGachaTicket>) this.player.Value.gacha_tickets).Select<PlayerGachaTicket, Tuple<int, int>>((Func<PlayerGachaTicket, Tuple<int, int>>) (x => Tuple.Create<int, int>(x.ticket_id, x.quantity))).ToList<Tuple<int, int>>();
    return true;
  }

  private bool checkTutorialGachaState()
  {
    if (Persist.newTutorial.Data.tutoialGacha)
      return false;
    if (Persist.tutorialGacha.Data.haveGachaTicket)
    {
      Future<WebAPI.Response.TutorialTutorialRagnarokResume> future = WebAPI.TutorialTutorialRagnarokResume((System.Action<WebAPI.Response.UserError>) null);
      future.RunOn<WebAPI.Response.TutorialTutorialRagnarokResume>((MonoBehaviour) this, (System.Action<WebAPI.Response.TutorialTutorialRagnarokResume>) (_ =>
      {
        SMManager.Get<Player>().name = Persist.tutorial.Data.PlayerName;
        Singleton<TutorialRoot>.GetInstance().StartTutorial(future.Result);
      }));
      return true;
    }
    Singleton<NGGameDataManager>.GetInstance().StartSceneAsyncProxy((System.Action<NGGameDataManager.StartSceneProxyResult>) (data =>
    {
      if (((IEnumerable<PlayerGachaTicket>) Player.Current.gacha_tickets).Where<PlayerGachaTicket>((Func<PlayerGachaTicket, bool>) (x => x.quantity > 0)).Any<PlayerGachaTicket>((Func<PlayerGachaTicket, bool>) (x => x.ticket_id == 675)))
      {
        Persist.tutorialGacha.Data.haveGachaTicket = true;
        Singleton<TutorialRoot>.GetInstance().resumeTutorialForVer710();
        Future<WebAPI.Response.TutorialTutorialRagnarokResume> future = WebAPI.TutorialTutorialRagnarokResume((System.Action<WebAPI.Response.UserError>) null);
        future.RunOn<WebAPI.Response.TutorialTutorialRagnarokResume>((MonoBehaviour) this, (System.Action<WebAPI.Response.TutorialTutorialRagnarokResume>) (_ =>
        {
          SMManager.Get<Player>().name = Persist.tutorial.Data.PlayerName;
          Singleton<TutorialRoot>.GetInstance().StartTutorial(future.Result);
        }));
      }
      else
      {
        Singleton<TutorialRoot>.GetInstance().EndTutorial();
        Persist.tutorialGacha.Data.haveGachaTicket = false;
        Persist.tutorialGacha.Flush();
        MypageScene.ChangeScene(false, false, false);
      }
    }));
    return true;
  }

  public string makeKeyPreviewInheritance(int baseUnitId, List<int> materialUnitIds)
  {
    string str = string.Format("{0:X8}-", (object) baseUnitId);
    foreach (int num in (IEnumerable<int>) materialUnitIds.OrderBy<int, int>((Func<int, int>) (i => i)))
      str += num.ToString("x8");
    return str;
  }

  public void clearPreviewInheritance(int baseUnitId = 0)
  {
    if (baseUnitId == 0)
    {
      this.dicPreviewInheritance.Clear();
    }
    else
    {
      string delKey = string.Format("{0:X8}", (object) baseUnitId);
      foreach (string key in this.dicPreviewInheritance.Keys.Where<string>((Func<string, bool>) (s => s.StartsWith(delKey))).ToList<string>())
        this.dicPreviewInheritance.Remove(key);
    }
  }

  public OverkillersSlotRelease.Conditions[] getOverkillersSlotReleaseConditions(
    int same_character_id)
  {
    OverkillersSlotRelease.Conditions[] conditionsArray1;
    if (this.dicOverkillersSlotReleaseConditions.TryGetValue(same_character_id, out conditionsArray1))
      return conditionsArray1;
    OverkillersSlotRelease.Conditions[] conditionsArray2 = OverkillersSlotRelease.find(same_character_id)?.getConditions() ?? new OverkillersSlotRelease.Conditions[0];
    this.dicOverkillersSlotReleaseConditions.Add(same_character_id, conditionsArray2);
    return conditionsArray2;
  }

  public void resetOverkillersSlotReleaseConditions()
  {
    this.dicOverkillersSlotReleaseConditions.Clear();
  }

  public enum FromUnityPopup
  {
    None,
    Unit00484Scene,
    Unit004JobChangeScene,
    Unit0042Scene,
  }

  [Serializable]
  public class TimeCounter
  {
    private Modified<Player> player;
    private Modified<SeaPlayer> seaPlayer;
    private float apElapsedSeconds;
    private int ap_full_remain;
    private float bpElapsedSeconds;
    private int bp_full_remain;
    private float dpElapsedSeconds;
    private int dp_full_remain;

    public float ApRecoverySecondsPerPoint
    {
      get
      {
        return this.player.Value == null || this.player.Value.ap >= this.player.Value.ap_max ? 0.0f : (float) this.player.Value.ap_auto_healing_sec - this.apElapsedSeconds;
      }
    }

    public float ApFullRecoverySeconds
    {
      get
      {
        return this.player.Value == null ? 0.0f : (float) ((this.player.Value.ap_max - this.player.Value.ap) * this.player.Value.ap_auto_healing_sec) - this.apElapsedSeconds;
      }
    }

    public float BpRecoverySecondsPerPoint
    {
      get
      {
        return this.player.Value == null || this.player.Value.bp >= this.player.Value.bp_max ? 0.0f : (float) this.player.Value.bp_auto_healing_sec - this.bpElapsedSeconds;
      }
    }

    public float BpFullRecoverySeconds
    {
      get
      {
        return this.player.Value == null ? 0.0f : (float) ((this.player.Value.bp_max - this.player.Value.bp) * this.player.Value.bp_auto_healing_sec) - this.bpElapsedSeconds;
      }
    }

    public float DpRecoverySecondsPerPoint
    {
      get
      {
        return this.seaPlayer.Value == null || this.seaPlayer.Value.dp >= this.seaPlayer.Value.dp_max ? 0.0f : (float) this.seaPlayer.Value.dp_auto_healing_sec - this.dpElapsedSeconds;
      }
    }

    public float DpFullRecoverySeconds
    {
      get
      {
        return this.seaPlayer.Value == null ? 0.0f : (float) ((this.seaPlayer.Value.dp_max - this.seaPlayer.Value.dp) * this.seaPlayer.Value.dp_auto_healing_sec) - this.dpElapsedSeconds;
      }
    }

    public void Init(Modified<Player> p, Modified<SeaPlayer> s)
    {
      this.player = p;
      this.seaPlayer = s;
    }

    private bool ApDeltaTime(bool isChanged, float delta)
    {
      if (this.player.Value.ap >= this.player.Value.ap_max)
      {
        this.apElapsedSeconds = 0.0f;
        this.ap_full_remain = 0;
        return isChanged;
      }
      if (isChanged && this.ap_full_remain != this.player.Value.ap_full_remain)
      {
        this.ap_full_remain = this.player.Value.ap_full_remain;
        this.apElapsedSeconds = (float) (this.player.Value.ap_auto_healing_sec - this.player.Value.ap_full_remain % this.player.Value.ap_auto_healing_sec);
      }
      else
        isChanged = (int) this.apElapsedSeconds < (int) (this.apElapsedSeconds += delta);
      return isChanged;
    }

    private bool BpDeltaTime(bool isChanged, float delta)
    {
      if (this.player.Value.bp >= this.player.Value.bp_max)
      {
        this.bpElapsedSeconds = 0.0f;
        this.bp_full_remain = 0;
        return isChanged;
      }
      if (isChanged && this.bp_full_remain != this.player.Value.bp_full_remain)
      {
        this.bp_full_remain = this.player.Value.bp_full_remain;
        this.bpElapsedSeconds = (float) (this.player.Value.bp_auto_healing_sec - this.player.Value.bp_full_remain % this.player.Value.bp_auto_healing_sec);
      }
      else
        isChanged = (int) this.bpElapsedSeconds < (int) (this.bpElapsedSeconds += delta);
      return isChanged;
    }

    private bool DpDeltaTime(bool isChanged, float delta)
    {
      if (this.seaPlayer.Value.dp >= this.seaPlayer.Value.dp_max)
      {
        this.dpElapsedSeconds = 0.0f;
        this.dp_full_remain = 0;
        return isChanged;
      }
      if (isChanged && this.dp_full_remain != this.seaPlayer.Value.dp_full_remain)
      {
        this.dp_full_remain = this.seaPlayer.Value.dp_full_remain;
        this.dpElapsedSeconds = (float) Mathf.Max((this.seaPlayer.Value.dp_max - this.seaPlayer.Value.dp) * this.seaPlayer.Value.dp_auto_healing_sec - this.seaPlayer.Value.dp_full_remain, 0);
      }
      else
        isChanged = (int) this.dpElapsedSeconds < (int) (this.dpElapsedSeconds += delta);
      return isChanged;
    }

    private bool ApRecoveryPoint()
    {
      int num = (int) ((double) this.apElapsedSeconds / (double) this.player.Value.ap_auto_healing_sec);
      if (num <= 0)
        return false;
      this.player.Value.ap += num;
      this.apElapsedSeconds -= (float) (num * this.player.Value.ap_auto_healing_sec);
      if (this.player.Value.ap >= this.player.Value.ap_max)
      {
        this.player.Value.ap = this.player.Value.ap_max;
        this.apElapsedSeconds = 0.0f;
      }
      return true;
    }

    private bool BpRecoveryPoint()
    {
      int num = (int) ((double) this.bpElapsedSeconds / (double) this.player.Value.bp_auto_healing_sec);
      if (num <= 0)
        return false;
      this.player.Value.bp += num;
      this.bpElapsedSeconds -= (float) (num * this.player.Value.bp_auto_healing_sec);
      if (this.player.Value.bp >= this.player.Value.bp_max)
      {
        this.player.Value.bp = this.player.Value.bp_max;
        this.bpElapsedSeconds = 0.0f;
      }
      return true;
    }

    private bool DpRecoveryPoint()
    {
      int num = (int) ((double) this.dpElapsedSeconds / (double) this.seaPlayer.Value.dp_auto_healing_sec);
      if (num <= 0)
        return false;
      this.seaPlayer.Value.dp += num;
      this.dpElapsedSeconds -= (float) (num * this.seaPlayer.Value.dp_auto_healing_sec);
      if (this.seaPlayer.Value.dp >= this.seaPlayer.Value.dp_max)
      {
        this.seaPlayer.Value.dp = this.seaPlayer.Value.dp_max;
        this.dpElapsedSeconds = 0.0f;
      }
      return true;
    }

    public bool AddDeltaTime(float delta)
    {
      if (this.player.Value == null)
        return false;
      bool isChanged = this.ApDeltaTime(this.player.IsChangedOnce(), delta);
      bool flag = isChanged | this.BpDeltaTime(isChanged, delta);
      if (this.ApRecoveryPoint() || this.BpRecoveryPoint())
      {
        this.player.NotifyChanged();
        this.player.IsChangedOnce();
      }
      if (this.seaPlayer.Value != null)
      {
        flag |= this.DpDeltaTime(this.seaPlayer.IsChangedOnce(), delta);
        if (this.DpRecoveryPoint())
        {
          this.seaPlayer.NotifyChanged();
          this.seaPlayer.IsChangedOnce();
        }
      }
      return flag;
    }
  }

  public class Boost
  {
    private int[] m_TypeIdList;
    private int[] m_PeriodActiveList;

    public int[] TypeIdList
    {
      get
      {
        return this.m_TypeIdList;
      }
    }

    public Decimal DiscountGearDrilling
    {
      get
      {
        BoostBonusGearDrilling bonusGearDrilling = ((IEnumerable<BoostBonusGearDrilling>) MasterData.BoostBonusGearDrillingList).FirstOrDefault<BoostBonusGearDrilling>((Func<BoostBonusGearDrilling, bool>) (fd => ((IEnumerable<int>) this.m_PeriodActiveList).Contains<int>(fd.period_id_BoostPeriod)));
        return bonusGearDrilling != null ? (Decimal) bonusGearDrilling.increase_price : new Decimal(10, 0, 0, false, (byte) 1);
      }
    }

    public Decimal BootRateGearDrilling
    {
      get
      {
        BoostBonusGearDrilling bonusGearDrilling = ((IEnumerable<BoostBonusGearDrilling>) MasterData.BoostBonusGearDrillingList).FirstOrDefault<BoostBonusGearDrilling>((Func<BoostBonusGearDrilling, bool>) (fd => ((IEnumerable<int>) this.m_PeriodActiveList).Contains<int>(fd.period_id_BoostPeriod)));
        return bonusGearDrilling != null ? (Decimal) bonusGearDrilling.boot_rate : new Decimal(10, 0, 0, false, (byte) 1);
      }
    }

    public Decimal DiscountGearCombine
    {
      get
      {
        BoostBonusGearCombine bonusGearCombine = ((IEnumerable<BoostBonusGearCombine>) MasterData.BoostBonusGearCombineList).FirstOrDefault<BoostBonusGearCombine>((Func<BoostBonusGearCombine, bool>) (fd => ((IEnumerable<int>) this.m_PeriodActiveList).Contains<int>(fd.period_id_BoostPeriod)));
        return bonusGearCombine != null ? (Decimal) bonusGearCombine.increase_price : new Decimal(10, 0, 0, false, (byte) 1);
      }
    }

    public Decimal DiscountUnitBuildup
    {
      get
      {
        BoostBonusUnitBuildup bonusUnitBuildup = ((IEnumerable<BoostBonusUnitBuildup>) MasterData.BoostBonusUnitBuildupList).FirstOrDefault<BoostBonusUnitBuildup>((Func<BoostBonusUnitBuildup, bool>) (fd => ((IEnumerable<int>) this.m_PeriodActiveList).Contains<int>(fd.period_id_BoostPeriod)));
        return bonusUnitBuildup != null ? (Decimal) bonusUnitBuildup.increase_price : new Decimal(10, 0, 0, false, (byte) 1);
      }
    }

    public Decimal DiscountUnitCompose
    {
      get
      {
        BoostBonusUnitCompose bonusUnitCompose = ((IEnumerable<BoostBonusUnitCompose>) MasterData.BoostBonusUnitComposeList).FirstOrDefault<BoostBonusUnitCompose>((Func<BoostBonusUnitCompose, bool>) (fd => ((IEnumerable<int>) this.m_PeriodActiveList).Contains<int>(fd.period_id_BoostPeriod)));
        return bonusUnitCompose != null ? (Decimal) bonusUnitCompose.increase_price : new Decimal(10, 0, 0, false, (byte) 1);
      }
    }

    public Decimal DiscountUnitTransmigrate
    {
      get
      {
        BoostBonusUnitTransmigrate unitTransmigrate = ((IEnumerable<BoostBonusUnitTransmigrate>) MasterData.BoostBonusUnitTransmigrateList).FirstOrDefault<BoostBonusUnitTransmigrate>((Func<BoostBonusUnitTransmigrate, bool>) (fd => ((IEnumerable<int>) this.m_PeriodActiveList).Contains<int>(fd.period_id_BoostPeriod)));
        return unitTransmigrate != null ? (Decimal) unitTransmigrate.increase_price : new Decimal(10, 0, 0, false, (byte) 1);
      }
    }

    public Boost(int[] periodActiveList, int[] typeList)
    {
      this.Init(periodActiveList, typeList);
    }

    public void Init(int[] periodActiveList, int[] typeList)
    {
      this.m_TypeIdList = typeList;
      this.m_PeriodActiveList = periodActiveList;
    }
  }

  public class StartSceneProxyResult
  {
    public bool IsBreak;
  }
}
