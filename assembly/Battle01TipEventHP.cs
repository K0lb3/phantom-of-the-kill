﻿// Decompiled with JetBrains decompiler
// Type: Battle01TipEventHP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Battle01TipEventHP : Battle01TipEventBase
{
  private UnitIcon unitIcon;

  public override IEnumerator onInitAsync()
  {
    Battle01TipEventHP battle01TipEventHp = this;
    Future<GameObject> f = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    battle01TipEventHp.unitIcon = battle01TipEventHp.cloneIcon<UnitIcon>(f.Result, 0);
    battle01TipEventHp.selectIcon(0);
  }

  private IEnumerator doSetIcon(UnitUnit unit)
  {
    IEnumerator e = this.unitIcon.SetUnit(unit, unit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitIcon.BottomModeValue = UnitIconBase.BottomMode.Nothing;
  }

  public override void setData(BL.DropData e, BL.Unit unit)
  {
  }
}
