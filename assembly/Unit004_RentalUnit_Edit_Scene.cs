﻿// Decompiled with JetBrains decompiler
// Type: Unit004_RentalUnit_Edit_Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Unit004_RentalUnit_Edit_Scene : NGSceneBase
{
  private static readonly string DEFAULT_NAME = "unit004_RentalUnit_Edit";
  private Unit004_RentalUnit_Edit_Menu rentalUnitEditMenu;

  public static void ChangeScene(bool bstack = true)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene(Unit004_RentalUnit_Edit_Scene.DEFAULT_NAME, bstack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    Unit004_RentalUnit_Edit_Scene rentalUnitEditScene = this;
    if ((UnityEngine.Object) rentalUnitEditScene.rentalUnitEditMenu == (UnityEngine.Object) null)
      rentalUnitEditScene.rentalUnitEditMenu = rentalUnitEditScene.gameObject.GetComponent<Unit004_RentalUnit_Edit_Menu>();
    PlayerDeck playerDeck = SMManager.Get<PlayerDeck[]>()[0];
    Player player = SMManager.Get<Player>();
    PlayerUnit[] playerUnits = SMManager.Get<PlayerUnit[]>();
    IEnumerator e;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      playerUnits = ((IEnumerable<PlayerUnit>) playerUnits).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsSea)).ToArray<PlayerUnit>();
      e = rentalUnitEditScene.SetSeaBgm();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(false);
    rentalUnitEditScene.rentalUnitEditMenu.SetIconType(UnitMenuBase.IconType.Normal);
    e = rentalUnitEditScene.rentalUnitEditMenu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = rentalUnitEditScene.rentalUnitEditMenu.Initalize(playerDeck, playerUnits, new Promise<int?[]>(), player.max_cost, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onStartScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  private IEnumerator SetSeaBgm()
  {
    Unit004_RentalUnit_Edit_Scene rentalUnitEditScene = this;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    SeaHomeMap seaHomeMap = ((IEnumerable<SeaHomeMap>) MasterData.SeaHomeMapList).ActiveSeaHomeMap(ServerTime.NowAppTimeAddDelta());
    if (seaHomeMap != null && !string.IsNullOrEmpty(seaHomeMap.bgm_cuesheet_name) && !string.IsNullOrEmpty(seaHomeMap.bgm_cue_name))
    {
      rentalUnitEditScene.bgmFile = seaHomeMap.bgm_cuesheet_name;
      rentalUnitEditScene.bgmName = seaHomeMap.bgm_cue_name;
    }
  }

  public override IEnumerator onEndSceneAsync()
  {
    Unit004_RentalUnit_Edit_Scene rentalUnitEditScene = this;
    float startTime = Time.time;
    while (!rentalUnitEditScene.isTweenFinished && (double) Time.time - (double) startTime < (double) rentalUnitEditScene.tweenTimeoutTime)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    rentalUnitEditScene.isTweenFinished = true;
    yield return (object) null;
    // ISSUE: reference to a compiler-generated method
    yield return (object) rentalUnitEditScene.\u003C\u003En__0();
  }
}
