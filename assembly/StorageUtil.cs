﻿// Decompiled with JetBrains decompiler
// Type: StorageUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.IO;
using UnityEngine;

public static class StorageUtil
{
  private static readonly string m_persistentDataPath = Application.dataPath + "/../\\Data";
  private static readonly string m_temporaryCachePath = Application.dataPath + "/../\\Cache";

  static StorageUtil()
  {
    if (!Directory.Exists(StorageUtil.m_persistentDataPath))
      Directory.CreateDirectory(StorageUtil.m_persistentDataPath);
    if (Directory.Exists(StorageUtil.m_temporaryCachePath))
      return;
    Directory.CreateDirectory(StorageUtil.m_temporaryCachePath);
  }

  public static string persistentDataPath
  {
    get
    {
      return StorageUtil.m_persistentDataPath;
    }
  }

  public static string temporaryCachePath
  {
    get
    {
      return StorageUtil.m_temporaryCachePath;
    }
  }
}
