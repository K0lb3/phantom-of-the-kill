﻿// Decompiled with JetBrains decompiler
// Type: RequestDispatcher
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using System.Collections;
using UnityEngine;

public class RequestDispatcher : MonoBehaviour
{
  public static IEnumerator EquipGear(
    int changeGearIndex,
    int? afterGearID,
    int playerUnitID,
    System.Action<WebAPI.Response.UserError> errorHandler,
    bool isEarthMode = false)
  {
    if (isEarthMode)
    {
      EarthDataManager instance = Singleton<EarthDataManager>.GetInstance();
      if (afterGearID.HasValue && !instance.EquipGear(playerUnitID, afterGearID.Value))
        errorHandler((WebAPI.Response.UserError) null);
    }
    else
    {
      IEnumerator e = WebAPI.UnitEquip(changeGearIndex, afterGearID, playerUnitID, errorHandler).Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }
}
