﻿// Decompiled with JetBrains decompiler
// Type: PopupPvpClassNameEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using UnityEngine;

public class PopupPvpClassNameEffect : MonoBehaviour
{
  [SerializeField]
  private UILabel txtDescription;
  [SerializeField]
  private UILabel txtDescription2;

  public void Init(PvpClassKind classData, PvpClassKind.Condition condition)
  {
    this.SetDescription(classData, condition);
  }

  private void SetDescription(PvpClassKind classData, PvpClassKind.Condition condition)
  {
    Consts instance = Consts.GetInstance();
    string text = "";
    string name = classData.name;
    switch (condition)
    {
      case PvpClassKind.Condition.DOWN:
        if (classData.PreviousClass != null)
          name = classData.PreviousClass.name;
        text = instance.VERSUS_002689POPUP_DESCRIPTION_DOWN;
        break;
      case PvpClassKind.Condition.STAY:
        text = instance.VERSUS_002689POPUP_DESCRIPTION_STAY;
        break;
      case PvpClassKind.Condition.STAY_TOPCLASS:
      case PvpClassKind.Condition.TITLE_TOPCLASS:
        text = instance.VERSUS_002689POPUP_DESCRIPTION_TOP;
        break;
      case PvpClassKind.Condition.UP:
      case PvpClassKind.Condition.TITLE:
        if (classData.NextClass != null)
          name = classData.NextClass.name;
        text = instance.VERSUS_002689POPUP_DESCRIPTION_UP;
        break;
    }
    this.txtDescription.SetText(Consts.Format(instance.VERSUS_002689POPUP_DESCRIPTION_NAME, (IDictionary) new Hashtable()
    {
      {
        (object) "name",
        (object) name
      }
    }));
    this.txtDescription2.SetText(text);
  }
}
