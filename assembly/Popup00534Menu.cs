﻿// Decompiled with JetBrains decompiler
// Type: Popup00534Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Popup00534Menu : BackButtonMonoBehaiviour
{
  [SerializeField]
  public GameObject CompositeDescription;
  [SerializeField]
  public GameObject RepairDescription;

  public void IbtnOk()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.IbtnOk();
  }
}
