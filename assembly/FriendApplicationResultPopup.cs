﻿// Decompiled with JetBrains decompiler
// Type: FriendApplicationResultPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class FriendApplicationResultPopup : BackButtonMenuBase
{
  public UIButton OK;

  public virtual void IbtnPopupOk()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnPopupOk();
  }
}
