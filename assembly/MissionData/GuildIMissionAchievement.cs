﻿// Decompiled with JetBrains decompiler
// Type: MissionData.GuildIMissionAchievement
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

namespace MissionData
{
  internal class GuildIMissionAchievement : IMissionAchievement
  {
    private GuildMissionInfo data_;
    private GuildMission master_;
    private IMissionReward[] rewards_;
    private GuildIMission iMaster_;

    public bool isDaily
    {
      get
      {
        return false;
      }
    }

    public bool isGuild
    {
      get
      {
        return true;
      }
    }

    public bool isShow
    {
      get
      {
        DateTime? guildAchievedAt = this.data_.guild_achieved_at;
        DateTime? joinedAt = PlayerAffiliation.Current.joined_at;
        GuildMissionReward[] guildRewards = this.data_.guild_rewards;
        if ((guildRewards != null ? guildRewards.Length : 0) <= 0 || this.mission == null || !this.mission.isShow)
          return false;
        if (this.isReceived || !guildAchievedAt.HasValue)
          return true;
        if (!joinedAt.HasValue)
          return false;
        DateTime? nullable1 = joinedAt;
        DateTime? nullable2 = guildAchievedAt;
        return nullable1.HasValue & nullable2.HasValue && nullable1.GetValueOrDefault() < nullable2.GetValueOrDefault();
      }
    }

    public bool isCleared
    {
      get
      {
        if (this.isReceived)
          return true;
        return this.master_ != null && this.data_.guild_count >= this.master_.achievement_count;
      }
    }

    public bool isOwnCleared
    {
      get
      {
        return this.master_ != null && this.data_.count >= this.master_.num;
      }
    }

    public bool isReceived
    {
      get
      {
        return this.data_.received_count > 0;
      }
    }

    public object original
    {
      get
      {
        return (object) this.data_;
      }
    }

    public int progress_count
    {
      get
      {
        return this.data_.guild_count;
      }
    }

    public int own_progress_count
    {
      get
      {
        return this.data_.count;
      }
    }

    public IMissionReward[] rewards
    {
      get
      {
        if (this.rewards_ != null)
          return this.rewards_;
        this.rewards_ = this.data_.guild_rewards != null ? (IMissionReward[]) ((IEnumerable<GuildMissionReward>) this.data_.guild_rewards).Select<GuildMissionReward, GuildIMissionReward>((Func<GuildMissionReward, GuildIMissionReward>) (r => new GuildIMissionReward(r))).ToArray<GuildIMissionReward>() : new IMissionReward[0];
        return this.rewards_;
      }
    }

    public int mission_id
    {
      get
      {
        return this.data_.mission_id;
      }
    }

    public IMission mission
    {
      get
      {
        return (IMission) this.iMaster_;
      }
    }

    public GuildIMissionAchievement(GuildMissionInfo dat)
    {
      this.data_ = dat;
      this.rewards_ = (IMissionReward[]) null;
      this.iMaster_ = MasterData.GuildMission.TryGetValue(this.data_.mission_id, out this.master_) ? new GuildIMission(this.master_) : (GuildIMission) null;
    }
  }
}
