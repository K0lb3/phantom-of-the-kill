﻿// Decompiled with JetBrains decompiler
// Type: Guide0114UnitSortAndFilterButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;

public class Guide0114UnitSortAndFilterButton : GuideSortAndFilterButton
{
  public Guide0114UnitSortAndFilter menu;
  public Guide0114UnitSortAndFilter.GUIDE_UNIT_SORT_FILTER_CATEGORY categoryID;
  public GuideSortAndFilter.GUIDE_GEAR_CATEGORY_TYPE gearCategory;

  public override void PressButton()
  {
    switch (this.categoryID)
    {
      case Guide0114UnitSortAndFilter.GUIDE_UNIT_SORT_FILTER_CATEGORY.GEAR:
        this.menu.IbtnFilter<GearKindEnum>(this.menu.GearKindEnumList, this.gearKindEnum, (SortAndFilterButton) this);
        break;
      case Guide0114UnitSortAndFilter.GUIDE_UNIT_SORT_FILTER_CATEGORY.CATEGORY:
        this.menu.IbtnFilter<GuideSortAndFilter.GUIDE_GEAR_CATEGORY_TYPE>(this.menu.GearCategoryList, this.gearCategory, (SortAndFilterButton) this);
        break;
      case Guide0114UnitSortAndFilter.GUIDE_UNIT_SORT_FILTER_CATEGORY.SORT1:
        this.menu.IbtnSortType(this.sort1);
        break;
      case Guide0114UnitSortAndFilter.GUIDE_UNIT_SORT_FILTER_CATEGORY.SORT2:
        this.menu.IbtnOrderBuySort(this.orderBuySort);
        break;
    }
  }
}
