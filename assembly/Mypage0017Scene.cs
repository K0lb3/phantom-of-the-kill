﻿// Decompiled with JetBrains decompiler
// Type: Mypage0017Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;

public class Mypage0017Scene : NGSceneBase
{
  public Mypage0017Menu menu;

  public static void changeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("mypage001_7", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.menu.Init(SMManager.Get<PlayerPresent[]>());
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
