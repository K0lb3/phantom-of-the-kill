﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleLandformEffectGroup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleLandformEffectGroup
  {
    public int ID;
    public string play_prefab_file_name;

    public static BattleLandformEffectGroup Parse(MasterDataReader reader)
    {
      return new BattleLandformEffectGroup()
      {
        ID = reader.ReadInt(),
        play_prefab_file_name = reader.ReadString(true)
      };
    }
  }
}
