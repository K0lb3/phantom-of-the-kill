﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitGroupLargeCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitGroupLargeCategory
  {
    public int ID;
    public string name;
    public string short_label_name;
    public string description;
    public DateTime? start_at;

    public static UnitGroupLargeCategory Parse(MasterDataReader reader)
    {
      return new UnitGroupLargeCategory()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        short_label_name = reader.ReadString(true),
        description = reader.ReadString(true),
        start_at = reader.ReadDateTimeOrNull()
      };
    }

    public string GetSpriteName()
    {
      return string.Format("l_classification_{0}", (object) this.ID);
    }

    public enum Type
    {
      Normal = 1,
      School = 2,
      Earth = 3,
      Sea = 4,
      SecondStory = 5,
      Heaven = 6,
      ThirdStory = 7,
    }
  }
}
