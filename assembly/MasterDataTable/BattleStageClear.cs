﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleStageClear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleStageClear
  {
    public int ID;
    public int reward_group_id;
    public int only_first;
    public int entity_type_CommonRewardType;
    public int reward_id;
    public string reward_message;

    public static BattleStageClear Parse(MasterDataReader reader)
    {
      return new BattleStageClear()
      {
        ID = reader.ReadInt(),
        reward_group_id = reader.ReadInt(),
        only_first = reader.ReadInt(),
        entity_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_message = reader.ReadString(true)
      };
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
