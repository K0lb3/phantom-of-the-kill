﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BreakThroughBuildupSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BreakThroughBuildupSkill
  {
    public int ID;
    public int skill_id;

    public static BreakThroughBuildupSkill Parse(MasterDataReader reader)
    {
      return new BreakThroughBuildupSkill()
      {
        ID = reader.ReadInt(),
        skill_id = reader.ReadInt()
      };
    }
  }
}
