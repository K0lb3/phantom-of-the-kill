﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

namespace MasterDataTable
{
  [Serializable]
  public class GuildSetting
  {
    public int ID;
    public string key;
    public string value_type;
    public string value;

    public static GuildSetting Parse(MasterDataReader reader)
    {
      return new GuildSetting()
      {
        ID = reader.ReadInt(),
        key = reader.ReadString(true),
        value_type = reader.ReadString(true),
        value = reader.ReadString(true)
      };
    }

    public int? GetIntValue()
    {
      int result1 = 0;
      float result2 = 0.0f;
      if (this.value_type == "integer")
      {
        if (float.TryParse(this.value, out result2))
          return new int?(Mathf.FloorToInt(result2));
        if (int.TryParse(this.value, out result1))
          return new int?(result1);
      }
      return new int?();
    }

    public string GetStringValue()
    {
      return this.value;
    }
  }
}
