﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestStoryClearMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestStoryClearMessage
  {
    public int ID;
    public bool is_firsttime;
    public int quest_s_id;
    public string title;
    public string message;

    public static QuestStoryClearMessage Parse(MasterDataReader reader)
    {
      return new QuestStoryClearMessage()
      {
        ID = reader.ReadInt(),
        is_firsttime = reader.ReadBool(),
        quest_s_id = reader.ReadInt(),
        title = reader.ReadString(true),
        message = reader.ReadString(true)
      };
    }
  }
}
