﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.RouletteR001FreeAnimationPattern
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class RouletteR001FreeAnimationPattern
  {
    public int ID;
    public int action_pattern_id;
    public int weight;
    public int cutin;

    public static RouletteR001FreeAnimationPattern Parse(
      MasterDataReader reader)
    {
      return new RouletteR001FreeAnimationPattern()
      {
        ID = reader.ReadInt(),
        action_pattern_id = reader.ReadInt(),
        weight = reader.ReadInt(),
        cutin = reader.ReadInt()
      };
    }
  }
}
