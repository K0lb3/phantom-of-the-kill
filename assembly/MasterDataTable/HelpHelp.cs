﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.HelpHelp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class HelpHelp
  {
    public int ID;
    public int priority;
    public int category_HelpCategory;
    public string subcategory_name;
    public string title;
    public string description;
    public string image_name;

    public static HelpHelp Parse(MasterDataReader reader)
    {
      return new HelpHelp()
      {
        ID = reader.ReadInt(),
        priority = reader.ReadInt(),
        category_HelpCategory = reader.ReadInt(),
        subcategory_name = reader.ReadString(true),
        title = reader.ReadString(true),
        description = reader.ReadString(true),
        image_name = reader.ReadString(true)
      };
    }

    public HelpCategory category
    {
      get
      {
        HelpCategory helpCategory;
        if (!MasterData.HelpCategory.TryGetValue(this.category_HelpCategory, out helpCategory))
          Debug.LogError((object) ("Key not Found: MasterData.HelpCategory[" + (object) this.category_HelpCategory + "]"));
        return helpCategory;
      }
    }
  }
}
