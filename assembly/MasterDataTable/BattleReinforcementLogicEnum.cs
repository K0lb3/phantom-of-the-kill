﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleReinforcementLogicEnum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum BattleReinforcementLogicEnum
  {
    turn = 1,
    smash = 2,
    player_attack = 3,
    enemy_attack = 4,
    battle = 5,
    player_area_invasion = 6,
    enemy_area_invasion = 7,
    use_skill = 8,
  }
}
