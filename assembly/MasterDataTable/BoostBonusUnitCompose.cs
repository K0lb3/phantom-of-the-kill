﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BoostBonusUnitCompose
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BoostBonusUnitCompose
  {
    public int ID;
    public int period_id_BoostPeriod;
    public float increase_price;
    public float success_factor;

    public static BoostBonusUnitCompose Parse(MasterDataReader reader)
    {
      return new BoostBonusUnitCompose()
      {
        ID = reader.ReadInt(),
        period_id_BoostPeriod = reader.ReadInt(),
        increase_price = reader.ReadFloat(),
        success_factor = reader.ReadFloat()
      };
    }

    public BoostPeriod period_id
    {
      get
      {
        BoostPeriod boostPeriod;
        if (!MasterData.BoostPeriod.TryGetValue(this.period_id_BoostPeriod, out boostPeriod))
          Debug.LogError((object) ("Key not Found: MasterData.BoostPeriod[" + (object) this.period_id_BoostPeriod + "]"));
        return boostPeriod;
      }
    }
  }
}
