﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ExploreTimeConfig
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class ExploreTimeConfig
  {
    public int ID;
    public string key;
    public int milli_sec;

    public static ExploreTimeConfig Parse(MasterDataReader reader)
    {
      return new ExploreTimeConfig()
      {
        ID = reader.ReadInt(),
        key = reader.ReadString(true),
        milli_sec = reader.ReadInt()
      };
    }
  }
}
