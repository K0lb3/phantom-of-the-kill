﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TotalPaymentBonusContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TotalPaymentBonusContent
  {
    public int ID;
    public string reward_id;

    public static TotalPaymentBonusContent Parse(MasterDataReader reader)
    {
      return new TotalPaymentBonusContent()
      {
        ID = reader.ReadInt(),
        reward_id = reader.ReadString(true)
      };
    }
  }
}
