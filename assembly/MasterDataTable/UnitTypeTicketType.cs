﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitTypeTicketType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum UnitTypeTicketType
  {
    all = 1,
    character = 2,
    unit = 3,
    kind = 4,
    group_clothing = 5,
    group_generation = 6,
    group_large = 7,
    group_small = 8,
    element = 9,
    unit_type_category = 10, // 0x0000000A
    job = 11, // 0x0000000B
  }
}
