﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestSeaMissionReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestSeaMissionReward
  {
    public int ID;
    public int quest_m_QuestSeaM;
    public int reward_type_CommonRewardType;
    public int reward_id;
    public int quantity;
    public string message;
    public string result_message;

    public static QuestSeaMissionReward Parse(MasterDataReader reader)
    {
      return new QuestSeaMissionReward()
      {
        ID = reader.ReadInt(),
        quest_m_QuestSeaM = reader.ReadInt(),
        reward_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        quantity = reader.ReadInt(),
        message = reader.ReadString(true),
        result_message = reader.ReadString(true)
      };
    }

    public QuestSeaM quest_m
    {
      get
      {
        QuestSeaM questSeaM;
        if (!MasterData.QuestSeaM.TryGetValue(this.quest_m_QuestSeaM, out questSeaM))
          Debug.LogError((object) ("Key not Found: MasterData.QuestSeaM[" + (object) this.quest_m_QuestSeaM + "]"));
        return questSeaM;
      }
    }

    public CommonRewardType reward_type
    {
      get
      {
        return (CommonRewardType) this.reward_type_CommonRewardType;
      }
    }
  }
}
