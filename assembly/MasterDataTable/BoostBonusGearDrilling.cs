﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BoostBonusGearDrilling
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BoostBonusGearDrilling
  {
    public int ID;
    public int period_id_BoostPeriod;
    public float boot_rate;
    public float increase_price;

    public static BoostBonusGearDrilling Parse(MasterDataReader reader)
    {
      return new BoostBonusGearDrilling()
      {
        ID = reader.ReadInt(),
        period_id_BoostPeriod = reader.ReadInt(),
        boot_rate = reader.ReadFloat(),
        increase_price = reader.ReadFloat()
      };
    }

    public BoostPeriod period_id
    {
      get
      {
        BoostPeriod boostPeriod;
        if (!MasterData.BoostPeriod.TryGetValue(this.period_id_BoostPeriod, out boostPeriod))
          Debug.LogError((object) ("Key not Found: MasterData.BoostPeriod[" + (object) this.period_id_BoostPeriod + "]"));
        return boostPeriod;
      }
    }
  }
}
