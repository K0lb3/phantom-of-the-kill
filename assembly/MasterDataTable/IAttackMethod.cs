﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.IAttackMethod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  public abstract class IAttackMethod
  {
    private GearAttackClassification? attackClass_;

    public abstract object original { get; }

    public abstract GearKind kind { get; protected set; }

    public abstract BattleskillSkill skill { get; protected set; }

    public abstract string motionKey { get; }

    public GearAttackClassification attackClass
    {
      get
      {
        return !this.attackClass_.HasValue ? (this.attackClass_ = new GearAttackClassification?(this.getAttackClass())).Value : this.attackClass_.Value;
      }
    }

    private GearAttackClassification getAttackClass()
    {
      GearAttackClassification attackClassification = GearAttackClassification.none;
      if (this.skill != null)
      {
        if (this.skill.skill_type == BattleskillSkillType.magic)
        {
          attackClassification = GearAttackClassification.magic;
        }
        else
        {
          BattleskillEffect battleskillEffect = Array.Find<BattleskillEffect>(this.skill.Effects, (Predicate<BattleskillEffect>) (x => x.EffectLogic.Enum == BattleskillEffectLogicEnum.attack_classification));
          if (battleskillEffect != null)
            attackClassification = (GearAttackClassification) battleskillEffect.GetInt(BattleskillEffectLogicArgumentEnum.attack_classification_id);
        }
      }
      return attackClassification;
    }

    public CommonElement element
    {
      get
      {
        BattleskillSkill skill = this.skill;
        return skill == null ? CommonElement.none : skill.element;
      }
    }
  }
}
