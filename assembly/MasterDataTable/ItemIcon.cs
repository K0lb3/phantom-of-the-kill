﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.ItemIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;

namespace MasterDataTable
{
  public class ItemIcon
  {
    public static Future<UnityEngine.Sprite> LoadSpriteThumbnail(
      string name,
      bool withPath = false)
    {
      return withPath ? Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(name, 1f) : Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Sprite>(string.Format("Icons/Item_Icon_{0}", (object) name), 1f);
    }
  }
}
