﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.CommonElementName
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;

namespace MasterDataTable
{
  [Serializable]
  public class CommonElementName
  {
    private static readonly Dictionary<string, string> convTable_ = new Dictionary<string, string>()
    {
      {
        "火",
        "炎"
      }
    };
    public int ID;
    public string name;

    public static string GetName(int commonElementId)
    {
      CommonElementName commonElementName;
      string str;
      if (MasterData.CommonElementName.TryGetValue(commonElementId, out commonElementName))
      {
        if (!CommonElementName.convTable_.TryGetValue(commonElementName.name, out str))
          str = commonElementName.name;
      }
      else
        str = string.Empty;
      return str;
    }

    public static CommonElementName Parse(MasterDataReader reader)
    {
      return new CommonElementName()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true)
      };
    }
  }
}
