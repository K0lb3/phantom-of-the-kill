﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SlotS001MedalDeckEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SlotS001MedalDeckEntity
  {
    public int ID;
    public int deck_id;
    public int reward_type_id_CommonRewardType;
    public int reward_id;
    public int? reward_quantity;

    public static SlotS001MedalDeckEntity Parse(MasterDataReader reader)
    {
      return new SlotS001MedalDeckEntity()
      {
        ID = reader.ReadInt(),
        deck_id = reader.ReadInt(),
        reward_type_id_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_quantity = reader.ReadIntOrNull()
      };
    }

    public CommonRewardType reward_type_id
    {
      get
      {
        return (CommonRewardType) this.reward_type_id_CommonRewardType;
      }
    }
  }
}
