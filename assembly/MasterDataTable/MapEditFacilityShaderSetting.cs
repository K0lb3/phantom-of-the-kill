﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.MapEditFacilityShaderSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;

namespace MasterDataTable
{
  [Serializable]
  public class MapEditFacilityShaderSetting
  {
    public int ID;
    public int unit_UnitUnit;
    public string moving;
    public string installed;

    public static MapEditFacilityShaderSetting Parse(
      MasterDataReader reader)
    {
      return new MapEditFacilityShaderSetting()
      {
        ID = reader.ReadInt(),
        unit_UnitUnit = reader.ReadInt(),
        moving = reader.ReadStringOrNull(true),
        installed = reader.ReadStringOrNull(true)
      };
    }

    public UnitUnit unit
    {
      get
      {
        UnitUnit unitUnit;
        if (!MasterData.UnitUnit.TryGetValue(this.unit_UnitUnit, out unitUnit))
          Debug.LogError((object) ("Key not Found: MasterData.UnitUnit[" + (object) this.unit_UnitUnit + "]"));
        return unitUnit;
      }
    }

    public bool hasMovingMaterial
    {
      get
      {
        return !string.IsNullOrEmpty(this.moving);
      }
    }

    public Future<UnityEngine.Material> LoadMovingMaterial()
    {
      return MapEditFacilityShaderSetting.LoadMaterial(this.moving);
    }

    public bool hasInstalledMaterial
    {
      get
      {
        return !string.IsNullOrEmpty(this.installed);
      }
    }

    public Future<UnityEngine.Material> LoadInstalledMaterial()
    {
      return MapEditFacilityShaderSetting.LoadMaterial(this.installed);
    }

    public static Future<UnityEngine.Material> LoadMaterial(string materialName)
    {
      return string.IsNullOrEmpty(materialName) ? Future.Single<UnityEngine.Material>((UnityEngine.Material) null) : Singleton<ResourceManager>.GetInstance().Load<UnityEngine.Material>(string.Format("Units/SharedFacility/{0}", (object) materialName), 1f);
    }
  }
}
