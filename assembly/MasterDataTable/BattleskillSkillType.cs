﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleskillSkillType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum BattleskillSkillType
  {
    command = 1,
    release = 2,
    passive = 3,
    duel = 4,
    magic = 5,
    leader = 6,
    item = 7,
    enemy = 8,
    ailment = 9,
    growth = 10, // 0x0000000A
    attackClass = 11, // 0x0000000B
    attackElement = 12, // 0x0000000C
    attackMethod = 13, // 0x0000000D
  }
}
