﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.RouletteR001FreePeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class RouletteR001FreePeriod
  {
    public int ID;
    public string name;
    public DateTime? start_at;
    public DateTime? end_at;

    public static RouletteR001FreePeriod Parse(MasterDataReader reader)
    {
      return new RouletteR001FreePeriod()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        start_at = reader.ReadDateTimeOrNull(),
        end_at = reader.ReadDateTimeOrNull()
      };
    }
  }
}
