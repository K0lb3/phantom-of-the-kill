﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildRaidDamageRewardSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildRaidDamageRewardSet
  {
    public int ID;
    public string damage_ratio;
    public string damage_reward_id;

    public static GuildRaidDamageRewardSet Parse(MasterDataReader reader)
    {
      return new GuildRaidDamageRewardSet()
      {
        ID = reader.ReadInt(),
        damage_ratio = reader.ReadString(true),
        damage_reward_id = reader.ReadString(true)
      };
    }
  }
}
