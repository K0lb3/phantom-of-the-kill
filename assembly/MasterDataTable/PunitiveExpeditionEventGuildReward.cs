﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PunitiveExpeditionEventGuildReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PunitiveExpeditionEventGuildReward
  {
    public int ID;
    public int period;
    public int point;
    public int reward_type_id_CommonRewardType;
    public int reward_id;
    public int reward_quantity;
    public string display_text1;
    public string display_text2;
    public string image_name;
    public int alignment;

    public static PunitiveExpeditionEventGuildReward Parse(
      MasterDataReader reader)
    {
      return new PunitiveExpeditionEventGuildReward()
      {
        ID = reader.ReadInt(),
        period = reader.ReadInt(),
        point = reader.ReadInt(),
        reward_type_id_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_quantity = reader.ReadInt(),
        display_text1 = reader.ReadString(true),
        display_text2 = reader.ReadString(true),
        image_name = reader.ReadString(true),
        alignment = reader.ReadInt()
      };
    }

    public CommonRewardType reward_type_id
    {
      get
      {
        return (CommonRewardType) this.reward_type_id_CommonRewardType;
      }
    }
  }
}
