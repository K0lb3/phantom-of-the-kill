﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.SeaAlbumPiece
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class SeaAlbumPiece
  {
    public int ID;
    public int album_id;
    public int piece_id;
    public int same_character_id;
    public string name;
    public int count;
    public bool is_released;

    public static SeaAlbumPiece Parse(MasterDataReader reader)
    {
      return new SeaAlbumPiece()
      {
        ID = reader.ReadInt(),
        album_id = reader.ReadInt(),
        piece_id = reader.ReadInt(),
        same_character_id = reader.ReadInt(),
        name = reader.ReadString(true),
        count = reader.ReadInt(),
        is_released = reader.ReadBool()
      };
    }
  }
}
