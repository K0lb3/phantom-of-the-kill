﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearKindCorrelations
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearKindCorrelations
  {
    public int ID;
    public int attacker_GearKind;
    public int defender_GearKind;
    public bool is_advantage;

    public static GearKindCorrelations Parse(MasterDataReader reader)
    {
      return new GearKindCorrelations()
      {
        ID = reader.ReadInt(),
        attacker_GearKind = reader.ReadInt(),
        defender_GearKind = reader.ReadInt(),
        is_advantage = reader.ReadBool()
      };
    }

    public GearKind attacker
    {
      get
      {
        GearKind gearKind;
        if (!MasterData.GearKind.TryGetValue(this.attacker_GearKind, out gearKind))
          Debug.LogError((object) ("Key not Found: MasterData.GearKind[" + (object) this.attacker_GearKind + "]"));
        return gearKind;
      }
    }

    public GearKind defender
    {
      get
      {
        GearKind gearKind;
        if (!MasterData.GearKind.TryGetValue(this.defender_GearKind, out gearKind))
          Debug.LogError((object) ("Key not Found: MasterData.GearKind[" + (object) this.defender_GearKind + "]"));
        return gearKind;
      }
    }
  }
}
