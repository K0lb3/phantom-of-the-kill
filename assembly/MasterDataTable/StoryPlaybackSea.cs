﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.StoryPlaybackSea
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class StoryPlaybackSea
  {
    public int ID;
    public string name;
    public int quest_QuestSeaS;
    public int priority;

    public static StoryPlaybackSea Parse(MasterDataReader reader)
    {
      return new StoryPlaybackSea()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        quest_QuestSeaS = reader.ReadInt(),
        priority = reader.ReadInt()
      };
    }

    public QuestSeaS quest
    {
      get
      {
        QuestSeaS questSeaS;
        if (!MasterData.QuestSeaS.TryGetValue(this.quest_QuestSeaS, out questSeaS))
          Debug.LogError((object) ("Key not Found: MasterData.QuestSeaS[" + (object) this.quest_QuestSeaS + "]"));
        return questSeaS;
      }
    }
  }
}
