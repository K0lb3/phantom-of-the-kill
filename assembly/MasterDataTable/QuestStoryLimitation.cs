﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestStoryLimitation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestStoryLimitation
  {
    public int ID;
    public int quest_s_id_QuestStoryS;

    public static QuestStoryLimitation Parse(MasterDataReader reader)
    {
      return new QuestStoryLimitation()
      {
        ID = reader.ReadInt(),
        quest_s_id_QuestStoryS = reader.ReadInt()
      };
    }

    public QuestStoryS quest_s_id
    {
      get
      {
        QuestStoryS questStoryS;
        if (!MasterData.QuestStoryS.TryGetValue(this.quest_s_id_QuestStoryS, out questStoryS))
          Debug.LogError((object) ("Key not Found: MasterData.QuestStoryS[" + (object) this.quest_s_id_QuestStoryS + "]"));
        return questStoryS;
      }
    }
  }
}
