﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearMaterialQuestInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearMaterialQuestInfo
  {
    public int ID;
    public int gear_id;
    public string detail_desc1;
    public string detail_desc2;
    public string detail_desc3;

    public static GearMaterialQuestInfo Parse(MasterDataReader reader)
    {
      return new GearMaterialQuestInfo()
      {
        ID = reader.ReadInt(),
        gear_id = reader.ReadInt(),
        detail_desc1 = reader.ReadString(true),
        detail_desc2 = reader.ReadString(true),
        detail_desc3 = reader.ReadString(true)
      };
    }
  }
}
