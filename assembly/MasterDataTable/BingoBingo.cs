﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BingoBingo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BingoBingo
  {
    public int ID;
    public DateTime? end_at;
    public string complete_reward_group_ids;
    public string header_image_name;
    public int priority;

    public static BingoBingo Parse(MasterDataReader reader)
    {
      return new BingoBingo()
      {
        ID = reader.ReadInt(),
        end_at = reader.ReadDateTimeOrNull(),
        complete_reward_group_ids = reader.ReadStringOrNull(true),
        header_image_name = reader.ReadStringOrNull(true),
        priority = reader.ReadInt()
      };
    }
  }
}
