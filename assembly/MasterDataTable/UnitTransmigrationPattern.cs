﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitTransmigrationPattern
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitTransmigrationPattern
  {
    public int ID;
    public int rarity_name_UnitRarity;
    public int price;

    public static UnitTransmigrationPattern Parse(MasterDataReader reader)
    {
      return new UnitTransmigrationPattern()
      {
        ID = reader.ReadInt(),
        rarity_name_UnitRarity = reader.ReadInt(),
        price = reader.ReadInt()
      };
    }

    public UnitRarity rarity_name
    {
      get
      {
        UnitRarity unitRarity;
        if (!MasterData.UnitRarity.TryGetValue(this.rarity_name_UnitRarity, out unitRarity))
          Debug.LogError((object) ("Key not Found: MasterData.UnitRarity[" + (object) this.rarity_name_UnitRarity + "]"));
        return unitRarity;
      }
    }
  }
}
