﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.UnitAwakeningEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class UnitAwakeningEffect
  {
    public int ID;
    public int x;
    public int y;
    public int width;
    public int height;
    public float god_color_r;
    public float god_color_g;
    public float god_color_b;
    public float god_color_weight;

    public static UnitAwakeningEffect Parse(MasterDataReader reader)
    {
      return new UnitAwakeningEffect()
      {
        ID = reader.ReadInt(),
        x = reader.ReadInt(),
        y = reader.ReadInt(),
        width = reader.ReadInt(),
        height = reader.ReadInt(),
        god_color_r = reader.ReadFloat(),
        god_color_g = reader.ReadFloat(),
        god_color_b = reader.ReadFloat(),
        god_color_weight = reader.ReadFloat()
      };
    }
  }
}
