﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildRaidRankingRewardCondition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildRaidRankingRewardCondition
  {
    public int ID;
    public int? high;
    public int? low;
    public string display_text;
    public string image_name;

    public static GuildRaidRankingRewardCondition Parse(
      MasterDataReader reader)
    {
      return new GuildRaidRankingRewardCondition()
      {
        ID = reader.ReadInt(),
        high = reader.ReadIntOrNull(),
        low = reader.ReadIntOrNull(),
        display_text = reader.ReadString(true),
        image_name = reader.ReadString(true)
      };
    }
  }
}
