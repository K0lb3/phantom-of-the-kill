﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.DuelElementHitEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class DuelElementHitEffect
  {
    public int ID;
    public string original_effect_name;
    public int element_CommonElement;
    public string change_effect_name;

    public static DuelElementHitEffect Parse(MasterDataReader reader)
    {
      return new DuelElementHitEffect()
      {
        ID = reader.ReadInt(),
        original_effect_name = reader.ReadStringOrNull(true),
        element_CommonElement = reader.ReadInt(),
        change_effect_name = reader.ReadStringOrNull(true)
      };
    }

    public CommonElement element
    {
      get
      {
        return (CommonElement) this.element_CommonElement;
      }
    }
  }
}
