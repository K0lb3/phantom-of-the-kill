﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.CommonMypageSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class CommonMypageSetting
  {
    public int ID;
    public DateTime? start_at;
    public DateTime? end_at;
    public string background;
    public string bgm_name;
    public string bgm_file;

    public static CommonMypageSetting Parse(MasterDataReader reader)
    {
      return new CommonMypageSetting()
      {
        ID = reader.ReadInt(),
        start_at = reader.ReadDateTimeOrNull(),
        end_at = reader.ReadDateTimeOrNull(),
        background = reader.ReadStringOrNull(true),
        bgm_name = reader.ReadStringOrNull(true),
        bgm_file = reader.ReadStringOrNull(true)
      };
    }
  }
}
