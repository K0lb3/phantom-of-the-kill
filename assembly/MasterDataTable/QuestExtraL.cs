﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestExtraL
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestExtraL
  {
    public int ID;
    public string name;
    public int? description;
    public string background_image_name;
    public int category_QuestExtraCategory;
    public int? quest_ll_QuestExtraLL;
    public bool enabled_header;

    public static QuestExtraL Parse(MasterDataReader reader)
    {
      return new QuestExtraL()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        description = reader.ReadIntOrNull(),
        background_image_name = reader.ReadString(true),
        category_QuestExtraCategory = reader.ReadInt(),
        quest_ll_QuestExtraLL = reader.ReadIntOrNull(),
        enabled_header = reader.ReadBool()
      };
    }

    public QuestExtraCategory category
    {
      get
      {
        QuestExtraCategory questExtraCategory;
        if (!MasterData.QuestExtraCategory.TryGetValue(this.category_QuestExtraCategory, out questExtraCategory))
          Debug.LogError((object) ("Key not Found: MasterData.QuestExtraCategory[" + (object) this.category_QuestExtraCategory + "]"));
        return questExtraCategory;
      }
    }

    public QuestExtraLL quest_ll
    {
      get
      {
        if (!this.quest_ll_QuestExtraLL.HasValue)
          return (QuestExtraLL) null;
        QuestExtraLL questExtraLl;
        if (!MasterData.QuestExtraLL.TryGetValue(this.quest_ll_QuestExtraLL.Value, out questExtraLl))
          Debug.LogError((object) ("Key not Found: MasterData.QuestExtraLL[" + (object) this.quest_ll_QuestExtraLL.Value + "]"));
        return questExtraLl;
      }
    }
  }
}
