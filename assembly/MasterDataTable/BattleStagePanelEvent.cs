﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleStagePanelEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleStagePanelEvent
  {
    public int ID;
    public int initial_coordinate_x;
    public int initial_coordinate_y;

    public static BattleStagePanelEvent Parse(MasterDataReader reader)
    {
      return new BattleStagePanelEvent()
      {
        ID = reader.ReadInt(),
        initial_coordinate_x = reader.ReadInt(),
        initial_coordinate_y = reader.ReadInt()
      };
    }
  }
}
