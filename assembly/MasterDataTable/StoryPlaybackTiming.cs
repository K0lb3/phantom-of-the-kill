﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.StoryPlaybackTiming
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum StoryPlaybackTiming
  {
    before_battle = 1,
    located_player_unit = 2,
    after_battle = 3,
    duel_start = 4,
    turn_init = 5,
    turn_start = 6,
    in_area = 7,
    defeat_player = 8,
    wave_clear = 9,
    select_stage = 10, // 0x0000000A
  }
}
