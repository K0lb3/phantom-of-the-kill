﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.StoryPlaybackStory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class StoryPlaybackStory
  {
    public int ID;
    public string name;
    public int quest_QuestStoryS;
    public int priority;

    public static StoryPlaybackStory Parse(MasterDataReader reader)
    {
      return new StoryPlaybackStory()
      {
        ID = reader.ReadInt(),
        name = reader.ReadString(true),
        quest_QuestStoryS = reader.ReadInt(),
        priority = reader.ReadInt()
      };
    }

    public QuestStoryS quest
    {
      get
      {
        QuestStoryS questStoryS;
        if (!MasterData.QuestStoryS.TryGetValue(this.quest_QuestStoryS, out questStoryS))
          Debug.LogError((object) ("Key not Found: MasterData.QuestStoryS[" + (object) this.quest_QuestStoryS + "]"));
        return questStoryS;
      }
    }
  }
}
