﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PvpClassReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class PvpClassReward
  {
    public int ID;
    public int class_kind_PvpClassKind;
    public int class_reward_type_PvpClassRewardTypeEnum;
    public int reward_type_CommonRewardType;
    public int reward_id;
    public int reward_quantity;
    public string reward_message;

    public static PvpClassReward Parse(MasterDataReader reader)
    {
      return new PvpClassReward()
      {
        ID = reader.ReadInt(),
        class_kind_PvpClassKind = reader.ReadInt(),
        class_reward_type_PvpClassRewardTypeEnum = reader.ReadInt(),
        reward_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_quantity = reader.ReadInt(),
        reward_message = reader.ReadString(true)
      };
    }

    public PvpClassKind class_kind
    {
      get
      {
        PvpClassKind pvpClassKind;
        if (!MasterData.PvpClassKind.TryGetValue(this.class_kind_PvpClassKind, out pvpClassKind))
          Debug.LogError((object) ("Key not Found: MasterData.PvpClassKind[" + (object) this.class_kind_PvpClassKind + "]"));
        return pvpClassKind;
      }
    }

    public PvpClassRewardTypeEnum class_reward_type
    {
      get
      {
        return (PvpClassRewardTypeEnum) this.class_reward_type_PvpClassRewardTypeEnum;
      }
    }

    public CommonRewardType reward_type
    {
      get
      {
        return (CommonRewardType) this.reward_type_CommonRewardType;
      }
    }
  }
}
