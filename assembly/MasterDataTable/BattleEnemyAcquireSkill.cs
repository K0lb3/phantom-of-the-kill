﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.BattleEnemyAcquireSkill
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class BattleEnemyAcquireSkill
  {
    public int ID;
    public int group_id;
    public int level;
    public int skill_level_up_rate;
    public int skill_BattleskillSkill;

    public static BattleEnemyAcquireSkill Parse(MasterDataReader reader)
    {
      return new BattleEnemyAcquireSkill()
      {
        ID = reader.ReadInt(),
        group_id = reader.ReadInt(),
        level = reader.ReadInt(),
        skill_level_up_rate = reader.ReadInt(),
        skill_BattleskillSkill = reader.ReadInt()
      };
    }

    public BattleskillSkill skill
    {
      get
      {
        BattleskillSkill battleskillSkill;
        if (!MasterData.BattleskillSkill.TryGetValue(this.skill_BattleskillSkill, out battleskillSkill))
          Debug.LogError((object) ("Key not Found: MasterData.BattleskillSkill[" + (object) this.skill_BattleskillSkill + "]"));
        return battleskillSkill;
      }
    }
  }
}
