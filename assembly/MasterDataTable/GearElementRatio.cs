﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearElementRatio
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearElementRatio
  {
    public int ID;
    public int element_CommonElement;
    public int family_UnitFamily;
    public float ratio;

    public static GearElementRatio Parse(MasterDataReader reader)
    {
      return new GearElementRatio()
      {
        ID = reader.ReadInt(),
        element_CommonElement = reader.ReadInt(),
        family_UnitFamily = reader.ReadInt(),
        ratio = reader.ReadFloat()
      };
    }

    public CommonElement element
    {
      get
      {
        return (CommonElement) this.element_CommonElement;
      }
    }

    public UnitFamily family
    {
      get
      {
        return (UnitFamily) this.family_UnitFamily;
      }
    }
  }
}
