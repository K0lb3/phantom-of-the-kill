﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GearKindIncr
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GearKindIncr
  {
    public int ID;
    public int attack_kind_GearKind;
    public int defense_kind_GearKind;
    public int proficiency_UnitProficiency;
    public int attack;
    public int hit;

    public static GearKindIncr Parse(MasterDataReader reader)
    {
      return new GearKindIncr()
      {
        ID = reader.ReadInt(),
        attack_kind_GearKind = reader.ReadInt(),
        defense_kind_GearKind = reader.ReadInt(),
        proficiency_UnitProficiency = reader.ReadInt(),
        attack = reader.ReadInt(),
        hit = reader.ReadInt()
      };
    }

    public GearKind attack_kind
    {
      get
      {
        GearKind gearKind;
        if (!MasterData.GearKind.TryGetValue(this.attack_kind_GearKind, out gearKind))
          Debug.LogError((object) ("Key not Found: MasterData.GearKind[" + (object) this.attack_kind_GearKind + "]"));
        return gearKind;
      }
    }

    public GearKind defense_kind
    {
      get
      {
        GearKind gearKind;
        if (!MasterData.GearKind.TryGetValue(this.defense_kind_GearKind, out gearKind))
          Debug.LogError((object) ("Key not Found: MasterData.GearKind[" + (object) this.defense_kind_GearKind + "]"));
        return gearKind;
      }
    }

    public UnitProficiency proficiency
    {
      get
      {
        UnitProficiency unitProficiency;
        if (!MasterData.UnitProficiency.TryGetValue(this.proficiency_UnitProficiency, out unitProficiency))
          Debug.LogError((object) ("Key not Found: MasterData.UnitProficiency[" + (object) this.proficiency_UnitProficiency + "]"));
        return unitProficiency;
      }
    }
  }
}
