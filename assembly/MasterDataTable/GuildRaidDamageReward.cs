﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.GuildRaidDamageReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class GuildRaidDamageReward
  {
    public int ID;
    public int entity_type_CommonRewardType;
    public int reward_id;
    public int reward_value;
    public string reward_title;

    public static GuildRaidDamageReward Parse(MasterDataReader reader)
    {
      return new GuildRaidDamageReward()
      {
        ID = reader.ReadInt(),
        entity_type_CommonRewardType = reader.ReadInt(),
        reward_id = reader.ReadInt(),
        reward_value = reader.ReadInt(),
        reward_title = reader.ReadString(true)
      };
    }

    public CommonRewardType entity_type
    {
      get
      {
        return (CommonRewardType) this.entity_type_CommonRewardType;
      }
    }
  }
}
