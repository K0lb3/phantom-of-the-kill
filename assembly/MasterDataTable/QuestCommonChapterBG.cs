﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestCommonChapterBG
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestCommonChapterBG
  {
    public int ID;
    public string bg_heaven;
    public string bg_lost_ragnarok;

    public static QuestCommonChapterBG Parse(MasterDataReader reader)
    {
      return new QuestCommonChapterBG()
      {
        ID = reader.ReadInt(),
        bg_heaven = reader.ReadStringOrNull(true),
        bg_lost_ragnarok = reader.ReadStringOrNull(true)
      };
    }
  }
}
