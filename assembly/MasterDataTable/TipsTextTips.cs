﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.TipsTextTips
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class TipsTextTips
  {
    public int ID;
    public string title;
    public string description;
    public string subtitle;
    public string sourcename;
    public string image_name;
    public bool enable;

    public static TipsTextTips Parse(MasterDataReader reader)
    {
      return new TipsTextTips()
      {
        ID = reader.ReadInt(),
        title = reader.ReadString(true),
        description = reader.ReadString(true),
        subtitle = reader.ReadString(true),
        sourcename = reader.ReadString(true),
        image_name = reader.ReadString(true),
        enable = reader.ReadBool()
      };
    }
  }
}
