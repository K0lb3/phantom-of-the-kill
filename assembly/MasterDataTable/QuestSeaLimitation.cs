﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.QuestSeaLimitation
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace MasterDataTable
{
  [Serializable]
  public class QuestSeaLimitation
  {
    public int ID;
    public int quest_s_id_QuestSeaS;

    public static QuestSeaLimitation Parse(MasterDataReader reader)
    {
      return new QuestSeaLimitation()
      {
        ID = reader.ReadInt(),
        quest_s_id_QuestSeaS = reader.ReadInt()
      };
    }

    public QuestSeaS quest_s_id
    {
      get
      {
        QuestSeaS questSeaS;
        if (!MasterData.QuestSeaS.TryGetValue(this.quest_s_id_QuestSeaS, out questSeaS))
          Debug.LogError((object) ("Key not Found: MasterData.QuestSeaS[" + (object) this.quest_s_id_QuestSeaS + "]"));
        return questSeaS;
      }
    }
  }
}
