﻿// Decompiled with JetBrains decompiler
// Type: MasterDataTable.PvpVictoryEffectEnum
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace MasterDataTable
{
  public enum PvpVictoryEffectEnum
  {
    excellent_effect = 1,
    great_effect = 2,
    win_effect = 3,
    lose_effect = 4,
    draw_effect = 5,
  }
}
