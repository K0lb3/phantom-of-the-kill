﻿// Decompiled with JetBrains decompiler
// Type: ShopItemGetResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopItemGetResult : MonoBehaviour
{
  [SerializeField]
  private List<GameObject> IconObjects;
  [SerializeField]
  private GameObject messageBox;
  private System.Action onFinishCallback;

  public IEnumerator Init(
    List<ShopItemGetResultInfo> items,
    bool isOverflow,
    System.Action finishCallback)
  {
    this.onFinishCallback = finishCallback;
    this.IconObjects.ForEachIndex<GameObject>((System.Action<GameObject, int>) ((x, i) => x.SetActive(items.Count - 1 == i)));
    GameObject iconObject = this.IconObjects[items.Count - 1];
    int count = 0;
    foreach (Component component in iconObject.transform)
    {
      IEnumerator e = component.gameObject.GetOrAddComponent<CreateIconObject>().CreateThumbnail(items[count].rewardType, items[count].rewardId, items[count].quantity, true, false, new CommonQuestType?(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ++count;
    }
    if (isOverflow)
      this.messageBox.SetActive(true);
    else
      this.messageBox.SetActive(false);
  }

  public void OnTouch()
  {
    this.onFinishCallback();
  }
}
