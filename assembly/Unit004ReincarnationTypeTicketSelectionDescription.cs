﻿// Decompiled with JetBrains decompiler
// Type: Unit004ReincarnationTypeTicketSelectionDescription
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Unit004ReincarnationTypeTicketSelectionDescription : BackButtonMenuBase
{
  [SerializeField]
  private UILabel txtDescription_;
  private UnitTypeTicket ticket_;

  public void initialize(UnitTypeTicket ticket)
  {
    this.ticket_ = ticket;
  }

  private IEnumerator Start()
  {
    this.txtDescription_.SetTextLocalize(this.ticket_.description);
    yield break;
  }

  public override void onBackButton()
  {
    this.OnIbtnBack();
  }

  public void OnIbtnBack()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
