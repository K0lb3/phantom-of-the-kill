﻿// Decompiled with JetBrains decompiler
// Type: CommonRarityAnim
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class CommonRarityAnim : MonoBehaviour
{
  public MeshRenderer image_;
  public MeshRenderer image400_;
  public MeshRenderer image_blur_;
  public MeshRenderer image400_blur_;
  public List<GameObject> rarity_obj_list_;
  public List<GameObject> gacha_rarity_obj_list_;
  public List<CommonRarityAnim.RarityStart> rarity_list;

  [Serializable]
  public class RarityStart
  {
    public GameObject commonRariry;
    public List<GameObject> rarity_list;
    public GameObject rariryText2;
    public GameObject rariryText3;
    public GameObject rariryText4;
    public GameObject rariryText5;
    public GameObject rariryTextBlue3;
    public GameObject rariryTextBlue4;
    public GameObject rariryTextBlue5;
  }
}
