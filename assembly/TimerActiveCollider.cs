﻿// Decompiled with JetBrains decompiler
// Type: TimerActiveCollider
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

public class TimerActiveCollider : MonoBehaviour
{
  [SerializeField]
  private float timer;
  [SerializeField]
  private float delay;
  [SerializeField]
  private BoxCollider[] colliders;

  private void Start()
  {
    this.StartCoroutine(this.Counter());
  }

  private void OnEnable()
  {
    this.StartCoroutine(this.Counter());
  }

  private IEnumerator Counter()
  {
    yield return (object) new WaitForSeconds(this.delay);
    foreach (Collider collider in this.colliders)
      collider.enabled = false;
    yield return (object) new WaitForSeconds(this.timer);
    foreach (BoxCollider collider in this.colliders)
    {
      collider.enabled = true;
      collider.GetComponent<UIButton>().isEnabled = true;
    }
  }
}
