﻿// Decompiled with JetBrains decompiler
// Type: Popup05020Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using Earth;
using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Popup05020Menu : BackButtonMenuBase
{
  public override void onBackButton()
  {
    this.IbtnNo();
  }

  public void IbtnNo()
  {
    this.StartCoroutine(this.ShowResetPopup());
  }

  public void IbtnYes()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (Persist.earthBattleEnvironment.Data.core.battleInfo.quest_type == CommonQuestType.EarthExtra)
      this.StartCoroutine(Singleton<EarthDataManager>.GetInstance().BattleInitExtra(((IEnumerable<EarthExtraQuest>) MasterData.EarthExtraQuestList).FirstOrDefault<EarthExtraQuest>((Func<EarthExtraQuest, bool>) (x => x.ID == Persist.earthBattleEnvironment.Data.core.battleInfo.quest_s_id)), true));
    else
      this.StartCoroutine(Singleton<EarthDataManager>.GetInstance().BattleInitStory(true));
    Singleton<PopupManager>.GetInstance().closeAll(false);
  }

  private IEnumerator ShowResetPopup()
  {
    Future<GameObject> prefabF = Res.Prefabs.popup.popup_050_21__anim_popup01.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().open(prefabF.Result.Clone((Transform) null), false, false, true, true, false, false, "SE_1006");
  }
}
