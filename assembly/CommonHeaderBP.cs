﻿// Decompiled with JetBrains decompiler
// Type: CommonHeaderBP
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CommonHeaderBP : MonoBehaviour
{
  public GameObject[] objects;
  [SerializeField]
  protected int point;
  public UILabel timeText;
  public NGTweenParts timeContainer;

  public int Value
  {
    get
    {
      return this.point;
    }
  }

  public int setValue(int v)
  {
    for (int index = 0; index < this.objects.Length; ++index)
      this.objects[index].SetActive(index < v);
    return this.point = v;
  }

  public void setTime(float time)
  {
    if ((Object) this.timeContainer == (Object) null)
      return;
    bool flag = (double) time > 0.0;
    this.timeContainer.isActive = flag;
    if (!flag)
      return;
    this.timeText.SetTextLocalize(string.Format("{0:00}:{1:00}", (object) Mathf.FloorToInt(time / 60f), (object) Mathf.FloorToInt(time % 60f)));
  }
}
