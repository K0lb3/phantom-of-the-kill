﻿// Decompiled with JetBrains decompiler
// Type: Startup00014ClipEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Startup00014ClipEffect : MonoBehaviour
{
  public Startup00014MakeupMonthly Parent;

  public void PlaySe(string seName)
  {
    if (!((Object) this.Parent != (Object) null) || !this.Parent.isStartupScene)
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((Object) instance != (Object) null))
      return;
    instance.PlaySe(seName, false, 0.0f, -1);
  }

  public void EndScene()
  {
    this.Parent.EndScene();
  }

  public void StepLock(int isLock)
  {
    if (!((Object) this.Parent != (Object) null) || !this.Parent.isStartupScene)
      return;
    this.Parent.StepLock = isLock == 1;
  }

  public void Stamp()
  {
    if (!((Object) this.Parent != (Object) null) || !this.Parent.isStartupScene)
      return;
    this.Parent.Stamp();
  }
}
