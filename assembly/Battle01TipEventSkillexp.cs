﻿// Decompiled with JetBrains decompiler
// Type: Battle01TipEventSkillexp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battle01TipEventSkillexp : Battle01TipEventBase
{
  private UnitIcon unitIcon;

  public override IEnumerator onInitAsync()
  {
    Battle01TipEventSkillexp tipEventSkillexp = this;
    Future<GameObject> f = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    tipEventSkillexp.unitIcon = tipEventSkillexp.cloneIcon<UnitIcon>(f.Result, 0);
    tipEventSkillexp.selectIcon(0);
  }

  private IEnumerator doSetIcon(UnitUnit unit)
  {
    IEnumerator e = this.unitIcon.SetUnit(unit, unit.GetElement(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.unitIcon.BottomModeValue = UnitIconBase.BottomMode.Nothing;
  }

  public override void setData(BL.DropData e, BL.Unit unit)
  {
    Debug.LogWarning((object) (" ==== setData:" + (object) e.reward.Type));
    if (e.reward.Type != MasterDataTable.CommonRewardType.gear_experience_point)
      return;
    this.setText(Consts.Format(Consts.GetInstance().TipEvent_text_skillexp, (IDictionary) new Dictionary<string, string>()
    {
      ["skill"] = unit.playerUnit.equippedGearName
    }));
    Singleton<NGBattleManager>.GetInstance().StartCoroutine(this.doSetIcon(unit.unit));
  }
}
