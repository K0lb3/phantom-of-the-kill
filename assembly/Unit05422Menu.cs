﻿// Decompiled with JetBrains decompiler
// Type: Unit05422Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class Unit05422Menu : Unit00422Menu
{
  protected override void SetBottomMode(UnitIcon icon)
  {
    icon.BottomModeValue = UnitIconBase.BottomMode.FriendlyEarth;
  }

  protected override void SetFriendlyEffect(UnitIconBase icon, bool value)
  {
    icon.SetFriendlyEffect(value, true);
  }

  protected override void SetPosessionText(int value, int max)
  {
    this.TxtOwnnumber.SetTextLocalize(string.Format("{0}", (object) value));
  }
}
