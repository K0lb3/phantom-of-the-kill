﻿// Decompiled with JetBrains decompiler
// Type: ShopGiftMonth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using Gsc.Purchase;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class ShopGiftMonth : MonoBehaviour
{
  [SerializeField]
  private UIWidget pageUiWidget;
  [SerializeField]
  private UIScrollView scroll;
  [SerializeField]
  private UILabel periodLabel;
  [SerializeField]
  private GameObject leftArrow;
  [SerializeField]
  private GameObject rightArrow;
  [SerializeField]
  private UIGrid dailyGrid;
  [SerializeField]
  private GameObject kisekiInfo;
  [SerializeField]
  private UILabel kisekiInfoLabel;
  [SerializeField]
  private GameObject reachingItemBase;
  [SerializeField]
  private UIGrid reachingGrid;
  [SerializeField]
  private UISprite reachingBackground;
  private int defaultReachingBackgroundHeight;
  [SerializeField]
  private UIWidget benefits;
  [SerializeField]
  private UIGrid benefitsGrid;
  [SerializeField]
  private UILabel benefitDropUp;
  [SerializeField]
  private UILabel benefitZenyUp;
  [SerializeField]
  private UILabel benefitExpUp;
  [SerializeField]
  private GameObject receiveRestDay;
  [SerializeField]
  private UILabel receiveRestDayText;
  [SerializeField]
  private UIButton receiveButton;
  [SerializeField]
  private UILabel receiveButtonLabel;
  [SerializeField]
  private UIButton priceButton;
  [SerializeField]
  private UILabel priceLabel;
  private WebAPI.Response.CoinbonusHistory coinbonusHistory;
  private MonthlyPackInfo packInfo;
  private string productId;
  private DateTime now;
  private bool bOnEnable;

  private void OnEnable()
  {
    if (!this.bOnEnable)
      this.bOnEnable = true;
    else
      this.StartCoroutine(this.ArrowRetryActive());
  }

  private IEnumerator ArrowRetryActive()
  {
    if (this.leftArrow.activeSelf)
    {
      this.leftArrow.SetActive(false);
      yield return (object) null;
      this.leftArrow.SetActive(true);
    }
    if (this.rightArrow.activeSelf)
    {
      this.rightArrow.SetActive(false);
      yield return (object) null;
      this.rightArrow.SetActive(true);
    }
  }

  public IEnumerator Init(WebAPI.Response.CoinbonusHistory coinbonusHistory)
  {
    ShopGiftMonth shopGiftMonth = this;
    shopGiftMonth.pageUiWidget.alpha = 0.0f;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    shopGiftMonth.now = ServerTime.NowAppTime();
    shopGiftMonth.coinbonusHistory = coinbonusHistory;
    shopGiftMonth.reachingItemBase.SetActive(false);
    shopGiftMonth.defaultReachingBackgroundHeight = shopGiftMonth.reachingBackground.height;
    if (coinbonusHistory.monthly_packs.Length >= 2)
    {
      shopGiftMonth.leftArrow.gameObject.SetActive(false);
      shopGiftMonth.rightArrow.gameObject.SetActive(true);
    }
    else
    {
      shopGiftMonth.leftArrow.gameObject.SetActive(false);
      shopGiftMonth.rightArrow.gameObject.SetActive(false);
    }
    shopGiftMonth.packInfo = coinbonusHistory.monthly_packs[0];
    // ISSUE: reference to a compiler-generated method
    shopGiftMonth.productId = ((IEnumerable<CoinGroup>) coinbonusHistory.coin_groups).First<CoinGroup>(new Func<CoinGroup, bool>(shopGiftMonth.\u003CInit\u003Eb__30_0)).GetProductId();
    yield return (object) shopGiftMonth.ShowPage();
    shopGiftMonth.pageUiWidget.alpha = 1f;
  }

  private IEnumerator ShowPage()
  {
    ShopGiftMonth shopGiftMonth = this;
    Future<GameObject> prefab = new ResourceObject("Prefabs/common/dir_Reward_IconOnly_Item").Load<GameObject>();
    IEnumerator e = prefab.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject withLoupeIcon = prefab.Result;
    DateTime dateTime1 = shopGiftMonth.packInfo.pack.start_at.Value;
    DateTime dateTime2 = shopGiftMonth.packInfo.pack.end_at.Value;
    shopGiftMonth.periodLabel.text = dateTime1.ToString("yyyy/MM/dd") + "～" + dateTime2.ToString("yyyy/MM/dd");
    for (int index1 = 0; index1 < shopGiftMonth.dailyGrid.transform.childCount; ++index1)
      UnityEngine.Object.Destroy((UnityEngine.Object) shopGiftMonth.dailyGrid.transform.GetChild(index1).gameObject);
    shopGiftMonth.dailyGrid.transform.Clear();
    MonthlyPackReward[] monthlyPackRewardArray = shopGiftMonth.packInfo.rewards;
    int index;
    for (index = 0; index < monthlyPackRewardArray.Length; ++index)
    {
      MonthlyPackReward monthlyPackReward = monthlyPackRewardArray[index];
      e = withLoupeIcon.Clone(shopGiftMonth.dailyGrid.transform).GetComponent<ItemIconDetail>().Init((MasterDataTable.CommonRewardType) monthlyPackReward.reward_type_id, monthlyPackReward.reward_id, monthlyPackReward.reward_quantity, true, (TransformSizeInfo) null, (TransformSizeInfo) null, (SpriteTransformSizeInfo) null, (TransformSizeInfo) null);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    monthlyPackRewardArray = (MonthlyPackReward[]) null;
    shopGiftMonth.dailyGrid.Reposition();
    GameObject gameObject = (GameObject) null;
    for (int index1 = 0; index1 < shopGiftMonth.reachingGrid.transform.childCount; ++index1)
      UnityEngine.Object.Destroy((UnityEngine.Object) shopGiftMonth.reachingGrid.transform.GetChild(index1).gameObject);
    shopGiftMonth.reachingGrid.transform.Clear();
    MonthlyPackExtraReward[] monthlyPackExtraRewardArray = shopGiftMonth.packInfo.extra_rewards;
    for (index = 0; index < monthlyPackExtraRewardArray.Length; ++index)
    {
      MonthlyPackExtraReward reward = monthlyPackExtraRewardArray[index];
      GameObject scrollItem = shopGiftMonth.reachingItemBase.Clone(shopGiftMonth.reachingGrid.transform);
      scrollItem.SetActive(true);
      yield return (object) scrollItem.GetComponent<ShopGiftMonthReachingItem>().Init(withLoupeIcon, reward);
      gameObject = scrollItem;
      scrollItem = (GameObject) null;
    }
    monthlyPackExtraRewardArray = (MonthlyPackExtraReward[]) null;
    shopGiftMonth.reachingBackground.height = shopGiftMonth.defaultReachingBackgroundHeight + (int) ((double) shopGiftMonth.reachingGrid.cellHeight * (double) (shopGiftMonth.packInfo.extra_rewards.Length - 1) + 10.0);
    shopGiftMonth.reachingGrid.Reposition();
    shopGiftMonth.benefits.SetAnchor(gameObject.transform);
    if (shopGiftMonth.packInfo.pack.drop_item_rate_text == "")
      shopGiftMonth.benefitDropUp.gameObject.SetActive(false);
    else
      shopGiftMonth.benefitDropUp.text = shopGiftMonth.packInfo.pack.drop_item_rate_text;
    if (shopGiftMonth.packInfo.pack.money_rate_text == "")
      shopGiftMonth.benefitZenyUp.gameObject.SetActive(false);
    else
      shopGiftMonth.benefitZenyUp.text = shopGiftMonth.packInfo.pack.money_rate_text;
    if (shopGiftMonth.packInfo.pack.player_exp_rate_text == "")
      shopGiftMonth.benefitExpUp.gameObject.SetActive(false);
    else
      shopGiftMonth.benefitExpUp.text = shopGiftMonth.packInfo.pack.player_exp_rate_text;
    shopGiftMonth.benefitsGrid.Reposition();
    if (!shopGiftMonth.packInfo.player_pack.rest_receive_day.HasValue)
    {
      shopGiftMonth.receiveRestDay.SetActive(false);
    }
    else
    {
      shopGiftMonth.receiveRestDay.SetActive(true);
      int? restReceiveDay = shopGiftMonth.packInfo.player_pack.rest_receive_day;
      int num = 0;
      if (restReceiveDay.GetValueOrDefault() > num & restReceiveDay.HasValue)
      {
        shopGiftMonth.receiveRestDayText.text = string.Format("残り{0}日", (object) shopGiftMonth.packInfo.player_pack.rest_receive_day);
      }
      else
      {
        TimeSpan timeSpan = new DateTime(shopGiftMonth.now.Year, shopGiftMonth.now.Month, shopGiftMonth.now.Day, 23, 59, 59) - shopGiftMonth.now;
        shopGiftMonth.receiveRestDayText.text = timeSpan.Hours <= 0 ? string.Format("残り{0}分", (object) timeSpan.Minutes) : string.Format("残り{0}時間", (object) timeSpan.Hours);
      }
    }
    if (shopGiftMonth.packInfo.player_pack.is_purchasable)
    {
      shopGiftMonth.priceButton.gameObject.SetActive(true);
      // ISSUE: reference to a compiler-generated method
      string localizedPrice = ((IEnumerable<ProductInfo>) PurchaseFlow.ProductList).First<ProductInfo>(new Func<ProductInfo, bool>(shopGiftMonth.\u003CShowPage\u003Eb__31_0)).LocalizedPrice;
      shopGiftMonth.priceLabel.SetTextLocalize(localizedPrice.Replace("\\", "￥"));
    }
    else
      shopGiftMonth.priceButton.gameObject.SetActive(false);
    if (!shopGiftMonth.packInfo.player_pack.rest_receive_day.HasValue)
    {
      shopGiftMonth.receiveButton.gameObject.SetActive(false);
      shopGiftMonth.kisekiInfo.SetActive(true);
      CoinProduct activeProductData = MasterData.CoinProductList.GetActiveProductData(shopGiftMonth.productId);
      shopGiftMonth.kisekiInfoLabel.text = string.Format("購入時、有償石{0}個と特典付与", (object) activeProductData.additional_paid_coin);
    }
    else
    {
      shopGiftMonth.receiveButton.gameObject.SetActive(true);
      shopGiftMonth.kisekiInfo.SetActive(false);
      if (shopGiftMonth.packInfo.player_pack.is_received)
      {
        shopGiftMonth.receiveButtonLabel.text = "受取済";
        shopGiftMonth.receiveButton.isEnabled = false;
      }
      else
      {
        shopGiftMonth.receiveButtonLabel.text = "受取";
        shopGiftMonth.receiveButton.isEnabled = true;
      }
    }
    if (!shopGiftMonth.priceButton.gameObject.activeSelf || !shopGiftMonth.receiveButton.gameObject.activeSelf)
    {
      if (shopGiftMonth.priceButton.gameObject.activeSelf)
      {
        Transform transform = shopGiftMonth.priceButton.gameObject.transform;
        transform.localPosition = new Vector3(0.0f, transform.localPosition.y, transform.localPosition.z);
      }
      if (shopGiftMonth.receiveButton.gameObject.activeSelf)
      {
        Transform transform = shopGiftMonth.receiveButton.gameObject.transform;
        transform.localPosition = new Vector3(0.0f, transform.localPosition.y, transform.localPosition.z);
      }
    }
    shopGiftMonth.scroll.ResetPosition();
  }

  public void OnDetailButton()
  {
    Singleton<NGSceneManager>.GetInstance().StartCoroutine(this.ShowOnDetailButton());
  }

  private IEnumerator ShowOnDetailButton()
  {
    Future<GameObject> detailPopupf = Res.Prefabs.popup.popup_006_3_1__anim_popup01.Load<GameObject>();
    IEnumerator e = detailPopupf.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = Singleton<PopupManager>.GetInstance().open(detailPopupf.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup00631Menu>().InitGachaDetail("月パック", this.packInfo.descriptions);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void OrLeftArrow()
  {
    this.leftArrow.gameObject.SetActive(false);
    this.rightArrow.gameObject.SetActive(true);
    this.packInfo = this.coinbonusHistory.monthly_packs[0];
    this.productId = ((IEnumerable<CoinGroup>) this.coinbonusHistory.coin_groups).First<CoinGroup>((Func<CoinGroup, bool>) (x => x.id == this.packInfo.pack.coin_group_id)).GetProductId();
    this.StartCoroutine(this.ShowPage());
  }

  public void OrRightArrow()
  {
    this.leftArrow.gameObject.SetActive(true);
    this.rightArrow.gameObject.SetActive(false);
    this.packInfo = this.coinbonusHistory.monthly_packs[1];
    this.productId = ((IEnumerable<CoinGroup>) this.coinbonusHistory.coin_groups).First<CoinGroup>((Func<CoinGroup, bool>) (x => x.id == this.packInfo.pack.coin_group_id)).GetProductId();
    this.StartCoroutine(this.ShowPage());
  }

  public void OnBuy()
  {
    this.StartCoroutine(this.DoBuy());
  }

  private IEnumerator DoBuy()
  {
    ShopGiftMonth shopGiftMonth = this;
    Future<WebAPI.Response.CoinbonusPackVerifyCheck> handler = WebAPI.CoinbonusPackVerifyCheck(4, shopGiftMonth.packInfo.pack.id, 0, (System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = handler.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (handler.Result != null && PurchaseFlow.Instance.Purchase(shopGiftMonth.productId))
      shopGiftMonth.StartCoroutine(PurchaseBehaviorLoadingLayer.Enable());
  }

  public void OrReceive()
  {
    this.StartCoroutine(this.DoReceive());
  }

  private IEnumerator DoReceive()
  {
    PurchaseBehavior.PopupDismiss(true);
    yield return (object) Singleton<NGSceneManager>.GetInstance().StartCoroutine(Shop0079Menu.OnReciveSucceeded(MasterData.CoinProductList.GetActiveProductData(this.productId).type, this.packInfo.pack.id));
  }
}
