﻿// Decompiled with JetBrains decompiler
// Type: Tower029UnitSelectionScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class Tower029UnitSelectionScene : NGSceneBase
{
  [SerializeField]
  private Tower029UnitSelectionMenu menu;

  public static void ChangeScene(
    TowerUtil.UnitSelectionMode mode,
    TowerProgress progress,
    TowerUtil.SequenceType type)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("tower029_unit_selection", true, (object) mode, (object) progress, (object) type);
  }

  public IEnumerator onStartSceneAsync(
    TowerUtil.UnitSelectionMode mode,
    TowerProgress progress,
    TowerUtil.SequenceType type)
  {
    Tower029UnitSelectionScene unitSelectionScene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) null;
    IEnumerator e = unitSelectionScene.menu.InitializeAsync(mode, progress, type);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unitSelectionScene.bgmFile = TowerUtil.BgmFile;
    unitSelectionScene.bgmName = TowerUtil.BgmName;
  }

  public void onStartScene(
    TowerUtil.UnitSelectionMode mode,
    TowerProgress progress,
    TowerUtil.SequenceType type)
  {
    this.menu.onStartScene(mode);
  }

  public override void onEndScene()
  {
    Singleton<CommonRoot>.GetInstance().isLoading = true;
  }
}
