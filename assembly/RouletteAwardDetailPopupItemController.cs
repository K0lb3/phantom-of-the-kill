﻿// Decompiled with JetBrains decompiler
// Type: RouletteAwardDetailPopupItemController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class RouletteAwardDetailPopupItemController : MonoBehaviour
{
  [SerializeField]
  private GameObject awardIconContainer;
  [SerializeField]
  private UILabel awardDescription;

  public IEnumerator Init(RouletteR001FreeDeckEntity awardData)
  {
    IEnumerator e = this.awardIconContainer.GetOrAddComponent<CreateIconObject>().CreateThumbnail(awardData.reward_type_id, awardData.reward_id, awardData.reward_quantity ?? 1, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.awardDescription.SetTextLocalize(awardData.reward_title);
  }
}
