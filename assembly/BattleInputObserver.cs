﻿// Decompiled with JetBrains decompiler
// Type: BattleInputObserver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class BattleInputObserver : BattleMonoBehaviour
{
  private BattleInputObserver.Mode mode;
  public bool isCameraMoveMode;
  public bool isDispositionMode;
  public bool isTouchEnable;
  private BattleCameraController cameraController;
  private BattleUnitController unitController;
  private NGBattle3DObjectManager objectManager;
  private BattleTimeManager btm;
  private Func<bool, bool> checkCancelPressed;
  private Func<bool> checkCancelClick;
  private BattleInputObserver.SelectTarget mSelectTargets;
  private HashSet<BL.Panel> dispositionPanels;
  private BL.Panel mDownPanel;

  public bool isUnitMoveMode
  {
    get
    {
      return this.mode == BattleInputObserver.Mode.unitmove;
    }
  }

  public bool isTargetSelectMode
  {
    get
    {
      return this.mode == BattleInputObserver.Mode.targetselect;
    }
  }

  public void setFuncCheckCancelPressed(Func<bool, bool> func = null)
  {
    this.checkCancelPressed = func;
  }

  public void setFuncCheckCancelClick(Func<bool> func = null)
  {
    this.checkCancelClick = func;
  }

  protected override IEnumerator Start_Battle()
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    BattleInputObserver battleInputObserver = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    UICamera.fallThrough = battleInputObserver.gameObject;
    battleInputObserver.cameraController = battleInputObserver.GetComponent<BattleCameraController>();
    battleInputObserver.unitController = battleInputObserver.GetComponent<BattleUnitController>();
    battleInputObserver.objectManager = battleInputObserver.battleManager.getManager<NGBattle3DObjectManager>();
    battleInputObserver.btm = battleInputObserver.battleManager.getManager<BattleTimeManager>();
    battleInputObserver.isTouchEnable = true;
    return false;
  }

  private void OnDisable()
  {
    UICamera.fallThrough = (GameObject) null;
  }

  private void OnHover(bool isOver)
  {
  }

  public bool isCurrentUnitAction
  {
    get
    {
      BL.UnitPosition currentUnitPosition = this.env.core.currentUnitPosition;
      if (!(currentUnitPosition.unit != (BL.Unit) null))
        return false;
      if (this.isDispositionMode)
        return true;
      return !currentUnitPosition.isCompleted && currentUnitPosition.unit.isPlayerControl && currentUnitPosition.unit.hp > 0 && this.env.core.currentPhaseUnitp(currentUnitPosition);
    }
  }

  private void setUnitMoveMode(BL.UnitPosition up)
  {
    if (this.isCurrentUnitAction && up.unit != this.env.core.unitCurrent.unit || (this.isTargetSelectMode || !this.unitController.onPressMoveUnit(up)))
      return;
    if (this.isCameraMoveMode)
    {
      this.cameraController.onCancel();
      this.isCameraMoveMode = false;
    }
    this.mode = BattleInputObserver.Mode.unitmove;
    this.selectTargets = (BattleInputObserver.SelectTarget) null;
  }

  private void setCameraMoveMode()
  {
    this.isCameraMoveMode = true;
    this.cameraController.onPress();
  }

  private BattleInputObserver.SelectTarget selectTargets
  {
    get
    {
      return this.mSelectTargets;
    }
    set
    {
      if (this.mSelectTargets != null && this.mSelectTargets.Equals((object) value))
        return;
      if (this.mSelectTargets != null)
      {
        foreach (BL.Unit attackTarget in this.mSelectTargets.attackTargets)
        {
          BL.Panel fieldPanel = this.env.core.getFieldPanel(this.env.core.getUnitPosition(attackTarget), false);
          fieldPanel.unsetAttribute(BL.PanelAttribute.target_attack);
          if (this.mSelectTargets.selectFunc != null)
            this.objectManager.hideButton(fieldPanel);
        }
        foreach (BL.Unit healTarget in this.mSelectTargets.healTargets)
        {
          BL.Panel fieldPanel = this.env.core.getFieldPanel(this.env.core.getUnitPosition(healTarget), false);
          fieldPanel.unsetAttribute(BL.PanelAttribute.target_heal);
          if (this.mSelectTargets.selectFunc != null)
            this.objectManager.hideButton(fieldPanel);
        }
        foreach (BL.Panel panel in this.mSelectTargets.panels)
        {
          panel.unsetAttribute(BL.PanelAttribute.target_attack);
          if (this.mSelectTargets.selectFunc != null)
            this.objectManager.hideButton(panel);
        }
      }
      if (value != null)
      {
        foreach (BL.Unit attackTarget in value.attackTargets)
        {
          BL.Panel fieldPanel = this.env.core.getFieldPanel(this.env.core.getUnitPosition(attackTarget), false);
          fieldPanel.setAttribute(BL.PanelAttribute.target_attack);
          if (value.selectFunc != null)
            this.objectManager.setButton(fieldPanel, false, value.grayTargets.Contains(attackTarget));
        }
        foreach (BL.Unit healTarget in value.healTargets)
        {
          BL.Panel fieldPanel = this.env.core.getFieldPanel(this.env.core.getUnitPosition(healTarget), false);
          fieldPanel.setAttribute(BL.PanelAttribute.target_heal);
          if (value.selectFunc != null)
            this.objectManager.setButton(fieldPanel, true, value.grayTargets.Contains(healTarget));
        }
        foreach (BL.Panel panel in value.panels)
        {
          panel.setAttribute(BL.PanelAttribute.target_attack);
          if (value.selectFunc != null)
            this.objectManager.setButton(panel, false, false);
        }
      }
      this.mSelectTargets = value;
    }
  }

  public bool containsTargetSelect(BL.Unit unit)
  {
    if (!this.isTargetSelectMode)
      return false;
    if (this.selectTargets.targets.Contains(unit))
      return true;
    BL.UnitPosition unitPosition = this.env.core.getUnitPosition(unit);
    return unitPosition != null && this.selectTargets.panels.Contains(this.env.core.getFieldPanel(unitPosition, false));
  }

  public bool containsGrayTargetSelect(BL.Unit unit)
  {
    return this.isTargetSelectMode && this.selectTargets.grayTargets.Contains(unit);
  }

  public void setTargetSelectMode(
    List<BL.Unit> attackTargets,
    List<BL.Unit> healTargets,
    List<BL.Unit> grayTargets,
    List<BL.Panel> panels,
    System.Action<BL.Unit, BL.Panel> func)
  {
    attackTargets = attackTargets.Where<BL.Unit>((Func<BL.Unit, bool>) (u => u.isEnable && !u.isDead)).ToList<BL.Unit>();
    healTargets = healTargets.Where<BL.Unit>((Func<BL.Unit, bool>) (u => u.isEnable && !u.isDead)).ToList<BL.Unit>();
    grayTargets = grayTargets.Where<BL.Unit>((Func<BL.Unit, bool>) (u => u.isEnable && !u.isDead)).ToList<BL.Unit>();
    this.mode = BattleInputObserver.Mode.targetselect;
    this.selectTargets = new BattleInputObserver.SelectTarget(attackTargets, healTargets, grayTargets, panels, func);
    this.cameraController.onCancel();
    this.unitController.onCancel();
    this.env.core.currentUnitPosition.commit();
  }

  public void cancelTargetSelect()
  {
    if (!this.isTargetSelectMode)
      return;
    this.selectTargets = (BattleInputObserver.SelectTarget) null;
    this.mode = BattleInputObserver.Mode.none;
    this.env.core.currentUnitPosition.commit();
  }

  private void delegateSelectTargets(BL.Unit unit, BL.Panel panel)
  {
    if (!this.isTargetSelectMode || this.selectTargets.selectFunc == null)
      return;
    BL.Unit unit1 = !(unit != (BL.Unit) null) || !this.selectTargets.targets.Contains(unit) ? (BL.Unit) null : unit;
    BL.Panel panel1 = this.selectTargets.panels.Contains(panel) ? panel : (BL.Panel) null;
    if (!(unit1 != (BL.Unit) null) && panel1 == null)
      return;
    this.selectTargets.selectFunc(unit, panel);
  }

  public void setDispositionMode(HashSet<BL.Panel> pl)
  {
    Debug.LogWarning((object) (" === setDispositionMode:" + (object) pl));
    if (pl == null)
    {
      this.isDispositionMode = false;
      this.dispositionPanels = (HashSet<BL.Panel>) null;
    }
    else
    {
      this.isDispositionMode = true;
      this.dispositionPanels = pl;
    }
  }

  public HashSet<BL.Panel> getMovePanels(BL.UnitPosition up)
  {
    return !this.isDispositionMode ? up.movePanels : this.dispositionPanels;
  }

  private bool containsMovePanels(BL.Panel panel, BL.UnitPosition up)
  {
    return (this.isDispositionMode ? BattleFuncs.moveCompletePanels_(this.dispositionPanels, up.unit, false, false) : up.completePanels).Contains(panel);
  }

  public void onCancel(bool isTargetSelectCancel = false)
  {
    if (isTargetSelectCancel)
      this.cancelTargetSelect();
    if (this.isCameraMoveMode)
    {
      this.cameraController.onCancel();
      this.isCameraMoveMode = false;
    }
    if (!this.isUnitMoveMode)
      return;
    this.unitController.onCancel();
    this.mode = BattleInputObserver.Mode.none;
  }

  private BL.Panel downPanel
  {
    set
    {
      if (this.mDownPanel == value)
        return;
      if (this.mDownPanel != null)
        this.env.panelResource[this.mDownPanel].gameObject.GetComponent<BattlePanelParts>().buttonDown(false);
      this.mDownPanel = value;
      if (this.mDownPanel == null)
        return;
      this.env.panelResource[this.mDownPanel].gameObject.GetComponent<BattlePanelParts>().buttonDown(true);
    }
  }

  private void OnPress(bool pressed)
  {
    if (!this.isTouchEnable || !this.battleManager.isBattleEnable || this.checkCancelPressed != null && this.checkCancelPressed(pressed))
      return;
    if (pressed)
    {
      BL.Panel panel = NGBattle3DObjectManager.hitPanel(UICamera.lastTouchPosition);
      if (panel != null)
      {
        if (!this.isTargetSelectMode)
        {
          BL.UnitPosition fieldUnit = this.env.core.getFieldUnit(panel, false);
          if (fieldUnit != null)
          {
            if (this.env.core.phaseState.turnCount == 1 && this.env.core.unitCurrent.unit == (BL.Unit) null && fieldUnit.unit.isPlayerControl)
              this.env.unitResource[fieldUnit.unit].PlayVoiceDuelStart(fieldUnit.unit);
            this.setUnitMoveMode(fieldUnit);
          }
          if (!this.isUnitMoveMode && this.env.core.currentUnitPosition.movePanels.Contains(panel))
            this.setUnitMoveMode(this.env.core.currentUnitPosition);
        }
        this.downPanel = panel;
      }
      if (this.isDispositionMode || this.isUnitMoveMode)
        return;
      this.setCameraMoveMode();
    }
    else
    {
      this.downPanel = (BL.Panel) null;
      if (this.isCameraMoveMode)
      {
        this.cameraController.onRelease();
        this.isCameraMoveMode = false;
      }
      if (!this.isUnitMoveMode)
        return;
      this.unitController.onReleaseMoveUnit();
      this.mode = BattleInputObserver.Mode.none;
    }
  }

  private void OnClick_(BL.Panel panel)
  {
    if (this.isTargetSelectMode)
    {
      BL.UnitPosition fieldUnit = this.env.core.getFieldUnit(panel, false);
      BL.Unit unit = fieldUnit == null || !this.selectTargets.targets.Contains(fieldUnit.unit) ? (BL.Unit) null : fieldUnit.unit;
      BL.Panel panel1 = this.selectTargets.panels.Contains(panel) ? panel : (BL.Panel) null;
      if (!(unit != (BL.Unit) null) && panel1 == null)
        return;
      this.delegateSelectTargets(unit, panel1);
    }
    else
    {
      BL.UnitPosition up = this.env.core.currentUnitPosition;
      if (this.isCurrentUnitAction)
      {
        if (up.unit.isPlayerControl && this.containsMovePanels(panel, up))
        {
          up.startMoveRoute(this.env.core.getRouteNonCache(up, this.env.core.getFieldPanel(up, false), panel, this.getMovePanels(up), (HashSet<BL.Panel>) null), this.battleManager.defaultUnitSpeed, this.env);
        }
        else
        {
          bool flag = false;
          if (!this.isDispositionMode && !up.isActionComleted)
          {
            BattleUIController controller = this.battleManager.getController<BattleUIController>();
            if (controller.uiButtonEnable)
            {
              Func<BL.UnitPosition, bool> func = (Func<BL.UnitPosition, bool>) (tup =>
              {
                if (!up.unit.skillEffects.IsMoveSkillActionWaiting())
                  return true;
                return tup.unit.hp >= 1 && !this.btm.isRunning;
              });
              HashSet<BL.Panel> attackTargetPanels = BattleFuncs.getAttackTargetPanels(up, false, false);
              if (attackTargetPanels != null && attackTargetPanels.Contains(panel))
              {
                BL.UnitPosition fieldUnit = this.env.core.getFieldUnit(panel, false);
                if (func(fieldUnit))
                {
                  this.env.core.lookDirection(up, fieldUnit);
                  controller.startPreDuel(up, fieldUnit);
                  flag = true;
                }
              }
              HashSet<BL.Panel> healTargetPanels = BattleFuncs.getHealTargetPanels(up, false, false);
              if (healTargetPanels != null && healTargetPanels.Contains(panel))
              {
                BL.UnitPosition fieldUnit = this.env.core.getFieldUnit(panel, false);
                if (func(fieldUnit))
                {
                  this.env.core.lookDirection(up, fieldUnit);
                  controller.startHeal(up, fieldUnit);
                  flag = true;
                }
              }
            }
          }
          if (flag)
            return;
          this.cameraController.setLookAtTarget(panel, false);
        }
      }
      else
      {
        BL.UnitPosition fieldUnit = this.env.core.getFieldUnit(panel, false);
        if (!this.isDispositionMode)
        {
          if (this.env.core.phaseState.state == BL.Phase.player)
          {
            foreach (BL.UnitPosition up1 in this.env.core.unitPositions.value)
            {
              if (up1.isLocalMoved)
                up1.cancelMove(this.env);
            }
          }
          this.cameraController.setLookAtTarget(panel, false);
          this.env.core.fieldCurrent.value = panel;
          if (fieldUnit == null || !fieldUnit.unit.isView || fieldUnit.unit.isDead)
            return;
          this.btm.setCurrentUnit(fieldUnit.unit, 0.1f, false);
        }
        else
        {
          if (fieldUnit == null || fieldUnit.unit.isFacility)
            return;
          this.env.core.fieldCurrent.value = panel;
          this.btm.setCurrentUnit(fieldUnit.unit, 0.1f, false);
        }
      }
    }
  }

  private void OnClick()
  {
    if (!this.isTouchEnable || !this.battleManager.isBattleEnable || this.checkCancelClick != null && this.checkCancelClick())
      return;
    BL.Panel panel = NGBattle3DObjectManager.hitPanel(UICamera.lastTouchPosition);
    if (panel == null)
      return;
    this.OnClick_(panel);
  }

  private void OnDoubleClick()
  {
  }

  private void OnSelect(bool selected)
  {
    if (!this.battleManager.isBattleEnable)
      return;
    this.onCancel(false);
  }

  private void OnDrag(Vector2 delta)
  {
    if (!this.battleManager.isBattleEnable)
      return;
    if (this.isCameraMoveMode)
      this.cameraController.onDrag(delta);
    if (!this.isUnitMoveMode)
      return;
    this.downPanel = NGBattle3DObjectManager.hitPanel(UICamera.lastTouchPosition);
    this.unitController.onDrag(this.mDownPanel);
  }

  private void OnDrop(GameObject go)
  {
  }

  private void OnInput(string text)
  {
  }

  private void OnSubmit()
  {
  }

  private void OnScroll(float delta)
  {
  }

  private enum Mode
  {
    none,
    unitmove,
    targetselect,
  }

  private class SelectTarget
  {
    public List<BL.Unit> targets;
    public List<BL.Unit> grayTargets;
    public List<BL.Unit> attackTargets;
    public List<BL.Unit> healTargets;
    public List<BL.Panel> panels;
    public System.Action<BL.Unit, BL.Panel> selectFunc;

    public SelectTarget(
      List<BL.Unit> attackTargets,
      List<BL.Unit> healTargets,
      List<BL.Unit> grayTargets,
      List<BL.Panel> panels,
      System.Action<BL.Unit, BL.Panel> selectFunc)
    {
      this.attackTargets = attackTargets;
      this.healTargets = healTargets;
      this.targets = attackTargets.Concat<BL.Unit>((IEnumerable<BL.Unit>) healTargets).Except<BL.Unit>((IEnumerable<BL.Unit>) grayTargets).ToList<BL.Unit>();
      this.grayTargets = grayTargets;
      this.panels = panels;
      this.selectFunc = selectFunc;
    }
  }
}
