﻿// Decompiled with JetBrains decompiler
// Type: PopupAutoCloseOnAnyTap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class PopupAutoCloseOnAnyTap : BackButtonMenuBase
{
  protected void setEventOnAnyTap(Collider col = null)
  {
    if ((Object) col == (Object) null)
      col = (Collider) ((IEnumerable<BoxCollider>) this.transform.parent.GetComponentsInChildren<BoxCollider>()).FirstOrDefault<BoxCollider>();
    if (!((Object) col != (Object) null))
      return;
    EventDelegate.Set(col.gameObject.AddComponent<UIEventTrigger>().onRelease, new EventDelegate.Callback(this.onAnyTap));
  }

  public override void onBackButton()
  {
    this.onAnyTap();
  }

  private void onAnyTap()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
