﻿// Decompiled with JetBrains decompiler
// Type: Unit05411Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit05411Menu : Unit00411Menu
{
  [SerializeField]
  private UILabel TxtUnitCount;

  protected override void Update()
  {
    base.Update();
    this.ScrollUpdate();
  }

  public override IEnumerator Init(
    Player player,
    PlayerUnit[] playerUnits,
    bool isEquip)
  {
    Unit05411Menu unit05411Menu = this;
    unit05411Menu.SetIconType(UnitMenuBase.IconType.Normal);
    IEnumerator e = unit05411Menu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit05411Menu.InitializeInfo((IEnumerable<PlayerUnit>) playerUnits, (IEnumerable<PlayerMaterialUnit>) null, (Persist<Persist.UnitSortAndFilterInfo>) null, false, isEquip, false, false, false, (System.Action) null, 0);
    e = unit05411Menu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    unit05411Menu.InitializeEnd();
    unit05411Menu.TxtUnitCount.SetTextLocalize(unit05411Menu.allUnitInfos.Count);
  }

  protected override void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase unitIcon = this.allUnitIcons[unit_index];
    unitIcon.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
    ((UnitIcon) unitIcon).SetEarthButtonDetalEvent(this.allUnitInfos[info_index].playerUnit, this.getUnits());
    unitIcon.onClick = (System.Action<UnitIconBase>) (ui => Unit0542Scene.changeScene(true, unitIcon.PlayerUnit, this.getUnits()));
  }
}
