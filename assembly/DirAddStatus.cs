﻿// Decompiled with JetBrains decompiler
// Type: DirAddStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirAddStatus : MonoBehaviour
{
  [SerializeField]
  private GameObject[] AddStatus;

  public IEnumerator Init(List<IncrementalInfo> list)
  {
    Future<GameObject> addStatusPrefabF = Res.Prefabs.unit004_4_3.Add_Status.Load<GameObject>();
    IEnumerator e = addStatusPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    for (int index = 0; index < list.Count; ++index)
      addStatusPrefabF.Result.Clone(this.AddStatus[index].transform).GetComponent<global::AddStatus>().Init(list[index].name, list[index].value);
  }
}
