﻿// Decompiled with JetBrains decompiler
// Type: RouletteAwardWheelIconController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RouletteAwardWheelIconController : MonoBehaviour
{
  [SerializeField]
  private Transform numberDigitSpritesContainer;
  [SerializeField]
  private List<UISprite> numberDigitSprites;
  private const int maxNumber = 9999;
  private const string spriteNameFormat = "slc_{0}.png__GUI__roulette_5th__roulette_5th_prefab";
  [SerializeField]
  private GameObject awardIconContainer;

  public IEnumerator Init(int awardCount, MasterDataTable.CommonRewardType awardType, int awardID)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    RouletteAwardWheelIconController wheelIconController = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    wheelIconController.SetAwardCount(awardCount);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E2__current = (object) wheelIconController.StartCoroutine(wheelIconController.SetAwardIcon(awardType, awardID));
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private void SetAwardCount(int count)
  {
    if (count > 9999)
      count = 9999;
    List<int> intList = new List<int>();
    for (; count > 0; count /= 10)
      intList.Add(count % 10);
    intList.Reverse();
    for (int index = 0; index < this.numberDigitSprites.Count - 1; ++index)
    {
      if (index < intList.Count)
      {
        this.numberDigitSprites[index].spriteName = string.Format("slc_{0}.png__GUI__roulette_5th__roulette_5th_prefab", (object) intList[index]);
        this.numberDigitSprites[index].gameObject.SetActive(true);
      }
      else
        this.numberDigitSprites[index].gameObject.SetActive(false);
    }
    Bounds bounds = this.numberDigitSprites[this.numberDigitSprites.Count - 1].CalculateBounds(this.numberDigitSpritesContainer);
    for (int index = 0; index < this.numberDigitSprites.Count - 1; ++index)
    {
      if (this.numberDigitSprites[index].gameObject.activeSelf)
        bounds.Encapsulate(this.numberDigitSprites[index].CalculateBounds(this.numberDigitSpritesContainer));
    }
    for (int index = 0; index < this.numberDigitSprites.Count; ++index)
    {
      Vector3 localPosition = this.numberDigitSprites[index].transform.localPosition;
      this.numberDigitSprites[index].transform.localPosition = new Vector3(localPosition.x - bounds.center.x, localPosition.y - bounds.center.y, localPosition.z);
    }
  }

  private IEnumerator SetAwardIcon(MasterDataTable.CommonRewardType awardType, int awardID)
  {
    IEnumerator e = this.awardIconContainer.GetOrAddComponent<CreateIconObject>().CreateThumbnail(awardType, awardID, 0, false, false, new CommonQuestType?(), true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
