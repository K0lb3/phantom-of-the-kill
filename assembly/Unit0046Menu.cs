﻿// Decompiled with JetBrains decompiler
// Type: Unit0046Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeckOrganization;
using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Unit0046Menu : BackButtonMenuBase
{
  private DateTime serverTime = DateTime.MinValue;
  private Promise<int?[]> player_unit_ids = new Promise<int?[]>();
  public UIButton[] buttons = new UIButton[5];
  public List<GameObject> Models = new List<GameObject>();
  public int old_indicator_select = -1;
  [SerializeField]
  private int SELECT_MIN = 1;
  [SerializeField]
  private int SELECT_MAX = 5;
  protected const float LINK_HEIGHT = 100f;
  protected const float LINK_DEFHEIGHT = 136f;
  protected const float scale = 0.7352941f;
  [SerializeField]
  protected UILabel TxtTitle;
  public GameObject TopObject;
  public GameObject MiddleObject;
  public UILabel TxtCostValue;
  public UILabel TxtCombat;
  public UILabel TxtTeamNum;
  public NGHorizontalScrollParts indicator;
  private PlayerDeck[] playerDecks;
  private GameObject prefabResult;
  private PlayerDeck SelectDeck;
  public GameObject[] playerItems;
  public Transform[] ui3DModelTransfrom;
  public GameObject[] ui3DModelLoadDummy;
  public List<UI3DModel> ui3DModels;
  public GameObject ui3DModelPrefab;
  private int cost_max;
  public bool play;
  public bool coroutine;
  public bool stop;
  public GameObject backButton;
  public CoroutineData<bool> modelChange;
  public bool fromVersus;
  public bool fromMypage;
  private bool isSea_;
  private bool isColosseum_;
  public ColosseumUtility.Info info;
  private bool ChangeBackScene;
  [NonSerialized]
  public Unit0046Scene.LimitationData limitationData;
  private int m_windowHeight;
  private int m_windowWidth;
  private bool model_load;
  private RenderTextureRecoveryUtil util;

  public DateTime SevertTime
  {
    get
    {
      return this.serverTime;
    }
    set
    {
      this.serverTime = value;
    }
  }

  private IEnumerator WaitScrollSe()
  {
    yield return (object) new WaitForSeconds(0.3f);
    this.indicator.SeEnable = true;
  }

  private IEnumerator ShouldStop()
  {
    Unit0046Menu behaviour = this;
    while (behaviour.modelChange == null || behaviour.modelChange.Running)
      yield return (object) null;
    behaviour.modelChange.Stop();
    behaviour.modelChange = behaviour.StartCoroutine<bool>(behaviour.ModelChange());
  }

  private void SetTeamInfo(PlayerDeck deck)
  {
    this.TxtCostValue.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_0046_COST, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) deck.cost
      },
      {
        (object) "max",
        (object) this.cost_max
      }
    }));
    int combat = 0;
    ((IEnumerable<PlayerUnit>) deck.player_units).ForEach<PlayerUnit>((System.Action<PlayerUnit>) (x =>
    {
      if (!(x != (PlayerUnit) null))
        return;
      combat += Judgement.NonBattleParameter.FromPlayerUnit(x, false).Combat;
    }));
    this.TxtCombat.SetTextLocalize(combat.ToString());
    this.TxtTeamNum.SetTextLocalize(Consts.Format(Consts.GetInstance().UNIT_0046_MENU, (IDictionary) new Hashtable()
    {
      {
        (object) "num",
        (object) (this.indicator.selected + 1)
      }
    }));
  }

  protected override void Update()
  {
    if (this.playerDecks == null || this.indicator.selected >= this.playerDecks.Length || this.indicator.selected < 0)
      return;
    base.Update();
    if (this.SelectDeck == null || this.playerDecks[this.indicator.selected] != this.SelectDeck)
    {
      this.SelectDeck = this.playerDecks[this.indicator.selected];
      this.charaActive(this.SelectDeck);
      if (this.old_indicator_select != this.indicator.selected)
      {
        this.StopCoroutine("ShouldStop");
        this.StartCoroutine("ShouldStop");
      }
      this.SetTeamInfo(this.SelectDeck);
    }
    this.util.FixRenderTexture();
    if (this.m_windowHeight == 0 || this.m_windowWidth == 0)
    {
      this.m_windowHeight = Screen.height;
      this.m_windowWidth = Screen.width;
    }
    else
    {
      if (this.m_windowHeight == Screen.height && this.m_windowWidth == Screen.width)
        return;
      this.m_windowHeight = Screen.height;
      this.m_windowWidth = Screen.width;
      this.model_load = true;
      this.indicator.gameObject.SetActive(false);
      this.StartCoroutine(this.Init3DModelPrefab());
    }
  }

  public IEnumerator Init3DModelPrefab()
  {
    Unit0046Menu behaviour = this;
    Future<GameObject> prefabF = Res.Prefabs.gacha006_8.slc_3DModel.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    behaviour.ui3DModelPrefab = prefabF.Result;
    for (int index = 0; index < behaviour.ui3DModelTransfrom.Length; ++index)
    {
      UI3DModel component = behaviour.ui3DModelPrefab.Clone(behaviour.ui3DModelTransfrom[index]).GetComponent<UI3DModel>();
      component.lightOn = index == 0;
      behaviour.ui3DModels.Add(component);
    }
    while (behaviour.indicator.selected == -1)
      yield return (object) null;
    if (behaviour.model_load)
    {
      yield return (object) new WaitForSeconds(0.05f);
      behaviour.indicator.gameObject.SetActive(true);
      behaviour.model_load = false;
    }
    behaviour.modelChange = behaviour.StartCoroutine<bool>(behaviour.ModelChange());
  }

  private void InitOrSaveRenderTexture()
  {
    if ((UnityEngine.Object) this.util == (UnityEngine.Object) null)
    {
      Debug.Log((object) "Init RenderTextureRecoveryUtil.");
      this.util = this.GetComponent<RenderTextureRecoveryUtil>();
    }
    if (!((UnityEngine.Object) this.util != (UnityEngine.Object) null))
      return;
    Debug.Log((object) "Save RenderTexture Info.");
    this.util.SaveRenderTexture();
  }

  public IEnumerator ModelChange()
  {
    Unit0046Menu unit0046Menu = this;
    for (int index = 0; index < unit0046Menu.buttons.Length; ++index)
    {
      if (index < unit0046Menu.ui3DModels.Count)
        unit0046Menu.ui3DModels[index].Remove();
      unit0046Menu.ui3DModelLoadDummy[index].SetActive(unit0046Menu.playerDecks[unit0046Menu.indicator.selected].player_units[index] != (PlayerUnit) null);
    }
    unit0046Menu.old_indicator_select = unit0046Menu.indicator.selected;
    unit0046Menu.stop = true;
    yield return (object) unit0046Menu.StartCoroutine("OneProcessDestroy");
    unit0046Menu.coroutine = true;
    unit0046Menu.stop = false;
    for (int i = 0; i < unit0046Menu.buttons.Length; ++i)
    {
      if (unit0046Menu.ui3DModels.Count > i)
      {
        if (unit0046Menu.stop)
        {
          unit0046Menu.coroutine = false;
          yield break;
        }
        else
        {
          UI3DModel ui3DModel = unit0046Menu.ui3DModels[i];
          GameObject gameObject = (GameObject) null;
          ui3DModel.widget.depth = 18 - i;
          if (unit0046Menu.playerDecks[unit0046Menu.indicator.selected].player_units[i] != (PlayerUnit) null)
          {
            if ((UnityEngine.Object) ui3DModel.model_creater_ != (UnityEngine.Object) null)
              ui3DModel.model_creater_.BaseModel = (GameObject) null;
            unit0046Menu.StartCoroutine(ui3DModel.UnitEdit(unit0046Menu.playerDecks[unit0046Menu.indicator.selected].player_units[i]));
            while ((UnityEngine.Object) ui3DModel.model_creater_.BaseModel == (UnityEngine.Object) null)
            {
              if (unit0046Menu.stop)
              {
                unit0046Menu.coroutine = false;
                yield break;
              }
              else
                yield return (object) null;
            }
            gameObject = ui3DModel.model_creater_.BaseModel;
            unit0046Menu.ui3DModelLoadDummy[i].SetActive(false);
          }
          unit0046Menu.Models.Add(gameObject);
          ui3DModel.ModelCamera.transform.localPosition = new Vector3((float) (unit0046Menu.indicator.selected * 1000), (float) (i * 1000), 0.0f);
          ui3DModel = (UI3DModel) null;
        }
      }
    }
    unit0046Menu.coroutine = false;
    unit0046Menu.InitOrSaveRenderTexture();
    yield return (object) true;
  }

  private IEnumerator OneProcessDestroy()
  {
    Unit0046Menu unit0046Menu = this;
    if (ModelUnits.Instance.ModelList != null)
    {
      for (int index = 0; index < ModelUnits.Instance.ModelList.Count; ++index)
      {
        if ((bool) (UnityEngine.Object) ModelUnits.Instance.ModelList[index])
          unit0046Menu.StartCoroutine("StartDestroyPoolableObject", (object) ModelUnits.Instance.ModelList[index].gameObject);
      }
    }
    yield return (object) null;
  }

  private IEnumerator StartDestroyPoolableObject(GameObject go)
  {
    while ((bool) (UnityEngine.Object) go)
    {
      ObjectPoolController.Destroy(go);
      if (!go.activeSelf)
        break;
      yield return (object) null;
    }
  }

  public IEnumerator InitPlayerDecks(PlayerDeck[] _playerDecks)
  {
    Unit0046Menu unit0046Menu1 = this;
    Unit0046Menu unit0046Menu2 = unit0046Menu1;
    int num;
    if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
    {
      CommonQuestType? questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
      CommonQuestType commonQuestType = CommonQuestType.Sea;
      num = questType.GetValueOrDefault() == commonQuestType & questType.HasValue ? 1 : 0;
    }
    else
      num = 1;
    unit0046Menu2.isSea_ = num != 0;
    unit0046Menu1.isColosseum_ = Singleton<NGGameDataManager>.GetInstance().IsColosseum;
    unit0046Menu1.Clear3DModel();
    unit0046Menu1.cost_max = SMManager.Get<Player>().max_cost;
    yield return (object) null;
    unit0046Menu1.playerDecks = _playerDecks;
    IEnumerator e;
    if (!unit0046Menu1.player_unit_ids.HasResult || unit0046Menu1.player_unit_ids.Result == null)
    {
      if ((UnityEngine.Object) unit0046Menu1.prefabResult == (UnityEngine.Object) null)
      {
        Future<GameObject> prefabF = (Future<GameObject>) null;
        prefabF = !unit0046Menu1.isSea_ ? new ResourceObject("Prefabs/unit004_6/indicator").Load<GameObject>() : new ResourceObject("Prefabs/unit004_6_sea/indicator_sea").Load<GameObject>();
        e = prefabF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        unit0046Menu1.prefabResult = prefabF.Result;
        prefabF = (Future<GameObject>) null;
      }
      unit0046Menu1.indicator.destroyParts(false);
      // ISSUE: reference to a compiler-generated method
      e = ((IEnumerable<PlayerDeck>) unit0046Menu1.playerDecks).Select<PlayerDeck, IEnumerator>(new Func<PlayerDeck, IEnumerator>(unit0046Menu1.\u003CInitPlayerDecks\u003Eb__55_1)).WaitAll();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0046Menu1.indicator.SeEnable = false;
      yield return (object) new WaitForEndOfFrame();
      unit0046Menu1.indicator.SeEnable = true;
      unit0046Menu1.indicator.resetScrollView();
      int idx = !unit0046Menu1.isColosseum_ ? (!unit0046Menu1.fromVersus ? (unit0046Menu1.isSea_ ? Persist.seaDeckOrganized.Data.number : Persist.deckOrganized.Data.number) : Persist.versusDeckOrganized.Data.number) : Persist.colosseumDeckOrganized.Data.number;
      unit0046Menu1.indicator.setItemPositionQuick(idx);
      unit0046Menu1.SetTeamInfo(unit0046Menu1.playerDecks[unit0046Menu1.indicator.selected]);
    }
    else if (unit0046Menu1.player_unit_ids.Result != null)
    {
      unit0046Menu1.playerDecks[unit0046Menu1.indicator.selected].player_unit_ids = unit0046Menu1.player_unit_ids.Result;
      SMManager.Change<PlayerDeck[]>();
      e = unit0046Menu1.UpdateDeck(unit0046Menu1.indicator.selected);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unit0046Menu1.SetTeamInfo(unit0046Menu1.playerDecks[unit0046Menu1.indicator.selected]);
    }
    // ISSUE: reference to a compiler-generated method
    ((IEnumerable<UIButton>) unit0046Menu1.buttons).ForEach<UIButton>(new System.Action<UIButton>(unit0046Menu1.\u003CInitPlayerDecks\u003Eb__55_0));
    unit0046Menu1.StartCoroutine(unit0046Menu1.WaitScrollSe());
  }

  private IEnumerator AddDeck(PlayerDeck playerDeck, GameObject prefab)
  {
    IEnumerator e = this.indicator.instantiateParts(prefab, true).GetComponent<Unit0046Indicator>().InitPlayerDeck(SMManager.Get<Player>(), playerDeck);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.InitOrSaveRenderTexture();
  }

  private IEnumerator UpdateDeck(int index)
  {
    PlayerDeck playerDeck = this.playerDecks[this.indicator.selected];
    IEnumerator e = this.indicator.GridChild(index).GetComponent<Unit0046Indicator>().InitPlayerDeck(SMManager.Get<Player>(), playerDeck);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.InitOrSaveRenderTexture();
  }

  public IEnumerator InitPlayerItems(PlayerItem[] items)
  {
    Unit0046Menu unit0046Menu1 = this;
    Future<GameObject> itemIcon = (Future<GameObject>) null;
    Unit0046Menu unit0046Menu2 = unit0046Menu1;
    int num;
    if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
    {
      CommonQuestType? questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
      CommonQuestType commonQuestType = CommonQuestType.Sea;
      num = questType.GetValueOrDefault() == commonQuestType & questType.HasValue ? 1 : 0;
    }
    else
      num = 1;
    unit0046Menu2.isSea_ = num != 0;
    itemIcon = !unit0046Menu1.isSea_ ? new ResourceObject("Prefabs/ItemIcon/prefab").Load<GameObject>() : new ResourceObject("Prefabs/Sea/ItemIcon/prefab_sea").Load<GameObject>();
    IEnumerator e = itemIcon.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    for (int i = 0; i < unit0046Menu1.playerItems.Length; ++i)
    {
      unit0046Menu1.playerItems[i].transform.Clear();
      ItemIcon component = itemIcon.Result.CloneAndGetComponent<ItemIcon>(unit0046Menu1.playerItems[i].transform);
      component.transform.localScale = new Vector3()
      {
        x = 0.7352941f,
        y = 0.7352941f
      };
      if (i < items.Length)
      {
        e = component.InitByPlayerItem(items[i]);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      else
      {
        component.SetModeSupply();
        component.SetEmpty(true);
      }
    }
  }

  public void charaActive(PlayerDeck pDeck)
  {
    int index = 0;
    QuestScoreBonusTimetable[] array = ((IEnumerable<QuestScoreBonusTimetable>) SMManager.Get<QuestScoreBonusTimetable[]>()).Where<QuestScoreBonusTimetable>((Func<QuestScoreBonusTimetable, bool>) (x => x.start_at < this.SevertTime && x.end_at > this.SevertTime)).ToArray<QuestScoreBonusTimetable>();
    UnitBonus[] activeUnitBonus = UnitBonus.getActiveUnitBonus(ServerTime.NowAppTime(), new int?(), new int?());
    foreach (PlayerUnit playerUnit in pDeck.player_units)
    {
      if (playerUnit == (PlayerUnit) null)
        this.buttons[index].GetComponent<Unit0046Character>().CharaSetActive(false, (string) null, false);
      else
        this.buttons[index].GetComponent<Unit0046Character>().CharaSetActive(true, playerUnit.SpecialEffectType((IEnumerable<QuestScoreBonusTimetable>) array, (IEnumerable<UnitBonus>) activeUnitBonus), playerUnit.unit.GetPiece);
      ++index;
    }
  }

  public void CharacterChange(int num)
  {
    if (this.IsPushAndSet())
      return;
    Unit0046Menu.OneFormationInfo info = new Unit0046Menu.OneFormationInfo();
    info.playerDeck = this.playerDecks[this.indicator.selected];
    this.player_unit_ids = new Promise<int?[]>();
    info.player_unit_ids = this.player_unit_ids;
    info.num = num;
    ModelUnits.Instance.DestroyModelUnits();
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Unit00468Scene.changeScene004682(true, info);
  }

  public void DetailSceneChange(int num)
  {
    if (this.playerDecks[this.indicator.selected].player_units[num] == (PlayerUnit) null)
      this.CharacterChange(num);
    else
      Unit0042Scene.changeScene(true, this.playerDecks[this.indicator.selected].player_units[num], this.playerDecks[this.indicator.selected].player_units, false, false);
  }

  public void OnDisable()
  {
    this.Clear3DModel();
  }

  private void Clear3DModel()
  {
    this.ui3DModels.ForEach((System.Action<UI3DModel>) (obj =>
    {
      if (!((UnityEngine.Object) obj != (UnityEngine.Object) null))
        return;
      if ((UnityEngine.Object) obj.model_creater_ != (UnityEngine.Object) null)
        obj.DestroyModelCamera();
      if (!((UnityEngine.Object) obj.gameObject != (UnityEngine.Object) null) || !(obj.gameObject.name == "slc_3DModel(Clone)"))
        return;
      UnityEngine.Object.Destroy((UnityEngine.Object) obj.gameObject);
    }));
    ModelUnits.Instance.DestroyModelUnits();
    this.ui3DModels.Clear();
    this.Models.Clear();
  }

  public void onEndScene()
  {
    if (this.isColosseum_)
    {
      if (!this.ChangeBackScene)
      {
        Persist.colosseumDeckOrganized.Data.number = Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1);
        Persist.colosseumDeckOrganized.Flush();
      }
    }
    else if (this.fromVersus)
    {
      if (!this.ChangeBackScene)
      {
        Persist.versusDeckOrganized.Data.number = Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1);
        Persist.versusDeckOrganized.Flush();
      }
    }
    else if (this.isSea_)
    {
      Persist.seaDeckOrganized.Data.number = Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1);
      Persist.seaDeckOrganized.Flush();
    }
    else
    {
      Persist.deckOrganized.Data.number = Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1);
      Persist.deckOrganized.Flush();
    }
    this.fromVersus = false;
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
    {
      Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
      int num = ((IEnumerable<PlayerUnit>) this.SelectDeck.player_units).FirstOrDefault<PlayerUnit>() != (PlayerUnit) null ? Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1) : 0;
      Persist.colosseumDeckOrganized.Data.number = num;
      Persist.colosseumDeckOrganized.Flush();
      this.ChangeBackScene = true;
      Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
      Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(0, false);
      Colosseum0234Scene.ChangeScene(this.info);
    }
    else if (this.fromVersus)
    {
      int num = ((IEnumerable<PlayerUnit>) this.SelectDeck.player_units).FirstOrDefault<PlayerUnit>() != (PlayerUnit) null ? Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1) : 0;
      Persist.versusDeckOrganized.Data.number = num;
      Persist.versusDeckOrganized.Flush();
      this.ChangeBackScene = true;
      this.backScene();
    }
    else if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      if (this.fromMypage)
        Sea030_questScene.ChangeScene(false, false, false);
      else
        this.backScene();
    }
    else
      this.backScene();
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnEquip()
  {
    if (this.IsPushAndSet())
      return;
    PlayerDeck playerDeck = this.playerDecks[this.indicator.selected];
    this.player_unit_ids = new Promise<int?[]>();
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Unit00468Scene.changeScene00468(true, playerDeck, this.player_unit_ids);
  }

  public virtual void IbtnItemedit()
  {
    if (this.IsPushAndSet())
      return;
    Quest00210Scene.changeScene(true);
  }

  public void IbtnAutoOrganization()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.coOpenPopupAutoOrganization());
  }

  public void IbtnRentalEdit()
  {
    Unit004_RentalUnit_Edit_Scene.ChangeScene(true);
  }

  private IEnumerator coOpenPopupAutoOrganization()
  {
    Unit0046Menu unit0046Menu = this;
    bool bok = false;
    bool modeLimit = unit0046Menu.limitationData != null && unit0046Menu.limitationData.limitations_ != null && (uint) unit0046Menu.limitationData.limitations_.Length > 0U;
    IEnumerator waitPopup = Unit0046ConfirmAutoOrganization.doPopup(modeLimit, unit0046Menu.limitationData != null ? unit0046Menu.limitationData.description_ : (string) null, (EventDelegate.Callback) (() => bok = true), (EventDelegate.Callback) (() => this.IsPush = false));
    while (waitPopup.MoveNext())
      yield return waitPopup.Current;
    if (bok)
    {
      List<Filter> filters = new List<Filter>();
      if (modeLimit)
      {
        int filterno = 0;
        foreach (QuestLimitationBase limitation in unit0046Menu.limitationData.limitations_)
        {
          Filter filter = limitation.createFilter(filterno);
          if (filter != null)
          {
            filters.Add(filter);
            ++filterno;
          }
        }
      }
      PlayerUnit[] array = SMManager.Get<PlayerUnit[]>();
      if (unit0046Menu.isSea_)
        array = ((IEnumerable<PlayerUnit>) array).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsSea)).ToArray<PlayerUnit>();
      Creator deckCreator = new Creator((PlayerUnit[]) null, array, filters, unit0046Menu.SELECT_MIN, unit0046Menu.SELECT_MAX, unit0046Menu.cost_max);
      IEnumerator e = deckCreator.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (deckCreator.isSuccess)
      {
        if (!unit0046Menu.equalDeck(unit0046Menu.SelectDeck, deckCreator.result_))
          unit0046Menu.StartCoroutine(unit0046Menu.coUpdateDeck(deckCreator.result_));
      }
      else
      {
        bool bwait = true;
        ModalWindow.Show(Consts.GetInstance().UNIT_0046_AUTODECK_ERROR_TITLE, Consts.GetInstance().UNIT_0046_AUTODECK_ERROR_MESSAGE, (System.Action) (() => bwait = false));
        while (bwait)
          yield return (object) null;
      }
    }
  }

  private bool equalDeck(PlayerDeck a, List<PlayerUnit> b)
  {
    int[] array1 = ((IEnumerable<int?>) a.player_unit_ids).Where<int?>((Func<int?, bool>) (d => d.HasValue)).Select<int?, int>((Func<int?, int>) (d => d.Value)).OrderBy<int, int>((Func<int, int>) (i => i)).ToArray<int>();
    int[] array2 = b.Where<PlayerUnit>((Func<PlayerUnit, bool>) (u => u != (PlayerUnit) null)).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (u => u.id)).OrderBy<int, int>((Func<int, int>) (i => i)).ToArray<int>();
    if (array1.Length != array2.Length)
      return false;
    for (int index = 0; index < array1.Length; ++index)
    {
      if (array1[index] != array2[index])
        return false;
    }
    return true;
  }

  private IEnumerator coUpdateDeck(List<PlayerUnit> newdeck)
  {
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    bool isFailed = false;
    IEnumerator e1;
    if (this.isSea_)
    {
      e1 = WebAPI.SeaDeckEdit(this.SelectDeck.deck_number, newdeck.Where<PlayerUnit>((Func<PlayerUnit, bool>) (u => u != (PlayerUnit) null)).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (u => u.id)).ToArray<int>(), (System.Action<WebAPI.Response.UserError>) (e =>
      {
        isFailed = true;
        if (string.Equals(e.Code, "SEA000"))
          this.StartCoroutine(PopupUtility.SeaError(e));
        else
          WebAPI.DefaultUserErrorCallback(e);
      })).Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
    else
    {
      e1 = WebAPI.DeckEdit(this.SelectDeck.deck_type_id, this.SelectDeck.deck_number, newdeck.Where<PlayerUnit>((Func<PlayerUnit, bool>) (u => u != (PlayerUnit) null)).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (u => u.id)).ToArray<int>(), (System.Action<WebAPI.Response.UserError>) (e =>
      {
        isFailed = true;
        WebAPI.DefaultUserErrorCallback(e);
      })).Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    if (!isFailed)
    {
      Singleton<NGSceneManager>.GetInstance().destroyCurrentScene();
      if (this.limitationData != null)
        Unit0046Scene.changeScene(false, this.limitationData.limitations_, this.limitationData.description_, false);
      else
        Unit0046Scene.changeScene(false, (QuestLimitationBase[]) null, (string) null, false);
    }
  }

  public class OneFormationInfo
  {
    public Promise<int?[]> player_unit_ids;
    public int num;

    public PlayerDeck playerDeck { get; set; }
  }
}
