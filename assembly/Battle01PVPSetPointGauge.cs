﻿// Decompiled with JetBrains decompiler
// Type: Battle01PVPSetPointGauge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UnityEngine;

public class Battle01PVPSetPointGauge : BattleMonoBehaviour
{
  private int oldVal = 1;
  [SerializeField]
  private NGTweenGaugeScale hpGauge;
  [SerializeField]
  private NGTweenGaugeScale damegeGauge;
  [SerializeField]
  private GameObject effect;
  [SerializeField]
  private ParticleSystem[] particles;
  [SerializeField]
  private GameObject anim;
  private BattleTimeManager _btm;

  protected override IEnumerator Start_Original()
  {
    yield break;
  }

  public void Start_Battle_Debug()
  {
  }

  public void initValue(int remVal, int maxVal)
  {
    this.hpGauge.setValue(remVal, maxVal, true, -1f, -1f);
    this.damegeGauge.setValue(remVal, maxVal, true, -1f, -1f);
    this.oldVal = remVal;
  }

  public void setValue(int remVal, int maxVal)
  {
    if (this.oldVal == remVal)
      return;
    this.btm.setScheduleAction((System.Action) (() =>
    {
      this.animGauge(remVal, maxVal);
      if (!this.effect.activeSelf)
        this.effect.SetActive(true);
      if ((UnityEngine.Object) this.anim != (UnityEngine.Object) null)
      {
        this.anim.SetActive(false);
        this.anim.SetActive(true);
      }
      Singleton<NGSoundManager>.GetInstance().playSE("SE_0538", false, 0.0f, -1);
    }), 1f, (System.Action) null, (Func<bool>) null, false);
    this.oldVal = remVal;
  }

  public void initValueHistory(int remVal, int maxVal)
  {
    if (maxVal <= 0)
      return;
    float x = (float) remVal / (float) maxVal;
    this.hpGauge.transform.localScale = new Vector3(x, this.hpGauge.transform.localScale.y, this.hpGauge.transform.localScale.z);
    this.damegeGauge.transform.localScale = new Vector3(x, this.damegeGauge.transform.localScale.y, this.damegeGauge.transform.localScale.z);
  }

  private void animGauge(int remVal, int maxVal)
  {
    int max = this.hpGauge.GetComponent<UISprite>().width / 2;
    int n = max - (maxVal - remVal) * max / maxVal;
    this.hpGauge.setValue(n, max, true, -1f, -1f);
    this.damegeGauge.setValue(n, max, true, -1f, -1f);
  }

  private BattleTimeManager btm
  {
    get
    {
      if ((UnityEngine.Object) this._btm == (UnityEngine.Object) null)
        this._btm = this.battleManager.getManager<BattleTimeManager>();
      return this._btm;
    }
  }
}
