﻿// Decompiled with JetBrains decompiler
// Type: GuildBattleEntryPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;

public class GuildBattleEntryPopup : BackButtonMenuBase
{
  public void onClickedEntry()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.coConfirmEntry());
  }

  public override void onBackButton()
  {
    this.onClickedClose();
  }

  public void onClickedClose()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  private IEnumerator coConfirmEntry()
  {
    GuildBattleEntryPopup battleEntryPopup = this;
    bool bwait = true;
    bool bok = false;
    ModalWindow.ShowYesNo(Consts.GetInstance().GUILD_MAP_MATING_ENTRY_TITLE, Consts.GetInstance().GUILD_MAP_MATING_ENTRY_MESSAGE, (System.Action) (() =>
    {
      bok = true;
      bwait = false;
    }), (System.Action) (() => bwait = false));
    while (bwait)
      yield return (object) null;
    if (!bok)
    {
      battleEntryPopup.IsPush = false;
    }
    else
    {
      Singleton<PopupManager>.GetInstance().dismiss(false);
      Future<WebAPI.Response.GvgMatchingEntry> mating = WebAPI.GvgMatchingEntry((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e1 = mating.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (mating.Result != null)
        Guild0282Scene.ChangeScene();
    }
  }
}
