﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Gacha0063Scene : NGSceneBase
{
  private Dictionary<int, Transform> gachas = new Dictionary<int, Transform>();
  private const int GachaModuleMax = 3;
  [SerializeField]
  private Gacha0063Menu gacha0063Menu;
  [SerializeField]
  private NGHorizontalScrollParts scrollParts;
  [SerializeField]
  private UICenterOnChild centerOnChild;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private Transform recycleGachaFolder;
  public GachaType gachaType;
  public static int DoGachaNumber;
  protected Modified<GachaModule[]> gachaModule;
  public bool apiUpdate;
  public DateTime serverTime;
  private bool update;
  private bool seEnable;
  private bool apiError;
  private bool duringRetry;
  private bool isConnected;
  private Coroutine connectGachaUpdate;
  private List<Gacha0063hindicator> gacha0063hindicatorList;
  private GameObject kisekiPrefab;
  private GameObject pointPrefab;
  private GameObject ticketListPrefab;
  private GameObject ticketPrefab;
  private GameObject detailPopup;
  public GameObject dirPanel;
  public GameObject dirPanelSpecial;
  private Vector2 beforeCurrentGachaScrollPosition;

  public UIScrollView ScrollView
  {
    get
    {
      return this.scrollView;
    }
  }

  public override IEnumerator onInitSceneAsync()
  {
    Gacha0063Scene gacha0063Scene = this;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    gacha0063Scene.gachaModule = SMManager.Observe<GachaModule[]>();
    gacha0063Scene.gachaModule.NotifyChanged();
    Gacha0063Scene.DoGachaNumber = 0;
    if (PerformanceConfig.GetInstance().IsTuningGachaInitialize)
    {
      gacha0063Scene.connectGachaUpdate = gacha0063Scene.StartCoroutine(gacha0063Scene.updateGachaParameter());
      yield return (object) null;
    }
    IEnumerator e = gacha0063Scene.SetBackGround();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = gacha0063Scene.CreatePrefab();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = gacha0063Scene.gacha0063Menu.CreatePrefab();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e = this.onStartSceneAsync(Gacha0063Scene.DoGachaNumber);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator onStartSceneAsync(int gachaNumber, bool forceApiUpdate)
  {
    this.apiUpdate |= forceApiUpdate;
    IEnumerator e = this.onStartSceneAsync(gachaNumber);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private bool IsSyncRemote()
  {
    return ((IEnumerable<bool>) new bool[4]
    {
      this.apiUpdate,
      DateTime.Now > Singleton<NGGameDataManager>.GetInstance().lastGachaTime.AddMinutes(10.0),
      DateTime.Now.Hour != Singleton<NGGameDataManager>.GetInstance().lastGachaTime.Hour,
      Singleton<NGGameDataManager>.GetInstance().isChangeHaveGachaTiket()
    }).Any<bool>((Func<bool, bool>) (a => a));
  }

  public IEnumerator onStartSceneAsync(int gachaNumber)
  {
    Gacha0063Scene gacha0063Scene = this;
    if (gacha0063Scene.gachas.ContainsKey(gachaNumber))
    {
      NGxScroll componentInChildren = gacha0063Scene.gachas[gachaNumber].GetComponentInChildren<NGxScroll>();
      gacha0063Scene.beforeCurrentGachaScrollPosition = (UnityEngine.Object) componentInChildren != (UnityEngine.Object) null ? componentInChildren.GetScrollPosition() : Vector2.zero;
    }
    gacha0063Scene.seEnable = false;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    if (!gacha0063Scene.isConnected)
      gacha0063Scene.connectGachaUpdate = gacha0063Scene.StartCoroutine(gacha0063Scene.updateGachaParameter());
    yield return (object) gacha0063Scene.connectGachaUpdate;
    gacha0063Scene.isConnected = false;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    gacha0063Scene.serverTime = ServerTime.NowAppTimeAddDelta();
    e = OnDemandDownload.WaitLoadUnitResource(((IEnumerable<GachaModule>) SMManager.Get<GachaModule[]>()).SelectMany<GachaModule, GachaModuleNewentity>((Func<GachaModule, IEnumerable<GachaModuleNewentity>>) (x => (IEnumerable<GachaModuleNewentity>) x.newentity)).Where<GachaModuleNewentity>((Func<GachaModuleNewentity, bool>) (x => (x.reward_type_id == 1 || x.reward_type_id == 24) && MasterData.UnitUnit.ContainsKey(x.reward_id))).Select<GachaModuleNewentity, UnitUnit>((Func<GachaModuleNewentity, UnitUnit>) (x => MasterData.UnitUnit[x.reward_id])), false, (IEnumerable<string>) new string[2]
    {
      "unit_pickup",
      "unit_large"
    }, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = gacha0063Scene.CreateGachaList(gachaNumber);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (gachaNumber != 0)
      gacha0063Scene.StartPoint(gachaNumber);
    gacha0063Scene.seEnable = true;
    gacha0063Scene.StartCoroutine(gacha0063Scene.WaitScrollSe());
    Singleton<CommonRoot>.GetInstance().setBackground(gacha0063Scene.backgroundPrefab);
    Persist.lastAccessTime.Data.gachaRootLastAccessTime = DateTime.Now;
    Persist.lastAccessTime.Flush();
    Singleton<CommonRoot>.GetInstance().UpdateFooterGachaButton();
  }

  public IEnumerator updateGachaParameter()
  {
    this.update = false;
    this.apiError = false;
    this.isConnected = true;
    if (this.IsSyncRemote() && Singleton<TutorialRoot>.GetInstance().IsTutorialFinish())
    {
      Future<WebAPI.Response.Gacha> future = WebAPI.Gacha((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      IEnumerator e1 = future.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (future.Result == null)
      {
        this.apiError = true;
      }
      else
      {
        this.duringRetry = future.Result.during_retry_gacha;
        if (this.duringRetry)
        {
          Consts instance = Consts.GetInstance();
          ModalWindow.Show(instance.GACHA_NOT_END_TITLE, instance.GACHA_NOT_END_DESCRIPTION, (System.Action) (() => Gacha00613Scene.ChangeScene(true, true)));
        }
        else
        {
          if (((IEnumerable<GachaModule>) this.gachaModule.Value).Where<GachaModule>((Func<GachaModule, bool>) (x => x.type == 6)).Count<GachaModule>() > 0)
          {
            Future<WebAPI.Response.GachaG007PanelPanelInfo> panelFuture = WebAPI.GachaG007PanelPanelInfo((System.Action<WebAPI.Response.UserError>) (e =>
            {
              WebAPI.DefaultUserErrorCallback(e);
              Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
            }));
            e1 = panelFuture.Wait();
            while (e1.MoveNext())
              yield return e1.Current;
            e1 = (IEnumerator) null;
            if (panelFuture.Result == null)
            {
              this.apiError = true;
              yield break;
            }
            else
              panelFuture = (Future<WebAPI.Response.GachaG007PanelPanelInfo>) null;
          }
          Singleton<NGGameDataManager>.GetInstance().lastGachaTime = DateTime.Now;
          this.update = true;
          this.apiUpdate = false;
          future = (Future<WebAPI.Response.Gacha>) null;
        }
      }
    }
  }

  private int GetGachaPageLength(GachaModule[] gachaModules)
  {
    int num1 = ((IEnumerable<GachaModule>) gachaModules).Count<GachaModule>((Func<GachaModule, bool>) (c => c.type != 4));
    GachaModule gachaModule = ((IEnumerable<GachaModule>) gachaModules).FirstOrDefault<GachaModule>((Func<GachaModule, bool>) (n => n.type == 4));
    int num2 = gachaModule != null ? gachaModule.gacha.Length : 0;
    return num1 + num2;
  }

  public void onStartScene()
  {
    if (this.apiError || this.duringRetry)
      return;
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.EndAnim()));
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.StartCoroutine(this.waitClearLoading());
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.PlayAnim()));
  }

  public void onStartScene(int gacha_type)
  {
    if (this.apiError || this.duringRetry)
      return;
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.EndAnim()));
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.StartCoroutine(this.waitClearLoading());
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.PlayAnim()));
  }

  public void onStartScene(int gacha_type, bool forceApiUpdate)
  {
    if (this.apiError || this.duringRetry)
      return;
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.EndAnim()));
    Singleton<PopupManager>.GetInstance().closeAll(false);
    this.StartCoroutine(this.waitClearLoading());
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.PlayAnim()));
  }

  private IEnumerator waitClearLoading()
  {
    yield return (object) new WaitForEndOfFrame();
    Singleton<CommonRoot>.GetInstance().isLoading = false;
  }

  public override void onEndScene()
  {
    if (this.apiError || this.duringRetry)
      return;
    this.seEnable = false;
    this.scrollParts.SeEnable = false;
    this.gacha0063hindicatorList.ForEach((System.Action<Gacha0063hindicator>) (x => x.EndAnim()));
  }

  private void StartPoint(int moduleNumber)
  {
    if (!this.gachas.ContainsKey(moduleNumber))
      return;
    this.scrollParts.setItemPosition(this.scrollParts.GetIndex(this.gachas[moduleNumber].transform));
    this.scrollParts.setItemPositionQuick(this.scrollParts.GetIndex(this.gachas[moduleNumber].transform));
  }

  public void BackPage()
  {
    int index = this.scrollParts.selected - 1;
    if (index < 0)
      return;
    this.centerOnChild.CenterOn(this.gacha0063hindicatorList[index].transform);
  }

  public void NextPage()
  {
    int index = this.scrollParts.selected + 1;
    if (index >= this.gacha0063hindicatorList.Count)
      return;
    this.centerOnChild.CenterOn(this.gacha0063hindicatorList[index].transform);
  }

  private IEnumerator WaitScrollSe()
  {
    yield return (object) new WaitForSeconds(0.3f);
    this.scrollParts.SeEnable = this.seEnable;
  }

  private IEnumerator CreatePrefab()
  {
    Future<GameObject> prefabF = Res.Prefabs.gacha006_3.hindicator_640_23_kiseki.Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.kisekiPrefab = prefabF.Result;
    prefabF = (Future<GameObject>) null;
    prefabF = Res.Prefabs.gacha006_3.hindicator_640_23_point.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.pointPrefab = prefabF.Result;
    prefabF = (Future<GameObject>) null;
    prefabF = Res.Prefabs.gacha006_3.hindicator_640_23_ticket_list.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ticketListPrefab = prefabF.Result;
    prefabF = (Future<GameObject>) null;
    prefabF = Res.Prefabs.gacha006_3.hindicator_640_23_ticket.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.ticketPrefab = prefabF.Result;
    prefabF = (Future<GameObject>) null;
    if ((UnityEngine.Object) this.detailPopup == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.popup.popup_006_3_1__anim_popup01.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.detailPopup = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.dirPanel == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.gacha006_3.dir_panel.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.dirPanel = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    if ((UnityEngine.Object) this.dirPanelSpecial == (UnityEngine.Object) null)
    {
      prefabF = Res.Prefabs.gacha006_3.dir_panel_special.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.dirPanelSpecial = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
  }

  private IEnumerator CreateGachaList(int gachaNumber)
  {
    this.scrollParts.destroyParts(false);
    this.gacha0063hindicatorList = new List<Gacha0063hindicator>();
    this.gacha0063hindicatorList.Clear();
    this.gachas.Clear();
    GachaModule[] gachaModuleArray = SMManager.Get<GachaModule[]>();
    if (gachaModuleArray != null)
    {
      bool flag = !Singleton<TutorialRoot>.GetInstance().IsTutorialFinish();
      Dictionary<int, PlayerGachaTicket> ticketDict = flag ? new Dictionary<int, PlayerGachaTicket>() : ((IEnumerable<PlayerGachaTicket>) SMManager.Get<Player>().gacha_tickets).ToDictionary<PlayerGachaTicket, int>((Func<PlayerGachaTicket, int>) (x => x.ticket_id));
      foreach (GachaModule gachaModule in gachaModuleArray)
      {
        if (!this.IsLimited(gachaModule))
        {
          if (flag)
          {
            this.AddGachaIndicator(this.ticketPrefab, gachaModule);
            break;
          }
          switch (gachaModule.type)
          {
            case 2:
              this.AddGachaIndicator(this.pointPrefab, gachaModule);
              continue;
            case 4:
              if (((IEnumerable<GachaModuleGacha>) gachaModule.gacha).Any<GachaModuleGacha>((Func<GachaModuleGacha, bool>) (x => x.payment_id.HasValue && ticketDict.ContainsKey(x.payment_id.Value) && ticketDict[x.payment_id.Value].quantity > 0)))
              {
                this.AddGachaIndicator(this.ticketListPrefab, gachaModule);
                continue;
              }
              continue;
            default:
              this.AddGachaIndicator(this.kisekiPrefab, gachaModule);
              continue;
          }
        }
      }
      this.scrollParts.resetScrollView();
      this.scrollParts.setItemPosition(0);
      this.scrollParts.setItemPositionQuick(0);
      yield return (object) null;
      IEnumerable<\u003C\u003Ef__AnonymousType10<Gacha0063hindicator, int>> setupGachaItemList = this.gacha0063hindicatorList.Select((gacha, count) => new
      {
        gacha = gacha,
        count = count
      });
      foreach (var data in setupGachaItemList)
      {
        IEnumerator e = data.gacha.Set(this.detailPopup);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      if (PerformanceConfig.GetInstance().IsTuningGachaInitialize && gachaNumber != 0)
      {
        var data = setupGachaItemList.FirstOrDefault(x => x.gacha.GachaNumber == gachaNumber);
        if (data != null)
        {
          NGxScroll componentInChildren = data.gacha.transform.GetComponentInChildren<NGxScroll>();
          if ((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null && (UnityEngine.Object) componentInChildren.scrollView != (UnityEngine.Object) null && (double) componentInChildren.scrollView.panel.height < (double) componentInChildren.scrollView.bounds.size.y)
            componentInChildren.ResolvePosition(this.beforeCurrentGachaScrollPosition);
        }
      }
    }
  }

  private void AddGachaIndicator(GameObject prefab, GachaModule gachaModule)
  {
    bool flag = false;
    if (gachaModule.type == 4)
    {
      if (gachaModule.gacha != null && gachaModule.gacha.Length != 0)
        flag = true;
    }
    else if (gachaModule.gacha != null && gachaModule.gacha.Length != 0 && gachaModule.gacha.Length < 3)
      flag = true;
    if (!flag)
      return;
    Gacha0063hindicator component = this.scrollParts.instantiateParts(prefab, false).GetComponent<Gacha0063hindicator>();
    this.gacha0063Menu.scene = this;
    DateTime rootLastAccessTime = Persist.lastAccessTime.Data.gachaRootLastAccessTime;
    DateTime? nullable1 = ((IEnumerable<GachaModuleGacha>) gachaModule.gacha).Max<GachaModuleGacha, DateTime?>((Func<GachaModuleGacha, DateTime?>) (x => x.start_at));
    Gacha0063hindicator gacha0063hindicator = component;
    int num;
    if (nullable1.HasValue)
    {
      DateTime dateTime = rootLastAccessTime;
      DateTime? nullable2 = nullable1;
      num = nullable2.HasValue ? (dateTime < nullable2.GetValueOrDefault() ? 1 : 0) : 0;
    }
    else
      num = 0;
    gacha0063hindicator.SetNewIconVisibility(num != 0);
    component.InitGachaModuleGacha(this.gacha0063Menu, gachaModule, this.serverTime, this.scrollView);
    this.gacha0063hindicatorList.Add(component);
    this.gachas[gachaModule.number] = component.transform;
  }

  private IEnumerator SetBackGround()
  {
    Gacha0063Scene gacha0063Scene = this;
    Future<GameObject> fBG = Res.Prefabs.BackGround.GachaTopBackground.Load<GameObject>();
    IEnumerator e = fBG.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    gacha0063Scene.backgroundPrefab = fBG.Result;
    gacha0063Scene.backgroundPrefab.GetComponent<UI2DSprite>().color = Color.white;
    Singleton<CommonRoot>.GetInstance().setBackground(gacha0063Scene.backgroundPrefab);
  }

  public bool IsLimited(GachaModule module)
  {
    DateTime? endAt = module.period.end_at;
    DateTime serverTime = this.serverTime;
    TimeSpan? nullable = endAt.HasValue ? new TimeSpan?(endAt.GetValueOrDefault() - serverTime) : new TimeSpan?();
    return module.period.end_at.HasValue && nullable.Value.Milliseconds < 0;
  }

  public GameObject GetTutorialGachaTop()
  {
    return this.gacha0063hindicatorList[0].gameObject;
  }
}
