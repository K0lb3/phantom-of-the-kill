﻿// Decompiled with JetBrains decompiler
// Type: NGHorizontalScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class NGHorizontalScrollParts : MonoBehaviour
{
  public string functionName = "onItemChanged";
  [SerializeField]
  private bool seEnable = true;
  [SerializeField]
  private int displayNum = 1;
  private List<SelectParts> dotList = new List<SelectParts>();
  private Queue<GameObject> tempParts = new Queue<GameObject>();
  private float scrollX = float.NaN;
  public int selected = -1;
  public GameObject dotPrefab;
  public GameObject scrollView;
  public GameObject leftArrow;
  public GameObject rightArrow;
  public UIWidget dot;
  public GameObject eventReceiver;
  public bool isSpringPanelCheck;
  protected int nbParts;
  protected GameObject scrollGrid;
  [SerializeField]
  private bool isMultiple;
  private UIScrollView _uiScrollView;
  private UIGrid grid;
  private GameObject tempPartsObj;
  public int setIndex;

  public int PartsCnt
  {
    get
    {
      return this.nbParts;
    }
  }

  public UIScrollView uiScrollView
  {
    get
    {
      if ((UnityEngine.Object) this._uiScrollView == (UnityEngine.Object) null)
        this._uiScrollView = this.scrollView.GetComponent<UIScrollView>();
      return this._uiScrollView;
    }
  }

  public bool SeEnable
  {
    get
    {
      return this.seEnable;
    }
    set
    {
      this.seEnable = value;
    }
  }

  private void initTempParts()
  {
    if (!((UnityEngine.Object) this.tempPartsObj == (UnityEngine.Object) null))
      return;
    this.tempPartsObj = new GameObject("tempParts");
    this.tempPartsObj.transform.parent = this.transform;
    this.tempPartsObj.SetActive(false);
  }

  private void Awake()
  {
    this.initTempParts();
    this.grid = this.scrollView.GetComponentsInChildren<UIGrid>(true)[0];
    this.scrollGrid = this.grid.gameObject;
    for (int index = 0; index < this.scrollGrid.transform.childCount; ++index)
      this.addNbParts();
  }

  private void Start()
  {
    if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
    {
      CommonQuestType? questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
      CommonQuestType commonQuestType = CommonQuestType.Sea;
      if (!(questType.GetValueOrDefault() == commonQuestType & questType.HasValue))
        return;
    }
    this.StartCoroutine(this.initDotPrefab("Prefabs/Sea/HorizontalDotContainer_sea"));
  }

  private IEnumerator initDotPrefab(string path)
  {
    Future<GameObject> f = new ResourceObject(path).Load<GameObject>();
    IEnumerator e = f.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.dotPrefab = f.Result;
  }

  public void initParts(GameObject prefab, int nb)
  {
    this.initTempParts();
    for (int index = 0; index < nb; ++index)
    {
      GameObject gameObject = NGUITools.AddChild(this.tempPartsObj, prefab);
      gameObject.transform.localScale = Vector3.one;
      this.tempParts.Enqueue(gameObject);
    }
  }

  public void resetScrollView()
  {
    UIScrollView uiScrollView = this.uiScrollView;
    uiScrollView.UpdateScrollbars(true);
    uiScrollView.ResetPosition();
    uiScrollView.RestrictWithinBounds(false, true, false);
    this.grid.Reposition();
  }

  public GameObject instantiateParts(GameObject partsPrefab, bool isChache = true)
  {
    GameObject gameObject;
    if (isChache && this.tempParts.Count > 0)
    {
      gameObject = this.tempParts.Dequeue();
      gameObject.transform.parent = this.scrollGrid.transform;
      gameObject.transform.localPosition = Vector3.zero;
      gameObject.transform.localScale = Vector3.one;
    }
    else
      gameObject = NGUITools.AddChild(this.scrollGrid, partsPrefab);
    this.addNbParts();
    return gameObject;
  }

  public void destroyParts(bool isChache = true)
  {
    if (isChache)
    {
      List<Transform> transformList = new List<Transform>();
      foreach (Transform transform in this.scrollGrid.transform)
        transformList.Add(transform);
      foreach (Transform transform in transformList)
      {
        transform.parent = this.tempPartsObj.transform;
        this.tempParts.Enqueue(transform.gameObject);
      }
      transformList.Clear();
    }
    else
      this.scrollGrid.transform.Clear();
    if ((UnityEngine.Object) this.dot != (UnityEngine.Object) null)
    {
      this.dot.transform.Clear();
      this.dotList.Clear();
    }
    this.nbParts = 0;
    this.scrollX = float.NaN;
    this.selected = -1;
  }

  protected void addNbParts()
  {
    ++this.nbParts;
    if (!((UnityEngine.Object) this.dot != (UnityEngine.Object) null) || !((UnityEngine.Object) this.dotPrefab != (UnityEngine.Object) null))
      return;
    SelectParts component = this.dotPrefab.CloneAndGetComponent<SelectParts>(this.dot.transform);
    foreach (NGTweenParts componentsInChild in component.GetComponentsInChildren<NGTweenParts>(true))
      componentsInChild.timeOutTime = 0.2f;
    this.dotList.Add(component);
    this.resetDotTranslate();
  }

  private void resetDotTranslate()
  {
    if ((UnityEngine.Object) this.dot == (UnityEngine.Object) null || (UnityEngine.Object) this.dotPrefab == (UnityEngine.Object) null)
      return;
    float width = (float) this.dotPrefab.GetComponent<UIWidget>().width;
    float num1 = width * 1.2f;
    float num2 = (float) ((double) (-this.dotList.Count / 2) * (double) num1 + (this.dotList.Count % 2 == 1 ? 0.0 : (double) width / 2.0));
    float num3 = 0.0f;
    foreach (SelectParts dot in this.dotList)
    {
      dot.transform.localPosition = new Vector3(num3 + num2, dot.transform.localPosition.y, dot.transform.localPosition.z);
      num3 += num1;
    }
    this.setDotAndArrow(this.selected);
  }

  private int calcSelectPosition()
  {
    float x = this.scrollView.transform.localPosition.x;
    return Mathf.Clamp((double) x <= 0.0 ? ((double) x >= -((double) this.grid.cellWidth * (double) this.nbParts) ? Mathf.RoundToInt(-x / this.grid.cellWidth) : this.nbParts - 1) : 0, 0, this.nbParts - 1);
  }

  private void setDotAndArrow(int idx)
  {
    int num = 0;
    foreach (SelectParts dot in this.dotList)
    {
      if (num == idx)
        dot.setValue(1);
      else
        dot.setValue(0);
      ++num;
    }
    if (idx != -1 && (UnityEngine.Object) this.leftArrow != (UnityEngine.Object) null)
      this.leftArrow.GetComponent<NGTweenParts>().isActive = this.isMultiple ? idx > this.displayNum / 2 : (uint) idx > 0U;
    if (idx == -1 || !((UnityEngine.Object) this.rightArrow != (UnityEngine.Object) null))
      return;
    this.rightArrow.GetComponent<NGTweenParts>().isActive = this.isMultiple ? idx < this.nbParts - this.displayNum / 2 - 1 : idx < this.nbParts - 1;
  }

  private void setDotAndArrowForth(int idx)
  {
    this.setDotAndArrow(idx);
    if (idx != -1 && (UnityEngine.Object) this.leftArrow != (UnityEngine.Object) null)
    {
      UIWidget component = this.leftArrow.GetComponent<UIWidget>();
      if (idx != 0)
        component.alpha = 1f;
    }
    if (idx == -1 || !((UnityEngine.Object) this.rightArrow != (UnityEngine.Object) null))
      return;
    UIWidget component1 = this.rightArrow.GetComponent<UIWidget>();
    if (idx >= this.nbParts - 1)
      return;
    component1.alpha = 1f;
  }

  private void LateUpdate()
  {
    if ((double) this.scrollX == (double) this.scrollView.transform.localPosition.x)
      return;
    this.scrollX = this.scrollView.transform.localPosition.x;
    int idx = this.calcSelectPosition();
    if (idx == this.selected)
      return;
    this.setDotAndArrow(idx);
    this.selected = idx;
    this.playSE();
    if (!((UnityEngine.Object) this.eventReceiver != (UnityEngine.Object) null) || string.IsNullOrEmpty(this.functionName))
      return;
    SpringPanel component = this.scrollView.GetComponent<SpringPanel>();
    if (!this.isSpringPanelCheck || (UnityEngine.Object) component == (UnityEngine.Object) null || !component.enabled)
      this.eventReceiver.SendMessage(this.functionName, (object) this.selected, SendMessageOptions.DontRequireReceiver);
    else
      component.onFinished = (SpringPanel.OnFinished) (() => this.eventReceiver.SendMessage(this.functionName, (object) this.selected, SendMessageOptions.DontRequireReceiver));
  }

  public void playSE()
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if (!((UnityEngine.Object) instance != (UnityEngine.Object) null) || !this.seEnable)
      return;
    NGSoundManager ngSoundManager = instance;
    string clip;
    if (!Singleton<NGGameDataManager>.GetInstance().IsSea || Singleton<NGGameDataManager>.GetInstance().QuestType.HasValue)
    {
      CommonQuestType? questType = Singleton<NGGameDataManager>.GetInstance().QuestType;
      CommonQuestType commonQuestType = CommonQuestType.Sea;
      if (!(questType.GetValueOrDefault() == commonQuestType & questType.HasValue))
      {
        clip = "SE_1005";
        goto label_5;
      }
    }
    clip = "SE_1043";
label_5:
    ngSoundManager.playSE(clip, false, 0.0f, -1);
  }

  public void setItemPosition(int idx)
  {
    UICenterOnChild component = this.scrollGrid.GetComponent<UICenterOnChild>();
    if ((UnityEngine.Object) component == (UnityEngine.Object) null)
      return;
    this.selected = -1;
    if (idx < 0)
      return;
    if (idx >= this.scrollGrid.transform.childCount)
      return;
    try
    {
      Transform child = this.scrollGrid.transform.GetChild(idx);
      component.CenterOn(child);
    }
    catch (Exception ex)
    {
    }
  }

  public void setItemPositionQuick(int idx)
  {
    if ((UnityEngine.Object) this.scrollGrid == (UnityEngine.Object) null)
      return;
    if (idx < 0 || idx >= this.scrollGrid.transform.childCount)
    {
      Debug.LogWarning((object) ("invalid childIndex=" + (object) idx));
    }
    else
    {
      this.selected = idx;
      Vector3 localPosition = this.scrollView.transform.localPosition;
      this.scrollView.transform.localPosition = new Vector3(-this.grid.cellWidth * (float) this.selected, localPosition.y, localPosition.z);
      this.setDotAndArrow(this.selected);
    }
  }

  public void resetCenterItem(int idx)
  {
    if ((UnityEngine.Object) this.scrollGrid == (UnityEngine.Object) null)
      return;
    if (idx < 0 || idx >= this.scrollGrid.transform.childCount)
    {
      Debug.LogWarning((object) ("invalid childIndex=" + (object) idx));
    }
    else
    {
      this.selected = idx;
      this.scrollView.GetComponent<UIScrollView>().ResetHorizontalCenter(this.grid.cellWidth * (float) this.selected);
      this.setDotAndArrow(this.selected);
    }
  }

  public void disabledClipping()
  {
    UIPanel component;
    if ((UnityEngine.Object) this.scrollView == (UnityEngine.Object) null || (UnityEngine.Object) (component = this.scrollView.GetComponent<UIPanel>()) == (UnityEngine.Object) null || component.clipping == UIDrawCall.Clipping.None)
      return;
    component.clipping = UIDrawCall.Clipping.None;
  }

  public GameObject getItem(int idx, Transform tra = null)
  {
    if ((UnityEngine.Object) tra == (UnityEngine.Object) null)
      tra = this.scrollGrid.transform;
    int num = 0;
    foreach (Transform transform in tra)
    {
      if (num == idx)
        return transform.gameObject;
      ++num;
    }
    return (GameObject) null;
  }

  public GameObject GridChild(int index)
  {
    return this.scrollGrid.transform.GetChild(index).gameObject;
  }

  public IEnumerable<GameObject> GridChildren()
  {
    return this.scrollGrid.transform.GetChildren().Select<Transform, GameObject>((Func<Transform, GameObject>) (t => t.gameObject));
  }

  public int GetIndex(Transform trans)
  {
    Tuple<int, Transform> tuple = this.scrollGrid.transform.GetChildren().Select<Transform, Tuple<int, Transform>>((Func<Transform, int, Tuple<int, Transform>>) ((child, index) => Tuple.Create<int, Transform>(index, child))).FirstOrDefault<Tuple<int, Transform>>((Func<Tuple<int, Transform>, bool>) (x => x.Item2.Equals((object) trans)));
    return tuple != null ? tuple.Item1 : 0;
  }

  private void OnValidate()
  {
    if (this.setIndex < 0 || this.setIndex >= this.nbParts)
      return;
    this.setItemPosition(this.setIndex);
  }
}
