﻿// Decompiled with JetBrains decompiler
// Type: Startup00010ScoreDraw
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;
using UnityEngine;

public class Startup00010ScoreDraw : MonoBehaviour
{
  public bool zero_draw = true;
  public List<Startup00010Score> score_sprite_list;

  public void Draw(int score)
  {
    foreach (Startup00010Score scoreSprite in this.score_sprite_list)
    {
      if (score == 0)
      {
        if (!this.zero_draw)
          scoreSprite.gameObject.SetActive(false);
      }
      else
        scoreSprite.gameObject.SetActive(true);
      scoreSprite.SetActive(score % 10);
      score /= 10;
    }
  }
}
