﻿// Decompiled with JetBrains decompiler
// Type: Popup00743Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Popup00743Menu : BackButtonMenuBase
{
  [SerializeField]
  private GameObject baseSheet;
  [SerializeField]
  private UIScrollView scrollView;
  [SerializeField]
  private UILabel txtPrice;
  [SerializeField]
  private UILabel txtCaption;
  private bool bResetScrollView;
  private bool bLockScroll;

  private void Awake()
  {
    this.baseSheet.SetActive(false);
    this.bResetScrollView = false;
    this.bLockScroll = false;
  }

  private void Start()
  {
  }

  public IEnumerator Init(List<Shop00743Menu.Medal> medals)
  {
    if (medals == null)
      medals = new List<Shop00743Menu.Medal>();
    UIGrid componentInChildren;
    while ((UnityEngine.Object) (componentInChildren = this.scrollView.GetComponentInChildren<UIGrid>()) == (UnityEngine.Object) null)
      yield return (object) null;
    DateTime nolimit = new DateTime(0L);
    if (!medals.Any<Shop00743Menu.Medal>((Func<Shop00743Menu.Medal, bool>) (m => m.limit == nolimit)))
      medals.Add(new Shop00743Menu.Medal());
    int num = 0;
    int count = medals.Count;
    foreach (Shop00743Menu.Medal medal in medals)
    {
      GameObject gameObject = NGUITools.AddChild(componentInChildren.gameObject, this.baseSheet);
      gameObject.SetActive(true);
      Popup00743MenuSheet component = gameObject.GetComponent<Popup00743MenuSheet>();
      component.txtLimit.SetTextLocalize(medal.limit != nolimit ? string.Format(Consts.GetInstance().SHOP_00743_POPUP_DATE_LIMIT, (object) medal.limit) : Consts.GetInstance().SHOP_00743_POPUP_DATE_NOLIMIT);
      component.txtCount.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00743_POPUP_PRICE, (object) medal.count));
      num += medal.count;
      if (--count == 0)
      {
        this.txtCaption.topAnchor.target = gameObject.transform;
        this.txtCaption.bottomAnchor.target = gameObject.transform;
        this.txtCaption.ResetAnchors();
        this.txtCaption.Update();
      }
    }
    this.txtPrice.SetTextLocalize(string.Format(Consts.GetInstance().SHOP_00743_POPUP_TOTAL_PRICE, (object) num));
    this.bResetScrollView = true;
    this.bLockScroll = medals.Count <= 3;
  }

  private void resetScrollView()
  {
    this.scrollView.ResetPosition();
    this.scrollView.RestrictWithinBounds(false, false, true);
    this.scrollView.GetComponentInChildren<UIGrid>().Reposition();
    this.scrollView.UpdateScrollbars(true);
  }

  protected override void Update()
  {
    base.Update();
    if (!this.bResetScrollView)
      return;
    this.bResetScrollView = false;
    this.resetScrollView();
    if (!this.bLockScroll)
      return;
    this.scrollView.enabled = false;
  }

  public void onClosePressed()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public override void onBackButton()
  {
    this.onClosePressed();
  }
}
