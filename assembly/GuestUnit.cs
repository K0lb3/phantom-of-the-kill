﻿// Decompiled with JetBrains decompiler
// Type: GuestUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections.Generic;
using UniLinq;

public class GuestUnit
{
  public static int[] GetGuestsID(int stageID)
  {
    List<int> intList = new List<int>();
    foreach (KeyValuePair<int, BattleStageGuest> keyValuePair in MasterData.BattleStageGuest.Where<KeyValuePair<int, BattleStageGuest>>((Func<KeyValuePair<int, BattleStageGuest>, bool>) (x => x.Value.stage_BattleStage == stageID)).ToList<KeyValuePair<int, BattleStageGuest>>())
    {
      KeyValuePair<int, BattleStageGuest> unit = keyValuePair;
      if (MasterData.BattleStagePlayer.Any<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage_BattleStage == stageID && x.Value.deck_position == unit.Value.deck_position)))
        intList.Add(unit.Value.ID);
    }
    return intList.ToArray();
  }
}
