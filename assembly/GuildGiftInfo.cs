﻿// Decompiled with JetBrains decompiler
// Type: GuildGiftInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;

[Serializable]
public class GuildGiftInfo
{
  public GuildGiftScrollParts scroll;

  public GuildMemberGift gift { get; set; }

  public GuildGiftInfo TempCopy()
  {
    GuildGiftInfo guildGiftInfo = (GuildGiftInfo) this.MemberwiseClone();
    guildGiftInfo.scroll = (GuildGiftScrollParts) null;
    return guildGiftInfo;
  }
}
