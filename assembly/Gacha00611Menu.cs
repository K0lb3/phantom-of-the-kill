﻿// Decompiled with JetBrains decompiler
// Type: Gacha00611Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Gacha00611Menu : EquipmentDetailMenuBase
{
  private static readonly string COLOR_TAG_GREEN = "[00ff00]{0}[-]";
  private static readonly string COLOR_TAG_RED = "[ff0000]{0}[-]";
  private static readonly string COLOR_TAG_YELLOW = "[ffff00]{0}[-]";
  private Unit004431Menu.Param sendParam = new Unit004431Menu.Param();
  protected string seGaugeUp = "SE_1053";
  protected string seRankUp = "SE_1054";
  protected string seSkillAwaked = "SE_1055";
  [SerializeField]
  protected UILabel TxtAttack;
  [SerializeField]
  protected UILabel TxtCritical;
  [SerializeField]
  protected UILabel TxtDefense;
  [SerializeField]
  protected UILabel TxtDexterity;
  [SerializeField]
  protected UILabel TxtEvasion;
  [SerializeField]
  protected UILabel TxtExp;
  [SerializeField]
  protected UILabel TxtNextExp;
  [SerializeField]
  protected UILabel TxtMagicAttack;
  [SerializeField]
  protected UILabel TxtMagicDefense;
  [SerializeField]
  protected UILabel TxtRange;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UILabel TxtWait;
  [SerializeField]
  protected UILabel TxtWeapontype;
  [SerializeField]
  public GearKindIcon Weapon;
  [SerializeField]
  private SprGearAttack WeaponSpAttack;
  [SerializeField]
  private UILabel TxtWeaponAttack;
  [SerializeField]
  private SprGearElement WeaponSpElement;
  [SerializeField]
  private UILabel TxtWeaponElement;
  [SerializeField]
  protected UI2DSprite DynWeaponIll;
  [SerializeField]
  protected Transform DynWeaponModel;
  [SerializeField]
  protected GameObject NewIcon;
  [SerializeField]
  protected UISprite SlcGauge;
  [SerializeField]
  private Transform TopStarPos;
  [SerializeField]
  private GameObject charaThum;
  [SerializeField]
  private UIButton btnBack;
  [SerializeField]
  private GameObject DirAddStauts;
  [SerializeField]
  public GameObject SkillArrowObject;
  [SerializeField]
  public GameObject WeaponSkillOneRoot;
  [SerializeField]
  public UIButton[] WeaponSkillButtonOne;
  [SerializeField]
  public BattleSkillIcon[] WeaponSkillOne;
  [SerializeField]
  public GameObject WeaponSkillTwoRoot;
  [SerializeField]
  public UIButton[] WeaponSkillButtonTwo;
  [SerializeField]
  public BattleSkillIcon[] WeaponSkillTwo;
  public GameObject unitPrefab;
  public GameObject SkillDialog;
  [SerializeField]
  public UI2DSprite rarityStarsIcon;
  [SerializeField]
  private GameObject floatingSkillDialog;
  private System.Action<GearGearSkill, bool> showSkillDialog;
  [SerializeField]
  private GameObject rankUpObject;
  [SerializeField]
  private GameObject maxRankUpObject;
  [SerializeField]
  private GameObject dirEquipNone;
  [SerializeField]
  private GameObject dirEquipUnit;
  [SerializeField]
  private GameObject remainingManaSeedContainer;
  [SerializeField]
  private UILabel remainingManaSeedLabel;
  private bool isRankup;
  private bool isMaxRankup;
  private int maxWidth;
  private Decimal startGauge;
  private Decimal addGauge;
  private int base_gear_level;
  private bool isStopSe;
  private GameCore.ItemInfo target;
  private List<GearGearSkill> evoSkill;
  private List<GearGearSkill> addSkill;
  private List<GearGearSkill> addReisouSkill;
  private GameObject skillGetPrefab;
  private GameObject commonElementIconPrefab;
  private GameObject popupAttackClassPrefab;
  private GameObject popupAttachedElementPrefab;
  [SerializeField]
  protected GameObject DirReisou;
  [SerializeField]
  protected UILabel TxtReisouRank;
  [SerializeField]
  protected GameObject DirReisouExpGauge;
  [SerializeField]
  protected UISprite SlcReisouGauge;
  [SerializeField]
  protected GameObject DynReisouIcon;
  [SerializeField]
  private GameObject rankUpReisouObject;
  protected GameCore.ItemInfo baseReisouInfo;
  protected GameCore.ItemInfo targetReisouInfo;
  protected ItemIcon reisouIcon;
  private bool isReisouRankup;
  private int maxReisouWidth;
  private Decimal startReisouGauge;
  private Decimal addReisouGauge;
  private int base_reisou_level;
  [SerializeField]
  protected GameObject dirBottom;
  private const int defaultIllustPosY = 123;
  private const int reisouIllustPosY = 0;
  private System.Action cahngeSceneCallback;

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (this.btnBack.GetComponent<Collider>().enabled)
    {
      if (this.cahngeSceneCallback != null)
        this.cahngeSceneCallback();
      else
        this.backScene();
    }
    this.btnBack.GetComponent<Collider>().enabled = false;
  }

  public UIButton BackSceneButton
  {
    get
    {
      return this.btnBack;
    }
  }

  public IEnumerator Initialize(
    GameCore.ItemInfo TargetInfo,
    bool NewFlag,
    int countWeapon,
    bool select = true,
    GameCore.ItemInfo baseInfo = null,
    GameCore.ItemInfo TargetReisouInfo = null,
    GameCore.ItemInfo baseReisouInfo = null,
    bool showEquipUnit = false)
  {
    Gacha00611Menu gacha00611Menu = this;
    gacha00611Menu.target = TargetInfo;
    gacha00611Menu.baseReisouInfo = baseReisouInfo;
    gacha00611Menu.targetReisouInfo = TargetReisouInfo;
    PlayerUnit[] equiptargets = ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => ((IEnumerable<int?>) x.equip_gear_ids).Contains<int?>(new int?(TargetInfo.itemID)))).ToArray<PlayerUnit>();
    gacha00611Menu.isStopSe = false;
    UnitIcon[] componentsInChildren = gacha00611Menu.charaThum.GetComponentsInChildren<UnitIcon>();
    if (componentsInChildren != null)
      ((IEnumerable<UnitIcon>) componentsInChildren).ForEach<UnitIcon>((System.Action<UnitIcon>) (x => UnityEngine.Object.Destroy((UnityEngine.Object) x.gameObject)));
    Future<GameObject> iconPrefabF;
    IEnumerator e;
    if ((UnityEngine.Object) gacha00611Menu.unitPrefab == (UnityEngine.Object) null)
    {
      iconPrefabF = Res.Prefabs.UnitIcon.normal.Load<GameObject>();
      e = iconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha00611Menu.unitPrefab = iconPrefabF.Result;
      iconPrefabF = (Future<GameObject>) null;
    }
    UnitIcon unitScript = gacha00611Menu.unitPrefab.Clone(gacha00611Menu.charaThum.transform).GetComponent<UnitIcon>();
    if (((IEnumerable<PlayerUnit>) equiptargets).Count<PlayerUnit>() <= 0)
    {
      unitScript.SetEmpty();
      unitScript.SelectUnit = !showEquipUnit && select;
    }
    else
    {
      unitScript.setBottom(equiptargets[0]);
      unitScript.ShowBottomInfo(UnitSortAndFilter.SORT_TYPES.Level);
      e = unitScript.SetUnit(equiptargets[0].unit, equiptargets[0].GetElement(), false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      unitScript.setLevelText(equiptargets[0]);
    }
    if (showEquipUnit)
    {
      unitScript.Button.isEnabled = false;
      gacha00611Menu.charaThum.GetComponent<UIButton>().isEnabled = false;
    }
    gacha00611Menu.sendParam.gearId = TargetInfo.itemID;
    GearGear targetGear = TargetInfo.gear;
    gacha00611Menu.sendParam.gearKindId = targetGear.kind_GearKind;
    Future<UnityEngine.Sprite> spriteF = targetGear.LoadSpriteBasic(1f);
    e = spriteF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    gacha00611Menu.DynWeaponIll.sprite2D = spriteF.Result;
    gacha00611Menu.DynWeaponIll.width = Mathf.FloorToInt(spriteF.Result.textureRect.width);
    gacha00611Menu.DynWeaponIll.height = Mathf.FloorToInt(spriteF.Result.textureRect.height);
    gacha00611Menu.DynWeaponIll.transform.localScale = (Vector3) new Vector2(0.8f, 0.8f);
    gacha00611Menu.Weapon.Init(targetGear.kind, TargetInfo.GetElement());
    if ((UnityEngine.Object) gacha00611Menu.SkillDialog == (UnityEngine.Object) null)
    {
      iconPrefabF = Res.Prefabs.battle017_11_1_1.SkillDetailDialog.Load<GameObject>();
      e = iconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha00611Menu.SkillDialog = iconPrefabF.Result;
      iconPrefabF = (Future<GameObject>) null;
    }
    gacha00611Menu.SetSkillDeteilEvent(TargetInfo);
    if (targetGear.kind.is_attack)
    {
      if (targetGear.hasAttackClass)
      {
        gacha00611Menu.WeaponSpAttack.InitGearAttackID((int) targetGear.gearClassification.attack_classification);
        gacha00611Menu.TxtWeaponAttack.SetText(targetGear.attackClassificationName);
      }
      else
      {
        gacha00611Menu.WeaponSpAttack.gameObject.SetActive(false);
        gacha00611Menu.TxtWeaponAttack.gameObject.SetActive(false);
      }
      if ((UnityEngine.Object) gacha00611Menu.commonElementIconPrefab == (UnityEngine.Object) null)
      {
        iconPrefabF = Res.Icons.CommonElementIcon.Load<GameObject>();
        yield return (object) iconPrefabF.Wait();
        gacha00611Menu.commonElementIconPrefab = iconPrefabF.Result;
        iconPrefabF = (Future<GameObject>) null;
      }
      if ((UnityEngine.Object) gacha00611Menu.popupAttackClassPrefab == (UnityEngine.Object) null)
      {
        iconPrefabF = PopupAttackClassDetail.createPrefabLoader();
        yield return (object) iconPrefabF.Wait();
        gacha00611Menu.popupAttackClassPrefab = iconPrefabF.Result;
        iconPrefabF = (Future<GameObject>) null;
      }
      if ((UnityEngine.Object) gacha00611Menu.popupAttachedElementPrefab == (UnityEngine.Object) null)
      {
        iconPrefabF = PopupGearAttachedElementDetail.createPrefabLoader();
        yield return (object) iconPrefabF.Wait();
        gacha00611Menu.popupAttachedElementPrefab = iconPrefabF.Result;
        iconPrefabF = (Future<GameObject>) null;
      }
      gacha00611Menu.WeaponSpElement.Initialize(gacha00611Menu.commonElementIconPrefab.GetComponent<CommonElementIcon>(), targetGear.attachedElement);
      gacha00611Menu.TxtWeaponElement.SetText(CommonElementName.GetName((int) targetGear.attachedElement));
    }
    else
    {
      gacha00611Menu.WeaponSpAttack.gameObject.SetActive(false);
      gacha00611Menu.TxtWeaponAttack.gameObject.SetActive(false);
      gacha00611Menu.WeaponSpElement.gameObject.SetActive(false);
      gacha00611Menu.TxtWeaponElement.gameObject.SetActive(false);
    }
    gacha00611Menu.dirEquipNone.SetActive(!showEquipUnit);
    gacha00611Menu.dirEquipUnit.SetActive(showEquipUnit);
    Judgement.GearParameter gearParameter1 = TargetInfo.isWeaponMaterial ? Judgement.GearParameter.FromGearGear(targetGear) : Judgement.GearParameter.FromPlayerGear(TargetInfo);
    Judgement.GearParameter gearParameter2 = (Judgement.GearParameter) null;
    if (baseInfo != null)
      gearParameter2 = Judgement.GearParameter.FromPlayerGear(baseInfo);
    if (targetGear.attack_type == GearAttackType.physical)
    {
      gacha00611Menu.SetParameter(gacha00611Menu.TxtAttack, gearParameter1.Power, gearParameter2 != null ? gearParameter1.Power - gearParameter2.Power : 0);
      gacha00611Menu.TxtMagicAttack.SetTextLocalize(0);
    }
    else
    {
      gacha00611Menu.TxtAttack.SetTextLocalize(0);
      gacha00611Menu.SetParameter(gacha00611Menu.TxtMagicAttack, gearParameter1.Power, gearParameter2 != null ? gearParameter1.Power - gearParameter2.Power : 0);
    }
    gacha00611Menu.SetParameter(gacha00611Menu.TxtCritical, gearParameter1.Critical, gearParameter2 != null ? gearParameter1.Critical - gearParameter2.Critical : 0);
    gacha00611Menu.SetParameter(gacha00611Menu.TxtDefense, gearParameter1.PhysicalDefense, gearParameter2 != null ? gearParameter1.PhysicalDefense - gearParameter2.PhysicalDefense : 0);
    gacha00611Menu.SetParameter(gacha00611Menu.TxtMagicDefense, gearParameter1.MagicDefense, gearParameter2 != null ? gearParameter1.MagicDefense - gearParameter2.MagicDefense : 0);
    gacha00611Menu.SetParameter(gacha00611Menu.TxtEvasion, gearParameter1.Evasion, gearParameter2 != null ? gearParameter1.Evasion - gearParameter2.Evasion : 0);
    gacha00611Menu.SetParameter(gacha00611Menu.TxtDexterity, gearParameter1.Hit, gearParameter2 != null ? gearParameter1.Hit - gearParameter2.Hit : 0);
    gacha00611Menu.TxtRange.SetTextLocalize(targetGear.min_range.ToString() + "-" + (object) targetGear.max_range);
    gacha00611Menu.TxtWait.SetTextLocalize(targetGear.weight);
    gacha00611Menu.TxtExp.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_RANK, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) TargetInfo.gearLevel
      },
      {
        (object) "max",
        (object) TargetInfo.gearLevelLimit
      }
    }));
    gacha00611Menu.TxtNextExp.SetTextLocalize(TargetInfo.gearExpNext);
    gacha00611Menu.TxtTitle.SetText(TargetInfo.Name());
    string text1 = targetGear.gearClassification?.name ?? "-";
    gacha00611Menu.TxtWeapontype.SetText(text1);
    gacha00611Menu.maxWidth = gacha00611Menu.SlcGauge.width;
    Decimal num1 = new Decimal();
    if (TargetInfo.gearExpNext + TargetInfo.gearExp != 0)
      num1 = (Decimal) TargetInfo.gearExp / (Decimal) (TargetInfo.gearExpNext + TargetInfo.gearExp);
    if (baseInfo != null && baseInfo.gearExpNext + baseInfo.gearExp != 0)
      num1 = (Decimal) baseInfo.gearExp / (Decimal) (baseInfo.gearExpNext + baseInfo.gearExp);
    Decimal num2 = (Decimal) gacha00611Menu.maxWidth * num1;
    if (num2 == Decimal.Zero)
    {
      gacha00611Menu.SlcGauge.gameObject.SetActive(false);
    }
    else
    {
      gacha00611Menu.SlcGauge.gameObject.SetActive(true);
      gacha00611Menu.SlcGauge.width = (int) num2;
    }
    RarityIcon.SetRarity(targetGear, gacha00611Menu.rarityStarsIcon);
    gacha00611Menu.NewIcon.SetActive(NewFlag);
    e = gacha00611Menu.SetIncrementalParameter(TargetInfo, gacha00611Menu.DirAddStauts);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (baseInfo != null)
    {
      Decimal num3 = baseInfo.gearExpNext + baseInfo.gearExp == 0 ? Decimal.Zero : (Decimal) baseInfo.gearExp / (Decimal) (baseInfo.gearExpNext + baseInfo.gearExp);
      Decimal num4 = TargetInfo.gearExpNext + TargetInfo.gearExp == 0 ? Decimal.Zero : (Decimal) TargetInfo.gearExp / (Decimal) (TargetInfo.gearExpNext + TargetInfo.gearExp);
      gacha00611Menu.isRankup = TargetInfo.gearLevel > baseInfo.gearLevel;
      gacha00611Menu.isMaxRankup = TargetInfo.gearLevelLimit > baseInfo.gearLevelLimit;
      gacha00611Menu.startGauge = num3;
      gacha00611Menu.addGauge = (Decimal) (TargetInfo.gearLevel - baseInfo.gearLevel) - num3 + num4;
      gacha00611Menu.base_gear_level = baseInfo.gearLevel;
      if (gacha00611Menu.isRankup)
        gacha00611Menu.TxtExp.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_RANK, (IDictionary) new Hashtable()
        {
          {
            (object) "now",
            (object) baseInfo.gearLevel
          },
          {
            (object) "max",
            (object) baseInfo.gearLevelLimit
          }
        }));
      else if (gacha00611Menu.isMaxRankup)
      {
        UILabel txtExp = gacha00611Menu.TxtExp;
        string bugu0059Rank = Consts.GetInstance().BUGU_0059_RANK;
        Hashtable hashtable1 = new Hashtable();
        Hashtable hashtable2 = hashtable1;
        string str;
        if (TargetInfo.gearLevel >= baseInfo.gearLevel)
        {
          if (TargetInfo.gearLevel <= baseInfo.gearLevel)
            str = TargetInfo.gearLevel.ToString();
          else
            str = Gacha00611Menu.COLOR_TAG_GREEN.F((object) TargetInfo.gearLevel);
        }
        else
          str = Gacha00611Menu.COLOR_TAG_RED.F((object) TargetInfo.gearLevel);
        hashtable2.Add((object) "now", (object) str);
        hashtable1.Add((object) "max", (object) Gacha00611Menu.COLOR_TAG_GREEN.F((object) TargetInfo.gearLevelLimit));
        Hashtable hashtable3 = hashtable1;
        string text2 = Consts.Format(bugu0059Rank, (IDictionary) hashtable3);
        txtExp.SetTextLocalize(text2);
      }
      if (baseInfo.skills.Length < TargetInfo.skills.Length)
      {
        gacha00611Menu.addSkill = new List<GearGearSkill>();
        for (int length = baseInfo.skills.Length; length < TargetInfo.skills.Length; ++length)
          gacha00611Menu.addSkill.Add(TargetInfo.skills[length]);
      }
      if (baseInfo.skills.Length != 0)
      {
        gacha00611Menu.evoSkill = new List<GearGearSkill>();
        for (int index = 0; index < baseInfo.skills.Length; ++index)
        {
          if (baseInfo.skills[index].ID != TargetInfo.skills[index].ID && baseInfo.skills[index].release_rank < TargetInfo.skills[index].release_rank)
            gacha00611Menu.evoSkill.Add(TargetInfo.skills[index]);
        }
      }
    }
    if (countWeapon > 1)
      ModalWindow.Show(Consts.Format(Consts.GetInstance().GACHA_0061MULTIPLE_WEAPONS_TITLE, (IDictionary) null), Consts.Format(Consts.GetInstance().GACHA_0061MULTIPLE_WEAPONS_NUM, (IDictionary) new Hashtable()
      {
        {
          (object) "weapon",
          (object) TargetInfo.name.ToString()
        },
        {
          (object) "num",
          (object) countWeapon.ToString()
        }
      }), (System.Action) (() => {}));
    gacha00611Menu.remainingManaSeedContainer.SetActive(false);
    if (TargetInfo.gear != null && TargetInfo.gear.kind.Enum == GearKindEnum.accessories && TargetInfo.gear.disappearance_type_GearDisappearanceType == 1)
    {
      gacha00611Menu.remainingManaSeedContainer.SetActive(true);
      gacha00611Menu.remainingManaSeedLabel.SetTextLocalize(TargetInfo.gearAccessoryRemainingAmount);
    }
    if (gacha00611Menu.targetReisouInfo == null)
    {
      gacha00611Menu.TxtReisouRank.gameObject.SetActive(false);
      gacha00611Menu.DirReisouExpGauge.SetActive(false);
    }
    else
    {
      gacha00611Menu.TxtReisouRank.SetTextLocalize(Consts.GetInstance().UNIT_00443_REISOU_RANK.F((object) gacha00611Menu.targetReisouInfo.gearLevel, (object) gacha00611Menu.targetReisouInfo.gearLevelLimit));
      if (baseReisouInfo != null)
      {
        Decimal num3 = baseReisouInfo.gearExpNext + baseReisouInfo.gearExp == 0 ? Decimal.Zero : (Decimal) baseReisouInfo.gearExp / (Decimal) (baseReisouInfo.gearExpNext + baseReisouInfo.gearExp);
        Decimal num4 = gacha00611Menu.targetReisouInfo.gearExpNext + gacha00611Menu.targetReisouInfo.gearExp == 0 ? Decimal.Zero : (Decimal) gacha00611Menu.targetReisouInfo.gearExp / (Decimal) (gacha00611Menu.targetReisouInfo.gearExpNext + gacha00611Menu.targetReisouInfo.gearExp);
        gacha00611Menu.isReisouRankup = gacha00611Menu.targetReisouInfo.gearLevel > baseReisouInfo.gearLevel;
        gacha00611Menu.startReisouGauge = num3;
        gacha00611Menu.addReisouGauge = (Decimal) (gacha00611Menu.targetReisouInfo.gearLevel - baseReisouInfo.gearLevel) - num3 + num4;
        gacha00611Menu.base_reisou_level = baseReisouInfo.gearLevel;
        if (gacha00611Menu.isReisouRankup)
        {
          gacha00611Menu.TxtReisouRank.SetTextLocalize(Consts.GetInstance().UNIT_00443_REISOU_RANK.F((object) baseReisouInfo.gearLevel, (object) baseReisouInfo.gearLevelLimit));
          GearGearSkill[] skills1 = baseReisouInfo.skills;
          GearGearSkill[] skills2 = gacha00611Menu.targetReisouInfo.skills;
          if (baseReisouInfo.skills.Length < gacha00611Menu.targetReisouInfo.skills.Length)
          {
            gacha00611Menu.addReisouSkill = new List<GearGearSkill>();
            for (int length = baseReisouInfo.skills.Length; length < gacha00611Menu.targetReisouInfo.skills.Length; ++length)
              gacha00611Menu.addReisouSkill.Add(gacha00611Menu.targetReisouInfo.skills[length]);
          }
        }
      }
      iconPrefabF = Res.Prefabs.ItemIcon.prefab.Load<GameObject>();
      e = iconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha00611Menu.reisouIcon = iconPrefabF.Result.CloneAndGetComponent<ItemIcon>(gacha00611Menu.DynReisouIcon.transform);
      e = gacha00611Menu.reisouIcon.InitByItemInfo(gacha00611Menu.targetReisouInfo);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha00611Menu.reisouIcon.setEquipReisouDisp();
      gacha00611Menu.reisouIcon.onClick = (System.Action<ItemIcon>) (_ => {});
      gacha00611Menu.reisouIcon.DisableLongPressEvent();
      GameCore.ItemInfo itemInfo = gacha00611Menu.targetReisouInfo;
      if (baseReisouInfo != null)
        itemInfo = baseReisouInfo;
      gacha00611Menu.maxReisouWidth = gacha00611Menu.SlcReisouGauge.width;
      float num5 = (float) itemInfo.gearExp / (float) (itemInfo.gearExp + itemInfo.gearExpNext);
      float num6 = (float) gacha00611Menu.maxReisouWidth * num5;
      if ((double) num6 == 0.0 || itemInfo.gearExp + itemInfo.gearExpNext == 0)
      {
        gacha00611Menu.SlcReisouGauge.gameObject.SetActive(false);
      }
      else
      {
        gacha00611Menu.SlcReisouGauge.gameObject.SetActive(true);
        gacha00611Menu.SlcReisouGauge.width = (int) num6;
      }
      iconPrefabF = (Future<GameObject>) null;
    }
    if (gacha00611Menu.evoSkill != null && gacha00611Menu.evoSkill.Count > 0 || gacha00611Menu.addSkill != null && gacha00611Menu.addSkill.Count > 0 || gacha00611Menu.addReisouSkill != null && gacha00611Menu.addReisouSkill.Count > 0)
    {
      iconPrefabF = Res.Prefabs.battle.BuguSkillGetPrefab.Load<GameObject>();
      e = iconPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      gacha00611Menu.skillGetPrefab = iconPrefabF.Result;
      iconPrefabF = (Future<GameObject>) null;
    }
    if (TargetInfo.isReisou)
    {
      gacha00611Menu.dirBottom.SetActive(false);
      gacha00611Menu.DynWeaponIll.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
    }
    else
    {
      gacha00611Menu.dirBottom.SetActive(true);
      gacha00611Menu.DynWeaponIll.transform.localPosition = new Vector3(0.0f, 123f, 0.0f);
    }
  }

  private void SetParameter(UILabel label, int param, int up)
  {
    if (up > 0)
      label.SetTextLocalize(Gacha00611Menu.COLOR_TAG_YELLOW.F((object) param));
    else if (up < 0)
      label.SetTextLocalize(Gacha00611Menu.COLOR_TAG_RED.F((object) param));
    else
      label.SetTextLocalize(param);
  }

  public void OpenPopupWeaponAttack()
  {
    if (this.target == null || this.target.gear == null || !((UnityEngine.Object) this.popupAttackClassPrefab != (UnityEngine.Object) null))
      return;
    this.StartCoroutine(this.doPopupAttackClassDetail());
  }

  private IEnumerator doPopupAttackClassDetail()
  {
    Gacha00611Menu gacha00611Menu = this;
    if (!gacha00611Menu.IsPushAndSet())
    {
      yield return (object) PopupAttackClassDetail.show(gacha00611Menu.popupAttackClassPrefab, gacha00611Menu.target.gear);
      gacha00611Menu.IsPush = false;
    }
  }

  public void OpenPopupWeaponElement()
  {
    if (this.target == null || this.target.gear == null || !((UnityEngine.Object) this.popupAttachedElementPrefab != (UnityEngine.Object) null))
      return;
    this.StartCoroutine(this.doPopupAttachedElementDetail());
  }

  private IEnumerator doPopupAttachedElementDetail()
  {
    Gacha00611Menu gacha00611Menu = this;
    if (!gacha00611Menu.IsPushAndSet())
    {
      yield return (object) PopupGearAttachedElementDetail.show(gacha00611Menu.popupAttachedElementPrefab, gacha00611Menu.target.gear);
      gacha00611Menu.IsPush = false;
    }
  }

  public void StartAnime()
  {
    this.StartCoroutine("Play");
  }

  public void StopAnime()
  {
    this.isStopSe = true;
    this.StopCoroutine("Play");
    Singleton<NGSoundManager>.GetInstance().stopSE(-1);
  }

  private IEnumerator Play()
  {
    Decimal addValue;
    Decimal now;
    Decimal moveTone;
    if (this.addGauge > Decimal.Zero)
    {
      addValue = this.addGauge / new Decimal(300, 0, 0, false, (byte) 1) > new Decimal(33, 0, 0, false, (byte) 3) ? new Decimal(33, 0, 0, false, (byte) 3) : this.addGauge / new Decimal(1200, 0, 0, false, (byte) 1);
      now = new Decimal();
      if (!this.isStopSe)
        Singleton<NGSoundManager>.GetInstance().playSE(this.seGaugeUp, true, 0.0f, -1);
      while (now < this.addGauge)
      {
        Decimal d1 = this.startGauge + now;
        now += addValue;
        if (now > this.addGauge)
          now = this.addGauge;
        int int32_1 = Decimal.ToInt32(Decimal.Floor(d1));
        Decimal d2 = this.startGauge + now;
        moveTone = d2 - Decimal.Floor(d2);
        int int32_2 = Decimal.ToInt32(Decimal.Floor(d2));
        if (this.isRankup && int32_1 < int32_2)
        {
          this.TxtExp.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_RANK, (IDictionary) new Hashtable()
          {
            {
              (object) "now",
              (object) Gacha00611Menu.COLOR_TAG_GREEN.F((object) (int32_2 + this.base_gear_level))
            },
            {
              (object) "max",
              (object) this.target.gearLevelLimit
            }
          }));
          if ((UnityEngine.Object) this.rankUpObject != (UnityEngine.Object) null)
          {
            Singleton<NGSoundManager>.GetInstance().stopSE(-1);
            if (!this.isStopSe)
              Singleton<NGSoundManager>.GetInstance().playSE(this.seRankUp, false, 0.0f, -1);
            this.rankUpObject.SetActive(true);
            ((IEnumerable<UITweener>) this.rankUpObject.GetComponentsInChildren<UITweener>()).ForEach<UITweener>((System.Action<UITweener>) (x =>
            {
              x.enabled = true;
              x.PlayForward();
            }));
            this.SlcGauge.width = this.maxWidth;
          }
          yield return (object) new WaitForSeconds(0.6f);
          if (!this.isStopSe)
            Singleton<NGSoundManager>.GetInstance().playSE(this.seGaugeUp, true, 0.0f, -1);
        }
        if ((UnityEngine.Object) this.SlcGauge != (UnityEngine.Object) null)
        {
          Decimal num = (Decimal) this.maxWidth * moveTone;
          if (num == Decimal.Zero)
          {
            this.SlcGauge.gameObject.SetActive(false);
          }
          else
          {
            this.SlcGauge.gameObject.SetActive(true);
            this.SlcGauge.width = (int) num;
          }
        }
        yield return (object) null;
      }
      yield return (object) new WaitForSeconds(0.3f);
      Singleton<NGSoundManager>.GetInstance().stopSE(-1);
    }
    if (this.isMaxRankup && (UnityEngine.Object) this.maxRankUpObject != (UnityEngine.Object) null)
    {
      this.maxRankUpObject.SetActive(true);
      ((IEnumerable<UITweener>) this.maxRankUpObject.GetComponentsInChildren<UITweener>()).ForEach<UITweener>((System.Action<UITweener>) (x =>
      {
        x.enabled = true;
        x.PlayForward();
      }));
      yield return (object) new WaitForSeconds(0.6f);
    }
    GameObject popup;
    BattleResultBuguSkillGet o;
    IEnumerator e;
    if (this.evoSkill != null)
    {
      foreach (GearGearSkill skill in this.evoSkill)
      {
        popup = Singleton<PopupManager>.GetInstance().open(this.skillGetPrefab, false, false, false, true, false, false, "SE_1006");
        popup.SetActive(false);
        o = popup.GetComponent<BattleResultBuguSkillGet>();
        e = o.Init(false, this.target, skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        popup.SetActive(true);
        o.doStart();
        bool isFinished = false;
        o.SetCallback((System.Action) (() => isFinished = true));
        while (!isFinished)
          yield return (object) null;
        yield return (object) new WaitForSeconds(0.6f);
        popup = (GameObject) null;
        o = (BattleResultBuguSkillGet) null;
      }
    }
    if (this.addSkill != null)
    {
      foreach (GearGearSkill skill in this.addSkill)
      {
        popup = Singleton<PopupManager>.GetInstance().open(this.skillGetPrefab, false, false, false, true, false, false, "SE_1006");
        popup.SetActive(false);
        o = popup.GetComponent<BattleResultBuguSkillGet>();
        e = o.Init(true, this.target, skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        popup.SetActive(true);
        if (!this.isStopSe)
          Singleton<NGSoundManager>.GetInstance().playSE(this.seSkillAwaked, false, 0.0f, -1);
        o.doStart();
        bool isFinished = false;
        o.SetCallback((System.Action) (() => isFinished = true));
        while (!isFinished)
          yield return (object) null;
        yield return (object) new WaitForSeconds(0.6f);
        popup = (GameObject) null;
        o = (BattleResultBuguSkillGet) null;
      }
    }
    if (this.addReisouGauge > Decimal.Zero)
    {
      now = this.addReisouGauge / new Decimal(300, 0, 0, false, (byte) 1) > new Decimal(33, 0, 0, false, (byte) 3) ? new Decimal(33, 0, 0, false, (byte) 3) : this.addReisouGauge / new Decimal(300, 0, 0, false, (byte) 1);
      addValue = new Decimal();
      if (!this.isStopSe)
        Singleton<NGSoundManager>.GetInstance().playSE(this.seGaugeUp, true, 0.0f, -1);
      while (addValue < this.addReisouGauge)
      {
        Decimal d1 = this.startReisouGauge + addValue;
        addValue += now;
        if (addValue > this.addReisouGauge)
          addValue = this.addReisouGauge;
        int int32_1 = Decimal.ToInt32(Decimal.Floor(d1));
        Decimal d2 = this.startReisouGauge + addValue;
        moveTone = d2 - Decimal.Floor(d2);
        int int32_2 = Decimal.ToInt32(Decimal.Floor(d2));
        if (this.isReisouRankup && int32_1 < int32_2)
        {
          this.TxtReisouRank.SetTextLocalize(Consts.GetInstance().UNIT_00443_REISOU_RANK.F((object) Gacha00611Menu.COLOR_TAG_GREEN.F((object) (int32_2 + this.base_reisou_level)), (object) this.targetReisouInfo.gearLevelLimit));
          if ((UnityEngine.Object) this.rankUpReisouObject != (UnityEngine.Object) null)
          {
            Singleton<NGSoundManager>.GetInstance().stopSE(-1);
            if (!this.isStopSe)
              Singleton<NGSoundManager>.GetInstance().playSE(this.seRankUp, false, 0.0f, -1);
            this.rankUpReisouObject.SetActive(true);
            ((IEnumerable<UITweener>) this.rankUpReisouObject.GetComponentsInChildren<UITweener>()).ForEach<UITweener>((System.Action<UITweener>) (x =>
            {
              x.enabled = true;
              x.PlayForward();
            }));
            this.SlcReisouGauge.width = this.maxReisouWidth;
          }
          yield return (object) new WaitForSeconds(0.6f);
          Singleton<NGSoundManager>.GetInstance().playSE(this.seGaugeUp, true, 0.0f, -1);
        }
        if ((UnityEngine.Object) this.SlcReisouGauge != (UnityEngine.Object) null)
        {
          Decimal num = (Decimal) this.maxReisouWidth * moveTone;
          if (num == Decimal.Zero)
          {
            this.SlcReisouGauge.gameObject.SetActive(false);
          }
          else
          {
            this.SlcReisouGauge.gameObject.SetActive(true);
            this.SlcReisouGauge.width = (int) num;
          }
        }
        yield return (object) null;
      }
      Singleton<NGSoundManager>.GetInstance().stopSE(-1);
      yield return (object) new WaitForSeconds(0.3f);
    }
    if (this.addReisouSkill != null)
    {
      foreach (GearGearSkill skill in this.addReisouSkill)
      {
        popup = Singleton<PopupManager>.GetInstance().open(this.skillGetPrefab, false, false, false, true, false, false, "SE_1006");
        popup.SetActive(false);
        o = popup.GetComponent<BattleResultBuguSkillGet>();
        e = o.Init(true, this.targetReisouInfo, skill);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        popup.SetActive(true);
        if (!this.isStopSe)
          Singleton<NGSoundManager>.GetInstance().playSE(this.seSkillAwaked, false, 0.0f, -1);
        o.doStart();
        bool isFinished = false;
        o.SetCallback((System.Action) (() => isFinished = true));
        while (!isFinished)
          yield return (object) null;
        yield return (object) new WaitForSeconds(0.6f);
        popup = (GameObject) null;
        o = (BattleResultBuguSkillGet) null;
      }
    }
  }

  protected void SetSkillDialog(UIButton button, GearGearSkill skill_data, bool isRelease)
  {
    if (this.showSkillDialog == null)
    {
      Battle0171111Event dialog = this.SkillDialog.Clone(this.floatingSkillDialog.transform).GetComponentInChildren<Battle0171111Event>();
      dialog.transform.parent.gameObject.SetActive(false);
      this.showSkillDialog = (System.Action<GearGearSkill, bool>) ((skill, release) =>
      {
        dialog.setData(skill.skill);
        if (release)
          dialog.setSkillLv(skill.skill_level, skill.skill.upper_level);
        else
          dialog.setSkillLv(0, skill.skill.upper_level);
        dialog.Show();
      });
    }
    EventDelegate.Set(button.onClick, (EventDelegate.Callback) (() => this.showSkillDialog(skill_data, isRelease)));
  }

  protected void SetSkillDeteilEvent(GameCore.ItemInfo gear)
  {
    BattleSkillIcon[] battleSkillIconArray = this.WeaponSkillOne;
    UIButton[] uiButtonArray = this.WeaponSkillButtonOne;
    this.WeaponSkillOneRoot.SetActive(true);
    this.WeaponSkillTwoRoot.SetActive(false);
    GearGear gear1 = gear.gear;
    if (gear1.rememberSkills.Count <= 0)
      return;
    GearGearSkill[] skills = gear.skills;
    int num = gear1.rememberSkills.Count > skills.Length ? 1 : 0;
    bool flag = num == 0 && gear1.rememberSkills[0].Count > 0 && !gear1.rememberSkills[0].All<GearGearSkill>((Func<GearGearSkill, bool>) (x => x.isReleased(gear)));
    if (((num | (flag ? 1 : 0)) != 0 || gear1.skills.Length > 1) && (gear1.rememberSkills.Count > 1 || gear1.rememberSkills.Count == 1 && gear1.rememberSkills[0].Count > 1))
    {
      battleSkillIconArray = this.WeaponSkillTwo;
      uiButtonArray = this.WeaponSkillButtonTwo;
      this.WeaponSkillOneRoot.SetActive(false);
      this.WeaponSkillTwoRoot.SetActive(true);
    }
    if (num != 0)
    {
      for (int index = 0; index < gear1.rememberSkills.Count && battleSkillIconArray.Length > index; ++index)
      {
        BattleSkillIcon battleSkillIcon = battleSkillIconArray[index];
        if (gear1.rememberSkills[index][0].release_rank > gear.gearLevel)
          battleSkillIcon.EnableNeedRankIcon(gear1.rememberSkills[index][0].release_rank);
        else
          battleSkillIcon.EnableNeedRankIcon(0);
        this.StartCoroutine(battleSkillIcon.Init(gear1.rememberSkills[index][0].skill));
        this.SetSkillDialog(uiButtonArray[index], gear1.rememberSkills[index][0], gear1.rememberSkills[index][0].isReleased(gear));
      }
    }
    else if (flag)
    {
      for (int index = 0; index < gear1.rememberSkills[0].Count && battleSkillIconArray.Length > index; ++index)
      {
        BattleSkillIcon battleSkillIcon = battleSkillIconArray[index];
        if (gear1.rememberSkills[0][index].release_rank > gear.gearLevel)
          battleSkillIcon.EnableNeedRankIcon(gear1.rememberSkills[0][index].release_rank);
        else
          battleSkillIcon.EnableNeedRankIcon(0);
        this.StartCoroutine(battleSkillIcon.Init(gear1.rememberSkills[0][index].skill));
        this.SetSkillDialog(uiButtonArray[index], gear1.rememberSkills[0][index], gear1.rememberSkills[0][index].isReleased(gear));
      }
    }
    else
    {
      for (int index = 0; index < gear.skills.Length && battleSkillIconArray.Length > index; ++index)
      {
        this.StartCoroutine(battleSkillIconArray[index].Init(gear.skills[index].skill));
        this.SetSkillDialog(uiButtonArray[index], gear.skills[index], true);
      }
    }
    this.SkillArrowObject.SetActive(flag);
  }

  public void SetChangeScene(System.Action changeScene)
  {
    this.cahngeSceneCallback = changeScene;
  }

  public void changeScene()
  {
    if (this.cahngeSceneCallback != null)
      this.cahngeSceneCallback();
    else
      this.backScene();
  }

  public void choiceUnitChangeScene()
  {
    Unit00468Scene.changeScene004431(true, this.sendParam);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
