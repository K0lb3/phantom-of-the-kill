﻿// Decompiled with JetBrains decompiler
// Type: StartupMenuObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using AppSetup;
using UnityEngine;

public class StartupMenuObject : MonoBehaviour
{
  public GameObject menu;
  public GameObject CodeError;
  public GameObject CodeErrorFgGID;
  public GameObject InputBlock;
  public GameObject SameTerminal;
  public GameObject Unknown;
  public GameObject Success;
  public GameObject BackCollider;
  public Startup0008TopPageMenu userPolicy;
  public Startup00016Menu privacy;
  public Startup00017Menu data_load;
  public Startup00018Menu data_load_fggid;
  public Transfer01281Menu data_load_warning;
  public Transfer01282Menu data_load_select;
  public SetupFPSController PopupSelectFPS;
  public SetupSoundController PopupSelectSound;
  public PopupConfirm PopupConfirmFPSAndSound;
  public UILabel txtApplicationVersion;
  public UILabel txtScreenSize;
  public GameObject PGSSignInButton;
  public TopBackGroundAnimation backGroundAnim;
  public TopPressBtnAnimation btnAnim;
  public UILabel txtUserID;
  public UILabel txtVersion;
  public UIButton ibtnFggMissionLoading;
  public UIButton ibtnFggMissionConnected;
  public UIButton ibtnFggMissionDisconnected;
  public GameObject dirFggMission;
  public UIButton iconGoogle;
  public UIButton fggMissionClose;
  public UIButton ibtnSight1;
  public UIButton ibtnSight2;
  public UIButton ibtnSight3;
  public SpreadColorButton popup008Back;
  public SpreadColorButton popup008Next;
  public UIButton popup016Close;
  public UIButton popup0178OK;
  public SpreadColorButton popup018Decide;
  public SpreadColorButton popup018Back;
  public SpreadColorButton popup01281Back;
  public SpreadColorButton popup01281Next;
  public SpreadColorButton popup01282Back;
  public SpreadColorButton popup01282Fggid;
  public SpreadColorButton popup01282Code;
  public UIButton popup01288OK;
  public UIButton popup01289OK;
  public SpreadColorButton popup019Decide;
  public SpreadColorButton popup019Back;
  public UIButton popup012811OK;
  public UIButton popup012812OK;
  public UIButton popup012813OK;
  public UIButton buttonPressStart;
}
