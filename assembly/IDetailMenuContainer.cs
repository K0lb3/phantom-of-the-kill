﻿// Decompiled with JetBrains decompiler
// Type: IDetailMenuContainer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public interface IDetailMenuContainer
{
  GameObject detailPrefab { get; }

  GameObject gearKindIconPrefab { get; }

  GameObject gearIconPrefab { get; }

  GameObject skillDetailDialogPrefab { get; }

  GameObject specialPointDetailDialogPrefab { get; }

  GameObject profIconPrefab { get; }

  GameObject skillTypeIconPrefab { get; }

  GameObject skillfullnessIconPrefab { get; }

  GameObject commonElementIconPrefab { get; }

  GameObject spAtkTypeIconPrefab { get; }

  GameObject statusDetailPrefab { get; }

  GameObject skillListPrefab { get; }

  GameObject StatusDetailPrefab { get; }

  GameObject TrainingPrefa { get; }

  GameObject GroupDetailDialogPrefab { get; }

  GameObject detailJobAbilityPrefab { get; }

  GameObject terraiAbilityDialogPrefab { get; }

  GameObject unityDetailPrefab { get; }

  GameObject stageItemPrefab { get; }

  GameObject skillLockIconPrefab { get; }
}
