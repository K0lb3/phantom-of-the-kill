﻿// Decompiled with JetBrains decompiler
// Type: IResourceLoader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

internal interface IResourceLoader
{
  Future<Object> Load(string path, ref ResourceInfo.Resource context);

  Future<Object> DownloadOrCache(string path, ref ResourceInfo.Resource context);

  Object LoadImmediatelyForSmallObject(string path, ref ResourceInfo.Resource context);
}
