﻿// Decompiled with JetBrains decompiler
// Type: OverkillersUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections.Generic;
using System.Linq;

public static class OverkillersUtil
{
  public static bool checkDelete(PlayerUnit[] units)
  {
    for (int index = 0; index < units.Length; ++index)
    {
      if (!(units[index] == (PlayerUnit) null) && (units[index].isAnyOverkillersUnits || units[index].overkillers_base_id > 0))
        return false;
    }
    return true;
  }

  public static bool checkDelete(PlayerUnit unit)
  {
    return !unit.isAnyOverkillersUnits && unit.overkillers_base_id <= 0;
  }

  public static HashSet<int> checkCompletedDeck(
    PlayerUnit[] units,
    out bool bCompleted,
    HashSet<int> excludeIds = null,
    bool[] ignores = null)
  {
    bCompleted = true;
    if (ignores == null)
      ignores = Enumerable.Repeat<bool>(false, units.Length).ToArray<bool>();
    else if (units.Length > ignores.Length)
      ignores = ((IEnumerable<bool>) ignores).Concat<bool>(Enumerable.Repeat<bool>(false, units.Length - ignores.Length)).ToArray<bool>();
    if (excludeIds == null)
    {
      excludeIds = new HashSet<int>();
      for (int index1 = 0; index1 < units.Length; ++index1)
      {
        if (!ignores[index1])
        {
          PlayerUnit unit = units[index1];
          if (!(unit == (PlayerUnit) null) && unit.over_killers_player_unit_ids != null && unit.over_killers_player_unit_ids.Length != 0)
          {
            bool flag = true;
            for (int index2 = 0; index2 < unit.over_killers_player_unit_ids.Length && unit.over_killers_player_unit_ids[index2] >= 0; ++index2)
            {
              if (unit.over_killers_player_unit_ids[index2] > 0)
              {
                flag = false;
                excludeIds.Add(unit.over_killers_player_unit_ids[index2]);
              }
            }
            if (flag)
            {
              int overkillersBaseId = unit.overkillers_base_id;
              if (overkillersBaseId > 0)
                excludeIds.Add(overkillersBaseId);
            }
          }
        }
      }
    }
    for (int index = 0; index < units.Length; ++index)
    {
      if (!ignores[index] && units[index] != (PlayerUnit) null && excludeIds.Contains(units[index].id))
      {
        bCompleted = false;
        break;
      }
    }
    return excludeIds;
  }

  public static List<int> createEquipedUnitIDList()
  {
    List<int> intList = new List<int>();
    ((IEnumerable<PlayerUnit>) SMManager.Get<PlayerUnit[]>()).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x.unit.IsNormalUnit)).ToArray<PlayerUnit>();
    foreach (PlayerUnit playerUnit in SMManager.Get<PlayerUnit[]>())
    {
      if (playerUnit.over_killers_player_unit_ids != null)
      {
        foreach (int killersPlayerUnitId in playerUnit.over_killers_player_unit_ids)
        {
          if (killersPlayerUnitId > 0)
          {
            intList.Add(killersPlayerUnitId);
            break;
          }
        }
      }
    }
    return intList;
  }
}
