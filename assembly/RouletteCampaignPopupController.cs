﻿// Decompiled with JetBrains decompiler
// Type: RouletteCampaignPopupController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class RouletteCampaignPopupController : MonoBehaviour
{
  private string campaignURL;

  public void Init(string campaignURL)
  {
    this.campaignURL = campaignURL;
  }

  public void OnTapApply()
  {
    Application.OpenURL(this.campaignURL);
  }

  public void OnTapReturn()
  {
    ModalWindow.ShowYesNo("確認", Consts.GetInstance().ROULETTE_CLOSE_CAMPAIGN_POPUP_CONTENT, (System.Action) (() =>
    {
      Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(false);
      Singleton<NGSceneManager>.GetInstance().backScene();
    }), (System.Action) (() => this.gameObject.SetActive(true)));
    this.gameObject.SetActive(false);
  }
}
