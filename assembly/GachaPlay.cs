﻿// Decompiled with JetBrains decompiler
// Type: GachaPlay
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GachaPlay : MonoBehaviour
{
  private string mError = string.Empty;
  private static GachaPlay Instance;

  public static GachaPlay GetInstance()
  {
    if ((UnityEngine.Object) GachaPlay.Instance == (UnityEngine.Object) null)
    {
      GameObject gameObject = GameObject.Find("Gacha Manager");
      if ((UnityEngine.Object) gameObject == (UnityEngine.Object) null)
      {
        gameObject = new GameObject("Gacha Manager");
        UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) gameObject);
      }
      GachaPlay.Instance = gameObject.GetComponent<GachaPlay>();
      if ((UnityEngine.Object) GachaPlay.Instance == (UnityEngine.Object) null)
        GachaPlay.Instance = gameObject.AddComponent<GachaPlay>();
    }
    return GachaPlay.Instance;
  }

  public bool isError
  {
    get
    {
      return !string.IsNullOrEmpty(this.mError);
    }
  }

  private void cleanup()
  {
    this.mError = string.Empty;
  }

  public IEnumerator ChargeGacha(
    string name,
    int num,
    int gacha_id,
    GachaType gacha_type,
    int payment_amount)
  {
    GachaPlay gachaPlay = this;
    gachaPlay.cleanup();
    IEnumerator e;
    if (num != 1)
    {
      e = gachaPlay.ChargeGachaMulti(name, gacha_id, gacha_type, payment_amount);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
    {
      switch (gacha_type)
      {
        case GachaType.sheet:
          // ISSUE: reference to a compiler-generated method
          Future<WebAPI.Response.GachaG007PanelPay> paramF1 = WebAPI.GachaChargePay<WebAPI.Response.GachaG007PanelPay>(name, num, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CChargeGacha\u003Eb__6_0));
          e = paramF1.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          WebAPI.Response.GachaG007PanelPay result_list1 = paramF1.Result;
          if (result_list1 == null)
          {
            yield break;
          }
          else
          {
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list1.player_units, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list1.player_unit_reserves, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list1.player_material_units, false, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            GachaResultData.GetInstance().SetData(result_list1);
            paramF1 = (Future<WebAPI.Response.GachaG007PanelPay>) null;
            result_list1 = (WebAPI.Response.GachaG007PanelPay) null;
            break;
          }
        case GachaType.retry:
          // ISSUE: reference to a compiler-generated method
          Future<WebAPI.Response.GachaG101RetryGiftPay> paramF2 = WebAPI.GachaChargePay<WebAPI.Response.GachaG101RetryGiftPay>(name, num, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CChargeGacha\u003Eb__6_1));
          e = paramF2.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          WebAPI.Response.GachaG101RetryGiftPay result_list2 = paramF2.Result;
          if (result_list2 == null)
          {
            yield break;
          }
          else
          {
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list2.temp_player_units, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list2.player_unit_reserves, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list2.player_material_units, false, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            GachaResultData.GetInstance().SetData(result_list2, name, gacha_id, num);
            paramF2 = (Future<WebAPI.Response.GachaG101RetryGiftPay>) null;
            result_list2 = (WebAPI.Response.GachaG101RetryGiftPay) null;
            break;
          }
        default:
          // ISSUE: reference to a compiler-generated method
          Future<WebAPI.Response.GachaG001ChargePay> paramF3 = WebAPI.GachaChargePay<WebAPI.Response.GachaG001ChargePay>(name, num, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CChargeGacha\u003Eb__6_2));
          e = paramF3.Wait();
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          WebAPI.Response.GachaG001ChargePay result_list3 = paramF3.Result;
          if (result_list3 == null)
          {
            yield break;
          }
          else
          {
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list3.player_units, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list3.player_unit_reserves, false, (IEnumerable<string>) null, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list3.player_material_units, false, false);
            while (e.MoveNext())
              yield return e.Current;
            e = (IEnumerator) null;
            GachaResultData.GetInstance().SetData(result_list3);
            paramF3 = (Future<WebAPI.Response.GachaG001ChargePay>) null;
            result_list3 = (WebAPI.Response.GachaG001ChargePay) null;
            break;
          }
      }
      EventTracker.TrackSpend("COIN", "COIN_GACHA_" + name + "_ID_" + (object) gacha_id, payment_amount);
      EventTracker.TrackEvent("GACHA", "COIN_GACHA", name + "_ID_" + (object) gacha_id, 1);
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    }
  }

  public IEnumerator ChargeGachaMulti(
    string name,
    int gacha_id,
    GachaType gacha_type,
    int payment_amount)
  {
    GachaPlay gachaPlay = this;
    gachaPlay.cleanup();
    IEnumerator e;
    switch (gacha_type)
    {
      case GachaType.sheet:
        // ISSUE: reference to a compiler-generated method
        Future<WebAPI.Response.GachaG007PanelMultiPay> paramF1 = WebAPI.GachaChargeMultiPay<WebAPI.Response.GachaG007PanelMultiPay>(name, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CChargeGachaMulti\u003Eb__7_0));
        e = paramF1.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        WebAPI.Response.GachaG007PanelMultiPay result_list1 = paramF1.Result;
        if (result_list1 == null)
        {
          yield break;
        }
        else
        {
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list1.player_units, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list1.player_unit_reserves, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list1.player_material_units, false, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          GachaResultData.GetInstance().SetData(result_list1);
          paramF1 = (Future<WebAPI.Response.GachaG007PanelMultiPay>) null;
          result_list1 = (WebAPI.Response.GachaG007PanelMultiPay) null;
          break;
        }
      case GachaType.retry:
        // ISSUE: reference to a compiler-generated method
        Future<WebAPI.Response.GachaG101RetryGiftMultiPay> paramF2 = WebAPI.GachaChargeMultiPay<WebAPI.Response.GachaG101RetryGiftMultiPay>(name, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CChargeGachaMulti\u003Eb__7_1));
        e = paramF2.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        WebAPI.Response.GachaG101RetryGiftMultiPay result_list2 = paramF2.Result;
        if (result_list2 == null)
        {
          yield break;
        }
        else
        {
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list2.temp_player_units, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list2.player_unit_reserves, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list2.player_material_units, false, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          GachaResultData.GetInstance().SetData(result_list2, name, gacha_id, payment_amount);
          paramF2 = (Future<WebAPI.Response.GachaG101RetryGiftMultiPay>) null;
          result_list2 = (WebAPI.Response.GachaG101RetryGiftMultiPay) null;
          break;
        }
      default:
        // ISSUE: reference to a compiler-generated method
        Future<WebAPI.Response.GachaG001ChargeMultiPay> paramF3 = WebAPI.GachaChargeMultiPay<WebAPI.Response.GachaG001ChargeMultiPay>(name, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CChargeGachaMulti\u003Eb__7_2));
        e = paramF3.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        WebAPI.Response.GachaG001ChargeMultiPay result_list3 = paramF3.Result;
        if (result_list3 == null)
        {
          yield break;
        }
        else
        {
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list3.player_units, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list3.player_unit_reserves, false, (IEnumerable<string>) null, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list3.player_material_units, false, false);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          GachaResultData.GetInstance().SetData(result_list3);
          paramF3 = (Future<WebAPI.Response.GachaG001ChargeMultiPay>) null;
          result_list3 = (WebAPI.Response.GachaG001ChargeMultiPay) null;
          break;
        }
    }
    EventTracker.TrackSpend("COIN", "COIN_GACHA_" + name + "_ID_" + (object) gacha_id, payment_amount);
    EventTracker.TrackEvent("GACHA", "COIN_GACHA", name + "_ID_" + (object) gacha_id, 1);
    Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
  }

  public IEnumerator ChargeGachaTutorial()
  {
    this.cleanup();
    IEnumerator e = Singleton<TutorialRoot>.GetInstance().SetTutorialGachaData(false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator FriendGacha(string name, int num, int gacha_id)
  {
    GachaPlay gachaPlay = this;
    gachaPlay.cleanup();
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.GachaG002FriendpointPay> paramF = WebAPI.GachaFriendPointPay(name, num, gacha_id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CFriendGacha\u003Eb__9_0));
    IEnumerator e = paramF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    WebAPI.Response.GachaG002FriendpointPay result_list = paramF.Result;
    if (result_list != null)
    {
      e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list.player_units, false, (IEnumerable<string>) null, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list.player_material_units, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      EventTracker.TrackSpend("POINT", "POINT_GACHA_" + name + "_ID_" + (object) gacha_id, num * 200);
      EventTracker.TrackEvent("GACHA", "POINT_GACHA", name + "_ID_" + (object) gacha_id, num);
      GachaResultData.GetInstance().SetData(result_list);
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    }
  }

  public IEnumerator TicketGacha(
    string name,
    int num,
    GachaModuleGacha gachaData,
    GameObject popupPrefab)
  {
    GachaPlay gachaPlay = this;
    gachaPlay.cleanup();
    // ISSUE: reference to a compiler-generated method
    Future<WebAPI.Response.GachaG004TicketPay> paramF = WebAPI.GachaTicketPay<WebAPI.Response.GachaG004TicketPay>(name, num, gachaData.id, new System.Action<WebAPI.Response.UserError>(gachaPlay.\u003CTicketGacha\u003Eb__10_0));
    IEnumerator e = paramF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    WebAPI.Response.GachaG004TicketPay result_list = paramF.Result;
    if (result_list != null)
    {
      e = OnDemandDownload.WaitLoadUnitResource((IEnumerable<PlayerUnit>) result_list.player_units, false, (IEnumerable<string>) null, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      e = OnDemandDownload.WaitLoadMaterialUnitResource((IEnumerable<PlayerMaterialUnit>) result_list.player_material_units, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      EventTracker.TrackSpend("TICKET", "TICKET_GACHA_ID_" + (object) gachaData.id, num * gachaData.payment_amount);
      EventTracker.TrackEvent("GACHA", "TICKET_GACHA", "ID_" + (object) gachaData.id, num);
      GachaResultData instance = GachaResultData.GetInstance();
      GachaResultData.ResultData.GachaTicketData gachaTicketData1 = new GachaResultData.ResultData.GachaTicketData(gachaData, name, popupPrefab);
      WebAPI.Response.GachaG004TicketPay data = result_list;
      GachaResultData.ResultData.GachaTicketData gachaTicketData2 = gachaTicketData1;
      instance.SetData(data, gachaTicketData2);
      Singleton<NGGameDataManager>.GetInstance().refreshHomeHome = true;
    }
  }

  private IEnumerator openWebErrorPopup(WebAPI.Response.UserError error)
  {
    yield return (object) PopupCommon.Show(error.Code, error.Reason, (System.Action) (() => Singleton<NGSceneManager>.GetInstance().changeScene("gacha006_3", false, (object[]) Array.Empty<object>())));
  }
}
