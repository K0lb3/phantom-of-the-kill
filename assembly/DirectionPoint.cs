﻿// Decompiled with JetBrains decompiler
// Type: DirectionPoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class DirectionPoint
{
  public Vector3 Point;
  public Vector3 Direction;

  public DirectionPoint(Vector3 direction, Vector3 point)
  {
    this.Direction = direction;
    this.Point = point;
  }

  public static DirectionPoint Zero()
  {
    return new DirectionPoint(Vector3.zero, Vector3.zero);
  }
}
