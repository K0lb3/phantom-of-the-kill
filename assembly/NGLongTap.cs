﻿// Decompiled with JetBrains decompiler
// Type: NGLongTap
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections;
using UnityEngine;

[RequireComponent(typeof (UIEventTrigger))]
public class NGLongTap : MonoBehaviour
{
  public float LongTapSeconds = 3f;
  public string MethodName = string.Empty;
  public MonoBehaviour Target;

  private void Start()
  {
    this.GetComponent<UIEventTrigger>().onPress.Add(new EventDelegate(new EventDelegate.Callback(this.tapStart)));
    this.GetComponent<UIEventTrigger>().onRelease.Add(new EventDelegate(new EventDelegate.Callback(this.tapEnd)));
  }

  private void tapStart()
  {
    Debug.Log((object) "tap start");
    this.StartCoroutine("checkLongTap");
  }

  private void tapEnd()
  {
    Debug.Log((object) "tap end");
    this.StopCoroutine("checkLongTap");
  }

  private IEnumerator checkLongTap()
  {
    yield return (object) new WaitForSeconds(this.LongTapSeconds);
    if ((Object) this.Target == (Object) null)
      Debug.Log((object) "no set long tap Target");
    else if (this.MethodName == string.Empty)
      Debug.Log((object) "no set long tap MethodName");
    else
      this.Target.SendMessage(this.MethodName);
  }
}
