﻿// Decompiled with JetBrains decompiler
// Type: Shop00721Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Shop00721Menu : BackButtonMenuBase
{
  [SerializeField]
  private NGxScroll scroll;
  [SerializeField]
  private GameObject slcKisekiBonus;
  [SerializeField]
  private UILabel txtOwnnumber;
  [SerializeField]
  private UILabel txtOwnnumberCharged;
  private int m_currentShopIdx;
  private GameObject productBannerPrefab;

  public int CurrentShopIDX
  {
    get
    {
      return this.m_currentShopIdx;
    }
    set
    {
      this.m_currentShopIdx = value;
    }
  }

  private void Start()
  {
  }

  protected override void Update()
  {
    base.Update();
    this.UpdateKisekiBonus();
  }

  private void UpdateKisekiBonus()
  {
    Modified<CoinBonus[]> modified = SMManager.Observe<CoinBonus[]>();
    if (modified == null || !modified.IsChangedOnce())
      return;
    this.slcKisekiBonus.SetActive((uint) modified.Value.Length > 0U);
  }

  public IEnumerator Init()
  {
    Shop00721Menu menu = this;
    IEnumerator e = ServerTime.WaitSync();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    DateTime serverTime = ServerTime.NowAppTime();
    menu.UpdateKisekiBonus();
    Player player = SMManager.Get<Player>();
    if (player != null)
    {
      menu.txtOwnnumber.SetTextLocalize(player.coin);
      menu.txtOwnnumberCharged.SetTextLocalize(player.paid_coin);
    }
    if ((UnityEngine.Object) menu.productBannerPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> prefabF = Res.Prefabs.shop007_21.dir_SpecialShop_ProductBanner.Load<GameObject>();
      e = prefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      menu.productBannerPrefab = prefabF.Result;
      prefabF = (Future<GameObject>) null;
    }
    Shop[] shopArray = SMManager.Get<Shop[]>();
    menu.scroll.Clear();
    if (shopArray != null)
    {
      Shop shop = ((IEnumerable<Shop>) shopArray).FirstOrDefault<Shop>((Func<Shop, bool>) (x => x.id == 5000));
      int len = shop.articles.Length;
      if (menu.CurrentShopIDX >= len)
        menu.CurrentShopIDX = len - 1;
      for (int i = 0; i < len; ++i)
      {
        PlayerShopArticle article = shop.articles[i];
        GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(menu.productBannerPrefab);
        menu.scroll.Add(gameObject, false);
        e = gameObject.GetComponent<Shop00721SpecialShopProduction>().Init(serverTime, shop.id, article, menu, i);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
      shop = (Shop) null;
    }
    menu.scroll.ResolvePosition(menu.CurrentShopIDX);
  }

  public virtual void Foreground()
  {
  }

  public void IbtnBuyKiseki()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility.BuyKiseki(false));
  }

  public void IbtnFonds()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_19());
  }

  public void IbtnSpecific()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(PopupUtility._007_18());
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    this.CurrentShopIDX = 0;
    Shop0071Scene.changeScene(false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void VScrollBar()
  {
  }
}
