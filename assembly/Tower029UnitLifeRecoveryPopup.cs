﻿// Decompiled with JetBrains decompiler
// Type: Tower029UnitLifeRecoveryPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Tower029UnitLifeRecoveryPopup : BackButtonMonoBehaiviour
{
  [SerializeField]
  private UILabel lblTitle;
  [SerializeField]
  private UILabel lblDesc;
  [SerializeField]
  private UILabel lblDesc2;
  [SerializeField]
  private UILabel lblPosession;
  [SerializeField]
  private UILabel lblPossesionValue;
  private GameObject unitSelectionPopup;
  private System.Action<TowerUtil.UnitSelectionMode, TowerUtil.SequenceType> actionUnitSelection;
  private TowerProgress progress;
  private int requiredCoin;
  private bool isNoEntryUnit;

  public void Initialize(
    GameObject popup,
    TowerProgress progress,
    System.Action<TowerUtil.UnitSelectionMode, TowerUtil.SequenceType> action,
    int coin,
    bool noEntryUnit)
  {
    if ((UnityEngine.Object) this.GetComponent<UIWidget>() != (UnityEngine.Object) null)
      this.GetComponent<UIWidget>().alpha = 0.0f;
    this.progress = progress;
    this.requiredCoin = coin;
    TowerUtil.PayRecoveryCoinNum = this.requiredCoin;
    this.unitSelectionPopup = popup;
    this.actionUnitSelection = action;
    this.isNoEntryUnit = noEntryUnit;
    this.lblTitle.SetTextLocalize(Consts.GetInstance().POPUP_TOWER_UNIT_RECOVERY_STONE_TITLE);
    this.lblDesc.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_TOWER_UNIT_RECOVERY_STONE_DESC, (IDictionary) new Hashtable()
    {
      {
        (object) nameof (coin),
        (object) coin
      }
    }));
    this.lblDesc2.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_TOWER_UNIT_RECOVERY_STONE_DESC2, (IDictionary) new Hashtable()
    {
      {
        (object) "level",
        (object) TowerUtil.BorderLevel
      },
      {
        (object) "num",
        (object) TowerUtil.MaxUnitNum
      }
    }));
    this.lblPosession.SetTextLocalize(Consts.GetInstance().POPUP_TOWER_UNIT_RECOVERY_STONE_POSESSION);
    this.lblPossesionValue.SetTextLocalize(Player.Current.coin);
  }

  public void onYesButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    if (Player.Current.coin < this.requiredCoin)
    {
      Singleton<PopupManager>.GetInstance().closeAll(false);
      ModalWindow.Show(Consts.GetInstance().TOWER_MODAL_SHORTAGE_COIN_RECOVERY_TITLE, Consts.Format(Consts.GetInstance().TOWER_MODAL_SHORTAGE_COIN_RECOVERY_DESC, (IDictionary) new Hashtable()
      {
        {
          (object) "coin",
          (object) TowerUtil.RecoveryCoinNum
        }
      }), (System.Action) (() => {}));
    }
    else
    {
      GameObject prefab = this.unitSelectionPopup.Clone((Transform) null);
      prefab.SetActive(false);
      if (this.isNoEntryUnit)
        prefab.GetComponent<Tower029UnitSelectionStartPopup>().Initialize(this.actionUnitSelection, (System.Action) null, TowerUtil.SequenceType.Recovery);
      else
        prefab.GetComponent<Tower029UnitSelectionRecoveryPopup>().Initialize(this.progress, this.actionUnitSelection, TowerUtil.SequenceType.Recovery);
      prefab.SetActive(true);
      Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    }
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }
}
