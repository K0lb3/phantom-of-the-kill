﻿// Decompiled with JetBrains decompiler
// Type: Util.IOSUtil
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace Util
{
  public static class IOSUtil
  {
    private static bool? isDeviceGenerationiPhoneX;
    private static bool? isDeviceGenerationiPad;
    private static Rect? safeArea;

    public static bool IsDeviceGenerationiPhoneX
    {
      get
      {
        if (IOSUtil.isDeviceGenerationiPhoneX.HasValue)
          return IOSUtil.isDeviceGenerationiPhoneX.Value;
        IOSUtil.isDeviceGenerationiPhoneX = new bool?(UIRoot.IsSafeArea());
        return IOSUtil.isDeviceGenerationiPhoneX.Value;
      }
    }

    public static bool IsDeviceGenerationiPad
    {
      get
      {
        if (IOSUtil.isDeviceGenerationiPad.HasValue)
          return IOSUtil.isDeviceGenerationiPad.Value;
        IOSUtil.isDeviceGenerationiPad = new bool?((double) ((float) Screen.height / (float) Screen.width) < 1.3400000333786);
        return IOSUtil.isDeviceGenerationiPad.Value;
      }
    }

    public static Rect SafeArea
    {
      get
      {
        if (IOSUtil.safeArea.HasValue)
          return IOSUtil.safeArea.Value;
        IOSUtil.safeArea = new Rect?(Screen.safeArea);
        return IOSUtil.safeArea.Value;
      }
    }
  }
}
