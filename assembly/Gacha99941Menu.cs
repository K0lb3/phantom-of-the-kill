﻿// Decompiled with JetBrains decompiler
// Type: Gacha99941Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Gacha99941Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtDescription01;
  [SerializeField]
  protected UILabel TxtPopupTitle;

  public virtual void IbtnPopupBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }

  public void SetText()
  {
    this.TxtDescription01.SetText(Consts.Format(Consts.GetInstance().GACHA_99941MENU_DESCRIPTION01, (IDictionary) null));
    this.TxtPopupTitle.SetText(Consts.Format(Consts.GetInstance().GACHA_99941MENU_DESCRIPTION02, (IDictionary) null));
  }

  public override void onBackButton()
  {
    this.IbtnPopupBack();
  }
}
