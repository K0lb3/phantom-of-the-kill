﻿// Decompiled with JetBrains decompiler
// Type: Guide01142indicator1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using UnityEngine;

public class Guide01142indicator1 : Unit00443indicator
{
  [SerializeField]
  private GameObject NumPossession;
  [SerializeField]
  private UILabel txtUnit;

  public override void Init(GearGear gear)
  {
    this.SetParam(gear, (Judgement.GearParameter) null, 1, 5);
    this.SetSkillDeteilEvent(gear);
    this.SetWeaponAttack(gear);
    this.SetWeaponElement(gear);
    this.Weapon.Init(gear.kind_GearKind, CommonElement.none);
  }

  public override void SetUnit(int num)
  {
    if (!((Object) this.NumPossession != (Object) null))
      return;
    this.txtUnit.SetTextLocalize(num);
    this.NumPossession.SetActive(true);
  }
}
