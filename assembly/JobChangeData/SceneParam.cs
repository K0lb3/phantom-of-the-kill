﻿// Decompiled with JetBrains decompiler
// Type: JobChangeData.SceneParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace JobChangeData
{
  public class SceneParam
  {
    public int playerUnitId_ { get; private set; }

    public int? jobId_ { get; private set; }

    public Action onBackScene_ { get; private set; }

    public SceneParam(int playerUnitId, int? jobId = null, Action onBackScene = null)
    {
      this.playerUnitId_ = playerUnitId;
      this.jobId_ = jobId;
      this.onBackScene_ = onBackScene;
    }
  }
}
