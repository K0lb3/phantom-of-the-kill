﻿// Decompiled with JetBrains decompiler
// Type: Friend00895Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

public class Friend00895Menu : NGMenuBase
{
  private bool isPush;

  private IEnumerator BackSceneAsync()
  {
    Singleton<PopupManager>.GetInstance().closeAll(false);
    Singleton<NGSceneManager>.GetInstance().changeScene("friend008_5", false, (object[]) Array.Empty<object>());
    Singleton<NGSceneManager>.GetInstance().destroyScene("friend008_19");
    yield break;
  }

  public virtual void IbtnOk()
  {
    if (this.isPush)
      return;
    this.isPush = true;
    this.StartCoroutine(this.BackSceneAsync());
  }
}
