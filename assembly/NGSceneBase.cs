﻿// Decompiled with JetBrains decompiler
// Type: NGSceneBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class NGSceneBase : MonoBehaviour
{
  public bool isActiveHeader = true;
  public CommonRoot.HeaderType headerType = CommonRoot.HeaderType.Keep;
  public bool isActiveFooter = true;
  public CommonRoot.FooterType footerType = CommonRoot.FooterType.Keep;
  public bool isActiveBackground = true;
  public NGSceneBase.GuildChatDisplayingStatus currentSceneGuildChatDisplayingStatus = NGSceneBase.GuildChatDisplayingStatus.Closed;
  [SerializeField]
  private bool isUseDLC = true;
  [SerializeField]
  private bool isBackScene = true;
  [SerializeField]
  [Tooltip("CommonFooter等汎用メニューからシーン遷移した後に戻って来たく無いシーンは外す")]
  private bool topGlobalBack = true;
  [SerializeField]
  protected string bgmName = "bgm001";
  public float bgmFadeDuration = 3f;
  public string sceneName;
  public bool isTweenFinished;
  [SerializeField]
  protected NGMenuBase menuBase;
  protected NGMenuBase[] menuBases;
  public NGSceneBase.LockLayout lockLayout;
  private int? revisionIsSea_;
  public bool continueBackground;
  public GameObject backgroundPrefab;
  public float tweenTimeoutTime;
  [SerializeField]
  protected string bgmFile;
  public bool isDontAutoDestroy;
  protected UITweener[] tweens;
  public bool isAlphaActive;

  public bool IsPush
  {
    get
    {
      bool flag = false;
      if ((UnityEngine.Object) this.menuBase != (UnityEngine.Object) null)
        flag |= this.menuBase.IsPush;
      if (this.menuBases != null)
        flag |= ((IEnumerable<NGMenuBase>) this.menuBases).Any<NGMenuBase>((Func<NGMenuBase, bool>) (x => x.IsPush));
      return flag;
    }
    set
    {
      if ((UnityEngine.Object) this.menuBase != (UnityEngine.Object) null)
        this.menuBase.IsPush = value;
      if (this.menuBases == null)
        return;
      for (int index = 0; index < this.menuBases.Length; ++index)
        this.menuBases[index].IsPush = value;
    }
  }

  public bool stackIsSea { get; set; }

  public int revisionIsSea
  {
    get
    {
      return !this.revisionIsSea_.HasValue ? 0 : this.revisionIsSea_.Value;
    }
    set
    {
      this.revisionIsSea_ = new int?(value);
    }
  }

  public bool IsModifiedIsSea
  {
    get
    {
      if (!this.revisionIsSea_.HasValue)
        return true;
      int revisionIsSea1 = Singleton<NGGameDataManager>.GetInstance().revisionIsSea;
      int? revisionIsSea2 = this.revisionIsSea_;
      int valueOrDefault = revisionIsSea2.GetValueOrDefault();
      return !(revisionIsSea1 == valueOrDefault & revisionIsSea2.HasValue);
    }
  }

  public bool checkIsUseDLC()
  {
    return this.isUseDLC;
  }

  public bool IsBackScene
  {
    get
    {
      return this.isBackScene;
    }
  }

  public bool IsTopGlobalBack
  {
    get
    {
      return this.topGlobalBack;
    }
  }

  public virtual bool needGarbageCollectionOnLoaded
  {
    get
    {
      return true;
    }
  }

  private void Awake()
  {
    Singleton<NGSceneManager>.GetInstance().onChangeSceneAwake(this);
  }

  public virtual IEnumerator Start()
  {
    yield break;
  }

  public void onTweenFinished()
  {
    this.isTweenFinished = true;
  }

  public void startTweens()
  {
    if (this.tweens == null)
      this.tweens = NGTween.findTweeners(this.gameObject, true);
    bool flag1 = NGTween.playTweens(this.tweens, NGTween.Kind.START_END, false);
    bool isTweensError = NGTween.isTweensError;
    bool flag2 = NGTween.playTweens(this.tweens, NGTween.Kind.START, false) | flag1;
    if (NGTween.isTweensError | isTweensError)
      this.tweens = (UITweener[]) null;
    this.isTweenFinished = !flag2;
    this.tweenTimeoutTime = 0.3f;
    if (this.isTweenFinished || this.tweens == null)
      return;
    foreach (UITweener tween in this.tweens)
    {
      int num1 = Mathf.Abs(tween.tweenGroup);
      if (tween.style == UITweener.Style.Once && tween.gameObject.activeInHierarchy && (num1 == 11 || num1 == 12))
      {
        float num2 = tween.duration + tween.delay;
        if ((double) this.tweenTimeoutTime < (double) num2)
          this.tweenTimeoutTime = num2;
      }
    }
  }

  public IEnumerator setupCommonRoot()
  {
    CommonRoot root = Singleton<CommonRoot>.GetInstance();
    if (this.isActiveBackground && !this.continueBackground)
    {
      IEnumerator e = root.setBackgroundAsync(this.backgroundPrefab);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    root.isActiveBackground = this.isActiveBackground;
    if (this.headerType != CommonRoot.HeaderType.Keep)
      root.headerType = this.headerType;
    if (this.footerType != CommonRoot.FooterType.Keep)
      root.footerType = this.footerType;
    root.isActiveHeader = this.isActiveHeader;
    root.isActiveFooter = this.isActiveFooter;
  }

  public IEnumerator PlayBGM()
  {
    if (!string.IsNullOrEmpty(this.bgmName))
    {
      NGSoundManager sm = Singleton<NGSoundManager>.GetInstance();
      if (!Singleton<NGGameDataManager>.GetInstance().IsEarth && this.bgmName.Equals("bgm001") && string.IsNullOrEmpty(this.bgmFile))
      {
        IEnumerator e = ServerTime.WaitSync();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        CommonMypageSetting commonMypageSetting = ((IEnumerable<CommonMypageSetting>) MasterData.CommonMypageSettingList).FirstOrDefault<CommonMypageSetting>((Func<CommonMypageSetting, bool>) (x =>
        {
          DateTime dateTime1 = ServerTime.NowAppTime();
          DateTime? nullable = x.start_at;
          if ((nullable.HasValue ? (dateTime1 >= nullable.GetValueOrDefault() ? 1 : 0) : 0) == 0)
            return false;
          DateTime dateTime2 = ServerTime.NowAppTime();
          nullable = x.end_at;
          return nullable.HasValue && dateTime2 < nullable.GetValueOrDefault();
        }));
        if (commonMypageSetting != null)
        {
          this.bgmFile = commonMypageSetting.bgm_file;
          this.bgmName = commonMypageSetting.bgm_name;
        }
      }
      if (string.IsNullOrEmpty(this.bgmFile))
        sm.playBGMWithCrossFade(this.bgmName, this.bgmFadeDuration, 0.1f);
      else
        sm.PlayBgmFile(this.bgmFile, this.bgmName, 0, 0.0f, this.bgmFadeDuration, this.bgmFadeDuration);
      sm = (NGSoundManager) null;
    }
  }

  public void endTweens()
  {
    if (this.tweens == null)
      this.tweens = NGTween.findTweeners(this.gameObject, true);
    bool flag1 = NGTween.playTweens(this.tweens, NGTween.Kind.START_END, true);
    bool isTweensError = NGTween.isTweensError;
    bool flag2 = NGTween.playTweens(this.tweens, NGTween.Kind.END, false) | flag1;
    if (NGTween.isTweensError | isTweensError)
      this.tweens = (UITweener[]) null;
    this.isTweenFinished = !flag2;
    this.tweenTimeoutTime = 0.3f;
    if (this.isTweenFinished || this.tweens == null)
      return;
    foreach (UITweener tween in this.tweens)
    {
      int num1 = Mathf.Abs(tween.tweenGroup);
      if (tween.style == UITweener.Style.Once && tween.gameObject.activeInHierarchy && (num1 == 11 || num1 == 13))
      {
        float num2 = tween.duration + tween.delay;
        if ((double) this.tweenTimeoutTime < (double) num2)
          this.tweenTimeoutTime = num2;
      }
    }
  }

  public virtual void onSceneInitialized()
  {
  }

  public virtual void onEndScene()
  {
  }

  public virtual List<string> createResourceLoadList()
  {
    return (List<string>) null;
  }

  public virtual IEnumerator onInitSceneAsync()
  {
    yield break;
  }

  public virtual IEnumerator onEndSceneAsync()
  {
    yield break;
  }

  public virtual IEnumerator onDestroySceneAsync()
  {
    yield break;
  }

  public new Coroutine StartCoroutine(IEnumerator e)
  {
    return Singleton<NGSceneManager>.GetInstance().StartCoroutine(e);
  }

  public void SetupGuildChatForNextScene()
  {
    GuildChatManager guildChatManager = Singleton<CommonRoot>.GetInstance().guildChatManager;
    switch (this.currentSceneGuildChatDisplayingStatus)
    {
      case NGSceneBase.GuildChatDisplayingStatus.Opened:
        if (guildChatManager.GetCurrentGuildChatStatus() == GuildChatManager.GuildChatStatus.NotOpened)
          guildChatManager.OpenGuildChat();
        else if (guildChatManager.GetGuildChatPaused())
          guildChatManager.ResumeAndShowGuildChat();
        guildChatManager.RefreshMessageItemIcon();
        if (Persist.guildSetting.Exists)
        {
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.postNewChat, false);
          Persist.guildSetting.Flush();
        }
        Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.chat, false);
        break;
      case NGSceneBase.GuildChatDisplayingStatus.Paused:
        if (guildChatManager.GetCurrentGuildChatStatus() == GuildChatManager.GuildChatStatus.NotOpened)
          break;
        guildChatManager.PauseAndHideGuildChat();
        break;
      case NGSceneBase.GuildChatDisplayingStatus.Closed:
        if (guildChatManager.GetCurrentGuildChatStatus() == GuildChatManager.GuildChatStatus.NotOpened)
          break;
        guildChatManager.CloseGuildChat();
        break;
      case NGSceneBase.GuildChatDisplayingStatus.Customized:
        Debug.LogError((object) "The status of guild chat is not initialized!!!");
        break;
    }
  }

  public void SetupGuildChatAfterEndScene()
  {
    GuildChatManager guildChatManager = Singleton<CommonRoot>.GetInstance().guildChatManager;
    if (guildChatManager.GetCurrentGuildChatStatus() != GuildChatManager.GuildChatStatus.DetailedView)
      return;
    guildChatManager.CloseDetailedChat();
  }

  public enum LockLayout
  {
    None,
    Heaven,
  }

  public enum GuildChatDisplayingStatus
  {
    Opened,
    Paused,
    Closed,
    NotChanged,
    Customized,
  }
}
