﻿// Decompiled with JetBrains decompiler
// Type: SimpleCache`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class SimpleCache<Key, Value> where Value : UnityEngine.Object
{
  private Dictionary<Key, WeakReference> cacheDic = new Dictionary<Key, WeakReference>();
  private Func<Key, Promise<Value>, IEnumerator> loader;

  public SimpleCache(
    Func<Key, Promise<Value>, IEnumerator> loader,
    Func<Value, long> getSize,
    long maxSize,
    System.Action<Key, Value> unload = null)
  {
    this.loader = loader;
  }

  private Value GetTarget(Key key)
  {
    return !this.cacheDic.ContainsKey(key) ? default (Value) : this.cacheDic[key].Target as Value;
  }

  private IEnumerator Run(Key key, Promise<Value> promise)
  {
    IEnumerator e = this.loader(key, promise);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Value target = this.GetTarget(key);
    if ((UnityEngine.Object) target != (UnityEngine.Object) null)
      promise.Result = target;
    else
      this.SetValue(key, promise.Result);
  }

  public void SetValue(Key key, Value value)
  {
    if (!this.cacheDic.ContainsKey(key))
      this.cacheDic.Add(key, new WeakReference((object) value));
    else
      this.cacheDic[key].Target = (object) value;
  }

  public Value TryGet(Key key)
  {
    return this.GetTarget(key);
  }

  public Future<Value> Get(Key key)
  {
    Value target = this.GetTarget(key);
    return (UnityEngine.Object) target == (UnityEngine.Object) null ? new Future<Value>((Func<Promise<Value>, IEnumerator>) (promise => this.Run(key, promise))) : Future.Single<Value>(target);
  }

  public void Clear()
  {
    HashSet<Key> source = new HashSet<Key>((IEnumerable<Key>) this.cacheDic.Keys);
    Dictionary<Key, WeakReference> dictionary = new Dictionary<Key, WeakReference>();
    foreach (KeyValuePair<Key, WeakReference> keyValuePair in this.cacheDic)
    {
      if (keyValuePair.Value.Target != null)
        dictionary.Add(keyValuePair.Key, keyValuePair.Value);
    }
    this.cacheDic = dictionary;
    source.ExceptWith((IEnumerable<Key>) this.cacheDic.Keys);
    if (source.Count == 0)
      return;
    Debug.Log((object) ("Removed keys are:\n" + source.Select<Key, string>((Func<Key, string>) (x => x.ToString())).Join("\n")));
  }

  private class Wrap
  {
    public Value value;

    public Wrap(Value value)
    {
      this.value = value;
    }
  }
}
