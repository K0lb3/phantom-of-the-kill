﻿// Decompiled with JetBrains decompiler
// Type: BattleUI05ScoreRewardPopupMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class BattleUI05ScoreRewardPopupMenu : ResultMenuBase
{
  private GameObject rewardPopupPrefab;
  private int currentCnt;
  private QuestScoreBattleFinishContext campaign;

  public override IEnumerator Init(BattleInfo info, BattleEnd result)
  {
    this.campaign = result.score_campaigns[0];
    this.currentCnt = 0;
    IEnumerator e = this.LoadResources();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private IEnumerator LoadResources()
  {
    if ((UnityEngine.Object) this.rewardPopupPrefab == (UnityEngine.Object) null)
    {
      Future<GameObject> rewardPopupPrefabF = Res.Prefabs.battle.dir_RankingEvent_QuestPtTotalReward.Load<GameObject>();
      IEnumerator e = rewardPopupPrefabF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.rewardPopupPrefab = rewardPopupPrefabF.Result;
      rewardPopupPrefabF = (Future<GameObject>) null;
    }
  }

  public override IEnumerator Run()
  {
    BattleUI05ScoreRewardPopupMenu scoreRewardPopupMenu = this;
    BattleUI05ScoreRewardPopup script = Singleton<PopupManager>.GetInstance().open(scoreRewardPopupMenu.rewardPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<BattleUI05ScoreRewardPopup>();
    IEnumerator e = script.Init(scoreRewardPopupMenu.campaign.score_achivement_rewards[scoreRewardPopupMenu.currentCnt]);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    ++scoreRewardPopupMenu.currentCnt;
    bool toNext = false;
    script.SetTapCallBack((System.Action) (() =>
    {
      if (this.IsPush)
        return;
      toNext = true;
    }));
    while (!toNext)
      yield return (object) null;
    Singleton<PopupManager>.GetInstance().onDismiss();
    if (scoreRewardPopupMenu.currentCnt < scoreRewardPopupMenu.campaign.score_achivement_rewards.Length)
    {
      e = scoreRewardPopupMenu.Run();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  public override IEnumerator OnFinish()
  {
    yield break;
  }
}
