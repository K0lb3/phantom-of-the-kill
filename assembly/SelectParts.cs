﻿// Decompiled with JetBrains decompiler
// Type: SelectParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SelectParts : MonoBehaviour
{
  [SerializeField]
  protected int value = -1;
  public NGTweenParts[] objects;

  public int setValueNonTween(int v)
  {
    for (int index = 0; index < this.objects.Length; ++index)
      this.objects[index].resetActive(index == v);
    return this.value = v;
  }

  public int setValue(int v)
  {
    for (int index = 0; index < this.objects.Length; ++index)
      this.objects[index].isActive = index == v;
    return this.value = v;
  }

  public int setFirstValue(int v)
  {
    for (int index = 0; index < this.objects.Length; ++index)
    {
      if (index == v)
        this.objects[index].isActive = true;
      else
        this.objects[index].resetActive(false);
    }
    return this.value = v;
  }

  public int getValue()
  {
    return this.value;
  }

  public int inclement()
  {
    int v = this.value + 1;
    if (v >= this.objects.Length)
      v = this.objects.Length - 1;
    return this.setValue(v);
  }

  public int inclementLoop()
  {
    return this.setValue((this.value + 1) % this.objects.Length);
  }

  public int inclementLoopNonTween()
  {
    return this.setValueNonTween((this.value + 1) % this.objects.Length);
  }

  public int decrement()
  {
    int v = this.value - 1;
    if (v < 0)
      v = 0;
    return this.setValue(v);
  }

  public int decrementLoop()
  {
    int v = this.value - 1;
    if (v < 0)
      v = this.objects.Length - 1;
    return this.setValue(v);
  }
}
