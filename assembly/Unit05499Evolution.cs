﻿// Decompiled with JetBrains decompiler
// Type: Unit05499Evolution
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;

public class Unit05499Evolution : Unit00499Evolution
{
  protected override IEnumerator CreateIndicator(
    int evolutionPatternId,
    PlayerUnit[] playerUnits,
    PlayerMaterialUnit[] playerMaterialUnits)
  {
    Unit05499Evolution unit05499Evolution = this;
    Unit00499EvolutionIndicator component = unit05499Evolution.indicator.instantiateParts(unit05499Evolution.indicatorPrefab, true).GetComponent<Unit00499EvolutionIndicator>();
    component.TxtZeny.gameObject.SetActive(false);
    component.TxtZenyNeed.gameObject.SetActive(false);
    unit05499Evolution.linkEvolutionUnitsDict[evolutionPatternId] = component.linkEvolutionUnits;
    IEnumerator e = unit05499Evolution.menu.InitEvolutionUnits(playerUnits, playerMaterialUnits, unit05499Evolution.unitIconPrefab, component, evolutionPatternId, unit05499Evolution.evolutionMaterialDict[evolutionPatternId], true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
