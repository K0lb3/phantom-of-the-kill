﻿// Decompiled with JetBrains decompiler
// Type: UnitResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using UniLinq;
using UnityEngine;

public class UnitResult : MonoBehaviour
{
  [SerializeField]
  private GameObject linkCharacter;
  [Header("上限突破")]
  [SerializeField]
  private UnitResultLimitBreak[] limitBreaks;
  [Header("共鳴率・好感度")]
  [SerializeField]
  private GameObject slcDearDegreeBase;
  [SerializeField]
  private UISprite slcTextDearDegree;
  [SerializeField]
  private NGxBlinkEx dirDearDegree;
  [SerializeField]
  private UILabel txtDearDegree;
  [SerializeField]
  private UILabel txtDearDegreeUpAmount;
  [SerializeField]
  private GameObject txtDearDegreeNone;
  [Header("淘汰値")]
  [SerializeField]
  private NGxBlinkEx dirToutaBlink;
  [SerializeField]
  private UILabel txtToutaValue;
  [SerializeField]
  private UILabel txtToutaUpAmount;

  public IEnumerator Init(
    GameObject normalPrefab,
    Unit004832Menu.ResultPlayerUnit resultPlayerUnit)
  {
    UnitIcon linkCharacterScript = normalPrefab.CloneAndGetComponent<UnitIcon>(this.linkCharacter.transform);
    IEnumerator e = linkCharacterScript.setSimpleUnit(resultPlayerUnit.afterPlayerUnit);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    linkCharacterScript.BottomModeValue = UnitIconBase.BottomMode.Level;
    linkCharacterScript.setLevelText(resultPlayerUnit.afterPlayerUnit.level.ToString());
    linkCharacterScript.buttonBoxCollider.enabled = false;
    for (int index = 0; index < this.limitBreaks.Length; ++index)
    {
      if (index + 1 <= resultPlayerUnit.afterPlayerUnit.breakthrough_count)
        this.limitBreaks[index].OnOff(true);
      else
        this.limitBreaks[index].OnOff(false);
    }
    if (resultPlayerUnit.afterPlayerUnit.is_trust)
    {
      this.slcDearDegreeBase.SetActive(true);
      this.txtDearDegreeNone.SetActive(false);
      this.slcTextDearDegree.spriteName = !resultPlayerUnit.afterPlayerUnit.unit.IsSea ? (!resultPlayerUnit.afterPlayerUnit.unit.IsResonanceUnit ? "slc_txt_Relevance.png__GUI__023-4-6_sozai__023-4-6_sozai_prefab" : "slc_txt_Relevance.png__GUI__023-4-6_sozai__023-4-6_sozai_prefab") : "slc_text_Favorability.png__GUI__023-4-6_sozai__023-4-6_sozai_prefab";
      UISpriteData atlasSprite = this.slcTextDearDegree.GetAtlasSprite();
      this.slcTextDearDegree.width = atlasSprite.width;
      this.slcTextDearDegree.height = atlasSprite.height;
      this.dirDearDegree.SetChildren(this.dirDearDegree.transform.GetChildren().Select<Transform, GameObject>((Func<Transform, GameObject>) (x => x.gameObject)).ToArray<GameObject>());
      this.dirDearDegree.enabled = true;
      this.txtDearDegree.text = string.Format("{0}%", (object) (Math.Round((double) resultPlayerUnit.afterPlayerUnit.trust_rate * 100.0) / 100.0));
      this.txtDearDegreeUpAmount.text = string.Format("+{0}%", (object) (Math.Round((double) (resultPlayerUnit.afterPlayerUnit.trust_rate - resultPlayerUnit.beforePlayerUnit.trust_rate) * 100.0) / 100.0));
    }
    else
    {
      this.slcDearDegreeBase.SetActive(false);
      this.txtDearDegreeNone.SetActive(true);
    }
    this.dirToutaBlink.SetChildren(new GameObject[2]
    {
      this.txtToutaValue.gameObject,
      this.txtToutaUpAmount.gameObject
    });
    this.dirToutaBlink.enabled = true;
    this.txtToutaValue.text = (double) resultPlayerUnit.afterPlayerUnit.unityTotal >= 99.0 ? resultPlayerUnit.afterPlayerUnit.unityTotal.ToString() : resultPlayerUnit.afterPlayerUnit.unityTotal.ToString("f1");
    this.txtToutaUpAmount.text = string.Format("+{0}", (object) ((Decimal) resultPlayerUnit.afterPlayerUnit.unityTotal - (Decimal) resultPlayerUnit.beforePlayerUnit.unityTotal));
  }
}
