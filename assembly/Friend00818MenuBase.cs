﻿// Decompiled with JetBrains decompiler
// Type: Friend00818MenuBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Friend00818MenuBase : NGMenuBase
{
  [SerializeField]
  protected UILabel InpMesseage;
  [SerializeField]
  protected UILabel TxtFriendmessage;
  [SerializeField]
  protected UILabel TxtListdescription01;
  [SerializeField]
  protected UILabel TxtListdescription02;
  [SerializeField]
  protected UILabel TxtListdescription04;
  [SerializeField]
  protected UILabel TxtTitle;

  protected virtual void IbtnBack()
  {
    Debug.Log((object) "click default event IbtnBack");
  }

  protected virtual void IbtnCancel()
  {
    Debug.Log((object) "click default event IbtnCancel");
  }

  protected virtual void IbtnTransmission()
  {
    Debug.Log((object) "click default event IbtnTransmission");
  }
}
