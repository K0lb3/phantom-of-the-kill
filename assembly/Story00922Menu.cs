﻿// Decompiled with JetBrains decompiler
// Type: Story00922Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Story00922Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  private NGxScroll ScrollContainer;
  private PlayerStoryQuestS quest;
  private int XL;

  public virtual void Foreground()
  {
  }

  public virtual void VScrollBar()
  {
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_2", false, (object) this.quest, (object) this.XL);
  }

  public IEnumerator InitEpisodeButton(
    PlayerStoryQuestS[] quests,
    PlayerStoryQuestS quest,
    int XL)
  {
    this.quest = quest;
    this.XL = XL;
    Array.Reverse((Array) quests);
    Future<GameObject> prefabScrollPartsF = Res.Prefabs.story009_2_2.story009_2_2_button.Load<GameObject>();
    IEnumerator e = prefabScrollPartsF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefabScrollParts = prefabScrollPartsF.Result;
    ((IEnumerable<PlayerStoryQuestS>) quests).OrderBy<PlayerStoryQuestS, int>((Func<PlayerStoryQuestS, int>) (x => x.quest_story_s.ID)).ForEach<PlayerStoryQuestS>((System.Action<PlayerStoryQuestS>) (q =>
    {
      if (!q.is_clear || q.quest_story_s.StoryDetails().Length == 0)
        return;
      foreach (StoryPlaybackStoryDetail storyDetail in q.quest_story_s.StoryDetails())
      {
        Story00922ScrollParts component = prefabScrollParts.CloneAndGetComponent<Story00922ScrollParts>((GameObject) null);
        this.ScrollContainer.Add(component.gameObject, false);
        component.Init(this, storyDetail);
      }
    }));
    this.ScrollContainer.ResolvePosition();
  }
}
