﻿// Decompiled with JetBrains decompiler
// Type: PressSpriteChangeButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class PressSpriteChangeButton : UIButton
{
  [SerializeField]
  public UnityEngine.Sprite idle;
  [SerializeField]
  public UnityEngine.Sprite press;

  protected override void OnPress(bool isPressed)
  {
    base.OnPress(isPressed);
    if (!this.enabled)
      return;
    UI2DSprite component = this.tweenTarget.GetComponent<UI2DSprite>();
    if (isPressed)
    {
      component.sprite2D = this.press;
    }
    else
    {
      component.sprite2D = this.idle;
      Singleton<NGSoundManager>.GetInstance().PlaySe("SE_1002", false, 0.0f, -1);
    }
  }
}
