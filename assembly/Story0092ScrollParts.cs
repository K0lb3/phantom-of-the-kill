﻿// Decompiled with JetBrains decompiler
// Type: Story0092ScrollParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using UnityEngine;

public class Story0092ScrollParts : MonoBehaviour
{
  [SerializeField]
  private UISprite IbtnChapterSprite;
  [SerializeField]
  private UIButton IbtnChapter;
  [SerializeField]
  private UISprite SlcNew;
  [SerializeField]
  private UILabel TxtChapter;
  private PlayerStoryQuestS quest;
  private NGMenuBase menu;
  private int XL;

  public void onClickChapterButton()
  {
    if (this.menu.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
    Singleton<NGSceneManager>.GetInstance().changeScene("story009_2_2", false, (object) this.quest, (object) this.XL);
  }

  public void Init(
    Story0092Menu menu,
    PlayerStoryQuestS quest,
    PlayerStoryQuestS backQuest,
    int XL)
  {
    this.quest = quest;
    this.menu = (NGMenuBase) menu;
    this.XL = XL;
    EventDelegate.Add(this.IbtnChapter.onClick, new EventDelegate.Callback(this.onClickChapterButton));
    this.SlcNew.gameObject.SetActive(false);
    this.TxtChapter.SetTextLocalize(quest.quest_story_s.quest_l.short_name + " " + quest.quest_story_s.quest_m.short_name + " " + quest.quest_story_s.quest_m.name);
  }
}
