﻿// Decompiled with JetBrains decompiler
// Type: Guild0281Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Guild0281Menu : BackButtonMenuBase
{
  public static readonly int GUILDTOP_TO_HQTOP = 2813;
  public static readonly int HQTOP_TO_GUILDTOP = 2831;
  public static readonly int GUILDTOP_TO_OTHER = 28113;
  public static readonly int OTHER_TO_GUILDTOP = 28112;
  public static readonly int HQTOP_TO_OTHER = 28313;
  public static readonly int OTHER_TO_HQTOP = 28312;
  public static readonly int IBTN_TWEENGROUP = 0;
  public Guild0281Menu.SceneType sceneType = Guild0281Menu.SceneType.NONE;
  private UITweener[] tweens;
  public const int GuildTopAPI = -1;
  private WebAPI.Response.GuildTop resGuildTopData;
  [SerializeField]
  private Transform guildBasePos;
  private GameObject guildBasePrefab;
  private Guild0282GuildBase guildBase;
  [SerializeField]
  private UI2DSprite guildTitleImage;
  private Future<UnityEngine.Sprite> futureGuildTitleImage;
  [SerializeField]
  private UILabel guildName;
  [SerializeField]
  private UISprite guildRank1;
  [SerializeField]
  private UISprite guildRank10;
  [SerializeField]
  private UILabel nextExp;
  [SerializeField]
  private NGTweenGaugeScale expGauge;
  [SerializeField]
  private UILabel currentMember;
  [SerializeField]
  private UILabel maxMember;
  [SerializeField]
  private UILabel ranking;
  [SerializeField]
  private UILabel battleCount;
  [SerializeField]
  private UILabel win;
  [SerializeField]
  private UILabel role;
  private GuildRegistration guild;
  [SerializeField]
  private GameObject newGift;
  [SerializeField]
  private GameObject newMenu;
  [SerializeField]
  private GameObject bankBadge;
  private GuildInfoPopup guildInfoPopup;
  private GuildMemberObject guildMemberPopup;
  private UI3DModel UIModel;
  [SerializeField]
  private GameObject Model;
  public CircularMotionPositionSet CirculButton;
  [SerializeField]
  protected MypageSlidePanelDragged centerObject;
  private EventInfo eventInfo;
  [SerializeField]
  private GameObject slc_Badge_bikkuri;
  [SerializeField]
  private Guild0281GuildMapButton guildMapButton;
  [SerializeField]
  private Guild0281HuntingButton huntingButton;
  [SerializeField]
  private Guild0281RaidButton raidButton;
  [SerializeField]
  private GameObject raidShopButton;
  [SerializeField]
  private GameObject slcRaidShopNewBadge;
  private DateTime serverTime;
  private GameObject hqMainFacilityLevelupAnimObj;
  private GameObject guildBaseLevelupObj;
  private GameObject guildTopLevelupAnimObj;
  [SerializeField]
  private Guild0281FacilityInfoController facilityInfoController;
  [SerializeField]
  private GameObject hqButton;
  private GuildImageCache guildImageCache;
  private GuildImageCache guildImageCacheLevelUp;
  private WebAPI.Response.GvgResult gvgResult;
  private GameObject gvgResultAnim;
  private GameObject gvgResultObj;
  private GameObject gvgResultDetail;
  private GameObject gvgResultGuildReward;
  private bool isRaidRankingResult;
  private int raid_rank_period_id;
  private int raid_total_damage;
  private int raid_damage_rank;
  private WebAPI.Response.GuildTopRaid_guild_ranking_rewards[] raid_guild_ranking_rewards;
  private WebAPI.Response.GuildTopRaid_guild_ranking_guild_rewards[] raid_guild_ranking_rewardsExtra;
  private bool _isOpenGuildMemberOrInfoPopup;
  private const int TIMING_BATTLE_ENTRY_POPUP = 1;

  public WebAPI.Response.GvgResult GVGResult
  {
    get
    {
      return this.gvgResult;
    }
  }

  public bool isOpenGuildMemberOrInfoPopup
  {
    set
    {
      this._isOpenGuildMemberOrInfoPopup = value;
    }
    get
    {
      return this._isOpenGuildMemberOrInfoPopup;
    }
  }

  private IEnumerator ResourceLoadGuild(GuildRegistration data)
  {
    this.futureGuildTitleImage = EmblemUtility.LoadGuildEmblemSprite(data.appearance._current_emblem);
    IEnumerator e = this.futureGuildTitleImage.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Future<GameObject> fgObj;
    if ((UnityEngine.Object) this.guildBasePrefab == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.guild028_2.GuildBase.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.guildBasePrefab = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    this.guildImageCache = new GuildImageCache();
    e = this.guildImageCache.ResourceLoad(data.appearance);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) this.hqMainFacilityLevelupAnimObj == (UnityEngine.Object) null)
    {
      fgObj = Res.Prefabs.guild028_2.dir_guildbase_main_facility_level_up_anim.Load<GameObject>();
      e = fgObj.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.hqMainFacilityLevelupAnimObj = fgObj.Result;
      fgObj = (Future<GameObject>) null;
    }
    if (this.JudgeLevelUpEffect())
    {
      if ((UnityEngine.Object) this.guildBaseLevelupObj == (UnityEngine.Object) null)
      {
        fgObj = Res.Prefabs.guild028_2.GuildBase_for_levelup_anim.Load<GameObject>();
        e = fgObj.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.guildBaseLevelupObj = fgObj.Result;
        fgObj = (Future<GameObject>) null;
      }
      if ((UnityEngine.Object) this.guildTopLevelupAnimObj == (UnityEngine.Object) null)
      {
        fgObj = Res.Prefabs.popup.guild_base_levelup_anim.Load<GameObject>();
        e = fgObj.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.guildTopLevelupAnimObj = fgObj.Result;
        fgObj = (Future<GameObject>) null;
      }
      this.guildImageCacheLevelUp = new GuildImageCache();
      e = this.guildImageCacheLevelUp.GuildBankLevelUpResourceLoad(1);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
  }

  private bool JudgeLevelUpEffect()
  {
    return !(Persist.guildTopLevel.Data.guildID != PlayerAffiliation.Current.guild.guild_id) && Persist.guildTopLevel.Data.level < PlayerAffiliation.Current.guild.appearance.level;
  }

  private void SetLocalDataGuildLevel()
  {
    Persist.guildTopLevel.Data.guildID = PlayerAffiliation.Current.guild.guild_id;
    Persist.guildTopLevel.Data.level = PlayerAffiliation.Current.guild.appearance.level;
    Persist.guildTopLevel.Flush();
  }

  public IEnumerator InitializeAsync(
    Guild0281Menu.SceneType type,
    WebAPI.Response.GuildTop data = null)
  {
    Guild0281Menu guild0281Menu = this;
    if (data != null)
    {
      guild0281Menu.resGuildTopData = data;
      PlayerAffiliation.Current.raid_period = data.raid_period;
      PlayerAffiliation.Current.raid_aggregating = data.raid_aggregating;
    }
    guild0281Menu.guild = PlayerAffiliation.Current.guild;
    IEnumerator e1;
    if (guild0281Menu.guildInfoPopup == null)
    {
      guild0281Menu.guildInfoPopup = new GuildInfoPopup();
      guild0281Menu.guildInfoPopup.SetSendRequestCallback(new System.Action(guild0281Menu.onButtonGuildAbout));
      guild0281Menu.guildInfoPopup.SetCancelRequestCallback(new System.Action(guild0281Menu.onButtonGuildAbout));
      e1 = guild0281Menu.guildInfoPopup.ResourceLoad();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
    if (guild0281Menu.guildMemberPopup == null)
    {
      guild0281Menu.guildMemberPopup = new GuildMemberObject();
      e1 = guild0281Menu.guildMemberPopup.ResourceLoad();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
    e1 = guild0281Menu.ResourceLoadGuild(guild0281Menu.guild);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    e1 = ServerTime.WaitSync();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    guild0281Menu.serverTime = ServerTime.NowAppTimeAddDelta();
    e1 = guild0281Menu.SetCharacterModel();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    e1 = guild0281Menu.facilityInfoController.InitializeAsync();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    guild0281Menu.gvgResult = (WebAPI.Response.GvgResult) null;
    if (PlayerAffiliation.Current.guild.gvg_status == GvgStatus.finished)
    {
      Future<WebAPI.Response.GvgResult> result = WebAPI.GvgResult((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = result.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (result != null)
      {
        guild0281Menu.gvgResult = result.Result;
        Future<GameObject> f;
        if ((UnityEngine.Object) guild0281Menu.gvgResultAnim == (UnityEngine.Object) null)
        {
          f = Res.Prefabs.guild.dir_guild_GB_result_anim.Load<GameObject>();
          e1 = f.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          guild0281Menu.gvgResultAnim = f.Result;
          f = (Future<GameObject>) null;
        }
        switch (guild0281Menu.gvgResult.score.battle_status)
        {
          case GvgBattleStatus.win:
            f = Res.Prefabs.guild.dir_guild_GB_result_win.Load<GameObject>();
            e1 = f.Wait();
            while (e1.MoveNext())
              yield return e1.Current;
            e1 = (IEnumerator) null;
            guild0281Menu.gvgResultObj = f.Result;
            f = (Future<GameObject>) null;
            break;
          case GvgBattleStatus.lose:
            f = Res.Prefabs.guild.dir_guild_GB_result_lose.Load<GameObject>();
            e1 = f.Wait();
            while (e1.MoveNext())
              yield return e1.Current;
            e1 = (IEnumerator) null;
            guild0281Menu.gvgResultObj = f.Result;
            f = (Future<GameObject>) null;
            break;
          case GvgBattleStatus.draw:
            f = Res.Prefabs.guild.dir_guild_GB_result_draw.Load<GameObject>();
            e1 = f.Wait();
            while (e1.MoveNext())
              yield return e1.Current;
            e1 = (IEnumerator) null;
            guild0281Menu.gvgResultObj = f.Result;
            f = (Future<GameObject>) null;
            break;
        }
        if ((UnityEngine.Object) guild0281Menu.gvgResultDetail == (UnityEngine.Object) null)
        {
          f = Res.Prefabs.guild.dir_guild_GB_result_detail.Load<GameObject>();
          e1 = f.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          guild0281Menu.gvgResultDetail = f.Result;
          f = (Future<GameObject>) null;
        }
        if ((UnityEngine.Object) guild0281Menu.gvgResultGuildReward == (UnityEngine.Object) null)
        {
          f = Res.Prefabs.guild.dir_guild_GB_result_guild_reward.Load<GameObject>();
          e1 = f.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          guild0281Menu.gvgResultGuildReward = f.Result;
          f = (Future<GameObject>) null;
        }
        Persist.guildTopLevel.Data.level = PlayerAffiliation.Current.guild.appearance.level;
        Persist.guildTopLevel.Flush();
        guild0281Menu.isOpenGuildMemberOrInfoPopup = false;
        result = (Future<WebAPI.Response.GvgResult>) null;
      }
    }
  }

  public void setRaidRankingResultFlag(WebAPI.Response.GuildTop data)
  {
    if (this.isRaidRankingResult)
      return;
    this.isRaidRankingResult = false;
    if (!data.raid_rank_period_id.HasValue || !data.raid_damage_rank.HasValue)
      return;
    int? raidDamageRank = data.raid_damage_rank;
    int num = 1;
    if (!(raidDamageRank.GetValueOrDefault() >= num & raidDamageRank.HasValue))
      return;
    this.raid_rank_period_id = data.raid_rank_period_id.Value;
    if (data.raid_total_damage.HasValue)
      this.raid_total_damage = data.raid_total_damage.Value;
    this.raid_damage_rank = data.raid_damage_rank.Value;
    this.raid_guild_ranking_rewards = data.raid_guild_ranking_rewards;
    this.raid_guild_ranking_rewardsExtra = data.raid_guild_ranking_guild_rewards;
    this.isRaidRankingResult = true;
  }

  public void InitializeGuildTop()
  {
    GuildUtil.rp = this.resGuildTopData.rp;
    GuildUtil.rp_max = ((IEnumerable<GuildRaidSettings>) MasterData.GuildRaidSettingsList).FirstOrDefault<GuildRaidSettings>((Func<GuildRaidSettings, bool>) (x => x.key == "RP_BASE_MAX")).value;
    this.sceneType = Guild0281Menu.SceneType.GuildTop;
    this.SetGuildButton();
    this.SetHuntingButton();
    this.SetRaidButton();
    this.JogDialSetting();
    this.SetGuildData(this.guild);
    this.hqButton.SetActive(PlayerAffiliation.Current.guild.appearance.GuildHQOpen());
    if (this.JudgeLevelUpEffect())
      this.StartCoroutine(this.PlayGuildTopLevelUpEffect());
    this.SetLocalDataGuildLevel();
    DateTime dateTime1 = ServerTime.NowAppTimeAddDelta();
    if (Singleton<NGGameDataManager>.GetInstance().raidMedalShopLatestStartTime.HasValue)
    {
      GameObject raidShopNewBadge = this.slcRaidShopNewBadge;
      DateTime? shopLatestStartTime = Singleton<NGGameDataManager>.GetInstance().raidMedalShopLatestStartTime;
      DateTime dateTime2 = dateTime1;
      int num = shopLatestStartTime.HasValue ? (shopLatestStartTime.GetValueOrDefault() <= dateTime2 ? 1 : 0) : 0;
      raidShopNewBadge.SetActive(num != 0);
    }
    else
      this.slcRaidShopNewBadge.SetActive(false);
    ShopShop shopShop;
    if (MasterData.ShopShop.TryGetValue(8000, out shopShop))
    {
      DateTime dateTime2 = dateTime1;
      DateTime? startAt = shopShop.start_at;
      if ((startAt.HasValue ? (dateTime2 >= startAt.GetValueOrDefault() ? 1 : 0) : 0) != 0)
      {
        this.raidShopButton.SetActive(true);
        return;
      }
    }
    this.raidShopButton.SetActive(false);
  }

  public void InitializeHQTop()
  {
    this.sceneType = Guild0281Menu.SceneType.HQTop;
    this.SetGuildData(this.guild);
  }

  public IEnumerator PlayGuildTopLevelUpEffect()
  {
    Guild0282GuildBase gbObjEff = this.guildBaseLevelupObj.Clone((Transform) null).GetComponent<Guild0282GuildBase>();
    gbObjEff.gameObject.SetActive(false);
    Guild0282GuildBase gbObj = this.guildBasePrefab.Clone((Transform) null).GetComponent<Guild0282GuildBase>();
    gbObj.gameObject.SetActive(false);
    IEnumerator e = this.guildImageCacheLevelUp.GuildBankLevelUpResourceLoad(1);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    while (this.isAnimation())
      yield return (object) null;
    e = this.showLevelupAnim(PlayerAffiliation.Current.guild.appearance.level, gbObj.gameObject, gbObjEff.gameObject);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    UnityEngine.Object.Destroy((UnityEngine.Object) gbObjEff.gameObject);
    UnityEngine.Object.Destroy((UnityEngine.Object) gbObj.gameObject);
  }

  private IEnumerator showLevelupAnim(
    int level,
    GameObject guildBase,
    GameObject guildBaseEff)
  {
    GameObject prefab = this.guildTopLevelupAnimObj.Clone((Transform) null);
    prefab.GetComponent<GuildLevelUpAnimPopup>().Initialize(level, guildBase, guildBaseEff, this.guildImageCacheLevelUp);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
    while (Singleton<PopupManager>.GetInstance().isOpen)
      yield return (object) null;
  }

  private void SetGuildButton()
  {
    this.guildMapButton.Initialize();
  }

  private void SetHuntingButton()
  {
    int? nullable = ((IEnumerable<EventInfo>) this.resGuildTopData.event_infos).FirstIndexOrNull<EventInfo>((Func<EventInfo, bool>) (x => x.IsGuild()));
    if (nullable.HasValue)
    {
      this.eventInfo = this.resGuildTopData.event_infos[nullable.Value];
      this.huntingButton.Initialize(this.eventInfo, this.serverTime);
    }
    else
      this.huntingButton.EventClose();
  }

  private void SetRaidButton()
  {
    this.raidButton.Initialize(this.resGuildTopData, this.serverTime);
    if (PlayerAffiliation.Current.guild.gvg_status != GvgStatus.out_of_term || this.resGuildTopData.raid_period == null)
      return;
    this.centerObject = this.raidButton.GetComponent<MypageSlidePanelDragged>();
  }

  public void onEndScene()
  {
    this.EndAnimation();
    this.facilityInfoController.ClearFacilityLevelUpEffects();
  }

  public void EndAnimation()
  {
    this.CirculButton.condition = CircularMotionPositionSet.CirculCondition.START;
    switch (this.sceneType)
    {
      case Guild0281Menu.SceneType.GuildTop:
        this.PlayTweens(MypageMenuBase.END_TWEEN_GROUP_JOGDIAL, (EventDelegate.Callback) null);
        this.PlayTweens(Guild0281Menu.GUILDTOP_TO_OTHER, (EventDelegate.Callback) null);
        break;
      case Guild0281Menu.SceneType.HQTop:
        this.PlayTweens(Guild0281Menu.HQTOP_TO_OTHER, (EventDelegate.Callback) null);
        break;
    }
  }

  public void onDestroySceneAsync()
  {
  }

  public void HQLevelUp(GuildBaseType type)
  {
    this.guildBase.HQLevelUp(type, this.guildImageCache);
  }

  private void SetGuildData(GuildRegistration data)
  {
    if (this.futureGuildTitleImage != null && (UnityEngine.Object) this.futureGuildTitleImage.Result != (UnityEngine.Object) null)
      this.guildTitleImage.sprite2D = this.futureGuildTitleImage.Result;
    if ((UnityEngine.Object) this.guildBase != (UnityEngine.Object) null)
      UnityEngine.Object.Destroy((UnityEngine.Object) this.guildBase.gameObject);
    this.guildBase = this.guildBasePrefab.Clone(this.guildBasePos.transform).GetComponent<Guild0282GuildBase>();
    this.guildBase.GetComponent<BoxCollider>().enabled = false;
    this.guildBase.SetSprite(data.appearance, this.guildImageCache, this.hqMainFacilityLevelupAnimObj);
    this.guildName.SetTextLocalize(data.guild_name);
    if (data.appearance.level / 10 == 0)
    {
      this.guildRank1.gameObject.SetActive(false);
      this.guildRank10.gameObject.SetActive(true);
      this.guildRank10.SetSprite(string.Format("slc_text_glv_number{0}.png__GUI__guild_common_other__guild_common_other_prefab", (object) (data.appearance.level % 10)));
    }
    else
    {
      this.guildRank10.gameObject.SetActive(true);
      this.guildRank10.SetSprite(string.Format("slc_text_glv_number{0}.png__GUI__guild_common_other__guild_common_other_prefab", (object) (data.appearance.level / 10)));
      this.guildRank1.SetSprite(string.Format("slc_text_glv_number{0}.png__GUI__guild_common_other__guild_common_other_prefab", (object) (data.appearance.level % 10)));
    }
    this.nextExp.SetTextLocalize(Consts.Format(Consts.GetInstance().Guild0281MENU_NEXTEXP, (IDictionary) new Hashtable()
    {
      {
        (object) "nextExp",
        (object) data.appearance.experience_next
      }
    }));
    if (data.appearance.experience_next == 0)
      this.expGauge.setValue(0, 1, true, -1f, -1f);
    else
      this.expGauge.setValue(data.appearance.experience, data.appearance.experience + data.appearance.experience_next, true, -1f, -1f);
    this.currentMember.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_4_CURRENT_MEMBER, (IDictionary) new Hashtable()
    {
      {
        (object) "currentMember",
        (object) data.appearance.membership_num
      }
    }));
    this.maxMember.SetTextLocalize(Consts.Format(Consts.GetInstance().POPUP_028_1_1_4_MAX_MEMBER, (IDictionary) new Hashtable()
    {
      {
        (object) "maxMember",
        (object) data.appearance.membership_capacity
      }
    }));
    this.ranking.SetTextLocalize(data.appearance.ranking_no);
    this.battleCount.SetTextLocalize(data.appearance.draw_num + data.appearance.win_num + data.appearance.lose_num);
    this.win.SetTextLocalize(data.appearance.win_num);
    this.role.SetTextLocalize(PlayerAffiliation.Current.role_name.name);
    this.SetBudgeForInitialize();
  }

  public void JogDialSetting()
  {
    this.CirculButton.Init(this.centerObject);
    this.CirculButton.condition = CircularMotionPositionSet.CirculCondition.START;
    this.PlayTweens(MypageMenuBase.START_TWEEN_GROUP_JOGDIAL, (EventDelegate.Callback) null);
  }

  public void onButtonGuildAbout()
  {
    if (this.IsPushAndSet())
      return;
    this.isOpenGuildMemberOrInfoPopup = true;
    GameObject prefab = this.guildInfoPopup.guildInfoPopup.Clone((Transform) null);
    prefab.SetActive(false);
    prefab.GetComponent<Guild028114Popup>().Initialize(this.guildInfoPopup, (GuildRegistration) null, false);
    prefab.SetActive(true);
    Singleton<PopupManager>.GetInstance().open(prefab, false, false, true, true, false, false, "SE_1006");
  }

  public void onButonGuildTop()
  {
    if (this.isAnimation())
      return;
    this.PlayTweens(MypageMenuBase.START_TWEEN_GROUP_JOGDIAL, (EventDelegate.Callback) null);
    Guild0281Scene.ChangeSceneGuildTop(false, this, true);
  }

  public void onButonHQTop()
  {
    if (this.isAnimation())
      return;
    this.PlayTweens(MypageMenuBase.END_TWEEN_GROUP_JOGDIAL, (EventDelegate.Callback) null);
    Guild0281Scene.ChangeSceneHQTop(false, this);
  }

  public void onButonGuildGift()
  {
    if (this.IsPushAndSet())
      return;
    Guild0286Scene.ChangeScene(true);
  }

  public void onButtonGuildBank()
  {
    if (this.IsPushAndSet())
      return;
    Guild0287Scene.ChangeScene();
  }

  public void onButtonRaidShop()
  {
    if (this.IsPushAndSet())
      return;
    Raid032ShopScene.ChangeScene(true);
  }

  public void onButtonGuildMenu()
  {
    if (this.IsPushAndSet())
      return;
    Guild0283Scene.ChangeScene();
  }

  public void onButtonGuildMap()
  {
    if (this.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    this.centerObject = this.guildMapButton.GetComponent<MypageSlidePanelDragged>();
    Guild0282Scene.ChangeScene();
  }

  public void onButtonGuildHunting()
  {
    if (this.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    this.centerObject = this.huntingButton.GetComponent<MypageSlidePanelDragged>();
    Quest00230Scene.ChangeScene(true, this.eventInfo);
  }

  public void onButtonRaidMap()
  {
    if (this.IsPushAndSet())
      return;
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_1002", false, 0.0f, -1);
    this.centerObject = this.raidButton.GetComponent<MypageSlidePanelDragged>();
    RaidTopScene.ChangeScene();
  }

  public void onButtonDrawMyGuild()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.ShowMyMemberList());
  }

  private IEnumerator ShowMyMemberList()
  {
    Guild0281Menu guild0281Menu = this;
    guild0281Menu.isOpenGuildMemberOrInfoPopup = true;
    guild0281Menu.slc_Badge_bikkuri.SetActive(false);
    if (Persist.guildSetting.Exists)
    {
      GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.changeRole, false);
      GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.newMember, false);
      Persist.guildSetting.Flush();
    }
    GameObject popup = guild0281Menu.guildMemberPopup.GuildMemberListPopup.Clone((Transform) null);
    GuildMemberListPopup component = popup.GetComponent<GuildMemberListPopup>();
    popup.SetActive(false);
    IEnumerator e = component.Initialize(guild0281Menu.guildMemberPopup, new System.Action(guild0281Menu.onButtonDrawMyGuild));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    popup.SetActive(true);
    Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
  }

  private IEnumerator SetCharacterModel()
  {
    IEnumerator e;
    if ((UnityEngine.Object) this.UIModel == (UnityEngine.Object) null)
    {
      Future<GameObject> fModel = Res.Prefabs.gacha006_8.slc_3DModel.Load<GameObject>();
      e = fModel.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.UIModel = fModel.Result.Clone(this.Model.transform).GetComponent<UI3DModel>();
      fModel = (Future<GameObject>) null;
    }
    e = this.UIModel.Unit(SMManager.Get<PlayerDeck[]>()[0].player_units[0], (System.Action) null);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.UIModel.ModelCamera.fieldOfView = 50f;
  }

  protected virtual void OnDisable()
  {
    if ((UnityEngine.Object) this.UIModel != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) this.UIModel.model_creater_ != (UnityEngine.Object) null)
        this.UIModel.DestroyModelCamera();
      if ((UnityEngine.Object) this.UIModel.gameObject != (UnityEngine.Object) null && this.UIModel.gameObject.name == "slc_3DModel(Clone)")
        UnityEngine.Object.Destroy((UnityEngine.Object) this.UIModel.gameObject);
    }
    ModelUnits.Instance.DestroyModelUnits();
  }

  public override void onBackButton()
  {
    if (Singleton<PopupManager>.GetInstance().isOpen)
    {
      if (this.isOpenGuildMemberOrInfoPopup)
        return;
      Singleton<PopupManager>.GetInstance().dismiss(false);
    }
    else if (Singleton<CommonRoot>.GetInstance().guildChatManager.GetCurrentGuildChatStatus() == GuildChatManager.GuildChatStatus.DetailedView)
      Singleton<CommonRoot>.GetInstance().guildChatManager.OnBackButtonClicked();
    else if (this.sceneType == Guild0281Menu.SceneType.HQTop)
      this.onButonGuildTop();
    else
      this.IbtnBack();
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Guild0281Scene component = this.GetComponent<Guild0281Scene>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
      component.setFlagBackScene(true);
    this.backScene();
  }

  protected override void Update()
  {
    base.Update();
    this.SetBudgeForUpdate();
  }

  private void SetBudgeForInitialize()
  {
    if (Persist.guildSetting.Exists)
    {
      GuildRole? role = PlayerAffiliation.Current.role;
      GuildRole guildRole1 = GuildRole.master;
      if (!(role.GetValueOrDefault() == guildRole1 & role.HasValue))
      {
        role = PlayerAffiliation.Current.role;
        GuildRole guildRole2 = GuildRole.sub_master;
        if (!(role.GetValueOrDefault() == guildRole2 & role.HasValue))
        {
          this.newMenu.SetActive(false);
          goto label_5;
        }
      }
      this.newMenu.SetActive(GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newApplicant));
label_5:
      if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.changeRole) || GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newMember))
        this.slc_Badge_bikkuri.SetActive(true);
      if (GuildUtil.getGuildMemberNum() != PlayerAffiliation.Current.guild.memberships.Length)
      {
        if (!this.slc_Badge_bikkuri.activeSelf && GuildUtil.getGuildMemberNum() != -1)
          this.slc_Badge_bikkuri.SetActive(true);
        GuildUtil.setGuildMemberNum(PlayerAffiliation.Current.guild.memberships.Length);
      }
      this.newGift.SetActive(GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newGift));
      this.bankBadge.SetActive(false);
      GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.startHuntingEvent, false);
      GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.receiveHuntingReward, false);
      GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guildLevelup, false);
      GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.baseRankUp, false);
      Persist.guildSetting.Flush();
      Singleton<CommonRoot>.GetInstance().SetGuildFooterBadgeBikkuri();
    }
    SM.GuildSignal.Current.removePlayershipEvent(GuildEventType.apply_applicant);
    GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry, false);
    GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_matched, false);
    GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_started, false);
    switch (PlayerAffiliation.Current.guild.gvg_status)
    {
      case GvgStatus.matching:
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry, true);
        break;
      case GvgStatus.preparing:
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_matched, true);
        break;
      case GvgStatus.fighting:
        GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.gvg_started, true);
        break;
    }
    RaidPeriod raidPeriod = this.resGuildTopData.raid_period;
    if (raidPeriod != null)
    {
      DateTime? nullable = raidPeriod.start_at;
      DateTime dateTime1 = ServerTime.NowAppTime();
      if ((nullable.HasValue ? (nullable.GetValueOrDefault() < dateTime1 ? 1 : 0) : 0) != 0)
      {
        nullable = raidPeriod.end_at;
        DateTime dateTime2 = ServerTime.NowAppTime();
        if ((nullable.HasValue ? (nullable.GetValueOrDefault() > dateTime2 ? 1 : 0) : 0) != 0)
        {
          GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid, true);
          goto label_21;
        }
      }
    }
    GuildUtil.setBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid, false);
label_21:
    if (Persist.guildSetting.Exists)
      Persist.guildSetting.Flush();
    GuildUtil.GuildBadgeLabelType labelType = GuildUtil.GuildBadgeLabelType.none;
    if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.gvg_entry))
      labelType = GuildUtil.GuildBadgeLabelType.entry;
    else if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.gvg_matched))
      labelType = GuildUtil.GuildBadgeLabelType.prepare;
    else if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.gvg_started))
      labelType = GuildUtil.GuildBadgeLabelType.battle;
    else if (GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.guild_raid))
      labelType = GuildUtil.GuildBadgeLabelType.raidBoss;
    if (labelType == GuildUtil.GuildBadgeLabelType.none)
    {
      Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.label, false);
    }
    else
    {
      Singleton<CommonRoot>.GetInstance().SetGuildFooterBadgeLabel(labelType);
      Singleton<CommonRoot>.GetInstance().SetGuildFooterBadge(GuildUtil.FooterGuildBadge.label, true);
    }
  }

  private void SetBudgeForUpdate()
  {
    if (!Persist.guildSetting.Exists)
      return;
    if (PlayerAffiliation.Current != null)
    {
      GuildRole? role = PlayerAffiliation.Current.role;
      GuildRole guildRole1 = GuildRole.master;
      if (!(role.GetValueOrDefault() == guildRole1 & role.HasValue))
      {
        role = PlayerAffiliation.Current.role;
        GuildRole guildRole2 = GuildRole.sub_master;
        if (!(role.GetValueOrDefault() == guildRole2 & role.HasValue))
          goto label_5;
      }
      this.newMenu.SetActive(GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newApplicant));
      goto label_6;
    }
label_5:
    this.newMenu.SetActive(false);
label_6:
    this.newGift.SetActive(GuildUtil.getBadgeState(GuildUtil.GuildBadgeInfoType.newGift));
  }

  public bool PlayTweens(int groupID, EventDelegate.Callback eventFinished = null)
  {
    if (this.tweens == null)
      this.tweens = ((IEnumerable<UITweener>) this.GetComponentsInChildren<UITweener>()).Where<UITweener>((Func<UITweener, bool>) (x => x.tweenGroup != Guild0281Menu.IBTN_TWEENGROUP)).ToArray<UITweener>();
    bool flag = NGTween.playTweens(this.tweens, groupID, false) & !NGTween.isTweensError;
    if (((eventFinished == null ? 0 : (this.tweens != null ? 1 : 0)) & (flag ? 1 : 0)) != 0)
    {
      UITweener uiTweener = (UITweener) null;
      foreach (UITweener tween in this.tweens)
      {
        if (tween.tweenGroup == groupID && ((UnityEngine.Object) uiTweener == (UnityEngine.Object) null || (double) uiTweener.duration + (double) uiTweener.delay < (double) tween.duration + (double) tween.delay))
          uiTweener = tween;
      }
      if ((UnityEngine.Object) uiTweener != (UnityEngine.Object) null)
        EventDelegate.Set(uiTweener.onFinished, eventFinished);
    }
    return flag;
  }

  private bool isAnimation()
  {
    if (this.tweens == null)
      this.tweens = ((IEnumerable<UITweener>) this.GetComponentsInChildren<UITweener>()).Where<UITweener>((Func<UITweener, bool>) (x => x.tweenGroup != Guild0281Menu.IBTN_TWEENGROUP)).ToArray<UITweener>();
    return (uint) ((IEnumerable<UITweener>) this.tweens).Where<UITweener>((Func<UITweener, bool>) (x => x.isActiveAndEnabled)).ToArray<UITweener>().Length > 0U;
  }

  public IEnumerator GuildBattleResult()
  {
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, true, false, false, "SE_1006");
    GameObject clone = this.gvgResultAnim.Clone((Transform) null);
    clone.SetActive(false);
    IEnumerator e = clone.GetComponent<GuildGBResultAnim>().InitializeAsync(this.gvgResult);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    clone.GetComponent<GuildGBResultAnim>().Initialize(this.gvgResult, this.gvgResultObj);
    clone.SetActive(true);
    GameObject obj = Singleton<PopupManager>.GetInstance().open(clone, false, false, true, true, false, false, "SE_1006");
    while (!((UnityEngine.Object) obj == (UnityEngine.Object) null))
      yield return (object) null;
    obj = Singleton<PopupManager>.GetInstance().open(this.gvgResultDetail, false, false, false, true, false, false, "SE_1006");
    obj.GetComponent<GuildGBResultDetail>().Initialize(this.gvgResult);
    while (!((UnityEngine.Object) obj == (UnityEngine.Object) null))
      yield return (object) null;
    clone = this.gvgResultGuildReward.Clone((Transform) null);
    clone.SetActive(false);
    e = clone.GetComponent<GuildGBResultReward>().Initialize(this.gvgResult);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    clone.SetActive(true);
    clone.GetComponent<GuildGBResultReward>().PositionReset();
    obj = Singleton<PopupManager>.GetInstance().open(clone, false, false, true, true, false, false, "SE_1006");
    while (!((UnityEngine.Object) obj == (UnityEngine.Object) null))
      yield return (object) null;
    yield return (object) this.startRankingReward();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public IEnumerator startRankingReward()
  {
    if (this.isRaidRankingResult)
    {
      GuildRaidRankingRewardPopupSequence rankingRewardPopupSeq = new GuildRaidRankingRewardPopupSequence();
      yield return (object) rankingRewardPopupSeq.Init(this.raid_rank_period_id, this.raid_damage_rank, this.guild.guild_name, this.raid_total_damage);
      if (this.raid_guild_ranking_rewards != null)
      {
        foreach (WebAPI.Response.GuildTopRaid_guild_ranking_rewards guildRankingReward in this.raid_guild_ranking_rewards)
        {
          if (guildRankingReward.rewards != null)
          {
            foreach (WebAPI.Response.GuildTopRaid_guild_ranking_rewardsRewards reward in guildRankingReward.rewards)
              rankingRewardPopupSeq.addRewardData(guildRankingReward.condition_id, (MasterDataTable.CommonRewardType) reward.reward_type_id, reward.reward_id, reward.reward_quantity, false);
          }
        }
      }
      if (this.raid_guild_ranking_rewardsExtra != null)
      {
        foreach (WebAPI.Response.GuildTopRaid_guild_ranking_guild_rewards rankingGuildRewards in this.raid_guild_ranking_rewardsExtra)
        {
          if (rankingGuildRewards.rewards != null)
          {
            foreach (GuildRankingGuildReward reward in rankingGuildRewards.rewards)
              rankingRewardPopupSeq.addRewardData(rankingGuildRewards.condition_id, GuildUtil.getCommonRewardType(reward.reward_type), reward.reward_id, reward.reward_quantity, true);
          }
        }
      }
      this.isRaidRankingResult = false;
      yield return (object) rankingRewardPopupSeq.Run();
    }
  }

  public bool checkOpenBattleEntryPopup()
  {
    if (this.guild.gvg_status != GvgStatus.can_entry)
      return false;
    int gvgId = this.guild.active_gvg_period_id.HasValue ? this.guild.active_gvg_period_id.Value : 0;
    Persist.GuildBattleUser guildBattleUser;
    try
    {
      guildBattleUser = Persist.guildBattleUser.Data;
    }
    catch
    {
      Persist.guildBattleUser.Delete();
      guildBattleUser = new Persist.GuildBattleUser();
      Persist.guildBattleUser.Data = guildBattleUser;
    }
    string myId = Player.Current.id;
    GuildMembership guildMembership = ((IEnumerable<GuildMembership>) this.guild.memberships).FirstOrDefault<GuildMembership>((Func<GuildMembership, bool>) (x => x.player.player_id == myId));
    if (guildMembership == null)
    {
      ++guildBattleUser.countTopIN;
      Persist.guildBattleUser.Flush();
      return false;
    }
    if (guildBattleUser.guildID != this.guild.guild_id || guildBattleUser.gvgID != gvgId || (guildBattleUser.roleNo != guildMembership._role || guildBattleUser.gvgCount != this.guild.gvg_count))
      guildBattleUser.reset(this.guild.guild_id, guildMembership._role, gvgId, this.guild.gvg_count);
    ++guildBattleUser.countTopIN;
    Persist.guildBattleUser.Flush();
    if (guildBattleUser.countTopIN != 1 || guildMembership.role != GuildRole.master && guildMembership.role != GuildRole.sub_master)
      return false;
    this.StartCoroutine(this.coDrawBattleEntryPopup());
    return true;
  }

  private IEnumerator coDrawBattleEntryPopup()
  {
    Guild0281Menu guild0281Menu = this;
    Future<GameObject> ld = Res.Prefabs.popup.popup_028_guild_battle_entry__anim_popup01.Load<GameObject>();
    IEnumerator e = ld.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!((UnityEngine.Object) ld.Result == (UnityEngine.Object) null))
    {
      Guild0281Scene myScene = guild0281Menu.gameObject.GetComponent<Guild0281Scene>();
      while (!myScene.isTweenFinished)
        yield return (object) null;
      Singleton<PopupManager>.GetInstance().open(ld.Result, false, false, false, true, false, false, "SE_1006");
    }
  }

  public enum SceneType
  {
    GuildTop,
    HQTop,
    NONE,
  }
}
