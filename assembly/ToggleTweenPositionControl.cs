﻿// Decompiled with JetBrains decompiler
// Type: ToggleTweenPositionControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class ToggleTweenPositionControl : MonoBehaviour
{
  [SerializeField]
  [Tooltip("トグルアニメーションの順方向での表現がOFF->ON")]
  private bool forwardOFF_ON_ = true;
  private bool initialize_ = true;
  [SerializeField]
  [Tooltip("トグルアニメーション用")]
  private TweenPosition tween_;
  [SerializeField]
  private bool isSwitch_;
  private Func<bool> checkCancel_;
  private bool isPlaying_;

  public bool isSwitch
  {
    get
    {
      return this.isSwitch_;
    }
  }

  private void Awake()
  {
    this.tween_.enabled = false;
    EventDelegate.Set(this.tween_.onFinished, new EventDelegate.Callback(this.onAnimeFinished));
  }

  private void Start()
  {
    if (!this.initialize_)
      return;
    this.resetSwitch(this.isSwitch_);
  }

  public void setCheckCancel(Func<bool> checkCancel)
  {
    this.checkCancel_ = checkCancel;
  }

  public void resetSwitch(bool flag)
  {
    this.isSwitch_ = flag;
    Vector3 vector3_1;
    Vector3 vector3_2;
    float num;
    if (this.forwardOFF_ON_)
    {
      vector3_1 = this.tween_.to;
      vector3_2 = this.tween_.from;
      num = flag ? 1f : 0.0f;
    }
    else
    {
      vector3_1 = this.tween_.from;
      vector3_2 = this.tween_.to;
      num = flag ? 0.0f : 1f;
    }
    this.tween_.value = flag ? vector3_1 : vector3_2;
    this.tween_.tweenFactor = num;
    this.initialize_ = false;
    this.isPlaying_ = false;
  }

  public void setSwitch(bool flag)
  {
    if (this.isSwitch_ == flag)
      return;
    this.isSwitch_ = flag;
    this.isPlaying_ = true;
    NGTween.playTween((UITweener) this.tween_, flag ? !this.forwardOFF_ON_ : this.forwardOFF_ON_);
  }

  private void onAnimeFinished()
  {
    this.isPlaying_ = false;
  }

  public void onClickedToggle()
  {
    if (this.isPlaying_ || this.checkCancel_ != null && this.checkCancel_())
      return;
    this.setSwitch(!this.isSwitch_);
  }
}
