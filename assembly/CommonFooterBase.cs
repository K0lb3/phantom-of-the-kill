﻿// Decompiled with JetBrains decompiler
// Type: CommonFooterBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class CommonFooterBase : NGMenuBase
{
  public void setDisableColor()
  {
    foreach (UIWidget componentsInChild in this.gameObject.GetComponentsInChildren<UISprite>(true))
      componentsInChild.color = new Color(0.4117647f, 0.4117647f, 0.4117647f);
  }

  public void resetDisableColor()
  {
    foreach (UIWidget componentsInChild in this.gameObject.GetComponentsInChildren<UISprite>(true))
      componentsInChild.color = new Color(0.5f, 0.5f, 0.5f);
  }
}
