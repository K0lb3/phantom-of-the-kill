﻿// Decompiled with JetBrains decompiler
// Type: Quest002272SceneChangeData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

public class Quest002272SceneChangeData
{
  public QuestScoreCampaignProgressScore_achivement_rewards achivement_reward;
  public int[] achivement_cleard;
  public string title;
  public int score;

  public Quest002272SceneChangeData(
    QuestScoreCampaignProgressScore_achivement_rewards achivementReward,
    int[] achivementCleard,
    string title,
    int score)
  {
    this.achivement_reward = achivementReward;
    this.achivement_cleard = achivementCleard;
    this.title = title;
    this.score = score;
  }
}
