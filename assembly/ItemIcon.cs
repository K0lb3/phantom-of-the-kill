﻿// Decompiled with JetBrains decompiler
// Type: ItemIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemIcon : IconPrefabBase
{
  public static readonly int Width = 123;
  public static readonly int Height = 147;
  public static readonly int ColumnValue = 5;
  public static readonly int RowValue = 8;
  public static readonly int RowScreenValue = 5;
  public static readonly int ScreenValue = ItemIcon.ColumnValue * ItemIcon.RowScreenValue;
  public static readonly int MaxValue = ItemIcon.ColumnValue * ItemIcon.RowValue;
  private static readonly int SelectedIndexFontSize = 22;
  private static readonly int ManaSeedsDurabilityFontSize = 28;
  private static Dictionary<int, UnityEngine.Sprite> gearCache = new Dictionary<int, UnityEngine.Sprite>();
  private static Dictionary<int, UnityEngine.Sprite> supplyCache = new Dictionary<int, UnityEngine.Sprite>();
  [SerializeField]
  private ItemIcon.BottomMode bottomMode = ItemIcon.BottomMode.Visible;
  public static bool IsPoolCache;
  private static GameObject elementIconPrefab;
  private static GameObject reisouEffect01Prefab;
  private static GameObject buguReisouEffect01Prefab;
  private GameCore.ItemInfo itemInfo;
  public GameObject removeButton;
  public UnityEngine.Sprite[] backSprite;
  public UnityEngine.Sprite[] backSpriteSpecificationOfEquipmentUnits;
  public UnityEngine.Sprite backSpriteWeaponMaterial;
  public UnityEngine.Sprite backSpriteReisou;
  public UnityEngine.Sprite nonBackSprite;
  public UnityEngine.Sprite[] raritySprite;
  public UnityEngine.Sprite nonTypeSprite;
  public ItemIcon.SpriteArray rankSprite;
  public UnityEngine.Sprite[] rankNumSprite;
  public UnityEngine.Sprite[] selectNumSprite;
  public UnityEngine.Sprite[] numberSprite;
  public UnityEngine.Sprite[] gearUnlimitSprite;
  public UnityEngine.Sprite[] limitRankSprite;
  private ItemSortAndFilter.SORT_TYPES currSort;
  public ItemIcon.Gear gear;
  public ItemIcon.Supply supply;
  public GameObject quantity;
  [SerializeField]
  private GameObject m_crossForOneDigitCount;
  [SerializeField]
  private GameObject m_crossForTwoDigitsCount;
  [SerializeField]
  private GameObject m_crossForThreeDigitsCount;
  [SerializeField]
  private GameObject m_crossForFourDigitsCount;
  [SerializeField]
  private GameObject[] m_onesDigit;
  [SerializeField]
  private GameObject[] m_tensDigit;
  [SerializeField]
  private GameObject[] m_hundredsDigit;
  [SerializeField]
  private GameObject[] m_thousandsDigit;
  public GameObject quantity_bonus;
  [SerializeField]
  private GameObject m_crossForOneDigitCount_bonus;
  [SerializeField]
  private GameObject m_crossForTwoDigitsCount_bonus;
  [SerializeField]
  private GameObject m_crossForThreeDigitsCount_bonus;
  [SerializeField]
  private GameObject m_crossForFourDigitsCount_bonus;
  [SerializeField]
  private GameObject[] m_onesDigit_bonus;
  [SerializeField]
  private GameObject[] m_tensDigit_bonus;
  [SerializeField]
  private GameObject[] m_hundredsDigit_bonus;
  [SerializeField]
  private GameObject[] m_thousandsDigit_bonus;
  public GameObject selectQuantity;
  public GameObject[] selectedLeftNum;
  public GameObject[] selectedRightNum;
  public GameObject checkmark;
  [SerializeField]
  private GameObject maxUpMark;
  private bool selected;
  private System.Action<ItemIcon> onClick_;
  private System.Action<ItemIcon> longPress_;

  public GameCore.ItemInfo ItemInfo
  {
    get
    {
      return this.itemInfo;
    }
  }

  public override bool Gray
  {
    get
    {
      return this.gray;
    }
    set
    {
      if (this.gray == value)
        return;
      this.gray = value;
      NGTween.playTweens(this.GetComponentsInChildren<UITweener>(true), NGTween.Kind.GRAYOUT, !value);
    }
  }

  public bool EnabledGear
  {
    get
    {
      return this.gear.root.activeSelf;
    }
  }

  public bool EnabledSupply
  {
    get
    {
      return this.supply.root.activeSelf;
    }
  }

  public void SetModeGear()
  {
    this.gear.root.SetActive(true);
    this.supply.root.SetActive(false);
  }

  public void SetModeSupply()
  {
    this.gear.root.SetActive(false);
    this.supply.root.SetActive(true);
  }

  public bool NewItem
  {
    get
    {
      return this.gear.root.activeSelf ? this.gear.newGear.activeSelf : this.supply.newSupply.activeSelf;
    }
    set
    {
      if (this.gear.root.activeSelf)
        this.gear.newGear.SetActive(value);
      else
        this.supply.newSupply.SetActive(value);
    }
  }

  public ItemIcon.BottomMode BottomModeValue
  {
    get
    {
      return this.bottomMode;
    }
    set
    {
      this.gear.bottom.SetActive(value == ItemIcon.BottomMode.Visible);
      this.gear.bottomWIconNone.SetActive(value == ItemIcon.BottomMode.Visible_wIconNone);
      this.supply.bottom.SetActive(value == ItemIcon.BottomMode.Visible || value == ItemIcon.BottomMode.Visible_wIconNone);
      this.bottomMode = value;
    }
  }

  public bool ForBattle
  {
    get
    {
      return !this.EnabledGear ? this.supply.forbattle.activeSelf : this.gear.forbattle.activeSelf;
    }
    set
    {
      if (this.EnabledGear)
        this.gear.forbattle.SetActive(value);
      else
        this.supply.forbattle.SetActive(value);
    }
  }

  public bool Favorite
  {
    get
    {
      return !this.EnabledGear ? this.supply.favorite.activeSelf : this.gear.favorite.activeSelf;
    }
    set
    {
      if (this.EnabledGear)
        this.gear.favorite.SetActive(value);
      else
        this.supply.favorite.SetActive(value);
    }
  }

  public bool Broken
  {
    get
    {
      return !this.EnabledGear ? this.supply.forbattle.activeSelf : this.gear.forbattle.activeSelf;
    }
    set
    {
      if (!this.EnabledGear)
        return;
      this.gear.broken.SetActive(value);
    }
  }

  public bool QuantitySupply
  {
    get
    {
      return this.quantity.activeSelf;
    }
    set
    {
      this.quantity.SetActive(value);
    }
  }

  public bool QuantitySupplyBonus
  {
    get
    {
      return this.quantity_bonus.activeSelf;
    }
    set
    {
      this.quantity_bonus.SetActive(value);
    }
  }

  public bool DefaltGearText
  {
    get
    {
      return this.gear.defaultGearTxt.activeSelf;
    }
    set
    {
      this.gear.defaultGearTxt.SetActive(value);
    }
  }

  public bool SelectQuantity
  {
    get
    {
      return this.selectQuantity.activeSelf;
    }
    set
    {
      this.selectQuantity.SetActive(value);
    }
  }

  public bool MaxUpMark
  {
    get
    {
      return !((UnityEngine.Object) this.maxUpMark == (UnityEngine.Object) null) && this.maxUpMark.activeSelf;
    }
    set
    {
      if (!((UnityEngine.Object) this.maxUpMark != (UnityEngine.Object) null))
        return;
      this.maxUpMark.SetActive(value);
    }
  }

  public IEnumerator InitByItemInfo(GameCore.ItemInfo info)
  {
    this.itemInfo = info;
    this.removeButton.SetActive(false);
    this.gear.sortRankMaxInfo.SetActive(false);
    this.gear.dynReisouEffect.transform.Clear();
    this.gear.rankLastDigit.SetActive(false);
    IEnumerator e;
    if (!info.isSupply)
    {
      e = this.InitByGear(info.gear, info.GetElement(), false, false, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (info.isWeaponMaterial)
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteWeaponMaterial;
      else if (info.isReisou)
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteReisou;
      if (info.isWeapon && info.gear.disappearance_type_GearDisappearanceType == 0)
      {
        this.gear.rank.SetActive(true);
        if (info.gearLevel == info.gearLevelLimit)
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.limitRankSprite[info.gearLevel - 1 < 0 ? 0 : info.gearLevel - 1];
        else
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.rankSprite[info.gearLevel - 1 < 0 ? 0 : info.gearLevel - 1];
        if (info.gearLevelUnLimit > 0)
        {
          this.gear.unlimit.SetActive(true);
          this.gear.unlimit.GetComponent<UI2DSprite>().sprite2D = this.gearUnlimitSprite[info.gearLevelUnLimit - 1];
        }
        else
          this.gear.unlimit.SetActive(false);
        this.EnableQuantity(0);
        if (this.currSort != ItemSortAndFilter.SORT_TYPES.HistoryGroupNumber)
          this.gear.sortRankMaxRank.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_RANK, (IDictionary) new Hashtable()
          {
            {
              (object) "now",
              (object) info.gearLevel
            },
            {
              (object) "max",
              (object) info.gearLevelLimit
            }
          }));
        else
          this.gear.sortRankMaxRank.SetTextLocalize(this.itemInfo.gear.history_group_number.ToString().PadLeft(4, '0'));
      }
      else if (info.isWeapon)
      {
        this.gear.rank.SetActive(false);
        this.gear.unlimit.SetActive(false);
        this.EnableQuantity(0);
        this.setActiveObject((Component) this.gear.attackClass, false);
      }
      else if (info.isReisou)
      {
        this.gear.rank.SetActive(true);
        this.gear.unlimit.SetActive(false);
        this.EnableQuantity(0);
        if (info.gearLevel < 10)
        {
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.rankSprite[info.gearLevel - 1 < 0 ? 0 : info.gearLevel - 1];
        }
        else
        {
          int num = info.gearLevel / 10;
          int index = info.gearLevel % 10;
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.rankSprite[num - 1];
          this.gear.rankLastDigit.SetActive(true);
          this.gear.rankLastDigit.GetComponent<UI2DSprite>().sprite2D = this.rankNumSprite[index];
        }
        this.setActiveObject((Component) this.gear.attackClass, false);
      }
      else
      {
        this.gear.rank.SetActive(false);
        this.gear.unlimit.SetActive(false);
        this.EnableQuantity(info.quantity);
        if (!info.isWeaponMaterial)
          this.setActiveObject((Component) this.gear.attackClass, false);
      }
      this.gear.favorite.SetActive(info.favorite);
      this.gear.broken.SetActive(info.broken);
      this.initManaseedsInfo(info);
      this.setReisouEffect(info);
    }
    else
    {
      e = this.InitBySupply(info.supply);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.EnableQuantity(info.quantity);
      this.supply.favorite.SetActive(info.favorite);
    }
    this.onClick = (System.Action<ItemIcon>) null;
  }

  public IEnumerator InitByPlayerItem(PlayerItem playerItem)
  {
    IEnumerator e = this.InitByItemInfo(new GameCore.ItemInfo(playerItem));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator InitByPlayerMaterialGear(PlayerMaterialGear playerItem)
  {
    IEnumerator e = this.InitByItemInfo(new GameCore.ItemInfo(playerItem, 0));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public IEnumerator InitBySupplyItem(SupplyItem supplyItem)
  {
    this.removeButton.SetActive(false);
    IEnumerator e = this.InitBySupply(supplyItem.Supply);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    this.EnableQuantity(supplyItem.ItemQuantity);
    if (this.itemInfo == null)
      this.itemInfo = new GameCore.ItemInfo(GameCore.ItemInfo.ItemType.Supply, 0);
    this.itemInfo.masterID = supplyItem.Supply.ID;
    this.supply.name.GetComponent<UILabel>().SetText(supplyItem.Supply.name);
    this.onClick = (System.Action<ItemIcon>) null;
  }

  public void InitByRemoveButton()
  {
    this.gear.root.SetActive(false);
    this.supply.root.SetActive(false);
    this.gear.button.onLongPress.Clear();
    this.supply.button.onLongPress.Clear();
    this.removeButton.SetActive(true);
  }

  private void InitManaseedsInfo(GearGear gear, int accessoryRemainAmount)
  {
    if ((this.ItemInfo == null || !this.ItemInfo.isWeaponMaterial) && (gear != null && gear.disappearance_num.HasValue))
    {
      this.gear.manaseedsBreakageRate.SetActive(false);
      this.gear.manaseedsDurabilityCount.SetActive(false);
      int num1 = gear.disappearance_num.Value;
      GameObject gameObject = (GameObject) null;
      int num2 = 0;
      switch (gear.disappearance_type_GearDisappearanceType)
      {
        case 1:
          this.gear.manaseedsDurabilityCount.SetActive(true);
          this.gear.manaseedsDurabilityCount_1.SetActive(false);
          this.gear.manaseedsDurabilityCount_10.SetActive(false);
          this.gear.manaseedsDurabilityCount_100.SetActive(false);
          num1 = accessoryRemainAmount;
          num2 = num1.ToString().Length;
          switch (num2)
          {
            case 1:
              gameObject = this.gear.manaseedsDurabilityCount_1;
              break;
            case 2:
              gameObject = this.gear.manaseedsDurabilityCount_10;
              break;
            case 3:
              gameObject = this.gear.manaseedsDurabilityCount_100;
              break;
          }
          break;
        case 2:
          this.gear.manaseedsBreakageRate.SetActive(true);
          num2 = num1.ToString().Length;
          switch (num2)
          {
            case 1:
              gameObject = this.gear.manaseedsBreakageRate_1;
              break;
            case 2:
              gameObject = this.gear.manaseedsBreakageRate_10;
              break;
          }
          break;
      }
      if (!((UnityEngine.Object) gameObject != (UnityEngine.Object) null))
        return;
      gameObject.SetActive(true);
      string[] strArray = new string[3]
      {
        "slc_num_digit_ones",
        "slc_num_digit_tens",
        "slc_num_digit_hundreds"
      };
      for (int index = 0; index < num2; ++index)
      {
        Transform transform = gameObject.transform.Find(strArray[index]);
        int result;
        if ((UnityEngine.Object) transform != (UnityEngine.Object) null && int.TryParse(num1.ToString().Substring(num2 - 1 - index, 1), out result))
        {
          UI2DSprite component = transform.GetComponent<UI2DSprite>();
          component.SetDimensions((int) this.numberSprite[result].textureRect.width, ItemIcon.ManaSeedsDurabilityFontSize);
          component.sprite2D = this.numberSprite[result];
        }
      }
    }
    else
    {
      this.gear.manaseedsBreakageRate.SetActive(false);
      this.gear.manaseedsDurabilityCount.SetActive(false);
    }
  }

  public void initManaseedsInfo(GameCore.ItemInfo info)
  {
    this.InitManaseedsInfo(info.gear, info.gearAccessoryRemainingAmount);
  }

  public void initManaseedsInfo(PlayerItem playerItem)
  {
    this.InitManaseedsInfo(playerItem.gear, playerItem.gear_accessory_remaining_amount);
  }

  public IEnumerator InitByGear(
    GearGear gear,
    CommonElement element = CommonElement.none,
    bool isWeaponMaterial = false,
    bool isReisouSet = false,
    bool isRouletteWheelIcon = false)
  {
    this.gear.dynReisouEffect.transform.Clear();
    if (gear != null)
    {
      this.gear.root.SetActive(true);
      this.supply.root.SetActive(false);
      if (gear.rarity.index > 0)
      {
        UI2DSprite component1 = this.gear.star.GetComponent<UI2DSprite>();
        Rect textureRect = this.raritySprite[gear.rarity.index - 1].textureRect;
        int width1 = (int) textureRect.width;
        textureRect = this.raritySprite[gear.rarity.index - 1].textureRect;
        int height1 = (int) textureRect.height;
        component1.SetDimensions(width1, height1);
        this.gear.star.GetComponent<UI2DSprite>().sprite2D = this.raritySprite[gear.rarity.index - 1];
        UnityEngine.Sprite sprite2D = this.gear.star.GetComponent<UI2DSprite>().sprite2D;
        UI2DSprite component2 = this.gear.sortRankMaxStar.GetComponent<UI2DSprite>();
        textureRect = sprite2D.textureRect;
        int width2 = (int) textureRect.width;
        textureRect = sprite2D.textureRect;
        int height2 = (int) textureRect.height;
        component2.SetDimensions(width2, height2);
        this.gear.sortRankMaxStar.sprite2D = sprite2D;
        UI2DSprite component3 = this.gear.starWIconNone.GetComponent<UI2DSprite>();
        textureRect = this.raritySprite[gear.rarity.index - 1].textureRect;
        int width3 = (int) textureRect.width;
        textureRect = this.raritySprite[gear.rarity.index - 1].textureRect;
        int height3 = (int) textureRect.height;
        component3.SetDimensions(width3, height3);
        this.gear.starWIconNone.GetComponent<UI2DSprite>().sprite2D = this.raritySprite[gear.rarity.index - 1];
      }
      else
      {
        this.gear.star.SetActive(false);
        this.gear.defaultGearTxt.SetActive(true);
        this.gear.starWIconNone.SetActive(false);
      }
      GearKind gearKind = gear.kind;
      this.SetGearType(gearKind, element);
      this.gear.favorite.SetActive(false);
      this.gear.rank.SetActive(false);
      this.gear.unlimit.SetActive(false);
      this.gear.broken.SetActive(false);
      this.gear.unknown.SetActive(false);
      if (gear.hasSpecificationOfEquipmentUnits)
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteSpecificationOfEquipmentUnits[gear.customize_flag];
      else
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[gear.customize_flag];
      UnityEngine.Sprite sprite;
      if (ItemIcon.gearCache.TryGetValue(gear.ID, out sprite))
      {
        this.gear.icon.sprite2D = sprite;
      }
      else
      {
        Future<UnityEngine.Sprite> spriteF = gear.LoadSpriteThumbnail();
        IEnumerator e = spriteF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.gear.icon.sprite2D = spriteF.Result;
        ItemIcon.gearCache[gear.ID] = spriteF.Result;
        spriteF = (Future<UnityEngine.Sprite>) null;
      }
      if (isReisouSet || gear.isReisou())
        this.setReisouEffect(gear.isReisou());
      if (gear.isReisou())
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteReisou;
      if (isRouletteWheelIcon)
        this.gear.attackClass.gameObject.SetActive(false);
      else if ((UnityEngine.Object) this.gear.attackClass != (UnityEngine.Object) null)
      {
        if (gearKind.is_attack && !gear.isReisou())
        {
          if (gear.hasAttackClass)
          {
            this.gear.attackClass.Initialize(gear.gearClassification.attack_classification, gear.attachedElement);
            this.gear.attackClass.gameObject.SetActive(true);
            goto label_27;
          }
          else if (gear.attachedElement != CommonElement.none)
          {
            this.setElementIcon(this.gear.attackClass.gameObject, gear.attachedElement);
            goto label_27;
          }
        }
        this.gear.attackClass.gameObject.SetActive(false);
      }
label_27:
      gearKind = (GearKind) null;
    }
    else
    {
      this.gear.root.SetActive(true);
      this.supply.root.SetActive(false);
      this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[0];
      this.gear.star.GetComponent<UI2DSprite>().sprite2D = this.raritySprite[0];
      this.gear.favorite.SetActive(false);
      this.gear.rank.SetActive(false);
      this.gear.unlimit.SetActive(false);
      this.gear.broken.SetActive(false);
      this.gear.unknown.SetActive(true);
      this.setActiveObject((Component) this.gear.attackClass, false);
    }
    if (isWeaponMaterial)
      this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteWeaponMaterial;
  }

  private void setElementIcon(GameObject go, CommonElement element)
  {
    UI2DSprite component = go.GetComponent<UI2DSprite>();
    if ((UnityEngine.Object) component != (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) ItemIcon.elementIconPrefab == (UnityEngine.Object) null)
        ItemIcon.elementIconPrefab = Resources.Load<GameObject>("Icons/CommonElementIcon");
      component.sprite2D = ItemIcon.elementIconPrefab.GetComponent<CommonElementIcon>().getIcon(element);
      go.SetActive(true);
    }
    else
      go.SetActive(false);
  }

  private void setReisouEffect(bool isReisou)
  {
    if (this.gear.dynReisouEffect.transform.childCount > 0)
      return;
    GameObject reisouEffect01Prefab;
    if (isReisou)
    {
      if ((UnityEngine.Object) ItemIcon.reisouEffect01Prefab == (UnityEngine.Object) null)
        ItemIcon.reisouEffect01Prefab = Resources.Load<GameObject>("Prefabs/ItemIcon/Reisou_Effect_01");
      reisouEffect01Prefab = ItemIcon.reisouEffect01Prefab;
    }
    else
    {
      if ((UnityEngine.Object) ItemIcon.buguReisouEffect01Prefab == (UnityEngine.Object) null)
        ItemIcon.buguReisouEffect01Prefab = Resources.Load<GameObject>("Prefabs/ItemIcon/BuguReisou_Effect_01");
      reisouEffect01Prefab = ItemIcon.buguReisouEffect01Prefab;
    }
    GameObject gameObject = reisouEffect01Prefab.Clone(this.gear.dynReisouEffect.transform);
    if (!((UnityEngine.Object) this.transform.parent != (UnityEngine.Object) null))
      return;
    float x = this.transform.parent.transform.localScale.x;
    Vector3 vector3 = new Vector3(x, x, x);
    ParticleSystem componentInChildren = gameObject.transform.GetComponentInChildren<ParticleSystem>();
    if (!((UnityEngine.Object) componentInChildren != (UnityEngine.Object) null))
      return;
    componentInChildren.transform.localScale = vector3;
  }

  private void setReisouEffect(GameCore.ItemInfo info)
  {
    if (!info.isReisou && !info.isEquipReisou)
      return;
    this.setReisouEffect(info.isReisou);
  }

  public IEnumerator InitByQuestDrop(QuestCommonDrop questDrop)
  {
    if (questDrop.entity_type == MasterDataTable.CommonRewardType.supply)
    {
      yield return (object) this.InitBySupply(MasterData.SupplySupply[questDrop.entity_id]);
    }
    else
    {
      GearGear gear = MasterData.GearGear[questDrop.entity_id];
      yield return (object) this.InitByGear(gear, gear.GetElement(), questDrop.entity_type == MasterDataTable.CommonRewardType.gear_body, false, false);
    }
  }

  public IEnumerator InitByShopContent(ShopContent content)
  {
    IEnumerator e;
    if (content.entity_type == MasterDataTable.CommonRewardType.supply)
    {
      SupplySupply supply = (SupplySupply) null;
      if (MasterData.SupplySupply.TryGetValue(content.entity_id, out supply))
      {
        e = this.InitBySupply(supply);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.EnableQuantity(0);
        this.BottomModeValue = ItemIcon.BottomMode.Nothing;
      }
      else
        this.SetEmpty(true);
    }
    else
    {
      GearGear gear = (GearGear) null;
      if (MasterData.GearGear.TryGetValue(content.entity_id, out gear))
      {
        e = this.InitByGear(gear, gear.GetElement(), content.entity_type == MasterDataTable.CommonRewardType.gear_body, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.QuantitySupply = false;
      }
      else
        this.SetEmpty(true);
    }
  }

  public IEnumerator InitByMaterialExchange(MasterDataTable.CommonRewardType type, int rewardID)
  {
    IEnumerator e;
    if (type == MasterDataTable.CommonRewardType.supply)
    {
      SupplySupply supply = (SupplySupply) null;
      if (MasterData.SupplySupply.TryGetValue(rewardID, out supply))
      {
        e = this.InitBySupply(supply);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.EnableQuantity(0);
        this.BottomModeValue = ItemIcon.BottomMode.Nothing;
      }
      else
        this.SetEmpty(true);
    }
    else
    {
      GearGear gear = (GearGear) null;
      if (MasterData.GearGear.TryGetValue(rewardID, out gear))
      {
        e = this.InitByGear(gear, gear.GetElement(), type == MasterDataTable.CommonRewardType.gear_body, false, false);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        this.QuantitySupply = false;
      }
      else
        this.SetEmpty(true);
    }
  }

  public IEnumerator InitForEquipGear()
  {
    this.gear.root.SetActive(true);
    this.supply.root.SetActive(false);
    this.gear.favorite.SetActive(false);
    this.gear.rank.SetActive(false);
    this.gear.unlimit.SetActive(false);
    this.gear.broken.SetActive(false);
    this.gear.unknown.SetActive(false);
    this.gear.item_back.SetActive(false);
    this.gear.backGround.SetActive(true);
    this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.nonBackSprite;
    this.gear.star.GetComponent<UI2DSprite>().sprite2D = this.raritySprite[0];
    this.gear.star.SetActive(false);
    this.gear.type.GetComponent<UI2DSprite>().sprite2D = this.nonTypeSprite;
    this.gear.type.SetActive(true);
    yield break;
  }

  public IEnumerator InitBySupply(SupplySupply supply)
  {
    this.supply.root.SetActive(true);
    this.gear.root.SetActive(false);
    UnityEngine.Sprite sprite;
    if (ItemIcon.supplyCache.TryGetValue(supply.ID, out sprite))
    {
      this.supply.icon.sprite2D = sprite;
    }
    else
    {
      Future<UnityEngine.Sprite> spriteF = supply.LoadSpriteThumbnail();
      IEnumerator e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      this.supply.icon.sprite2D = spriteF.Result;
      ItemIcon.supplyCache[supply.ID] = spriteF.Result;
      spriteF = (Future<UnityEngine.Sprite>) null;
    }
    ((IEnumerable<GameObject>) this.supply.rarities).ToggleOnce(supply.rarity.index - 1);
    this.supply.favorite.SetActive(false);
    this.selectQuantity.SetActive(false);
    this.supply.name.GetComponent<UILabel>().SetText(supply.name);
  }

  public void InitByItemInfoCache(GameCore.ItemInfo info)
  {
    this.itemInfo = info;
    this.removeButton.SetActive(false);
    this.gear.sortRankMaxInfo.SetActive(false);
    this.gear.rankLastDigit.SetActive(false);
    if (!info.isSupply)
    {
      this.InitByGearCache(info);
      if (info.isWeaponMaterial)
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteWeaponMaterial;
      else if (info.isReisou)
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteReisou;
      if (info.isWeapon && info.gear.disappearance_type_GearDisappearanceType == 0)
      {
        this.gear.rank.SetActive(true);
        if (info.gearLevel == info.gearLevelLimit)
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.limitRankSprite[info.gearLevel - 1 < 0 ? 0 : info.gearLevel - 1];
        else
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.rankSprite[info.gearLevel - 1 < 0 ? 0 : info.gearLevel - 1];
        if (info.gearLevelUnLimit > 0)
        {
          this.gear.unlimit.SetActive(true);
          this.gear.unlimit.GetComponent<UI2DSprite>().sprite2D = this.gearUnlimitSprite[info.gearLevelUnLimit - 1];
        }
        else
          this.gear.unlimit.SetActive(false);
        this.EnableQuantity(0);
        this.gear.sortRankMaxRank.SetTextLocalize(Consts.Format(Consts.GetInstance().BUGU_0059_RANK, (IDictionary) new Hashtable()
        {
          {
            (object) "now",
            (object) info.gearLevel
          },
          {
            (object) "max",
            (object) info.gearLevelLimit
          }
        }));
      }
      else if (info.isReisou)
      {
        this.gear.rank.SetActive(true);
        this.gear.unlimit.SetActive(false);
        this.EnableQuantity(0);
        if (info.gearLevel < 10)
        {
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.rankSprite[info.gearLevel - 1 < 0 ? 0 : info.gearLevel - 1];
        }
        else
        {
          int num = info.gearLevel / 10;
          int index = info.gearLevel % 10;
          this.gear.rank.GetComponent<UI2DSprite>().sprite2D = this.rankSprite[num - 1];
          this.gear.rankLastDigit.SetActive(true);
          this.gear.rankLastDigit.GetComponent<UI2DSprite>().sprite2D = this.rankNumSprite[index];
        }
        this.setActiveObject((Component) this.gear.attackClass, false);
      }
      else
      {
        this.gear.rank.SetActive(false);
        this.gear.unlimit.SetActive(false);
        this.EnableQuantity(info.quantity);
        if (!info.isWeaponMaterial)
          this.setActiveObject((Component) this.gear.attackClass, false);
      }
      this.gear.favorite.SetActive(info.favorite);
      this.gear.broken.SetActive(info.broken);
      this.initManaseedsInfo(info);
      this.setReisouEffect(info);
    }
    else
    {
      this.InitBySupplyCache(info.supply);
      this.EnableQuantity(info.quantity);
      this.supply.favorite.SetActive(info.favorite);
    }
    this.onClick = (System.Action<ItemIcon>) null;
  }

  public void InitBySupplyItemCache(SupplyItem supplyItem)
  {
    this.removeButton.SetActive(false);
    this.InitBySupplyCache(supplyItem.Supply);
    this.EnableQuantity(supplyItem.ItemQuantity);
    this.itemInfo.masterID = supplyItem.Supply.ID;
    this.supply.name.GetComponent<UILabel>().SetText(supplyItem.Supply.name);
    this.onClick = (System.Action<ItemIcon>) null;
  }

  public void InitByGearCache(GearGear info)
  {
    this.gear.dynReisouEffect.transform.Clear();
    if (info != null)
    {
      this.gear.root.SetActive(true);
      this.supply.root.SetActive(false);
      this.gear.icon.sprite2D = ItemIcon.gearCache[info.ID];
      this.gear.star.GetComponent<UI2DSprite>().SetDimensions((int) this.raritySprite[info.rarity.index - 1].textureRect.width, (int) this.raritySprite[info.rarity.index - 1].textureRect.height);
      this.gear.star.GetComponent<UI2DSprite>().sprite2D = this.raritySprite[info.rarity.index - 1];
      UnityEngine.Sprite sprite2D = this.gear.star.GetComponent<UI2DSprite>().sprite2D;
      UI2DSprite component = this.gear.sortRankMaxStar.GetComponent<UI2DSprite>();
      Rect textureRect = sprite2D.textureRect;
      int width = (int) textureRect.width;
      textureRect = sprite2D.textureRect;
      int height = (int) textureRect.height;
      component.SetDimensions(width, height);
      this.gear.sortRankMaxStar.sprite2D = sprite2D;
      GearKind kind = info.kind;
      this.SetGearType(kind, info.GetElement());
      if (info.hasSpecificationOfEquipmentUnits)
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSpriteSpecificationOfEquipmentUnits[info.customize_flag];
      else
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[info.customize_flag];
      if ((UnityEngine.Object) this.gear.attackClass != (UnityEngine.Object) null)
      {
        if (kind.is_attack)
        {
          if (info.hasAttackClass)
          {
            this.gear.attackClass.Initialize(info.gearClassification.attack_classification, info.attachedElement);
            this.gear.attackClass.gameObject.SetActive(true);
            goto label_11;
          }
          else if (info.attachedElement != CommonElement.none)
          {
            this.setElementIcon(this.gear.attackClass.gameObject, info.attachedElement);
            goto label_11;
          }
        }
        this.gear.attackClass.gameObject.SetActive(false);
      }
label_11:
      this.gear.favorite.SetActive(false);
      this.gear.rank.SetActive(false);
      this.gear.unlimit.SetActive(false);
      this.gear.broken.SetActive(false);
      this.gear.unknown.SetActive(false);
    }
    else
    {
      this.gear.root.SetActive(true);
      this.supply.root.SetActive(false);
      this.gear.star.GetComponent<UI2DSprite>().sprite2D = this.raritySprite[0];
      this.gear.favorite.SetActive(false);
      this.gear.rank.SetActive(false);
      this.gear.unlimit.SetActive(false);
      this.gear.broken.SetActive(false);
      this.gear.unknown.SetActive(true);
      this.setActiveObject((Component) this.gear.attackClass, false);
    }
  }

  public void InitByGearCache(GameCore.ItemInfo info)
  {
    this.InitByGearCache(info.gear);
  }

  public void InitBySupplyCache(SupplySupply supply)
  {
    this.supply.root.SetActive(true);
    this.gear.root.SetActive(false);
    this.supply.icon.sprite2D = ItemIcon.supplyCache[supply.ID];
    ((IEnumerable<GameObject>) this.supply.rarities).ToggleOnce(supply.rarity.index - 1);
    this.supply.favorite.SetActive(false);
    this.selectQuantity.SetActive(false);
    this.supply.name.GetComponent<UILabel>().SetText(supply.name);
  }

  public static IEnumerator LoadSprite(GameCore.ItemInfo info)
  {
    Future<UnityEngine.Sprite> spriteF;
    IEnumerator e;
    if (info != null && !ItemIcon.IsCache(info))
    {
      if (!info.isSupply)
      {
        GearGear gear = info.gear;
        spriteF = gear.LoadSpriteThumbnail();
        e = spriteF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        ItemIcon.gearCache[gear.ID] = spriteF.Result;
        gear = (GearGear) null;
        spriteF = (Future<UnityEngine.Sprite>) null;
      }
      else
      {
        SupplySupply supply = info.supply;
        spriteF = supply.LoadSpriteThumbnail();
        e = spriteF.Wait();
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        ItemIcon.supplyCache[supply.ID] = spriteF.Result;
        supply = (SupplySupply) null;
        spriteF = (Future<UnityEngine.Sprite>) null;
      }
    }
  }

  public static IEnumerator LoadSprite(GearGear gear)
  {
    if (!ItemIcon.IsCache(gear))
    {
      Future<UnityEngine.Sprite> spriteF = gear.LoadSpriteThumbnail();
      IEnumerator e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ItemIcon.gearCache[gear.ID] = spriteF.Result;
    }
  }

  public static IEnumerator LoadSprite(SupplyItem supply)
  {
    if (supply != null && !ItemIcon.IsCache(supply) && supply != null)
    {
      Future<UnityEngine.Sprite> spriteF = supply.Supply.LoadSpriteThumbnail();
      IEnumerator e = spriteF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      ItemIcon.supplyCache[supply.Supply.ID] = spriteF.Result;
      spriteF = (Future<UnityEngine.Sprite>) null;
    }
  }

  public void DisableLongPressEvent()
  {
    this.gear.button.gameObject.SetActive(false);
    this.supply.button.gameObject.SetActive(false);
  }

  public void EnableLongPressEvent(bool isLimited = false)
  {
    if (this.itemInfo != null)
    {
      if (this.itemInfo.isWeapon)
      {
        this.gear.button.gameObject.SetActive(true);
        EventDelegate.Set(this.gear.button.onLongPress, (EventDelegate.Callback) (() =>
        {
          if (Singleton<NGGameDataManager>.GetInstance().IsEarth)
          {
            Unit05443Scene.changeSceneLimited(true, this.itemInfo);
          }
          else
          {
            if (Singleton<NGGameDataManager>.GetInstance().IsSea)
              Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
            if (isLimited)
              Unit00443Scene.changeSceneLimited(true, this.itemInfo);
            else
              Unit00443Scene.changeScene(true, this.itemInfo);
          }
        }));
      }
      else if (this.itemInfo.isSupply)
      {
        this.supply.button.gameObject.SetActive(true);
        EventDelegate.Set(this.supply.button.onLongPress, (EventDelegate.Callback) (() =>
        {
          if (Singleton<NGGameDataManager>.GetInstance().IsSea)
            Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
          Bugu00561Scene.changeScene(true, this.itemInfo);
        }));
      }
      else
      {
        this.gear.button.gameObject.SetActive(true);
        EventDelegate.Set(this.gear.button.onLongPress, (EventDelegate.Callback) (() =>
        {
          if (Singleton<NGGameDataManager>.GetInstance().IsSea)
            Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
          Bugu00561Scene.changeScene(true, this.itemInfo);
        }));
      }
    }
    else
      Debug.LogError((object) "playerItem NULL");
  }

  public void EnableLongPressEvent(bool isGear, System.Action<ItemIcon> action)
  {
    if (isGear)
    {
      this.gear.button.gameObject.SetActive(true);
      EventDelegate.Set(this.gear.button.onLongPress, (EventDelegate.Callback) (() => action(this)));
    }
    else
    {
      this.supply.button.gameObject.SetActive(true);
      EventDelegate.Set(this.supply.button.onLongPress, (EventDelegate.Callback) (() => action(this)));
    }
  }

  public void EnableLongPressEvent(System.Action<GameCore.ItemInfo> action)
  {
    if (this.itemInfo == null)
      return;
    if (this.itemInfo.isSupply)
    {
      this.supply.button.gameObject.SetActive(true);
      EventDelegate.Set(this.supply.button.onLongPress, (EventDelegate.Callback) (() => action(this.itemInfo)));
    }
    else
    {
      this.gear.button.gameObject.SetActive(true);
      EventDelegate.Set(this.gear.button.onLongPress, (EventDelegate.Callback) (() => action(this.itemInfo)));
    }
  }

  public void EnableLongPressEventEmptyGear(System.Action<int> action, int index)
  {
    this.gear.button.gameObject.SetActive(true);
    EventDelegate.Set(this.gear.button.onLongPress, (EventDelegate.Callback) (() => action(index)));
  }

  public void ReleaseClickEvent()
  {
    this.gear.button.onClick.Clear();
    this.supply.button.onClick.Clear();
  }

  public bool Selected
  {
    get
    {
      return this.EnabledGear ? this.gear.selectedBack.activeSelf : this.supply.selectedBack.activeSelf;
    }
  }

  public void Deselect()
  {
    if (this.EnabledGear)
    {
      this.gear.selectedBack.SetActive(false);
      this.gear.selectedNum.SetActive(false);
    }
    else
    {
      this.supply.selectedBack.SetActive(false);
      ((IEnumerable<GameObject>) this.supply.selectedSupply).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(false)));
    }
  }

  public void DeselectByCheckIcon()
  {
    if (!this.EnabledGear)
      return;
    this.gear.selectedBack.SetActive(false);
    this.gear.selectedCheck.SetActive(false);
  }

  public void Select(int selectCount)
  {
    this.Deselect();
    if (this.EnabledGear)
    {
      this.gear.selectedBack.SetActive(true);
      this.gear.selectedNum.SetActive(true);
      UI2DSprite component = this.gear.selectedNum.GetComponent<UI2DSprite>();
      component.SetDimensions((int) this.selectNumSprite[selectCount].textureRect.width, ItemIcon.SelectedIndexFontSize);
      component.sprite2D = this.selectNumSprite[selectCount];
    }
    else
    {
      this.supply.selectedBack.SetActive(true);
      this.supply.selectedSupply[selectCount].SetActive(true);
    }
  }

  public void SelectByCheckIcon()
  {
    this.DeselectByCheckIcon();
    if (!this.EnabledGear)
      return;
    this.gear.selectedBack.SetActive(true);
    this.gear.selectedCheck.SetActive(true);
  }

  public UnityEngine.Sprite IconSprite
  {
    get
    {
      return !this.EnabledGear ? this.supply.icon.sprite2D : this.gear.icon.sprite2D;
    }
  }

  public System.Action<ItemIcon> onClick
  {
    get
    {
      return this.onClick_;
    }
    set
    {
      this.onClick_ = value;
      if (this.onClick_ == null)
        return;
      EventDelegate.Set(this.gear.button.onClick, (EventDelegate.Callback) (() => this.onClick_(this)));
      EventDelegate.Set(this.supply.button.onClick, (EventDelegate.Callback) (() => this.onClick_(this)));
      EventDelegate.Set(this.removeButton.GetComponent<UIButton>().onClick, (EventDelegate.Callback) (() => this.onClick_(this)));
    }
  }

  private void SetGearType(GearKind kind, CommonElement element)
  {
    this.gear.type.SetActive(false);
    if (!this.gear.root.activeSelf)
      return;
    if (!kind.isEquip)
    {
      this.gear.star.transform.localPosition = new Vector3(-12f, 0.0f);
    }
    else
    {
      this.gear.star.transform.localPosition = Vector3.zero;
      this.gear.type.GetComponent<UI2DSprite>().sprite2D = GearKindIcon.LoadSprite(kind.Enum, element);
      this.gear.type.SetActive(true);
    }
  }

  private void SetCross(ItemIcon.CounterDigits digits)
  {
    ((IEnumerable<GameObject>) new GameObject[4]
    {
      this.m_crossForOneDigitCount,
      this.m_crossForTwoDigitsCount,
      this.m_crossForThreeDigitsCount,
      this.m_crossForFourDigitsCount
    }).ToggleOnceEx((int) digits);
  }

  public void HideCounter()
  {
    this.QuantitySupply = false;
    if ((UnityEngine.Object) this.m_crossForOneDigitCount != (UnityEngine.Object) null)
      this.m_crossForOneDigitCount.SetActive(false);
    if ((UnityEngine.Object) this.m_crossForTwoDigitsCount != (UnityEngine.Object) null)
      this.m_crossForTwoDigitsCount.SetActive(false);
    if ((UnityEngine.Object) this.m_crossForThreeDigitsCount != (UnityEngine.Object) null)
      this.m_crossForThreeDigitsCount.SetActive(false);
    if ((UnityEngine.Object) this.m_crossForFourDigitsCount != (UnityEngine.Object) null)
      this.m_crossForFourDigitsCount.SetActive(false);
    ((IEnumerable<GameObject>) this.m_onesDigit).ToggleOnceEx(-1);
    ((IEnumerable<GameObject>) this.m_tensDigit).ToggleOnceEx(-1);
    ((IEnumerable<GameObject>) this.m_hundredsDigit).ToggleOnceEx(-1);
    ((IEnumerable<GameObject>) this.m_thousandsDigit).ToggleOnceEx(-1);
  }

  public void EnableQuantity(int quantity)
  {
    if (quantity <= 0)
    {
      this.HideCounter();
    }
    else
    {
      if (quantity > 9999)
        quantity = 9999;
      int num1 = quantity % 10;
      int num2 = quantity % 100 / 10;
      int num3 = quantity % 1000 / 100;
      int num4 = quantity % 10000 / 1000;
      ItemIcon.CounterDigits digits = ItemIcon.CounterDigits.OneDigit;
      if (quantity < 1)
      {
        this.HideCounter();
      }
      else
      {
        if (quantity >= 10)
          digits = quantity >= 100 ? (quantity >= 1000 ? ItemIcon.CounterDigits.FourDigits : ItemIcon.CounterDigits.ThreeDigits) : ItemIcon.CounterDigits.TwoDigits;
        this.SetCross(digits);
        this.SetOnesDigit(num1);
        this.SetTensDigit(num2, digits);
        this.SetHundredsDigit(num3, digits);
        this.SetThousandsDigit(num4, digits);
        this.QuantitySupply = true;
      }
    }
  }

  public void SetQuantityPositionY(float y)
  {
    Transform transform = this.quantity.transform;
    transform.localPosition = new Vector3(transform.localPosition.x, y, transform.localPosition.z);
  }

  private void SetOnesDigit(int num)
  {
    if (num > -1 && num < 10)
    {
      ((IEnumerable<GameObject>) this.m_onesDigit).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetTensDigit(int num, ItemIcon.CounterDigits digits)
  {
    if (num > -1 && num < 10)
    {
      if (ItemIcon.CounterDigits.TwoDigits > digits)
        ((IEnumerable<GameObject>) this.m_tensDigit).ToggleOnceEx(-1);
      else
        ((IEnumerable<GameObject>) this.m_tensDigit).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetHundredsDigit(int num, ItemIcon.CounterDigits digits)
  {
    if (num > -1 && num < 10)
    {
      if (ItemIcon.CounterDigits.ThreeDigits > digits)
        ((IEnumerable<GameObject>) this.m_hundredsDigit).ToggleOnceEx(-1);
      else
        ((IEnumerable<GameObject>) this.m_hundredsDigit).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetThousandsDigit(int num, ItemIcon.CounterDigits digits)
  {
    if (num > -1 && num < 10)
    {
      if (ItemIcon.CounterDigits.FourDigits > digits)
        ((IEnumerable<GameObject>) this.m_thousandsDigit).ToggleOnceEx(-1);
      else
        ((IEnumerable<GameObject>) this.m_thousandsDigit).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetCrossBonus(ItemIcon.CounterDigits digits)
  {
    ((IEnumerable<GameObject>) new GameObject[4]
    {
      this.m_crossForOneDigitCount_bonus,
      this.m_crossForTwoDigitsCount_bonus,
      this.m_crossForThreeDigitsCount_bonus,
      this.m_crossForFourDigitsCount_bonus
    }).ToggleOnceEx((int) digits);
  }

  private void HideCounterBonus()
  {
    this.QuantitySupplyBonus = false;
    if ((UnityEngine.Object) this.m_crossForOneDigitCount_bonus != (UnityEngine.Object) null)
      this.m_crossForOneDigitCount_bonus.SetActive(false);
    if ((UnityEngine.Object) this.m_crossForTwoDigitsCount_bonus != (UnityEngine.Object) null)
      this.m_crossForTwoDigitsCount_bonus.SetActive(false);
    if ((UnityEngine.Object) this.m_crossForThreeDigitsCount_bonus != (UnityEngine.Object) null)
      this.m_crossForThreeDigitsCount_bonus.SetActive(false);
    if ((UnityEngine.Object) this.m_crossForFourDigitsCount_bonus != (UnityEngine.Object) null)
      this.m_crossForFourDigitsCount_bonus.SetActive(false);
    ((IEnumerable<GameObject>) this.m_onesDigit_bonus).ToggleOnceEx(-1);
    ((IEnumerable<GameObject>) this.m_tensDigit_bonus).ToggleOnceEx(-1);
    ((IEnumerable<GameObject>) this.m_hundredsDigit_bonus).ToggleOnceEx(-1);
    ((IEnumerable<GameObject>) this.m_thousandsDigit_bonus).ToggleOnceEx(-1);
  }

  public void EnableQuantityBonus(int quantity)
  {
    if (quantity > 9999)
      quantity = 9999;
    int num1 = quantity % 10;
    int num2 = quantity % 100 / 10;
    int num3 = quantity % 1000 / 100;
    int num4 = quantity % 10000 / 1000;
    ItemIcon.CounterDigits digits = ItemIcon.CounterDigits.OneDigit;
    if (quantity >= 10)
      digits = quantity >= 100 ? (quantity >= 1000 ? ItemIcon.CounterDigits.FourDigits : ItemIcon.CounterDigits.ThreeDigits) : ItemIcon.CounterDigits.TwoDigits;
    this.SetCrossBonus(digits);
    this.SetOnesDigitBonus(num1);
    this.SetTensDigitBonus(num2, digits);
    this.SetHundredsDigitBonus(num3, digits);
    this.SetThousandsDigitBonus(num4, digits);
    this.QuantitySupplyBonus = true;
  }

  private void SetOnesDigitBonus(int num)
  {
    if (num > -1 && num < 10)
    {
      ((IEnumerable<GameObject>) this.m_onesDigit_bonus).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetTensDigitBonus(int num, ItemIcon.CounterDigits digits)
  {
    if (num > -1 && num < 10)
    {
      if (ItemIcon.CounterDigits.TwoDigits > digits)
        ((IEnumerable<GameObject>) this.m_tensDigit_bonus).ToggleOnceEx(-1);
      else
        ((IEnumerable<GameObject>) this.m_tensDigit_bonus).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetHundredsDigitBonus(int num, ItemIcon.CounterDigits digits)
  {
    if (num > -1 && num < 10)
    {
      if (ItemIcon.CounterDigits.ThreeDigits > digits)
        ((IEnumerable<GameObject>) this.m_hundredsDigit_bonus).ToggleOnceEx(-1);
      else
        ((IEnumerable<GameObject>) this.m_hundredsDigit_bonus).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  private void SetThousandsDigitBonus(int num, ItemIcon.CounterDigits digits)
  {
    if (num > -1 && num < 10)
    {
      if (ItemIcon.CounterDigits.FourDigits > digits)
        ((IEnumerable<GameObject>) this.m_thousandsDigit_bonus).ToggleOnceEx(-1);
      else
        ((IEnumerable<GameObject>) this.m_thousandsDigit_bonus).ToggleOnceEx(num);
    }
    else
    {
      if (!Debug.isDebugBuild)
        return;
      Debug.LogError((object) ("Illegal parameter (num): " + (object) num));
    }
  }

  public void SelectedQuantity(int quantity)
  {
    if (quantity == 0)
    {
      this.SelectQuantity = false;
    }
    else
    {
      this.SelectQuantity = true;
      if (quantity > 99)
      {
        Debug.LogWarning((object) ("set quantity over 99, count=" + (object) quantity));
        quantity = 99;
      }
      int index1 = quantity % 10;
      int index2 = Mathf.FloorToInt((float) (quantity / 10));
      for (int index3 = 0; index3 <= 9; ++index3)
      {
        this.selectedRightNum[index3].SetActive(false);
        this.selectedLeftNum[index3].SetActive(false);
      }
      this.selectedRightNum[index1].SetActive(true);
      if (index2 != 0)
        this.selectedLeftNum[index2].SetActive(true);
      else
        this.selectedLeftNum[index2].SetActive(true);
      this.checkmark.SetActive(true);
    }
  }

  public void SetEmpty(bool empty)
  {
    if (empty)
    {
      this.gear.dynReisouEffect.transform.Clear();
      if (this.EnabledGear)
      {
        this.supply.root.SetActive(false);
        this.gear.item_back.SetActive(true);
        this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.nonBackSprite;
        this.gear.bottom.SetActive(true);
        this.gear.bottomWIconNone.SetActive(false);
        this.gear.broken.SetActive(false);
        this.gear.button.transform.gameObject.SetActive(false);
        this.gear.favorite.SetActive(false);
        this.gear.forbattle.SetActive(false);
        this.gear.icon.transform.gameObject.SetActive(false);
        this.gear.star.SetActive(false);
        this.gear.rank.SetActive(false);
        this.gear.unlimit.SetActive(false);
        this.gear.type.GetComponent<UI2DSprite>().sprite2D = this.nonTypeSprite;
        this.gear.type.SetActive(true);
        this.gear.unknown.SetActive(false);
        this.gear.manaseedsBreakageRate.SetActive(false);
        this.gear.manaseedsDurabilityCount.SetActive(false);
        this.setActiveObject((Component) this.gear.attackClass, false);
      }
      else
      {
        this.gear.root.SetActive(false);
        this.supply.back.SetActive(true);
        this.supply.bottom.SetActive(true);
        this.supply.button.transform.gameObject.SetActive(false);
        this.supply.equals.SetActive(false);
        this.supply.favorite.SetActive(false);
        this.supply.forbattle.SetActive(false);
        this.supply.icon.transform.gameObject.SetActive(false);
        this.quantity.SetActive(false);
        ((IEnumerable<GameObject>) this.supply.rarities).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(false)));
        this.supply.name.SetActive(false);
      }
      this.EnableQuantity(0);
    }
    else if (this.EnabledGear)
    {
      this.gear.item_back.SetActive(true);
      this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[0];
      this.gear.bottom.SetActive(true);
      this.gear.bottomWIconNone.SetActive(false);
      this.gear.broken.SetActive(true);
      this.gear.button.transform.gameObject.SetActive(true);
      this.gear.favorite.SetActive(true);
      this.gear.forbattle.SetActive(false);
      this.gear.icon.transform.gameObject.SetActive(true);
      this.gear.star.SetActive(true);
      this.gear.rank.SetActive(false);
      this.gear.unlimit.SetActive(false);
      this.gear.type.GetComponent<UI2DSprite>().sprite2D = this.nonTypeSprite;
      this.gear.type.SetActive(false);
      this.setActiveObject((Component) this.gear.attackClass, false);
      this.gear.unknown.SetActive(true);
    }
    else
    {
      this.supply.back.SetActive(true);
      this.supply.bottom.SetActive(true);
      this.supply.button.transform.gameObject.SetActive(true);
      this.supply.equals.SetActive(true);
      this.supply.favorite.SetActive(true);
      this.supply.forbattle.SetActive(false);
      this.supply.icon.transform.gameObject.SetActive(true);
      this.quantity.SetActive(true);
      ((IEnumerable<GameObject>) this.supply.rarities).ForEach<GameObject>((System.Action<GameObject>) (x => x.SetActive(true)));
      this.supply.name.SetActive(true);
    }
  }

  private void setActiveObject(Component com, bool bActive)
  {
    if (!((UnityEngine.Object) com != (UnityEngine.Object) null))
      return;
    com.gameObject.SetActive(bActive);
  }

  public void setEquipPlus(bool bl)
  {
    this.gear.equipPlus.SetActive(bl);
  }

  public static bool IsCache(GameCore.ItemInfo info)
  {
    if (info == null)
      return false;
    return !info.isSupply ? ItemIcon.gearCache.ContainsKey(info.gear.ID) : ItemIcon.supplyCache.ContainsKey(info.supply.ID);
  }

  public static bool IsCache(PlayerItem playerItem)
  {
    if (playerItem == (PlayerItem) null)
      return false;
    return playerItem.gear != null ? ItemIcon.gearCache.ContainsKey(playerItem.gear.ID) : ItemIcon.supplyCache.ContainsKey(playerItem.supply.ID);
  }

  public static bool IsCache(SupplyItem supply)
  {
    return supply != null && ItemIcon.supplyCache.ContainsKey(supply.Supply.ID);
  }

  public static bool IsCache(GearGear gear)
  {
    return ItemIcon.gearCache.ContainsKey(gear.ID);
  }

  public static bool IsCache(SupplySupply supply)
  {
    return ItemIcon.supplyCache.ContainsKey(supply.ID);
  }

  public static void ClearCache()
  {
    if (ItemIcon.IsPoolCache)
    {
      ItemIcon.IsPoolCache = false;
      if (!PerformanceConfig.GetInstance().IsLowMemory)
        return;
    }
    ItemIcon.gearCache.Clear();
    ItemIcon.supplyCache.Clear();
    ItemIcon.elementIconPrefab = (GameObject) null;
    ItemIcon.reisouEffect01Prefab = (GameObject) null;
    ItemIcon.buguReisouEffect01Prefab = (GameObject) null;
  }

  public bool isButtonActive
  {
    set
    {
      if (this.EnabledGear)
        this.gear.button.transform.gameObject.SetActive(value);
      else
        this.supply.button.transform.gameObject.SetActive(value);
    }
  }

  public bool isBackActive
  {
    set
    {
      if (this.EnabledGear)
      {
        this.gear.item_back.SetActive(value);
        if (value)
          this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.nonBackSprite;
        else
          this.gear.backGround.GetComponent<UI2DSprite>().sprite2D = this.backSprite[0];
      }
      else
        this.supply.back.SetActive(value);
    }
  }

  public void ShowBottomInfo(ItemSortAndFilter.SORT_TYPES sort)
  {
    if (this.itemInfo == null || this.itemInfo.gear == null)
      return;
    bool flag1 = this.IsRankGear(this.itemInfo);
    this.currSort = sort;
    bool flag2 = sort == ItemSortAndFilter.SORT_TYPES.RankMax;
    if (sort == ItemSortAndFilter.SORT_TYPES.HistoryGroupNumber)
    {
      this.gear.rank.SetActive(false);
      this.gear.sortRankMaxInfo.SetActive(true);
      this.gear.star.SetActive(false);
      this.gear.sortRankMaxRank.SetTextLocalize(this.itemInfo.gear.history_group_number.ToString().PadLeft(4, '0'));
    }
    else if (flag1)
    {
      this.gear.rank.SetActive(!flag2);
      this.gear.sortRankMaxInfo.SetActive(flag2);
      this.gear.star.SetActive(!flag2);
    }
    else
    {
      this.gear.rank.SetActive(false);
      this.gear.sortRankMaxInfo.SetActive(false);
      this.gear.star.SetActive(true);
    }
  }

  private bool IsRankGear(GameCore.ItemInfo item)
  {
    return item != null && !item.isWeaponMaterial && (item.gear != null && item.gear.kind.isEquip) && item.gear.disappearance_type_GearDisappearanceType == 0;
  }

  public void ShowInRecipe()
  {
    this.EnableQuantity(0);
    this.gear.unlimit.SetActive(false);
  }

  public void setEquipReisouDisp()
  {
    this.ShowBottomInfo(ItemSortAndFilter.SORT_TYPES.Rarity);
    this.gear.rank.SetActive(false);
    this.gear.rankLastDigit.SetActive(false);
  }

  [Serializable]
  public class SpriteArray
  {
    [SerializeField]
    private UnityEngine.Sprite[] sprites_;
    private UnityEngine.Sprite errorSprite_;

    public UnityEngine.Sprite this[int i]
    {
      get
      {
        UnityEngine.Sprite sprite = this.sprites_ == null || i < 0 || i >= this.sprites_.Length ? (UnityEngine.Sprite) null : this.sprites_[i];
        if ((UnityEngine.Object) sprite != (UnityEngine.Object) null)
          return sprite;
        if (this.sprites_ == null)
          Debug.LogError((object) "SpriteArray is null");
        else
          Debug.LogError((object) string.Format("SpriteArray(Length={0}) index(={1}) is out of range", (object) this.sprites_.Length, (object) i));
        return this.errorSprite();
      }
    }

    private UnityEngine.Sprite errorSprite()
    {
      if ((UnityEngine.Object) this.errorSprite_ != (UnityEngine.Object) null)
        return this.errorSprite_;
      Texture2D texture = Resources.Load<Texture2D>("Sprites/1x1_alpha0");
      this.errorSprite_ = UnityEngine.Sprite.Create(texture, new Rect(0.0f, 0.0f, (float) texture.width, (float) texture.height), new Vector2(0.5f, 0.5f), 1f, 100U, SpriteMeshType.FullRect);
      return this.errorSprite_;
    }
  }

  private enum CounterDigits
  {
    OneDigit,
    TwoDigits,
    ThreeDigits,
    FourDigits,
  }

  public enum Sort
  {
    RARITY,
    GETORDER,
    CATEGORY,
    FAVORITE,
    MAX,
  }

  [Serializable]
  public class Gear
  {
    public GameObject root;
    public UI2DSprite icon;
    public LongPressButton button;
    public GameObject favorite;
    public GameObject rank;
    public GameObject rankLastDigit;
    public GameObject star;
    public GameObject sortRankMaxInfo;
    public UI2DSprite sortRankMaxStar;
    public UILabel sortRankMaxRank;
    public GameObject type;
    public GameObject broken;
    public GameObject bottom;
    public GameObject bottomWIconNone;
    public GameObject starWIconNone;
    public GameObject unknown;
    public GameObject forbattle;
    public GameObject item_back;
    public GameObject selectedNum;
    public GameObject selectedBack;
    public GameObject selectedCheck;
    public GameObject newGear;
    public GameObject equipPlus;
    public GameObject defaultGearTxt;
    public GameObject backGround;
    public GameObject manaseedsDurabilityCount;
    public GameObject manaseedsBreakageRate;
    public GameObject manaseedsDurabilityCount_1;
    public GameObject manaseedsDurabilityCount_10;
    public GameObject manaseedsDurabilityCount_100;
    public GameObject manaseedsBreakageRate_1;
    public GameObject manaseedsBreakageRate_10;
    public GameObject unlimit;
    public AttackClassIcon attackClass;
    public GameObject dynReisouEffect;
  }

  [Serializable]
  public class Supply
  {
    public GameObject root;
    public UI2DSprite icon;
    public LongPressButton button;
    public GameObject bottom;
    public GameObject favorite;
    public GameObject[] rarities;
    public GameObject equals;
    public Transform equalsPos;
    public GameObject forbattle;
    public GameObject back;
    public GameObject[] selectedSupply;
    public GameObject selectedBack;
    public GameObject name;
    public GameObject newSupply;
  }

  public enum BottomMode
  {
    Nothing,
    Visible,
    Visible_wIconNone,
  }
}
