﻿// Decompiled with JetBrains decompiler
// Type: GuildMapUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GuildMapUI
{
  public UIButton memberButton;
  [SerializeField]
  public GameObject guildBasePosition;
  public Guild0282GuildBase guildBase;
  [SerializeField]
  public List<GameObject> memberBasePosition;
  public List<Guild0282MemberBase> memberBaseList;
  public GameObject objectParent;

  public void SetActive(bool flag)
  {
    this.objectParent.SetActive(flag);
    this.memberButton.gameObject.SetActive(flag);
  }
}
