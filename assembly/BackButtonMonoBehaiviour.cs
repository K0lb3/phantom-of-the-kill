﻿// Decompiled with JetBrains decompiler
// Type: BackButtonMonoBehaiviour
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public abstract class BackButtonMonoBehaiviour : MonoBehaviour
{
  protected virtual void Update()
  {
  }

  public abstract void onBackButton();

  protected void showBackKeyToast()
  {
    ToastMessage.showBackKeyToast();
  }
}
