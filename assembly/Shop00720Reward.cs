﻿// Decompiled with JetBrains decompiler
// Type: Shop00720Reward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using System.Collections;
using UnityEngine;

public class Shop00720Reward : MonoBehaviour
{
  [SerializeField]
  private GameObject Thumbnail;
  [SerializeField]
  private UILabel Label;

  public IEnumerator Init(
    MasterDataTable.CommonRewardType rewardType,
    int rewardID,
    int quantity,
    string txt)
  {
    this.Label.SetTextLocalize(txt);
    IEnumerator e = this.Thumbnail.GetOrAddComponent<CreateIconObject>().CreateThumbnail(rewardType, rewardID, quantity, true, true, new CommonQuestType?(), false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }
}
