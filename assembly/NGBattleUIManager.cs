﻿// Decompiled with JetBrains decompiler
// Type: NGBattleUIManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;

public class NGBattleUIManager : BattleManagerBase
{
  public BattleUIController controller;
  public BattleStateController stateController;

  public override IEnumerator initialize(BattleInfo battleInfo, BE env_ = null)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    NGBattleUIManager ngBattleUiManager = this;
    if (num != 0)
      return false;
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    ngBattleUiManager.controller = ngBattleUiManager.gameObject.AddComponent<BattleUIController>();
    ngBattleUiManager.stateController = ngBattleUiManager.gameObject.AddComponent<BattleStateController>();
    return false;
  }

  public override IEnumerator cleanup()
  {
    this.controller = (BattleUIController) null;
    this.stateController = (BattleStateController) null;
    yield break;
  }
}
