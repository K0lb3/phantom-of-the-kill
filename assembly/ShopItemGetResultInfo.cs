﻿// Decompiled with JetBrains decompiler
// Type: ShopItemGetResultInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

public class ShopItemGetResultInfo
{
  public MasterDataTable.CommonRewardType rewardType;
  public int rewardId;
  public int quantity;

  public ShopItemGetResultInfo(MasterDataTable.CommonRewardType type, int reward_id, int quantity)
  {
    this.rewardId = reward_id;
    this.rewardType = type;
    this.quantity = quantity;
  }
}
