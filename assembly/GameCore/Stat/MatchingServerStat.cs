﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Stat.MatchingServerStat
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Collections.Generic;

namespace GameCore.Stat
{
  public class MatchingServerStat
  {
    public int peer_count;

    public object ToJson()
    {
      return (object) new Dictionary<string, object>()
      {
        {
          "peer_count",
          (object) this.peer_count
        }
      };
    }

    public void FromJson(object obj)
    {
      this.peer_count = (int) (long) ((IDictionary<string, object>) obj)["peer_count"];
    }
  }
}
