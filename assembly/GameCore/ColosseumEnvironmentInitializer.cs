﻿// Decompiled with JetBrains decompiler
// Type: GameCore.ColosseumEnvironmentInitializer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using SM;
using System;
using System.Collections.Generic;
using UniLinq;

namespace GameCore
{
  public class ColosseumEnvironmentInitializer
  {
    private static BL.Skill createSkill(PlayerUnitSkills playerSkill)
    {
      BL.Skill skill = new BL.Skill();
      skill.id = playerSkill.skill.ID;
      skill.level = playerSkill.level;
      skill.initSkillCounts();
      return skill;
    }

    private static BL.Skill createSkill(GearGearSkill gearSkill)
    {
      BL.Skill skill = new BL.Skill();
      skill.id = gearSkill.skill.ID;
      skill.level = gearSkill.skill_level;
      skill.initSkillCounts();
      return skill;
    }

    private static BL.Skill createSkill(PlayerAwakeSkill awakeSkill)
    {
      BL.Skill skill = new BL.Skill();
      skill.id = awakeSkill.masterData.ID;
      skill.level = awakeSkill.level;
      skill.initSkillCounts();
      return skill;
    }

    private static BL.MagicBullet createMagicBullet(PlayerUnitSkills playerSkill)
    {
      return new BL.MagicBullet()
      {
        skillId = playerSkill.skill.ID
      };
    }

    private static BL.MagicBullet createMagicBullet(GearGearSkill gearSkill)
    {
      return new BL.MagicBullet()
      {
        skillId = gearSkill.skill.ID
      };
    }

    private static BL.MagicBullet createMagicBullet(PlayerAwakeSkill awakeSkill)
    {
      return new BL.MagicBullet()
      {
        skillId = awakeSkill.masterData.ID
      };
    }

    private static BL.MagicBullet createMagicBullet(IAttackMethod attackMethod)
    {
      return new BL.MagicBullet()
      {
        skillId = attackMethod.skill.ID,
        attackMethod = attackMethod
      };
    }

    public static BL.Unit createUnitByPlayerUnit(PlayerUnit pu, int index, bool friend)
    {
      pu.resetOnceOverkillers();
      List<PlayerUnitSkills> list1 = ((IEnumerable<PlayerUnitSkills>) pu.skills).Concat<PlayerUnitSkills>((IEnumerable<PlayerUnitSkills>) pu.retrofitSkills).ToList<PlayerUnitSkills>();
      PlayerUnitSkills[] array1 = list1.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)).ToArray<PlayerUnitSkills>();
      PlayerUnitSkills[] array2 = list1.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.release)).ToArray<PlayerUnitSkills>();
      PlayerUnitSkills[] array3 = list1.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)).ToArray<PlayerUnitSkills>();
      PlayerUnitSkills[] array4 = list1.Where<PlayerUnitSkills>((Func<PlayerUnitSkills, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)).ToArray<PlayerUnitSkills>();
      IAttackMethod[] array5 = ((IEnumerable<IAttackMethod>) pu.battleOptionAttacks).Where<IAttackMethod>((Func<IAttackMethod, bool>) (v => v.kind.Enum == GearKindEnum.magic)).ToArray<IAttackMethod>();
      List<GearGearSkill> source1 = pu.equippedGear != (PlayerItem) null ? ((IEnumerable<GearGearSkill>) pu.equippedGear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)).ToList<GearGearSkill>() : new List<GearGearSkill>();
      List<GearGearSkill> source2 = pu.equippedGear != (PlayerItem) null ? ((IEnumerable<GearGearSkill>) pu.equippedGear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)).ToList<GearGearSkill>() : new List<GearGearSkill>();
      List<GearGearSkill> source3 = pu.equippedGear != (PlayerItem) null ? ((IEnumerable<GearGearSkill>) pu.equippedGear.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)).ToList<GearGearSkill>() : new List<GearGearSkill>();
      if (pu.equippedGear2 != (PlayerItem) null)
      {
        source1.AddRange(((IEnumerable<GearGearSkill>) pu.equippedGear2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)));
        source2.AddRange(((IEnumerable<GearGearSkill>) pu.equippedGear2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)));
        source3.AddRange(((IEnumerable<GearGearSkill>) pu.equippedGear2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)));
      }
      if (pu.equippedReisou != (PlayerItem) null)
      {
        source1.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)));
        source2.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)));
        source3.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)));
      }
      if (pu.equippedReisou2 != (PlayerItem) null)
      {
        source1.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.command)));
        source2.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.duel)));
        source3.AddRange(((IEnumerable<GearGearSkill>) pu.equippedReisou2.skills).Where<GearGearSkill>((Func<GearGearSkill, bool>) (v => v.skill.skill_type == BattleskillSkillType.magic)));
      }
      PlayerAwakeSkill[] playerAwakeSkillArray;
      if (pu.equippedExtraSkill == null)
        playerAwakeSkillArray = new PlayerAwakeSkill[0];
      else
        playerAwakeSkillArray = new PlayerAwakeSkill[1]
        {
          pu.equippedExtraSkill
        };
      PlayerAwakeSkill[] array6 = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.command)).ToArray<PlayerAwakeSkill>();
      PlayerAwakeSkill[] array7 = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.duel)).ToArray<PlayerAwakeSkill>();
      PlayerAwakeSkill[] array8 = ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.magic)).ToArray<PlayerAwakeSkill>();
      BL.Skill skill = array2.Length == 0 ? (BL.Skill) null : ColosseumEnvironmentInitializer.createSkill(((IEnumerable<PlayerUnitSkills>) array2).First<PlayerUnitSkills>());
      List<BL.Skill> list2 = ((IEnumerable<PlayerUnitSkills>) array1).Select<PlayerUnitSkills, BL.Skill>((Func<PlayerUnitSkills, BL.Skill>) (v => ColosseumEnvironmentInitializer.createSkill(v))).ToList<BL.Skill>();
      IEnumerable<BL.MagicBullet> first1 = ((IEnumerable<PlayerUnitSkills>) array3).Select<PlayerUnitSkills, BL.MagicBullet>((Func<PlayerUnitSkills, BL.MagicBullet>) (v => ColosseumEnvironmentInitializer.createMagicBullet(v)));
      IEnumerable<BL.Skill> second1 = ((IEnumerable<PlayerUnitSkills>) array4).Select<PlayerUnitSkills, BL.Skill>((Func<PlayerUnitSkills, BL.Skill>) (v => ColosseumEnvironmentInitializer.createSkill(v)));
      IEnumerable<BL.Skill> second2 = source1.Select<GearGearSkill, BL.Skill>((Func<GearGearSkill, BL.Skill>) (v => ColosseumEnvironmentInitializer.createSkill(v)));
      IEnumerable<BL.Skill> second3 = source2.Select<GearGearSkill, BL.Skill>((Func<GearGearSkill, BL.Skill>) (v => ColosseumEnvironmentInitializer.createSkill(v)));
      IEnumerable<BL.MagicBullet> second4 = source3.Select<GearGearSkill, BL.MagicBullet>((Func<GearGearSkill, BL.MagicBullet>) (v => ColosseumEnvironmentInitializer.createMagicBullet(v)));
      IEnumerable<BL.Skill> second5 = ((IEnumerable<PlayerAwakeSkill>) array6).Select<PlayerAwakeSkill, BL.Skill>((Func<PlayerAwakeSkill, BL.Skill>) (v => ColosseumEnvironmentInitializer.createSkill(v)));
      IEnumerable<BL.Skill> first2 = ((IEnumerable<PlayerAwakeSkill>) array7).Select<PlayerAwakeSkill, BL.Skill>((Func<PlayerAwakeSkill, BL.Skill>) (v => ColosseumEnvironmentInitializer.createSkill(v)));
      IEnumerable<BL.MagicBullet> second6 = ((IEnumerable<PlayerAwakeSkill>) array8).Select<PlayerAwakeSkill, BL.MagicBullet>((Func<PlayerAwakeSkill, BL.MagicBullet>) (v => ColosseumEnvironmentInitializer.createMagicBullet(v)));
      IEnumerable<BL.MagicBullet> second7 = ((IEnumerable<IAttackMethod>) array5).Select<IAttackMethod, BL.MagicBullet>((Func<IAttackMethod, BL.MagicBullet>) (v => ColosseumEnvironmentInitializer.createMagicBullet(v)));
      BL.Weapon weapon = new BL.Weapon();
      weapon.gearId = pu.equippedWeaponGearOrInitial.ID;
      BL.Unit unit = new BL.Unit();
      unit.unitId = pu.unit.ID;
      unit.playerUnit = pu;
      unit.lv = pu.level;
      unit.spawnTurn = pu.spawn_turn;
      unit.weapon = weapon;
      unit.optionWeapons = ((IEnumerable<IAttackMethod>) pu.battleOptionAttacks).Where<IAttackMethod>((Func<IAttackMethod, bool>) (x => x.kind.Enum != GearKindEnum.magic)).Select<IAttackMethod, BL.Weapon>((Func<IAttackMethod, BL.Weapon>) (y => new BL.Weapon()
      {
        attackMethod = y
      })).ToArray<BL.Weapon>();
      unit.gearLeftHand = pu.isLeftHandWeapon;
      unit.gearDualWield = pu.isDualWieldWeapon;
      unit.index = index;
      unit.friend = friend;
      unit.ougi = skill;
      unit.skills = list2.Concat<BL.Skill>(second2).Concat<BL.Skill>(second5).ToArray<BL.Skill>();
      unit.magicBullets = first1.Concat<BL.MagicBullet>(second4).Concat<BL.MagicBullet>(second6).Concat<BL.MagicBullet>(second7).ToArray<BL.MagicBullet>();
      foreach (BL.MagicBullet magicBullet in unit.magicBullets)
        magicBullet.setPrefabName(unit.job);
      unit.duelSkills = first2.Concat<BL.Skill>(second1).Concat<BL.Skill>(second3).OrderByDescending<BL.Skill, int>((Func<BL.Skill, int>) (x => x.skill.weight)).ThenBy<BL.Skill, int>((Func<BL.Skill, int>) (x => x.id)).ThenByDescending<BL.Skill, int>((Func<BL.Skill, int>) (x => x.level)).ToArray<BL.Skill>();
      unit.skillfull_shield = XorShift.Range(1, 5);
      unit.skillfull_weapon = XorShift.Range(1, 5);
      return unit;
    }

    public static ColosseumEnvironment initializeData(
      ColosseumInitialData colosseumInitialData,
      ColosseumEnvironment env_)
    {
      ColosseumEnvironment colosseumEnvironment = new ColosseumEnvironment();
      if (!colosseumInitialData.transaction_id.Equals(Persist.colosseumEnv.Data.id))
      {
        Persist.colosseumEnv.Data.id = colosseumInitialData.transaction_id;
        Persist.colosseumEnv.Flush();
      }
      colosseumEnvironment.today = string.Format("{0:D2}{1:D2}", (object) colosseumInitialData.now.Month, (object) colosseumInitialData.now.Day);
      colosseumEnvironment.bonusList = colosseumInitialData.bonusList;
      colosseumEnvironment.colosseumTransactionID = colosseumInitialData.transaction_id;
      Dictionary<int, BL.Unit> dictionary1 = new Dictionary<int, BL.Unit>();
      Dictionary<int, PlayerItem> dictionary2 = new Dictionary<int, PlayerItem>();
      Dictionary<int, PlayerItem> dictionary3 = new Dictionary<int, PlayerItem>();
      Dictionary<int, PlayerItem> dictionary4 = new Dictionary<int, PlayerItem>();
      Dictionary<int, PlayerItem> dictionary5 = new Dictionary<int, PlayerItem>();
      for (int index = 0; index < 5; ++index)
      {
        int key = 5 - index;
        PlayerUnit pu = (PlayerUnit) null;
        if (index < colosseumInitialData.player_unit_list.Length)
          pu = colosseumInitialData.player_unit_list[index];
        if (pu == (PlayerUnit) null)
        {
          dictionary1.Add(key, (BL.Unit) null);
          dictionary2.Add(key, (PlayerItem) null);
          dictionary3.Add(key, (PlayerItem) null);
        }
        else
        {
          pu.primary_equipped_gear = pu.FindEquippedGear(colosseumInitialData.player_gear_list);
          pu.primary_equipped_gear2 = pu.FindEquippedGear2(colosseumInitialData.player_gear_list);
          pu.primary_equipped_reisou = pu.FindEquippedReisou(colosseumInitialData.player_gear_list, colosseumInitialData.player_reisou_list);
          pu.primary_equipped_reisou2 = pu.FindEquippedReisou2(colosseumInitialData.player_gear_list, colosseumInitialData.player_reisou_list);
          dictionary2.Add(key, pu.primary_equipped_gear);
          dictionary3.Add(key, pu.primary_equipped_gear2);
          dictionary4.Add(key, pu.primary_equipped_reisou);
          dictionary5.Add(key, pu.primary_equipped_reisou2);
          pu.primary_equipped_awake_skill = pu.FindEquippedExtraSkill(colosseumInitialData.player_extra_skill_list);
          BL.Unit unitByPlayerUnit = ColosseumEnvironmentInitializer.createUnitByPlayerUnit(pu, index, false);
          unitByPlayerUnit.isPlayerControl = true;
          unitByPlayerUnit.isPlayerForce = true;
          dictionary1.Add(key, unitByPlayerUnit);
        }
      }
      Dictionary<int, BL.Unit> dictionary6 = new Dictionary<int, BL.Unit>();
      Dictionary<int, PlayerItem> dictionary7 = new Dictionary<int, PlayerItem>();
      Dictionary<int, PlayerItem> dictionary8 = new Dictionary<int, PlayerItem>();
      Dictionary<int, PlayerItem> dictionary9 = new Dictionary<int, PlayerItem>();
      Dictionary<int, PlayerItem> dictionary10 = new Dictionary<int, PlayerItem>();
      for (int index = 0; index < 5; ++index)
      {
        int key = 5 - index;
        PlayerUnit pu = (PlayerUnit) null;
        if (index < colosseumInitialData.opponent_unit_list.Length)
          pu = colosseumInitialData.opponent_unit_list[index];
        if (pu == (PlayerUnit) null)
        {
          dictionary6.Add(key, (BL.Unit) null);
          dictionary7.Add(key, (PlayerItem) null);
          dictionary8.Add(key, (PlayerItem) null);
        }
        else
        {
          pu.primary_equipped_gear = pu.FindEquippedGear(colosseumInitialData.opponent_gear_list);
          pu.primary_equipped_gear2 = pu.FindEquippedGear2(colosseumInitialData.opponent_gear_list);
          pu.primary_equipped_reisou = pu.FindEquippedReisou(colosseumInitialData.opponent_gear_list, colosseumInitialData.opponent_reisou_list);
          pu.primary_equipped_reisou2 = pu.FindEquippedReisou2(colosseumInitialData.opponent_gear_list, colosseumInitialData.opponent_reisou_list);
          dictionary7.Add(key, pu.primary_equipped_gear);
          dictionary8.Add(key, pu.primary_equipped_gear2);
          dictionary9.Add(key, pu.primary_equipped_reisou);
          dictionary10.Add(key, pu.primary_equipped_reisou2);
          pu.primary_equipped_awake_skill = pu.FindEquippedExtraSkill(colosseumInitialData.opponent_extra_skill_list);
          BL.Unit unitByPlayerUnit = ColosseumEnvironmentInitializer.createUnitByPlayerUnit(pu, index, false);
          unitByPlayerUnit.isPlayerControl = false;
          unitByPlayerUnit.isPlayerForce = false;
          dictionary6.Add(key, unitByPlayerUnit);
        }
      }
      colosseumEnvironment.playerUnitDict = dictionary1;
      colosseumEnvironment.playerGearDict = dictionary2;
      colosseumEnvironment.playerGearDict2 = dictionary3;
      colosseumEnvironment.playerReisouDict = dictionary4;
      colosseumEnvironment.playerReisouDict2 = dictionary5;
      colosseumEnvironment.opponentUnitDict = dictionary6;
      colosseumEnvironment.opponentGearDict = dictionary7;
      colosseumEnvironment.opponentGearDict2 = dictionary8;
      colosseumEnvironment.opponentReisouDict = dictionary9;
      colosseumEnvironment.opponentReisouDict2 = dictionary10;
      BL.Unit[] array1 = colosseumEnvironment.playerUnitDict.Values.ToArray<BL.Unit>();
      BL.Unit[] array2 = colosseumEnvironment.opponentUnitDict.Values.ToArray<BL.Unit>();
      foreach (BL.Unit unit in colosseumEnvironment.playerUnitDict.Values)
      {
        if (!(unit == (BL.Unit) null) && unit.is_leader)
        {
          foreach (PlayerUnitLeader_skills leaderSkill in unit.playerUnit.leader_skills)
            ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, leaderSkill.skill, leaderSkill.level, 0);
        }
      }
      foreach (BL.Unit unit in colosseumEnvironment.opponentUnitDict.Values)
      {
        if (!(unit == (BL.Unit) null) && unit.is_leader)
        {
          foreach (PlayerUnitLeader_skills leaderSkill in unit.playerUnit.leader_skills)
            ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, leaderSkill.skill, leaderSkill.level, 0);
        }
      }
      foreach (BL.Unit unit in colosseumEnvironment.playerUnitDict.Values)
      {
        if (!(unit == (BL.Unit) null))
        {
          PlayerAwakeSkill[] playerAwakeSkillArray;
          if (unit.playerUnit.equippedExtraSkill == null)
            playerAwakeSkillArray = new PlayerAwakeSkill[0];
          else
            playerAwakeSkillArray = new PlayerAwakeSkill[1]
            {
              unit.playerUnit.equippedExtraSkill
            };
          foreach (PlayerAwakeSkill playerAwakeSkill in ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.passive)).ToArray<PlayerAwakeSkill>())
          {
            if (!playerAwakeSkill.masterData.range_effect_passive_skill)
            {
              foreach (BattleskillEffect effect in playerAwakeSkill.masterData.Effects)
                unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, playerAwakeSkill.masterData, playerAwakeSkill.level, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
            }
            else
              ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, playerAwakeSkill.masterData, playerAwakeSkill.level, 0);
          }
        }
      }
      foreach (BL.Unit unit in colosseumEnvironment.opponentUnitDict.Values)
      {
        if (!(unit == (BL.Unit) null))
        {
          PlayerAwakeSkill[] playerAwakeSkillArray;
          if (unit.playerUnit.equippedExtraSkill == null)
            playerAwakeSkillArray = new PlayerAwakeSkill[0];
          else
            playerAwakeSkillArray = new PlayerAwakeSkill[1]
            {
              unit.playerUnit.equippedExtraSkill
            };
          foreach (PlayerAwakeSkill playerAwakeSkill in ((IEnumerable<PlayerAwakeSkill>) playerAwakeSkillArray).Where<PlayerAwakeSkill>((Func<PlayerAwakeSkill, bool>) (v => v.masterData.skill_type == BattleskillSkillType.passive)).ToArray<PlayerAwakeSkill>())
          {
            if (!playerAwakeSkill.masterData.range_effect_passive_skill)
            {
              foreach (BattleskillEffect effect in playerAwakeSkill.masterData.Effects)
                unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, playerAwakeSkill.masterData, playerAwakeSkill.level, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
            }
            else
              ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, playerAwakeSkill.masterData, playerAwakeSkill.level, 0);
          }
        }
      }
      foreach (BL.Unit unit in colosseumEnvironment.playerUnitDict.Values)
      {
        if (!(unit == (BL.Unit) null))
        {
          foreach (PlayerUnitSkills passiveSkill in unit.playerUnit.passiveSkills)
          {
            if (!passiveSkill.skill.range_effect_passive_skill)
            {
              foreach (BattleskillEffect effect in passiveSkill.skill.Effects)
                unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, passiveSkill.skill, passiveSkill.level, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
            }
            else
              ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, passiveSkill.skill, passiveSkill.level, 0);
          }
        }
      }
      foreach (BL.Unit unit in colosseumEnvironment.opponentUnitDict.Values)
      {
        if (!(unit == (BL.Unit) null))
        {
          foreach (PlayerUnitSkills passiveSkill in unit.playerUnit.passiveSkills)
          {
            if (!passiveSkill.skill.range_effect_passive_skill)
            {
              foreach (BattleskillEffect effect in passiveSkill.skill.Effects)
                unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, passiveSkill.skill, passiveSkill.level, true, 0, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
            }
            else
              ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, passiveSkill.skill, passiveSkill.level, 0);
          }
        }
      }
      foreach (KeyValuePair<int, BL.Unit> keyValuePair in colosseumEnvironment.playerUnitDict)
      {
        BL.Unit unit = keyValuePair.Value;
        if (!(unit == (BL.Unit) null))
        {
          PlayerItem playerItem1 = colosseumEnvironment.playerGearDict[keyValuePair.Key];
          if (playerItem1 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem1.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 1, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, skill.skill, skill.skill_level, 1);
            }
          }
          PlayerItem playerItem2 = colosseumEnvironment.playerGearDict2[keyValuePair.Key];
          if (playerItem2 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem2.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 2, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, skill.skill, skill.skill_level, 2);
            }
          }
          PlayerItem playerItem3 = colosseumEnvironment.playerReisouDict[keyValuePair.Key];
          if (playerItem3 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem3.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 1, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, skill.skill, skill.skill_level, 1);
            }
          }
          PlayerItem playerItem4 = colosseumEnvironment.playerReisouDict2[keyValuePair.Key];
          if (playerItem4 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem4.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 2, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array1, array2, unit, skill.skill, skill.skill_level, 2);
            }
          }
        }
      }
      foreach (KeyValuePair<int, BL.Unit> keyValuePair in colosseumEnvironment.opponentUnitDict)
      {
        BL.Unit unit = keyValuePair.Value;
        if (!(unit == (BL.Unit) null))
        {
          PlayerItem playerItem1 = colosseumEnvironment.opponentGearDict[keyValuePair.Key];
          if (playerItem1 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem1.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 1, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, skill.skill, skill.skill_level, 1);
            }
          }
          PlayerItem playerItem2 = colosseumEnvironment.opponentGearDict2[keyValuePair.Key];
          if (playerItem2 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem2.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 2, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, skill.skill, skill.skill_level, 2);
            }
          }
          PlayerItem playerItem3 = colosseumEnvironment.opponentReisouDict[keyValuePair.Key];
          if (playerItem3 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem3.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 1, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, skill.skill, skill.skill_level, 1);
            }
          }
          PlayerItem playerItem4 = colosseumEnvironment.opponentReisouDict2[keyValuePair.Key];
          if (playerItem4 != (PlayerItem) null)
          {
            foreach (GearGearSkill skill in playerItem4.skills)
            {
              if (skill.skill.skill_type != BattleskillSkillType.passive || !skill.skill.range_effect_passive_skill)
              {
                foreach (BattleskillEffect effect in skill.skill.Effects)
                  unit.skillEffects.Add(BL.SkillEffect.FromMasterData(effect, skill.skill, skill.skill_level, true, 2, (BL.Unit) null, 0, false, false, 0), new bool?(), (BL.ISkillEffectListUnit) null);
              }
              else
                ColosseumEnvironmentInitializer.setRangeSkills(array2, array1, unit, skill.skill, skill.skill_level, 2);
            }
          }
        }
      }
      foreach (BL.Unit beUnit in colosseumEnvironment.playerUnitDict.Values)
      {
        if (!(beUnit == (BL.Unit) null))
        {
          beUnit.setParameter(Judgement.BattleParameter.FromBeColosseumUnit(beUnit, beUnit.playerUnit.equippedGear, beUnit.playerUnit.equippedGear2, beUnit.playerUnit.equippedReisou, beUnit.playerUnit.equippedReisou2));
          beUnit.hp = beUnit.parameter.Hp;
          foreach (BL.MagicBullet magicBullet in beUnit.magicBullets)
            magicBullet.setAdditionalCost(beUnit.hp);
        }
      }
      foreach (BL.Unit beUnit in colosseumEnvironment.opponentUnitDict.Values)
      {
        if (!(beUnit == (BL.Unit) null))
        {
          beUnit.setParameter(Judgement.BattleParameter.FromBeColosseumUnit(beUnit, beUnit.playerUnit.equippedGear, beUnit.playerUnit.equippedGear2, beUnit.playerUnit.equippedReisou, beUnit.playerUnit.equippedReisou2));
          beUnit.hp = beUnit.parameter.Hp;
          foreach (BL.MagicBullet magicBullet in beUnit.magicBullets)
            magicBullet.setAdditionalCost(beUnit.hp);
        }
      }
      IEnumerable<BL.Unit> deckUnits1 = colosseumEnvironment.playerUnitDict.Values.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x != (BL.Unit) null));
      IEnumerable<BL.Unit> deckUnits2 = colosseumEnvironment.opponentUnitDict.Values.Where<BL.Unit>((Func<BL.Unit, bool>) (x => x != (BL.Unit) null));
      foreach (BL.ISkillEffectListUnit beUnit in deckUnits1)
        BattleFuncs.createBattleSkillEffectParams(beUnit, deckUnits1);
      foreach (BL.ISkillEffectListUnit beUnit in deckUnits2)
        BattleFuncs.createBattleSkillEffectParams(beUnit, deckUnits2);
      return colosseumEnvironment;
    }

    public static void setRangeSkills(
      BL.Unit[] units,
      BL.Unit[] targets,
      BL.Unit unit,
      BattleskillSkill skill,
      int level,
      int gearIndex = 0)
    {
      if (skill.target_type == BattleskillTargetType.myself || skill.target_type == BattleskillTargetType.player_single || skill.target_type == BattleskillTargetType.player_range)
      {
        foreach (BL.Unit unit1 in units)
        {
          if (!(unit1 == (BL.Unit) null))
          {
            foreach (BattleskillEffect effect in skill.Effects)
              unit1.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) null);
          }
        }
      }
      else if (skill.target_type == BattleskillTargetType.enemy_single || skill.target_type == BattleskillTargetType.enemy_range)
      {
        foreach (BL.Unit target in targets)
        {
          if (!(target == (BL.Unit) null))
          {
            foreach (BattleskillEffect effect in skill.Effects)
              target.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) null);
          }
        }
      }
      else
      {
        if (skill.target_type != BattleskillTargetType.complex_single && skill.target_type != BattleskillTargetType.complex_range)
          return;
        foreach (BattleskillEffect effect in skill.Effects)
        {
          if (effect.is_targer_enemy)
          {
            foreach (BL.Unit target in targets)
            {
              if (!(target == (BL.Unit) null))
                target.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) null);
            }
          }
          else
          {
            foreach (BL.Unit unit1 in units)
            {
              if (!(unit1 == (BL.Unit) null))
                unit1.skillEffects.Add(BL.SkillEffect.FromMasterData(unit, effect, skill, level, true, gearIndex), new bool?(), (BL.ISkillEffectListUnit) null);
            }
          }
        }
      }
    }
  }
}
