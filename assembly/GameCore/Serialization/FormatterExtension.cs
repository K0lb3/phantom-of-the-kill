﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.FormatterExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.IO;

namespace GameCore.Serialization
{
  public static class FormatterExtension
  {
    public static byte[] SerializeToMemory(
      this CrossSerializer serializer,
      object obj,
      SerializeContext context = null)
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        serializer.Serialize(obj, (Stream) memoryStream, context);
        return memoryStream.ToArray();
      }
    }

    public static object DeserializeFromMemory(
      this CrossSerializer serializer,
      byte[] buf,
      DeserializeContext context = null)
    {
      using (MemoryStream memoryStream = new MemoryStream(buf))
        return serializer.Deserialize((Stream) memoryStream, context);
    }
  }
}
