﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.SerializeInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;

namespace GameCore.Serialization
{
  internal class SerializeInfo
  {
    private Serializer serializer;
    private Deserializer deserializer;
    private TypeObject typeObj;
    private TreeObject treeObj;
    private System.Type type;

    public void AddProperty(string name, int treeId)
    {
      this.treeObj.fields.Add(name, treeId);
    }

    public int GetProperty(string name)
    {
      return this.treeObj.fields[name];
    }

    public int Serialize(object obj)
    {
      return this.serializer.Serialize(obj);
    }

    public object Deserialize(int treeId)
    {
      return this.deserializer.Deserialize(treeId);
    }

    public TypeObject TypeObject
    {
      get
      {
        return this.typeObj;
      }
    }

    public TreeObject TreeObject
    {
      get
      {
        return this.treeObj;
      }
    }

    public System.Type Type
    {
      get
      {
        return this.type;
      }
    }

    public System.Type[] GenericArguments
    {
      get
      {
        return this.type.GetGenericArguments();
      }
    }

    internal SerializeInfo(
      Serializer serializer,
      Deserializer deserializer,
      TypeObject typeObj,
      TreeObject treeObj,
      System.Type type)
    {
      this.serializer = serializer;
      this.deserializer = deserializer;
      this.typeObj = typeObj;
      this.treeObj = treeObj;
      this.type = type;
    }

    public Array ToArray(System.Type elementType, object obj)
    {
      int length;
      switch (obj)
      {
        case ICollection collection:
          length = collection.Count;
          break;
        case Array array:
          length = array.Length;
          break;
        default:
          ArrayList arrayList = new ArrayList();
          foreach (object obj1 in (IEnumerable) obj)
            arrayList.Add(obj1);
          obj = (object) arrayList;
          length = arrayList.Count;
          break;
      }
      Array instance = Array.CreateInstance(elementType, length);
      int num = 0;
      foreach (object obj1 in (IEnumerable) obj)
        instance.SetValue(obj1, num++);
      return instance;
    }
  }
}
