﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.VersionDeserializationBinder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System.Reflection;
using System.Runtime.Serialization;

namespace GameCore.Serialization
{
  public sealed class VersionDeserializationBinder : SerializationBinder
  {
    public override System.Type BindToType(string assemblyName, string typeName)
    {
      if (string.IsNullOrEmpty(assemblyName) || string.IsNullOrEmpty(typeName))
        return (System.Type) null;
      assemblyName = Assembly.GetExecutingAssembly().FullName;
      return System.Type.GetType(string.Format("{0}, {1}", (object) typeName, (object) assemblyName));
    }
  }
}
