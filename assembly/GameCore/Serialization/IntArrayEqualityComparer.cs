﻿// Decompiled with JetBrains decompiler
// Type: GameCore.Serialization.IntArrayEqualityComparer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UniLinq;

namespace GameCore.Serialization
{
  internal class IntArrayEqualityComparer : IEqualityComparer<int[]>
  {
    bool IEqualityComparer<int[]>.Equals(int[] x, int[] y)
    {
      if (x == null && y == null)
        return true;
      return x != null && y != null && ((IEnumerable<int>) x).SequenceEqual<int>((IEnumerable<int>) y);
    }

    int IEqualityComparer<int[]>.GetHashCode(int[] x)
    {
      int a = x.Length;
      int num = Math.Min(x.Length, 10);
      for (int index = 0; index < num; ++index)
        a = a.Combine(x[index]);
      return a;
    }
  }
}
