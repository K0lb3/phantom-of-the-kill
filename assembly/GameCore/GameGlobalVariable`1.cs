﻿// Decompiled with JetBrains decompiler
// Type: GameCore.GameGlobalVariable`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;

namespace GameCore
{
  public class GameGlobalVariable<T>
  {
    private T value;

    public T Get()
    {
      return this.value;
    }

    public void Reset(T v)
    {
      this.value = v;
    }

    public static GameGlobalVariable<T> New(params object[] args)
    {
      return new GameGlobalVariable<T>()
      {
        value = (T) Activator.CreateInstance(typeof (T), args)
      };
    }

    public static GameGlobalVariable<T> Null()
    {
      return new GameGlobalVariable<T>()
      {
        value = default (T)
      };
    }
  }
}
