﻿// Decompiled with JetBrains decompiler
// Type: GameCore.ColosseumBeforBonusParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace GameCore
{
  public class ColosseumBeforBonusParam
  {
    public int HP;
    public int attack;
    public int dexerityDisplay;
    public int criticalDisplay;
    public int attackCount;

    public ColosseumBeforBonusParam(AttackStatus status)
    {
      this.HP = status.duelParameter.attackerUnitParameter.Hp;
      this.attack = status.attack;
      this.dexerityDisplay = status.dexerityDisplay();
      this.criticalDisplay = status.criticalDisplay();
      this.attackCount = status.attackCount;
    }
  }
}
