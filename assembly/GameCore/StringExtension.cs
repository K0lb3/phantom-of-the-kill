﻿// Decompiled with JetBrains decompiler
// Type: GameCore.StringExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UniLinq;

namespace GameCore
{
  public static class StringExtension
  {
    private static readonly string hankaku = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!#$%&'()=^|@`{;+:*},<.>/?_";
    private static readonly string zenkaku = "０１２３４５６７８９ａｂｃｄｅｆｇｈｉｊｋｌｍｎｏｐｑｒｓｔｕｖｗｘｙｚＡＢＣＤＥＦＧＨＩＪＫＬＭＮＯＰＱＲＳＴＵＶＷＸＹＺ！＃＄％＆’（）＝＾｜＠｀｛；＋：＊｝，＜．＞／？＿";
    private static readonly Dictionary<char, char> hankakuToZenkakuDic = StringExtension.hankaku.Select((IEnumerable<char>) StringExtension.zenkaku, (h, z) => new
    {
      h = h,
      z = z
    }).ToDictionary(x => x.h, x => x.z);
    private static readonly Dictionary<char, char> zenkakuToHankakuDic = StringExtension.zenkaku.Select((IEnumerable<char>) StringExtension.hankaku, (z, h) => new
    {
      z = z,
      h = h
    }).ToDictionary(x => x.z, x => x.h);

    private static string ToHankaku(this char c)
    {
      return (StringExtension.zenkakuToHankakuDic.ContainsKey(c) ? StringExtension.zenkakuToHankakuDic[c] : c).ToString();
    }

    private static string ToZenkaku(this char c)
    {
      return (StringExtension.hankakuToZenkakuDic.ContainsKey(c) ? StringExtension.hankakuToZenkakuDic[c] : c).ToString();
    }

    public static string ToConverter(this string text)
    {
      return text == null ? (string) null : text.ToHankaku();
    }

    public static string ToZenkaku(this string text)
    {
      if (text == null)
        return (string) null;
      Regex regex1 = new Regex("\\[[a-z0-9]{6}\\]");
      Regex regex2 = new Regex("\\[\\-\\]");
      string input = text;
      MatchCollection matchCollection1 = regex1.Matches(input);
      MatchCollection matchCollection2 = regex2.Matches(text);
      string str = "";
      int index1 = 0;
      int index2 = 0;
      for (int index3 = 0; index3 < text.Length; ++index3)
      {
        if (matchCollection1.Count > index1 && index3 == matchCollection1[index1].Index + matchCollection1[index1].Length)
          ++index1;
        if (matchCollection2.Count > index2 && index3 == matchCollection2[index2].Index + matchCollection2[index2].Length)
          ++index2;
        str = matchCollection1.Count > index1 && matchCollection1[index1].Index <= index3 && index3 < matchCollection1[index1].Index + matchCollection1[index1].Length || matchCollection2.Count > index2 && matchCollection2[index2].Index <= index3 && index3 < matchCollection2[index2].Index + matchCollection2[index2].Length ? str + text[index3].ToHankaku() : str + text[index3].ToZenkaku();
      }
      return str;
    }

    public static string ToHankaku(this string text)
    {
      return text == null ? (string) null : text.Select<char, char>((Func<char, char>) (c => !StringExtension.zenkakuToHankakuDic.ContainsKey(c) ? c : StringExtension.zenkakuToHankakuDic[c])).ToStringForChars();
    }

    public static string F(this string format, params object[] args)
    {
      return string.Format(format, args);
    }

    public static bool isEmptyOrWhitespace(this string text)
    {
      return string.IsNullOrEmpty(text) || Regex.Match(text, "^[\\s|\\r|\\n]*$").Success;
    }
  }
}
