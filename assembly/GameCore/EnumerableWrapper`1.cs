﻿// Decompiled with JetBrains decompiler
// Type: GameCore.EnumerableWrapper`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;

namespace GameCore
{
  public class EnumerableWrapper<T> : IEnumerable<T>, IEnumerable
  {
    private IEnumerable<T> e;

    public EnumerableWrapper(IEnumerable<T> e)
    {
      this.e = e;
    }

    public IEnumerator<T> GetEnumerator()
    {
      if (this.e is T[] array)
      {
        for (int i = 0; i < array.Length; ++i)
          yield return array[i];
      }
      else
      {
        foreach (T obj in this.e)
          yield return obj;
      }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      throw new NotImplementedException();
    }
  }
}
