﻿// Decompiled with JetBrains decompiler
// Type: GameCore.util
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

namespace GameCore
{
  internal static class util
  {
    private static char tolower(char c)
    {
      return c < 'A' || c > 'Z' ? c : (char) ((uint) c + 32U);
    }

    public static int strncasecmp(char[] s1, int a, string s2, int b, int n)
    {
      for (int index = 0; index < n; ++index)
      {
        char ch1 = util.tolower(s1[a++]);
        char ch2 = b == s2.Length ? char.MinValue : util.tolower(s2[b++]);
        if ((int) ch1 != (int) ch2)
          return (int) ch1 - (int) ch2;
      }
      return 0;
    }

    public static int strncmp(char[] s1, int a, string s2, int b, int n)
    {
      for (int index = 0; index < n; ++index)
      {
        char ch1 = s1[a++];
        char ch2 = b == s2.Length ? char.MinValue : s2[b++];
        if ((int) ch1 != (int) ch2)
          return (int) ch1 - (int) ch2;
      }
      return 0;
    }

    public static bool isspace(char c)
    {
      return c != char.MinValue && c <= ' ';
    }

    public static bool isalnum(char c)
    {
      if ('A' <= c && c <= 'Z' || 'a' <= c && c <= 'z')
        return true;
      return '0' <= c && c <= '9';
    }
  }
}
