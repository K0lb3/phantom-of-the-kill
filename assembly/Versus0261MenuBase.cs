﻿// Decompiled with JetBrains decompiler
// Type: Versus0261MenuBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Versus0261MenuBase : NGMenuBase
{
  [SerializeField]
  protected UILabel TxtMatchRemain;
  [SerializeField]
  protected UILabel TxtRemain;

  public virtual void IbtnBack()
  {
    Debug.Log((object) "click default event IbtnBack");
  }

  public virtual void IbtnClassMatch()
  {
    Debug.Log((object) "click default event IbtnClassMatch");
  }

  public virtual void IbtnRundomMatch()
  {
    Debug.Log((object) "click default event IbtnRundomMatch");
  }

  public virtual void IbtnFriendMatch()
  {
    Debug.Log((object) "click default event IbtnFriendMatch");
  }

  public virtual void IbtnReplay()
  {
    Debug.Log((object) "click default event IbtnReplay");
  }
}
