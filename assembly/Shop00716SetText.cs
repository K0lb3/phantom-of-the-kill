﻿// Decompiled with JetBrains decompiler
// Type: Shop00716SetText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using System.Collections;
using UnityEngine;

public class Shop00716SetText : MonoBehaviour
{
  private SetPopupText spt;

  private void Start()
  {
  }

  private void Update()
  {
  }

  public void SetText(int prev, int next)
  {
    this.spt = this.GetComponent<SetPopupText>();
    if (!(bool) (Object) this.spt)
      return;
    this.spt.SetText(Consts.Format(Consts.GetInstance().SHOP_00716_SET_TEXT, (IDictionary) new Hashtable()
    {
      {
        (object) "item",
        (object) Consts.GetInstance().UNIQUE_ICON_KISEKI
      },
      {
        (object) "number",
        (object) "１"
      },
      {
        (object) "unit",
        (object) Consts.GetInstance().SHOP_00710_MENU
      }
    }), false);
  }
}
