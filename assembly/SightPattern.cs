﻿// Decompiled with JetBrains decompiler
// Type: SightPattern
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

[Serializable]
public class SightPattern
{
  [SerializeField]
  private GameObject sightImage;
  [SerializeField]
  private Vector3 mapScale;
  [SerializeField]
  private float scrollPower;

  public GameObject SightImage
  {
    get
    {
      return this.sightImage;
    }
  }

  public Vector3 MapScale
  {
    get
    {
      return this.mapScale;
    }
  }

  public float ScrollPower
  {
    get
    {
      return this.scrollPower;
    }
  }
}
