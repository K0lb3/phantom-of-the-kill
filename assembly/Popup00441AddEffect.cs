﻿// Decompiled with JetBrains decompiler
// Type: Popup00441AddEffect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections.Generic;
using UnityEngine;

public class Popup00441AddEffect : MonoBehaviour
{
  [SerializeField]
  private GameObject[] BG;
  [SerializeField]
  private UILabel[] TxtStatus;
  [SerializeField]
  private UILabel TxtNone;

  public void Init(int value)
  {
    Popup00441AddEffect.Type type = Popup00441AddEffect.Type.NONE;
    if (value > 0)
      type = Popup00441AddEffect.Type.ADD;
    else if (value < 0)
      type = Popup00441AddEffect.Type.SUB;
    ((IEnumerable<GameObject>) this.BG).ToggleOnce(-1);
    foreach (Component txtStatu in this.TxtStatus)
      txtStatu.gameObject.SetActive(false);
    this.TxtNone.gameObject.SetActive(false);
    if (type == Popup00441AddEffect.Type.ADD)
    {
      ((IEnumerable<GameObject>) this.BG).ToggleOnce(1);
      this.TxtStatus[1].SetTextLocalize(Math.Abs(value));
      this.TxtStatus[1].gameObject.SetActive(true);
    }
    else if (type == Popup00441AddEffect.Type.SUB)
    {
      ((IEnumerable<GameObject>) this.BG).ToggleOnce(0);
      this.TxtStatus[0].SetTextLocalize(Math.Abs(value));
      this.TxtStatus[0].gameObject.SetActive(true);
    }
    else
      this.TxtNone.gameObject.SetActive(true);
  }

  private enum Type
  {
    SUB,
    ADD,
    NONE,
  }
}
