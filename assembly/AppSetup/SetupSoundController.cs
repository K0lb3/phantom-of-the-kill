﻿// Decompiled with JetBrains decompiler
// Type: AppSetup.SetupSoundController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

namespace AppSetup
{
  public class SetupSoundController : MonoBehaviour
  {
    [SerializeField]
    private UIButton normalButton;
    [SerializeField]
    private UIButton highButton;

    public void OnNormalButton()
    {
      Persist.normalDLC.Data.IsSound = true;
      Persist.normalDLC.Data.IsSoundSetup = true;
      Persist.normalDLC.Flush();
    }

    public void OnHighButton()
    {
      Persist.normalDLC.Data.IsSound = false;
      Persist.normalDLC.Data.IsSoundSetup = true;
      Persist.normalDLC.Flush();
    }
  }
}
