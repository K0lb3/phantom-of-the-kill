﻿// Decompiled with JetBrains decompiler
// Type: Bugu0052Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Bugu0052Menu : Bugu005ItemListMenuBase
{
  private bool needClearCache = true;
  [SerializeField]
  private UILabel TxtNumberPattern1;

  public override Persist<Persist.ItemSortAndFilterInfo> GetPersist()
  {
    return Persist.bugu0052SortAndFilter;
  }

  protected override List<PlayerItem> GetItemList()
  {
    return ((IEnumerable<PlayerItem>) SMManager.Get<PlayerItem[]>()).Where<PlayerItem>((Func<PlayerItem, bool>) (x => x.isWeapon())).ToList<PlayerItem>();
  }

  protected override long GetRevisionItemList()
  {
    return SMManager.Revision<PlayerItem[]>();
  }

  protected override void BottomInfoUpdate()
  {
    this.TxtNumberPattern1.SetTextLocalize(Consts.Format(Consts.GetInstance().GEAR_0052_POSSESSION, (IDictionary) new Hashtable()
    {
      {
        (object) "now",
        (object) this.InventoryItems.Count<InventoryItem>()
      },
      {
        (object) "max",
        (object) SMManager.Get<Player>().max_items
      }
    }));
  }

  protected virtual void OnEnable()
  {
    if (!this.scroll.scrollView.isDragging)
      return;
    this.scroll.scrollView.Press(false);
  }

  public void onBackScene()
  {
    if ((UnityEngine.Object) this.SortPopupPrefab != (UnityEngine.Object) null)
      this.SortPopupPrefab.GetComponent<ItemSortAndFilter>().Initialize((Bugu005ItemListMenuBase) this, false);
    this.Sort(this.SortCategory, this.OrderBuySort, this.isEquipFirst);
    this.needClearCache = true;
  }

  public override void onEndScene()
  {
    base.onEndScene();
    Persist.sortOrder.Flush();
    if (!this.needClearCache)
      return;
    ItemIcon.ClearCache();
  }

  public void IbtnWeaponStorage()
  {
    if (this.IsPushAndSet())
      return;
    Bugu005WeaponStorageScene.ChangeScene(false);
  }

  public void IbtnSell()
  {
    if (this.IsPushAndSet())
      return;
    this.needClearCache = false;
    Bugu00525Scene.ChangeScene(true, Bugu00525Scene.Mode.Weapon);
  }

  public void IbtnConversion()
  {
    if (this.IsPushAndSet())
      return;
    this.needClearCache = false;
    Bugu005WeaponMaterialConversionScene.ChangeScene(true, Bugu005WeaponMaterialConversionScene.Mode.Weapon);
  }
}
