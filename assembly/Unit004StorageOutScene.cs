﻿// Decompiled with JetBrains decompiler
// Type: Unit004StorageOutScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Unit004StorageOutScene : NGSceneBase
{
  [SerializeField]
  private Unit004StorageOutMenu menu;

  public static void changeScene(bool stack)
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("unit004_storage_out", stack, (object[]) Array.Empty<object>());
  }

  public IEnumerator onStartSceneAsync()
  {
    IEnumerator e1 = this.SetStorageBackground();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    SMManager.Get<PlayerUnit[]>();
    e1 = WebAPI.UnitReservesCount((System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
    })).Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    int storageCount = SMManager.Get<PlayerUnitReservesCount>().count;
    Future<WebAPI.Response.UnitReservesIndex> reservesIndexF = WebAPI.UnitReservesIndex((System.Action<WebAPI.Response.UserError>) (e =>
    {
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      WebAPI.DefaultUserErrorCallback(e);
    }));
    e1 = reservesIndexF.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    e1 = this.menu.Init(reservesIndexF.Result.player_units, storageCount, Persist.unit004StorageSortAndFilter, false);
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
  }

  private IEnumerator SetStorageBackground()
  {
    Unit004StorageOutScene unit004StorageOutScene = this;
    Future<GameObject> bgF = new ResourceObject("Prefabs/BackGround/DefaultBackground_storage").Load<GameObject>();
    IEnumerator e = bgF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) bgF.Result != (UnityEngine.Object) null)
      unit004StorageOutScene.backgroundPrefab = bgF.Result;
  }

  public void onStartScene()
  {
  }
}
