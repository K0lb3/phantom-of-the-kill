﻿// Decompiled with JetBrains decompiler
// Type: Unit004JobDialog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Unit004JobDialog : BackButtonMenuBase
{
  [SerializeField]
  private Transform dirJobAfter;
  [SerializeField]
  private GameObject objClose;

  public IEnumerator Init(PlayerUnitJob_abilities jobAbility)
  {
    Future<GameObject> JobAfterPanelF = Singleton<NGGameDataManager>.GetInstance().IsSea ? Res.Prefabs.unit004_Job.Unit_job_after_sea.Load<GameObject>() : Res.Prefabs.unit004_Job.Unit_job_after.Load<GameObject>();
    IEnumerator e = JobAfterPanelF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject obj = JobAfterPanelF.Result.Clone(this.dirJobAfter);
    e = obj.GetComponent<Unit004JobAfter>().Init(false, jobAbility, (PlayerUnitJob_abilities) null, true);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    obj.GetComponentInChildren<Collider>(true).GetComponent<UIWidget>().depth += this.objClose.GetComponent<UIWidget>().depth;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
