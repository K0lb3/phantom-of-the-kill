﻿// Decompiled with JetBrains decompiler
// Type: Unit05443Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Unit05443Scene : NGSceneBase
{
  public Unit05443Menu menu;
  private bool targetGear_favorite;
  private static bool block;

  public static void changeScene(bool stack, ItemInfo choiceGear)
  {
    Unit05443Scene.block = false;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit054_4_3", (stack ? 1 : 0) != 0, (object) choiceGear);
  }

  public static void changeSceneLimited(bool stack, ItemInfo choiceGear)
  {
    Unit05443Scene.block = true;
    Singleton<NGSceneManager>.GetInstance().changeScene("unit054_4_3", (stack ? 1 : 0) != 0, (object) choiceGear);
  }

  public IEnumerator onStartSceneAsync(ItemInfo choiceGear)
  {
    foreach (PlayerItem playerItem in SMManager.Get<PlayerItem[]>())
    {
      if (playerItem.id == choiceGear.itemID)
      {
        choiceGear = new ItemInfo(playerItem);
        break;
      }
    }
    this.targetGear_favorite = choiceGear.favorite;
    IEnumerator e = this.menu.Init(choiceGear, Unit05443Scene.block, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public override IEnumerator onEndSceneAsync()
  {
    if (this.targetGear_favorite != this.menu.nowFavorite.gameObject.activeSelf)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      IEnumerator e = this.menu.FavoriteAPI();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      this.targetGear_favorite = this.menu.nowFavorite.gameObject.activeSelf;
    }
    else
    {
      yield return (object) new WaitForSeconds(0.5f);
      this.menu.EndScene();
    }
  }
}
