﻿// Decompiled with JetBrains decompiler
// Type: BattleUI01LayoutAutoLink
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class BattleUI01LayoutAutoLink : MonoBehaviour
{
  [SerializeField]
  private BattleUI01LayoutAuto target_;
  private bool isModified_;
  private bool isActive_;
  private bool isModeAuto_;

  public void activate(bool isActive, bool isModeAuto)
  {
    this.isModified_ = true;
    this.isActive_ = isActive;
    this.isModeAuto_ = isModeAuto;
  }

  public void flush(bool isEnabled)
  {
    if (!this.isModified_)
      return;
    this.isModified_ = false;
    this.target_.activate(this.isActive_, this.isModeAuto_, isEnabled);
  }
}
