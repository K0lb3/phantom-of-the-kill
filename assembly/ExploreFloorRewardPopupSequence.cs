﻿// Decompiled with JetBrains decompiler
// Type: ExploreFloorRewardPopupSequence
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class ExploreFloorRewardPopupSequence
{
  private GameObject rewardPopupPrefab;
  private List<BattleResultBonusInfo> RewardList;
  private bool enableAndroidBackKey;

  public ExploreFloorRewardPopupSequence(bool enableAndroidBackKey = false)
  {
    this.enableAndroidBackKey = enableAndroidBackKey;
  }

  public IEnumerator Init(ExploreFloor floorData)
  {
    this.RewardList = new List<BattleResultBonusInfo>();
    foreach (ExploreFloorReward exploreFloorReward in ((IEnumerable<ExploreFloorReward>) MasterData.ExploreFloorRewardList).Where<ExploreFloorReward>((Func<ExploreFloorReward, bool>) (x => x.floor == floorData.floor && x.period_id == floorData.period_id)))
      this.RewardList.Add(new BattleResultBonusInfo(exploreFloorReward.reward_id, (MasterDataTable.CommonRewardType) exploreFloorReward.reward_type_CommonRewardType, exploreFloorReward.reward_title, true));
    if (this.RewardList.Count > 0)
      yield return (object) this.LoadPopupPrefab();
  }

  public IEnumerator Run()
  {
    if (this.RewardList.Count > 0)
    {
      GameObject popup = this.rewardPopupPrefab.Clone((Transform) null);
      if ((UnityEngine.Object) popup.GetComponent<UIWidget>() != (UnityEngine.Object) null)
        popup.GetComponent<UIWidget>().alpha = 0.0f;
      popup.SetActive(false);
      BattleUI05ClearBonusSetting script = popup.GetComponent<BattleUI05ClearBonusSetting>();
      IEnumerator e = script.CreateClearBonusIcon(this.RewardList, false, true);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      script.SetClearBonusInfo(this.RewardList, false);
      GameObject gameObject = Singleton<PopupManager>.GetInstance().open(popup, false, false, true, true, false, false, "SE_1006");
      Singleton<NGSoundManager>.GetInstance().playSE("SE_1034", false, 0.0f, -1);
      bool toNext = false;
      RaidUtils.CreateTouchObject((EventDelegate.Callback) (() => toNext = true), gameObject.transform);
      while (!toNext)
        yield return (object) null;
      Singleton<PopupManager>.GetInstance().onDismiss();
      yield return (object) new WaitForSeconds(0.5f);
    }
  }

  private IEnumerator LoadPopupPrefab()
  {
    Future<GameObject> future = Singleton<ResourceManager>.GetInstance().Load<GameObject>("Prefabs/explore033_Top/explore_FloorArrival_reward", 1f);
    IEnumerator e = future.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if ((UnityEngine.Object) future.Result == (UnityEngine.Object) null)
      Debug.LogError((object) "failed to load dir_RaidBoss_result_Defeat_reward.prefab");
    else
      this.rewardPopupPrefab = future.Result;
  }

  private class BackButtonProgressBehaviour : BackButtonMenuBase
  {
    private System.Action onBack;

    public void SetAction(System.Action onBack)
    {
      this.onBack = onBack;
    }

    public override void onBackButton()
    {
      System.Action onBack = this.onBack;
      if (onBack == null)
        return;
      onBack();
    }
  }
}
