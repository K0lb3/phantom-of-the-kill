﻿// Decompiled with JetBrains decompiler
// Type: ExploreClipEffectPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using MasterDataTable;
using UnityEngine;

public class ExploreClipEffectPlayer : clipEffectPlayer
{
  public bool IsFootStepSoundOnly;

  public void SetUnit(UnitUnit unit)
  {
    this._mUnit = unit;
  }

  protected override void playSound(string seName)
  {
    ExploreSceneManager instanceOrNull = Singleton<ExploreSceneManager>.GetInstanceOrNull();
    if ((Object) instanceOrNull == (Object) null || !instanceOrNull.IsSceneActive || this.IsFootStepSoundOnly && !seName.Contains("FOOTSTEPS"))
      return;
    base.playSound(seName);
  }
}
