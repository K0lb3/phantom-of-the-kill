﻿// Decompiled with JetBrains decompiler
// Type: Quest0028Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class Quest0028Menu : BackButtonMenuBase
{
  private Dictionary<int, Quest0028Indicator> indicators = new Dictionary<int, Quest0028Indicator>();
  private Dictionary<int, int> deckIndexs = new Dictionary<int, int>();
  private Dictionary<int, int> deckIndexsBack = new Dictionary<int, int>();
  private Quest0028Menu.LimitedQuestData regulation = new Quest0028Menu.LimitedQuestData();
  private bool initialized;
  public NGHorizontalScrollParts indicator;
  [SerializeField]
  private GameObject _mainPanel;
  [SerializeField]
  [Tooltip("0:出撃/1:武器破損/2:編成 でボタンを配置")]
  private GameObject[] Btns;
  public static bool isGoingToBattle;
  private BattleStageGuest[] guest;
  private bool isLimitation;
  private bool isUserDeckStage;
  private bool friendPositionEnable;
  private PlayerDeck[] regulationDeck;
  [SerializeField]
  [Tooltip("大ボタンに\"編成\"が無い場合に替りにボタンを無効化して表示")]
  private UIButton btnResortie;
  private bool notExistMainEditButton;
  private PlayerDeck[] playerDecks;
  private QuestScoreBonusTimetable[] bonusTimeTables;
  private UnitBonus[] unitBonus;
  private PlayerHelper friend;
  private PlayerStoryQuestS story_quest;
  private PlayerExtraQuestS extra_quest;
  private PlayerCharacterQuestS char_quest;
  private PlayerQuestSConverter convert_quest;
  private PlayerSeaQuestS sea_quest;
  private GameObject apPopup;
  private QuestDetailManager detailManager_;
  private int selectDeck;
  private int[] DeckNums;
  private const int SUPPLY_DECK_MAX = 5;
  public bool story_only;
  public BattleInfo battleInfo;
  private bool isPlayingScript;
  private bool isPlayingMovie;
  private string limitationLabel;
  [SerializeField]
  protected UILabel TxtTitle;
  [SerializeField]
  protected UILabel TxtRecommendCombat;
  [SerializeField]
  protected GameObject[] dir_Items;
  private int recommenCombat;
  protected const float LINK_HEIGHT = 100f;
  protected const float LINK_DEFHEIGHT = 136f;
  protected const float scale = 0.7352941f;
  private bool isSea_;
  private System.Action<PlayerHelper> onSetHelper_;
  [SerializeField]
  private QuestMoviePlayer movieObj;

  public GameObject mainPanel
  {
    get
    {
      return this._mainPanel;
    }
  }

  protected QuestDetailManager detailManager
  {
    get
    {
      if (this.detailManager_ == null)
        this.detailManager_ = new QuestDetailManager();
      return this.detailManager_;
    }
  }

  public bool IsPlayingStory
  {
    set
    {
      this.isPlayingScript = value;
    }
    get
    {
      return this.isPlayingScript;
    }
  }

  private string quest_name
  {
    get
    {
      if (this.story_quest != null)
        return this.story_quest.quest_story_s.name;
      if (this.extra_quest != null)
        return this.extra_quest.quest_extra_s.name;
      if (this.char_quest != null)
        return this.char_quest.quest_character_s.name;
      if (this.convert_quest != null)
        return this.convert_quest.questS.name;
      return this.sea_quest != null ? this.sea_quest.quest_sea_s.name : "";
    }
  }

  private int questType
  {
    get
    {
      if (this.story_quest != null)
        return 1;
      if (this.extra_quest != null)
        return 3;
      if (this.char_quest != null)
        return 2;
      return this.convert_quest != null ? (this.convert_quest.questS.data_type == QuestSConverter.DataType.Character ? 2 : 4) : (this.sea_quest != null ? 9 : 0);
    }
  }

  private int questID
  {
    get
    {
      if (this.story_quest != null)
        return this.story_quest._quest_story_s;
      if (this.extra_quest != null)
        return this.extra_quest._quest_extra_s;
      if (this.char_quest != null)
        return this.char_quest._quest_character_s;
      if (this.convert_quest != null)
        return this.convert_quest._quest_s_id;
      return this.sea_quest != null ? this.sea_quest._quest_sea_s : 0;
    }
  }

  private int lost_ap
  {
    get
    {
      if (this.story_quest != null)
        return this.story_quest.consumed_ap;
      if (this.extra_quest != null)
        return this.extra_quest.consumed_ap;
      if (this.char_quest != null)
        return this.char_quest.consumed_ap;
      if (this.convert_quest != null)
        return this.convert_quest.consumed_ap;
      return this.sea_quest != null ? this.sea_quest.consumed_ap : -1;
    }
  }

  private UnitGender gender_restriction
  {
    get
    {
      if (this.story_quest != null)
        return this.story_quest.quest_story_s.gender_restriction;
      if (this.extra_quest != null)
        return this.extra_quest.quest_extra_s.gender_restriction;
      if (this.char_quest != null)
        return this.char_quest.quest_character_s.gender_restriction;
      if (this.convert_quest != null)
        return this.convert_quest.questS.gender_restriction;
      return this.sea_quest != null ? this.sea_quest.quest_sea_s.gender_restriction : UnitGender.none;
    }
  }

  protected Quest0028Menu.LimitedQuestData Regulation
  {
    get
    {
      return this.regulation;
    }
  }

  protected IEnumerator CallExtarDeckData(int id)
  {
    Future<WebAPI.Response.QuestLimitationExtra> apiF = WebAPI.QuestLimitationExtra(id, (System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = apiF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerDeck[] tmp = (PlayerDeck[]) null;
    QuestLimitationBase[] limits = (QuestLimitationBase[]) null;
    if (apiF.Result != null)
    {
      tmp = apiF.Result.limitation_player_decks;
      limits = apiF.Result.limitations;
    }
    this.regulation.SetInfo(tmp, limits);
    this.limitationLabel = ((IEnumerable<QuestExtraLimitationLabel>) MasterData.QuestExtraLimitationLabelList).FirstOrDefault<QuestExtraLimitationLabel>((Func<QuestExtraLimitationLabel, bool>) (q => q.quest_s_id_QuestExtraS == id))?.label;
  }

  protected IEnumerator CallStoryDeckData(int id)
  {
    Future<WebAPI.Response.QuestLimitationStory> apiF = WebAPI.QuestLimitationStory(id, (System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = apiF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerDeck[] tmp = (PlayerDeck[]) null;
    QuestLimitationBase[] limits = (QuestLimitationBase[]) null;
    if (apiF.Result != null)
    {
      tmp = apiF.Result.limitation_player_decks;
      limits = apiF.Result.limitations;
    }
    this.regulation.SetInfo(tmp, limits);
    this.limitationLabel = ((IEnumerable<QuestStoryLimitationLabel>) MasterData.QuestStoryLimitationLabelList).FirstOrDefault<QuestStoryLimitationLabel>((Func<QuestStoryLimitationLabel, bool>) (q => q.quest_s_id_QuestStoryS == id))?.label;
  }

  protected IEnumerator CallCharaDeckData(int id)
  {
    Future<WebAPI.Response.QuestLimitationCharacter> apiF = WebAPI.QuestLimitationCharacter(id, (System.Action<WebAPI.Response.UserError>) null);
    IEnumerator e = apiF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerDeck[] tmp = (PlayerDeck[]) null;
    QuestLimitationBase[] limits = (QuestLimitationBase[]) null;
    if (apiF.Result != null)
    {
      tmp = apiF.Result.limitation_player_decks;
      limits = apiF.Result.limitations;
    }
    this.regulation.SetInfo(tmp, limits);
    this.limitationLabel = ((IEnumerable<QuestCharacterLimitationLabel>) MasterData.QuestCharacterLimitationLabelList).FirstOrDefault<QuestCharacterLimitationLabel>((Func<QuestCharacterLimitationLabel, bool>) (q => q.quest_s_id_QuestCharacterS == id))?.label;
  }

  protected IEnumerator CallConvertDeckData(PlayerQuestSConverter data)
  {
    PlayerDeck[] d = (PlayerDeck[]) null;
    QuestLimitationBase[] lb = (QuestLimitationBase[]) null;
    IEnumerator e;
    if (data.questS.data_type == QuestSConverter.DataType.Character)
    {
      Future<WebAPI.Response.QuestLimitationCharacter> apiF = WebAPI.QuestLimitationCharacter(data.questS.ID, (System.Action<WebAPI.Response.UserError>) null);
      e = apiF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (apiF.Result != null)
      {
        d = apiF.Result.limitation_player_decks;
        lb = apiF.Result.limitations;
      }
      this.limitationLabel = ((IEnumerable<QuestCharacterLimitationLabel>) MasterData.QuestCharacterLimitationLabelList).FirstOrDefault<QuestCharacterLimitationLabel>((Func<QuestCharacterLimitationLabel, bool>) (q => q.quest_s_id_QuestCharacterS == data._quest_s_id))?.label;
      apiF = (Future<WebAPI.Response.QuestLimitationCharacter>) null;
    }
    else if (data.questS.data_type == QuestSConverter.DataType.Harmony)
    {
      Future<WebAPI.Response.QuestLimitationHarmony> apiF = WebAPI.QuestLimitationHarmony(data.questS.ID, (System.Action<WebAPI.Response.UserError>) null);
      e = apiF.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (apiF.Result != null)
      {
        d = apiF.Result.limitation_player_decks;
        lb = apiF.Result.limitations;
      }
      this.limitationLabel = ((IEnumerable<QuestHarmonyLimitationLabel>) MasterData.QuestHarmonyLimitationLabelList).FirstOrDefault<QuestHarmonyLimitationLabel>((Func<QuestHarmonyLimitationLabel, bool>) (q => q.quest_s_id_QuestHarmonyS == data._quest_s_id))?.label;
      apiF = (Future<WebAPI.Response.QuestLimitationHarmony>) null;
    }
    this.regulation.SetInfo(d, lb);
  }

  public void setEventSetHelper(System.Action<PlayerHelper> eventSetHelper)
  {
    this.onSetHelper_ = eventSetHelper;
  }

  public IEnumerator InitPlayerDecks(
    PlayerDeck[] playerDecks,
    List<PlayerItem> SupplyList,
    PlayerHelper friend,
    PlayerStoryQuestS story_quest,
    PlayerExtraQuestS extra_quest,
    PlayerCharacterQuestS char_quest,
    PlayerQuestSConverter convert_quest,
    PlayerSeaQuestS sea_quest,
    bool story_only)
  {
    Quest0028Menu quest0028Menu = this;
    quest0028Menu.initialized = false;
    quest0028Menu.notExistMainEditButton = (UnityEngine.Object) quest0028Menu.btnResortie != (UnityEngine.Object) null && (quest0028Menu.Btns.Length <= 2 || (UnityEngine.Object) quest0028Menu.Btns[2] == (UnityEngine.Object) null);
    bool flag1 = extra_quest != null && Quest0028Menu.IsExtraLimitation(extra_quest);
    bool flag2 = story_quest != null && Quest0028Menu.IsStoryLimitation(story_quest);
    bool flag3 = char_quest != null && Quest0028Menu.IsCharaLimitation(char_quest);
    bool flag4 = convert_quest != null && Quest0028Menu.IsConvertLimitation(convert_quest);
    bool flag5 = sea_quest != null && Quest0028Menu.IsSeaLimitation(sea_quest);
    quest0028Menu.isLimitation = flag1 | flag2 | flag3 | flag4 | flag5;
    quest0028Menu.isUserDeckStage = extra_quest != null && extra_quest.extra_quest_area == 3;
    quest0028Menu.friend = friend;
    quest0028Menu.story_quest = story_quest;
    quest0028Menu.extra_quest = extra_quest;
    quest0028Menu.char_quest = char_quest;
    quest0028Menu.convert_quest = convert_quest;
    quest0028Menu.sea_quest = sea_quest;
    quest0028Menu.story_only = story_only;
    quest0028Menu.isSea_ = Singleton<NGGameDataManager>.GetInstance().IsSea && sea_quest != null;
    if (quest0028Menu.story_only)
    {
      Singleton<CommonRoot>.GetInstance().isLoading = true;
      quest0028Menu.playerDecks = playerDecks;
      if (playerDecks != null)
      {
        quest0028Menu.selectDeck = quest0028Menu.isSea_ ? Persist.seaDeckOrganized.Data.number : Persist.deckOrganized.Data.number;
        quest0028Menu.QuestStart();
        yield break;
      }
    }
    IEnumerator e;
    if (flag1)
    {
      e = quest0028Menu.CallExtarDeckData(extra_quest.quest_extra_s.ID);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest0028Menu.regulationDeck = quest0028Menu.Regulation.Deck;
    }
    else if (flag2)
    {
      e = quest0028Menu.CallStoryDeckData(story_quest.quest_story_s.ID);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest0028Menu.regulationDeck = quest0028Menu.Regulation.Deck;
    }
    else if (flag3)
    {
      e = quest0028Menu.CallCharaDeckData(story_quest.quest_story_s.ID);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest0028Menu.regulationDeck = quest0028Menu.Regulation.Deck;
    }
    else if (flag4)
    {
      e = quest0028Menu.CallConvertDeckData(convert_quest);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest0028Menu.regulationDeck = quest0028Menu.Regulation.Deck;
    }
    else if (flag5)
    {
      e = quest0028Menu.CallStoryDeckData(sea_quest.quest_sea_s.ID);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      quest0028Menu.regulationDeck = quest0028Menu.Regulation.Deck;
    }
    quest0028Menu.TxtTitle.SetText(quest0028Menu.quest_name);
    QuestDetailData detailData = quest0028Menu.detailManager.getData((CommonQuestType) quest0028Menu.questType, quest0028Menu.questID, false);
    e = detailData.Wait((System.Action<WebAPI.Response.UserError>) (er =>
    {
      if (Singleton<NGGameDataManager>.GetInstance().IsSea && string.Equals(er.Code, "SEA000"))
        this.StartCoroutine(PopupUtility.SeaError(er));
      else
        WebAPI.DefaultUserErrorCallback(er);
    }));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest0028Menu.TxtRecommendCombat.SetTextLocalize(detailData.recommend_strength);
    int.TryParse(detailData.recommend_strength, out quest0028Menu.recommenCombat);
    Future<GameObject> prefabF = quest0028Menu.isSea_ ? new ResourceObject("Prefabs/quest002_8_sea/indicator_sea").Load<GameObject>() : Res.Prefabs.quest002_8.indicator.Load<GameObject>();
    e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefab = prefabF.Result;
    Future<GameObject> apPopupF = Res.Prefabs.popup.popup_002_7__anim_popup01.Load<GameObject>();
    e = apPopupF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest0028Menu.apPopup = apPopupF.Result;
    quest0028Menu.indicator.destroyParts(true);
    quest0028Menu.DeckNums = new int[playerDecks.Length];
    int cnt = 0;
    quest0028Menu.deckIndexs.Clear();
    quest0028Menu.deckIndexsBack.Clear();
    QuestScoreBonusTimetable[] scoreBonusTimetableArray = SMManager.Get<QuestScoreBonusTimetable[]>();
    quest0028Menu.bonusTimeTables = ((IEnumerable<QuestScoreBonusTimetable>) scoreBonusTimetableArray).Where<QuestScoreBonusTimetable>((Func<QuestScoreBonusTimetable, bool>) (x => x.start_at < ServerTime.NowAppTime() && x.end_at > ServerTime.NowAppTime() && x.quest_s_id == this.questID)).ToArray<QuestScoreBonusTimetable>();
    quest0028Menu.unitBonus = UnitBonus.getActiveUnitBonus(ServerTime.NowAppTime(), new int?(quest0028Menu.questType), new int?(quest0028Menu.questID));
    int battleStageID = 0;
    if (story_quest != null)
      battleStageID = story_quest.quest_story_s.stage_BattleStage;
    else if (extra_quest != null)
      battleStageID = extra_quest.quest_extra_s.wave == null ? extra_quest.quest_extra_s.stage_BattleStage : extra_quest.quest_extra_s.wave.first_quest_s_id;
    else if (char_quest != null)
      battleStageID = char_quest.quest_character_s.stage_BattleStage;
    else if (convert_quest != null)
      battleStageID = convert_quest.questS.stage_BattleStage;
    else if (sea_quest != null)
      battleStageID = sea_quest.quest_sea_s.stage_BattleStage;
    quest0028Menu.friendPositionEnable = Quest0028Menu.GetFriendPositionEnable(battleStageID);
    quest0028Menu.guest = ((IEnumerable<BattleStageGuest>) MasterData.BattleStageGuestList).Where<BattleStageGuest>((Func<BattleStageGuest, bool>) (x => x.stage_BattleStage == battleStageID)).ToArray<BattleStageGuest>();
    for (int i = 0; i < playerDecks.Length; ++i)
    {
      quest0028Menu.deckIndexsBack.Add(i, 0);
      PlayerDeck playerDeck = playerDecks[i];
      if (((IEnumerable<PlayerUnit>) playerDeck.player_units).FirstOrDefault<PlayerUnit>() != (PlayerUnit) null)
      {
        quest0028Menu.deckIndexs.Add(cnt, i);
        quest0028Menu.deckIndexsBack[i] = cnt;
        quest0028Menu.DeckNums[cnt] = playerDeck.deck_number;
        GameObject prefab1 = quest0028Menu.indicator.instantiateParts(prefab, true);
        e = quest0028Menu.AddDeck(playerDeck, friend, prefab1, quest0028Menu.bonusTimeTables, quest0028Menu.unitBonus, quest0028Menu.guest, battleStageID, quest0028Menu.recommenCombat);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        ++cnt;
      }
    }
    quest0028Menu.selectDeck = -1;
    int index = quest0028Menu.isSea_ ? Persist.seaDeckOrganized.Data.number : Persist.deckOrganized.Data.number;
    quest0028Menu.indicator.resetScrollView();
    quest0028Menu.indicator.setItemPositionQuick(quest0028Menu.deckIndexsBack[index]);
    if (SupplyList.Count <= 5)
    {
      e = quest0028Menu.SetSupplyIcons(SupplyList);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    else
      Debug.LogWarning((object) "!!! BUG !!! SUPPLYDECK OUT OF RANGE");
    quest0028Menu.playerDecks = playerDecks;
    quest0028Menu.initialized = true;
    quest0028Menu.StartCoroutine(quest0028Menu.WaitScrollSe());
  }

  protected override void Update()
  {
    if (!this.initialized || this.story_only)
      return;
    base.Update();
    if (this.playerDecks == null || this.selectDeck == this.DeckNums[this.indicator.selected])
      return;
    this.selectDeck = this.DeckNums[this.indicator.selected];
    bool flag = false;
    PlayerUnit[] deckUnitData = this.indicators[this.selectDeck].deckUnitData;
    for (int index = 0; index < deckUnitData.Length; ++index)
    {
      if (!(deckUnitData[index] == (PlayerUnit) null) && !deckUnitData[index].is_gesut && (!(deckUnitData[index].equippedGear == (PlayerItem) null) || !(deckUnitData[index].equippedGear2 == (PlayerItem) null)) && (deckUnitData[index].equippedGear != (PlayerItem) null && deckUnitData[index].equippedGear.broken || deckUnitData[index].equippedGear2 != (PlayerItem) null && deckUnitData[index].equippedGear2.broken))
      {
        flag = true;
        break;
      }
    }
    if (flag)
    {
      ((IEnumerable<GameObject>) this.Btns).ToggleOnceEx(1);
    }
    else
    {
      ((IEnumerable<GameObject>) this.Btns).ToggleOnceEx(0);
      if (this.notExistMainEditButton)
        this.btnResortie.isEnabled = true;
      Quest0028Indicator indicator = this.indicators[this.selectDeck];
      if ((UnityEngine.Object) indicator != (UnityEngine.Object) null && !indicator.isCompletedOverkillersDeck)
        this.setMainButtonEdit();
      else if (this.isLimitation && this.regulationDeck != null)
      {
        int[] array = ((IEnumerable<PlayerUnit>) indicator.deckUnitData).Where<PlayerUnit>((Func<PlayerUnit, bool>) (x => x != (PlayerUnit) null && !x.is_gesut)).Select<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.id)).ToArray<int>();
        int?[] checkedDeckIds = ((IEnumerable<int?>) this.regulationDeck[this.selectDeck].player_unit_ids).Where<int?>((Func<int?, bool>) (n => n.HasValue)).ToArray<int?>();
        Func<int, bool> predicate = (Func<int, bool>) (id => ((IEnumerable<int?>) checkedDeckIds).Any<int?>((Func<int?, bool>) (id_c =>
        {
          int? nullable = id_c;
          int num = id;
          return nullable.GetValueOrDefault() == num & nullable.HasValue;
        })));
        if (((IEnumerable<int>) array).All<int>(predicate))
          return;
        this.setMainButtonEdit();
      }
      else if (this.isLimitation && this.regulationDeck == null)
      {
        this.setMainButtonEdit();
      }
      else
      {
        if (this.gender_restriction == UnitGender.none || ((IEnumerable<PlayerUnit>) this.playerDecks[this.selectDeck].player_units).All<PlayerUnit>((Func<PlayerUnit, bool>) (x => x == (PlayerUnit) null || x.unit.character.gender == this.gender_restriction)))
          return;
        this.setMainButtonEdit();
      }
    }
  }

  private void setMainButtonEdit()
  {
    if (this.notExistMainEditButton)
      this.btnResortie.isEnabled = false;
    else
      ((IEnumerable<GameObject>) this.Btns).ToggleOnceEx(2);
  }

  private IEnumerator AddDeck(
    PlayerDeck playerDeck,
    PlayerHelper friend,
    GameObject prefab,
    QuestScoreBonusTimetable[] tables,
    UnitBonus[] unitBonus,
    BattleStageGuest[] guests,
    int battleStageID,
    int recommenCombat)
  {
    Quest0028Menu quest0028Menu = this;
    Quest0028Indicator indicatorScript = prefab.GetComponent<Quest0028Indicator>();
    quest0028Menu.preInitializeIndicator(indicatorScript);
    quest0028Menu.indicators.Add(playerDeck.deck_number, indicatorScript);
    IEnumerator e = indicatorScript.InitPlayerDeck(playerDeck, friend, quest0028Menu.extra_quest, quest0028Menu.story_quest, quest0028Menu.char_quest, quest0028Menu.convert_quest, quest0028Menu.sea_quest, quest0028Menu.regulationDeck, tables, unitBonus, guests, battleStageID, recommenCombat);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    if (!quest0028Menu.isUserDeckStage && !quest0028Menu.isLimitation && quest0028Menu.friendPositionEnable)
    {
      if (quest0028Menu.onSetHelper_ != null)
      {
        if ((UnityEngine.Object) indicatorScript.friendIcon_ != (UnityEngine.Object) null)
        {
          // ISSUE: reference to a compiler-generated method
          indicatorScript.friendIcon_.onClick = new System.Action<UnitIconBase>(quest0028Menu.\u003CAddDeck\u003Eb__73_0);
        }
        else
          EventDelegate.Set(indicatorScript.friendSelect.onClick, new EventDelegate.Callback(quest0028Menu.IbtnSelectFriend));
      }
      else if (friend == null)
        EventDelegate.Set(indicatorScript.friendSelect.onClick, new EventDelegate.Callback(quest0028Menu.IbtnBack));
    }
  }

  protected IEnumerator resetHelper(PlayerHelper friend)
  {
    Quest0028Menu quest0028Menu = this;
    quest0028Menu.friend = friend;
    foreach (KeyValuePair<int, Quest0028Indicator> indicator in quest0028Menu.indicators)
    {
      Quest0028Indicator indsc = indicator.Value;
      IEnumerator e = indsc.resetHelper(friend, quest0028Menu.bonusTimeTables, quest0028Menu.unitBonus);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (!quest0028Menu.isUserDeckStage && !quest0028Menu.isLimitation && quest0028Menu.friendPositionEnable)
      {
        if (quest0028Menu.onSetHelper_ != null)
        {
          if ((UnityEngine.Object) indsc.friendIcon_ != (UnityEngine.Object) null)
          {
            // ISSUE: reference to a compiler-generated method
            indsc.friendIcon_.onClick = new System.Action<UnitIconBase>(quest0028Menu.\u003CresetHelper\u003Eb__74_0);
          }
          else
            EventDelegate.Set(indsc.friendSelect.onClick, new EventDelegate.Callback(quest0028Menu.IbtnSelectFriend));
        }
        else if (friend == null)
          EventDelegate.Set(indsc.friendSelect.onClick, new EventDelegate.Callback(quest0028Menu.IbtnBack));
      }
      indsc = (Quest0028Indicator) null;
    }
  }

  protected virtual void preInitializeIndicator(Quest0028Indicator indicator)
  {
  }

  public virtual void IbtnItemedit()
  {
    if (this.IsPushAndSet())
      return;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
      this.StartCoroutine(this.changeSceneSupplyEdit());
    else
      Quest00210Scene.changeScene(true);
  }

  private IEnumerator changeSceneSupplyEdit()
  {
    Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Quest00210Scene.changeScene(true);
    yield break;
  }

  protected bool QuestStart()
  {
    bool flag = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 4;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    Player player = SMManager.Get<Player>();
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    int ap = player.ap;
    int num = Math.Max(player.max_cost, playerDeck.cost_limit);
    if (playerDeck.cost <= num)
    {
      if (ap < this.lost_ap)
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        Singleton<PopupManager>.GetInstance().open(this.apPopup, false, false, false, true, false, false, "SE_1006");
      }
      else
      {
        Quest0028Menu.isGoingToBattle = true;
        if (Singleton<NGGameDataManager>.GetInstance().IsColosseum)
        {
          Singleton<NGGameDataManager>.GetInstance().IsColosseum = false;
          Singleton<CommonRoot>.GetInstance().SetFooterEnable(true);
        }
        if (this.indicators.ContainsKey(this.selectDeck))
          Singleton<NGSoundManager>.GetInstance().playVoiceByID(this.indicators[this.selectDeck].deckUnitData[0].unit.unitVoicePattern, 70, 0, 0.0f);
        flag = true;
        this.indicator.SeEnable = false;
        if (this.story_quest != null)
          this.StartCoroutine(this.StoryMovie(this.story_quest.quest_story_s.ID, this.story_quest.is_new, new System.Action(this.StoryStartApi)));
        else if (this.extra_quest != null)
          this.StartCoroutine(this.StoryMovie(this.extra_quest.quest_extra_s.ID, this.extra_quest.is_new, new System.Action(this.ExtraStartApi)));
        else if (this.char_quest != null)
          this.StartCoroutine(this.StoryMovie(this.char_quest.quest_character_s.ID, this.char_quest.is_new, new System.Action(this.CharacterStartApi)));
        else if (this.convert_quest != null)
        {
          System.Action act = (System.Action) null;
          if (this.convert_quest.questS.data_type == QuestSConverter.DataType.Character)
            act = new System.Action(this.CharacterStartApi2);
          else if (this.convert_quest.questS.data_type == QuestSConverter.DataType.Harmony)
            act = new System.Action(this.HarmonyStartApi);
          this.StartCoroutine(this.StoryMovie(this.convert_quest.questS.ID, this.convert_quest.is_new, act));
        }
        else if (this.sea_quest != null)
        {
          this.StartCoroutine(this.StoryMovie(this.sea_quest.quest_sea_s.ID, this.sea_quest.is_new, new System.Action(this.SeaStartApi)));
        }
        else
        {
          Debug.LogError((object) "!!! BUG !!!");
          flag = false;
        }
      }
    }
    else
    {
      this.StartCoroutine(PopupCommon.Show(Consts.GetInstance().QUEST_0028_SORTIE_TITLE, Consts.GetInstance().QUEST_0028_COST_OVER_DESCRIPTION, (System.Action) null));
      Singleton<CommonRoot>.GetInstance().isLoading = false;
    }
    return flag;
  }

  public virtual void IbtnSortie()
  {
    if (this.IsPush)
      return;
    this.QuestStart();
  }

  private void StartBattle(BattleInfo info)
  {
    if (this.story_only)
    {
      int scriptId = this.getScriptID(info);
      if (scriptId != -1)
      {
        this.battleInfo = info;
        this.IsPlayingStory = true;
        Story0093Scene.changeScene(true, scriptId, new bool?(Singleton<NGGameDataManager>.GetInstance().IsSea && info.seaQuest != null), (System.Action) null);
      }
      else
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }
    else
    {
      NGBattleManager instance = Singleton<NGBattleManager>.GetInstance();
      instance.deleteSavedEnvironment();
      instance.startBattle(info, 0);
    }
  }

  private IEnumerator StoryMovie(int id, bool is_new, System.Action act)
  {
    if (!is_new || (UnityEngine.Object) this.movieObj == (UnityEngine.Object) null)
    {
      act();
    }
    else
    {
      yield return (object) null;
      if (this.movieObj.isPlayMovie(id))
      {
        while (Singleton<NGSoundManager>.GetInstance().IsVoicePlaying(0))
          yield return (object) null;
        this.isPlayingMovie = true;
        this.movieObj.Attach(id, act);
      }
      else
        act();
    }
  }

  private void StoryStartApi()
  {
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    WebAPI.BattleStoryStart(playerDeck.deck_number, playerDeck.deck_type_id, this.story_quest.quest_story_s.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    })).RunOn<WebAPI.Response.BattleStoryStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.BattleStoryStart>) (battle =>
    {
      if (battle == null)
        return;
      for (int index = 0; index < battle.helpers.Length; ++index)
      {
        battle.helpers[index].leader_unit = battle.helper_player_units[index];
        battle.helpers[index].leader_unit.importOverkillersUnits(battle.helper_player_unit_over_killers, false);
        battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
      }
      int[] guestsId = GuestUnit.GetGuestsID(battle.quest_s_id);
      this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, ((IEnumerable<PlayerHelper>) battle.helpers).FirstOrDefault<PlayerHelper>(), battle.enemy, ((IEnumerable<WebAPI.Response.BattleStoryStartEnemy_item>) battle.enemy_item).Select<WebAPI.Response.BattleStoryStartEnemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleStoryStartEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.user_deck_units, battle.user_deck_gears, battle.user_deck_enemy, ((IEnumerable<WebAPI.Response.BattleStoryStartUser_deck_enemy_item>) battle.user_deck_enemy_item).Select<WebAPI.Response.BattleStoryStartUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleStoryStartUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.panel, ((IEnumerable<WebAPI.Response.BattleStoryStartPanel_item>) battle.panel_item).Select<WebAPI.Response.BattleStoryStartPanel_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleStoryStartPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), guestsId, (PlayerUnit[]) null, (Tuple<int, int>[]) null));
    }));
    Persist.lastsortie.Data.SaveLastSortie(this.story_quest.quest_story_s.ID, this.story_quest.quest_story_s.quest_m_QuestStoryM, this.story_quest.quest_story_s.quest_l_QuestStoryL);
    Persist.lastsortie.Flush();
    if (MasterData.QuestStoryS[this.story_quest.quest_story_s.ID].quest_xl_QuestStoryXL == 6)
      Persist.integralNoahProcess.Data.lastIntegralNoahSId = this.story_quest.quest_story_s.ID;
    if (!Persist.storyModePopupInfo.Exists)
    {
      try
      {
        Persist.storyModePopupInfo.Data.reset();
        Persist.storyModePopupInfo.Flush();
      }
      catch
      {
        Persist.storyModePopupInfo.Delete();
        Persist.storyModePopupInfo.Data.reset();
      }
    }
    if (!Persist.storyModePopupInfo.Exists || Persist.storyModePopupInfo.Data.alreadyShow)
      return;
    PlayerStoryQuestS[] playerStoryQuestSArray = SMManager.Get<PlayerStoryQuestS[]>();
    if (playerStoryQuestSArray == null)
      return;
    if (!((IEnumerable<PlayerStoryQuestS>) playerStoryQuestSArray).Any<PlayerStoryQuestS>((Func<PlayerStoryQuestS, bool>) (x => x.quest_story_s.quest_l_QuestStoryL >= 19)))
      return;
    try
    {
      Persist.storyModePopupInfo.Data.alreadyShow = true;
      Persist.storyModePopupInfo.Flush();
    }
    catch
    {
    }
  }

  private void ExtraStartApi()
  {
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    if (this.extra_quest.quest_extra_s.wave != null)
      WebAPI.BattleWaveStart(playerDeck.deck_number, playerDeck.deck_type_id, this.extra_quest.quest_extra_s.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      })).RunOn<WebAPI.Response.BattleWaveStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.BattleWaveStart>) (battle =>
      {
        if (battle == null)
          return;
        for (int index = 0; index < battle.helpers.Length; ++index)
        {
          battle.helpers[index].leader_unit = battle.helper_player_units[index];
          battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
          battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
          battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
          battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
          battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
        }
        int[] guests = battle.wave_stage == null || battle.wave_stage.Length == 0 ? GuestUnit.GetGuestsID(battle.quest_s_id) : GuestUnit.GetGuestsID(battle.wave_stage[0].stage_id);
        List<BattleInfo.Wave> waveList = new List<BattleInfo.Wave>();
        foreach (BattleWaveStageInfo battleWaveStageInfo in battle.wave_stage)
          waveList.Add(new BattleInfo.Wave()
          {
            stage_id = battleWaveStageInfo.stage_id,
            enemies = battleWaveStageInfo.enemy,
            enemy_items = ((IEnumerable<BattleWaveStageInfoEnemy_item>) battleWaveStageInfo.enemy_item).Select<BattleWaveStageInfoEnemy_item, Tuple<int, int, int, int>>((Func<BattleWaveStageInfoEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(),
            user_enemies = battleWaveStageInfo.user_deck_enemy,
            user_enemy_items = ((IEnumerable<BattleWaveStageInfoUser_deck_enemy_item>) battleWaveStageInfo.user_deck_enemy_item).Select<BattleWaveStageInfoUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<BattleWaveStageInfoUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(),
            panels = battleWaveStageInfo.panel,
            panel_items = ((IEnumerable<BattleWaveStageInfoPanel_item>) battleWaveStageInfo.panel_item).Select<BattleWaveStageInfoPanel_item, Tuple<int, int, int, int>>((Func<BattleWaveStageInfoPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(),
            user_units = battleWaveStageInfo.user_deck_units,
            user_items = battleWaveStageInfo.user_deck_gears
          });
        this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, ((IEnumerable<PlayerHelper>) battle.helpers).FirstOrDefault<PlayerHelper>(), guests, (IEnumerable<BattleInfo.Wave>) waveList));
      }));
    else
      WebAPI.BattleExtraStart(playerDeck.deck_number, playerDeck.deck_type_id, this.extra_quest.quest_extra_s.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<NGSceneManager>.GetInstance().destroyLoadedScenes();
        Singleton<NGSceneManager>.GetInstance().clearStack();
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      })).RunOn<WebAPI.Response.BattleExtraStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.BattleExtraStart>) (battle =>
      {
        if (battle == null)
          return;
        for (int index = 0; index < battle.helpers.Length; ++index)
        {
          battle.helpers[index].leader_unit = battle.helper_player_units[index];
          battle.helpers[index].leader_unit.importOverkillersUnits(battle.helper_player_unit_over_killers, false);
          battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
          battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
          battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
          battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
          battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
        }
        int[] guestsId = GuestUnit.GetGuestsID(battle.quest_s_id);
        this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, ((IEnumerable<PlayerHelper>) battle.helpers).FirstOrDefault<PlayerHelper>(), battle.enemy, ((IEnumerable<WebAPI.Response.BattleExtraStartEnemy_item>) battle.enemy_item).Select<WebAPI.Response.BattleExtraStartEnemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleExtraStartEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.user_deck_units, battle.user_deck_gears, battle.user_deck_enemy, ((IEnumerable<WebAPI.Response.BattleExtraStartUser_deck_enemy_item>) battle.user_deck_enemy_item).Select<WebAPI.Response.BattleExtraStartUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleExtraStartUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.panel, ((IEnumerable<WebAPI.Response.BattleExtraStartPanel_item>) battle.panel_item).Select<WebAPI.Response.BattleExtraStartPanel_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleExtraStartPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), guestsId, (PlayerUnit[]) null, (Tuple<int, int>[]) null));
      }));
  }

  private void CharacterStartApi()
  {
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    WebAPI.BattleCharacterStart(playerDeck.deck_number, playerDeck.deck_type_id, this.char_quest.quest_character_s.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    })).RunOn<WebAPI.Response.BattleCharacterStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.BattleCharacterStart>) (battle =>
    {
      if (battle == null)
        return;
      for (int index = 0; index < battle.helpers.Length; ++index)
      {
        battle.helpers[index].leader_unit = battle.helper_player_units[index];
        battle.helpers[index].leader_unit.importOverkillersUnits(battle.helper_player_unit_over_killers, false);
        battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
      }
      int[] guestsId = GuestUnit.GetGuestsID(battle.quest_s_id);
      this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, ((IEnumerable<PlayerHelper>) battle.helpers).FirstOrDefault<PlayerHelper>(), battle.enemy, ((IEnumerable<WebAPI.Response.BattleCharacterStartEnemy_item>) battle.enemy_item).Select<WebAPI.Response.BattleCharacterStartEnemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleCharacterStartEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.user_deck_units, battle.user_deck_gears, battle.user_deck_enemy, ((IEnumerable<WebAPI.Response.BattleCharacterStartUser_deck_enemy_item>) battle.user_deck_enemy_item).Select<WebAPI.Response.BattleCharacterStartUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleCharacterStartUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.panel, ((IEnumerable<WebAPI.Response.BattleCharacterStartPanel_item>) battle.panel_item).Select<WebAPI.Response.BattleCharacterStartPanel_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleCharacterStartPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), guestsId, (PlayerUnit[]) null, (Tuple<int, int>[]) null));
    }));
  }

  private void CharacterStartApi2()
  {
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    WebAPI.BattleCharacterStart(playerDeck.deck_number, playerDeck.deck_type_id, this.convert_quest.questS.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    })).RunOn<WebAPI.Response.BattleCharacterStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.BattleCharacterStart>) (battle =>
    {
      if (battle == null)
        return;
      for (int index = 0; index < battle.helpers.Length; ++index)
      {
        battle.helpers[index].leader_unit = battle.helper_player_units[index];
        battle.helpers[index].leader_unit.importOverkillersUnits(battle.helper_player_unit_over_killers, false);
        battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
      }
      int[] guestsId = GuestUnit.GetGuestsID(battle.quest_s_id);
      this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, ((IEnumerable<PlayerHelper>) battle.helpers).FirstOrDefault<PlayerHelper>(), battle.enemy, ((IEnumerable<WebAPI.Response.BattleCharacterStartEnemy_item>) battle.enemy_item).Select<WebAPI.Response.BattleCharacterStartEnemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleCharacterStartEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.user_deck_units, battle.user_deck_gears, battle.user_deck_enemy, ((IEnumerable<WebAPI.Response.BattleCharacterStartUser_deck_enemy_item>) battle.user_deck_enemy_item).Select<WebAPI.Response.BattleCharacterStartUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleCharacterStartUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.panel, ((IEnumerable<WebAPI.Response.BattleCharacterStartPanel_item>) battle.panel_item).Select<WebAPI.Response.BattleCharacterStartPanel_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleCharacterStartPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), guestsId, (PlayerUnit[]) null, (Tuple<int, int>[]) null));
    }));
  }

  private void HarmonyStartApi()
  {
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    WebAPI.BattleHarmonyStart(playerDeck.deck_number, playerDeck.deck_type_id, this.convert_quest.questS.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      Singleton<CommonRoot>.GetInstance().isLoading = false;
      WebAPI.DefaultUserErrorCallback(error);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    })).RunOn<WebAPI.Response.BattleHarmonyStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.BattleHarmonyStart>) (battle =>
    {
      if (battle == null)
        return;
      for (int index = 0; index < battle.helpers.Length; ++index)
      {
        battle.helpers[index].leader_unit = battle.helper_player_units[index];
        battle.helpers[index].leader_unit.importOverkillersUnits(battle.helper_player_unit_over_killers, false);
        battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
      }
      int[] guestsId = GuestUnit.GetGuestsID(battle.quest_s_id);
      this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, ((IEnumerable<PlayerHelper>) battle.helpers).FirstOrDefault<PlayerHelper>(), battle.enemy, ((IEnumerable<WebAPI.Response.BattleHarmonyStartEnemy_item>) battle.enemy_item).Select<WebAPI.Response.BattleHarmonyStartEnemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleHarmonyStartEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.user_deck_units, battle.user_deck_gears, battle.user_deck_enemy, ((IEnumerable<WebAPI.Response.BattleHarmonyStartUser_deck_enemy_item>) battle.user_deck_enemy_item).Select<WebAPI.Response.BattleHarmonyStartUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleHarmonyStartUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.panel, ((IEnumerable<WebAPI.Response.BattleHarmonyStartPanel_item>) battle.panel_item).Select<WebAPI.Response.BattleHarmonyStartPanel_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.BattleHarmonyStartPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), guestsId, (PlayerUnit[]) null, (Tuple<int, int>[]) null));
    }));
  }

  private void SeaStartApi()
  {
    PlayerDeck playerDeck = this.playerDecks[this.selectDeck];
    WebAPI.SeaBattleStart(playerDeck.deck_number, playerDeck.deck_type_id, this.sea_quest.quest_sea_s.ID, this.friend == null ? "" : this.friend.target_player_id, this.friend == null ? 0 : this.friend.leader_player_unit_id, (System.Action<WebAPI.Response.UserError>) (error =>
    {
      if (string.Equals(error.Code, "SEA000"))
      {
        this.StartCoroutine(PopupUtility.SeaError(error));
      }
      else
      {
        Singleton<CommonRoot>.GetInstance().isLoading = false;
        WebAPI.DefaultUserErrorCallback(error);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }
    })).RunOn<WebAPI.Response.SeaBattleStart>((MonoBehaviour) Singleton<NGSceneManager>.GetInstance(), (System.Action<WebAPI.Response.SeaBattleStart>) (battle =>
    {
      if (battle == null)
        return;
      for (int index = 0; index < battle.helpers.Length; ++index)
      {
        battle.helpers[index].leader_unit = battle.helper_player_units[index];
        battle.helpers[index].leader_unit.importOverkillersUnits(battle.helper_player_unit_over_killers, false);
        battle.helpers[index].leader_unit.primary_equipped_gear = battle.helpers[index].leader_unit.FindEquippedGear(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_gear2 = battle.helpers[index].leader_unit.FindEquippedGear2(battle.helper_player_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou = battle.helpers[index].leader_unit.FindEquippedReisou(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_reisou2 = battle.helpers[index].leader_unit.FindEquippedReisou2(battle.helper_player_gears, battle.helper_player_reisou_gears);
        battle.helpers[index].leader_unit.primary_equipped_awake_skill = battle.helpers[index].leader_unit.FindEquippedExtraSkill(battle.helper_player_awake_skills);
      }
      PlayerHelper helper = (PlayerHelper) null;
      if (((IEnumerable<SeaPlayerHelper>) battle.helpers).FirstOrDefault<SeaPlayerHelper>() != null)
        helper = new Helper(((IEnumerable<SeaPlayerHelper>) battle.helpers).FirstOrDefault<SeaPlayerHelper>()).Clone();
      int[] guestsId = GuestUnit.GetGuestsID(battle.quest_s_id);
      this.StartBattle(BattleInfo.MakeBattleInfo(battle.battle_uuid, (CommonQuestType) battle.quest_type, battle.quest_s_id, battle.deck_type_id, battle.quest_loop_count, battle.deck_number, helper, battle.enemy, ((IEnumerable<WebAPI.Response.SeaBattleStartEnemy_item>) battle.enemy_item).Select<WebAPI.Response.SeaBattleStartEnemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.SeaBattleStartEnemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.user_deck_units, battle.user_deck_gears, battle.user_deck_enemy, ((IEnumerable<WebAPI.Response.SeaBattleStartUser_deck_enemy_item>) battle.user_deck_enemy_item).Select<WebAPI.Response.SeaBattleStartUser_deck_enemy_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.SeaBattleStartUser_deck_enemy_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), battle.panel, ((IEnumerable<WebAPI.Response.SeaBattleStartPanel_item>) battle.panel_item).Select<WebAPI.Response.SeaBattleStartPanel_item, Tuple<int, int, int, int>>((Func<WebAPI.Response.SeaBattleStartPanel_item, Tuple<int, int, int, int>>) (x => Tuple.Create<int, int, int, int>(x.id, x.reward_type_id, x.reward_id, x.reward_quantity))).ToArray<Tuple<int, int, int, int>>(), guestsId, (PlayerUnit[]) null, (Tuple<int, int>[]) null));
    }));
    Persist.lastsortie.Data.SaveLastSortie(this.sea_quest.quest_sea_s.ID, this.sea_quest.quest_sea_s.quest_m_QuestSeaM, this.sea_quest.quest_sea_s.quest_l_QuestSeaL);
    Persist.lastsortie.Flush();
  }

  private IEnumerator SetSupplyIcons(List<PlayerItem> SupplyList)
  {
    Future<GameObject> prefabF = !Singleton<NGGameDataManager>.GetInstance().IsSea || this.sea_quest == null ? Res.Prefabs.ItemIcon.prefab.Load<GameObject>() : new ResourceObject("Prefabs/Sea/ItemIcon/prefab_sea").Load<GameObject>();
    IEnumerator e = prefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    GameObject prefab = prefabF.Result;
    for (int i = 0; i < SupplyList.Count; ++i)
    {
      GameObject gameObject = prefab.Clone(this.dir_Items[i].transform);
      gameObject.transform.localScale = new Vector3(0.7352941f, 0.7352941f);
      e = gameObject.GetComponent<ItemIcon>().InitByPlayerItem(SupplyList[i]);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    for (int count = SupplyList.Count; count < Consts.GetInstance().DECK_SUPPLY_MAX; ++count)
    {
      GameObject gameObject = prefab.Clone(this.dir_Items[count].transform);
      gameObject.transform.localScale = new Vector3(0.7352941f, 0.7352941f);
      ItemIcon component = gameObject.GetComponent<ItemIcon>();
      component.SetModeSupply();
      component.SetEmpty(true);
    }
  }

  public void EndScene()
  {
    for (int index = 0; index < Consts.GetInstance().DECK_SUPPLY_MAX; ++index)
    {
      foreach (Component componentsInChild in this.dir_Items[index].GetComponentsInChildren<ItemIcon>())
        UnityEngine.Object.Destroy((UnityEngine.Object) componentsInChild.gameObject);
    }
    foreach (Quest0028Indicator quest0028Indicator in this.indicators.Values)
      quest0028Indicator.DestroyObject();
    this.indicators.Clear();
    this.SaveSetting();
  }

  protected void SaveSetting()
  {
    if (this.story_only)
      return;
    if (this.isSea_)
    {
      Persist.seaDeckOrganized.Data.number = this.deckIndexs[Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1)];
      Persist.seaDeckOrganized.Flush();
    }
    else
    {
      Persist.deckOrganized.Data.number = this.deckIndexs[Mathf.Clamp(this.indicator.selected, 0, this.playerDecks.Length - 1)];
      Persist.deckOrganized.Flush();
    }
  }

  private IEnumerator WaitScrollSe()
  {
    yield return (object) new WaitForSeconds(0.3f);
    this.indicator.SeEnable = true;
  }

  public void BtnRepair()
  {
    if (this.IsPushAndSet())
      return;
    if (Singleton<NGGameDataManager>.GetInstance().IsSea)
    {
      this.StartCoroutine(this.changeSceneRepair());
    }
    else
    {
      Bugu00524Scene.ChangeScene(true);
      this.onChangedSceneRepair(false);
    }
  }

  protected virtual void onChangedSceneRepair(bool isSea)
  {
  }

  private IEnumerator changeSceneRepair()
  {
    Singleton<CommonRoot>.GetInstance().headerType = CommonRoot.HeaderType.Normal;
    Bugu00524Scene.ChangeScene(true);
    this.onChangedSceneRepair(true);
    yield break;
  }

  public void BtnOrganization()
  {
    if (this.IsPushAndSet())
      return;
    Unit0046Scene.changeScene(true, this.regulation.limitationParams, this.limitationLabel, false);
  }

  public void BtnWeaponChange()
  {
    if (this.IsPushAndSet())
      return;
    Unit00468Scene.changeScene00412(true);
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    NGSceneManager instance1 = Singleton<NGSceneManager>.GetInstance();
    NGGameDataManager instance2 = Singleton<NGGameDataManager>.GetInstance();
    if (instance1.HasSavedChangeSceneParam() && instance2.IsFromUnityPopupStageList)
    {
      NGSceneManager.ChangeSceneParam changeSceneParam = instance1.GetSavedChangeSceneParam();
      string sceneName = changeSceneParam.sceneName;
      instance2.fromUnityPopup = sceneName == "unit004_8_4" ? NGGameDataManager.FromUnityPopup.Unit00484Scene : (sceneName == "unit004_JobChange" ? NGGameDataManager.FromUnityPopup.Unit004JobChangeScene : (sceneName == "unit004_2" || sceneName == "unit004_2_sea" ? NGGameDataManager.FromUnityPopup.Unit0042Scene : NGGameDataManager.FromUnityPopup.None));
      instance1.ModifySceneStack(changeSceneParam);
      instance1.ClearSavedChangeSceneParam();
      instance2.IsFromUnityPopupStageList = false;
      instance2.QuestType = new CommonQuestType?();
    }
    this.backScene();
  }

  public override void onBackButton()
  {
    if (this.isPlayingMovie)
      return;
    this.IbtnBack();
  }

  public virtual void IbtnSelectFriend()
  {
    if (this.IsPushAndSet())
      return;
    if (this.story_quest != null)
      Quest00282Scene.changeScene(true, this.story_quest, this.onSetHelper_);
    else if (this.extra_quest != null)
      Quest00282Scene.changeScene(true, this.extra_quest, this.onSetHelper_);
    else if (this.char_quest != null)
      Quest00282Scene.changeScene(true, this.char_quest, this.onSetHelper_);
    else if (this.convert_quest != null)
    {
      Quest00282Scene.changeScene(true, this.convert_quest, this.onSetHelper_);
    }
    else
    {
      if (this.sea_quest == null)
        return;
      Quest00282Scene.changeScene(true, this.sea_quest, this.onSetHelper_);
    }
  }

  public void IbtnBattleSetting()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().monitorCoroutine(this.doPopupBattleSetting());
  }

  private IEnumerator doPopupBattleSetting()
  {
    Quest0028Menu quest0028Menu = this;
    IEnumerator e = Quest0028PopupBattleSetting.show();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    quest0028Menu.IsPush = false;
  }

  private int getScriptID(BattleInfo info)
  {
    int num = -1;
    switch (info.quest_type)
    {
      case CommonQuestType.Story:
        StoryPlaybackStoryDetail playbackStoryDetail = ((IEnumerable<StoryPlaybackStoryDetail>) MasterData.StoryPlaybackStoryDetailList).Where<StoryPlaybackStoryDetail>((Func<StoryPlaybackStoryDetail, bool>) (x => x.quest_s_id_QuestStoryS == info.quest_s_id && x.timing == StoryPlaybackTiming.select_stage)).FirstOrDefault<StoryPlaybackStoryDetail>();
        if (playbackStoryDetail != null)
        {
          num = playbackStoryDetail.script_id;
          break;
        }
        break;
      case CommonQuestType.Character:
        StoryPlaybackCharacterDetail playbackCharacterDetail = ((IEnumerable<StoryPlaybackCharacterDetail>) MasterData.StoryPlaybackCharacterDetailList).Where<StoryPlaybackCharacterDetail>((Func<StoryPlaybackCharacterDetail, bool>) (x => x.quest_QuestCharacterS == info.quest_s_id && x.timing == StoryPlaybackTiming.select_stage)).FirstOrDefault<StoryPlaybackCharacterDetail>();
        if (playbackCharacterDetail != null)
        {
          num = playbackCharacterDetail.script_id;
          break;
        }
        break;
      case CommonQuestType.Extra:
        StoryPlaybackExtraDetail playbackExtraDetail = ((IEnumerable<StoryPlaybackExtraDetail>) MasterData.StoryPlaybackExtraDetailList).Where<StoryPlaybackExtraDetail>((Func<StoryPlaybackExtraDetail, bool>) (x => x.quest_QuestExtraS == info.quest_s_id && x.timing == StoryPlaybackTiming.select_stage)).FirstOrDefault<StoryPlaybackExtraDetail>();
        if (playbackExtraDetail != null)
        {
          num = playbackExtraDetail.script_id;
          break;
        }
        break;
      case CommonQuestType.Sea:
        StoryPlaybackSeaDetail playbackSeaDetail = ((IEnumerable<StoryPlaybackSeaDetail>) MasterData.StoryPlaybackSeaDetailList).Where<StoryPlaybackSeaDetail>((Func<StoryPlaybackSeaDetail, bool>) (x => x.quest_s_id_QuestSeaS == info.quest_s_id && x.timing == StoryPlaybackTiming.select_stage)).FirstOrDefault<StoryPlaybackSeaDetail>();
        if (playbackSeaDetail != null)
        {
          num = playbackSeaDetail.script_id;
          break;
        }
        break;
    }
    return num;
  }

  public static bool IsExtraLimitation(PlayerExtraQuestS extraQuest)
  {
    return ((IEnumerable<QuestExtraLimitation>) MasterData.QuestExtraLimitationList).Any<QuestExtraLimitation>((Func<QuestExtraLimitation, bool>) (n => n.quest_s_id_QuestExtraS == extraQuest.quest_extra_s.ID));
  }

  public static bool IsStoryLimitation(PlayerStoryQuestS storyQuest)
  {
    return ((IEnumerable<QuestStoryLimitation>) MasterData.QuestStoryLimitationList).Any<QuestStoryLimitation>((Func<QuestStoryLimitation, bool>) (n => n.quest_s_id_QuestStoryS == storyQuest.quest_story_s.ID));
  }

  public static bool IsCharaLimitation(PlayerCharacterQuestS charaQuest)
  {
    return ((IEnumerable<QuestCharacterLimitation>) MasterData.QuestCharacterLimitationList).Any<QuestCharacterLimitation>((Func<QuestCharacterLimitation, bool>) (n => n.quest_s_id_QuestCharacterS == charaQuest.quest_character_s.ID));
  }

  public static bool IsConvertLimitation(PlayerQuestSConverter convertQuest)
  {
    if (convertQuest.questS.data_type == QuestSConverter.DataType.Character)
      return ((IEnumerable<QuestCharacterLimitation>) MasterData.QuestCharacterLimitationList).Any<QuestCharacterLimitation>((Func<QuestCharacterLimitation, bool>) (n => n.quest_s_id_QuestCharacterS == convertQuest.questS.ID));
    return convertQuest.questS.data_type == QuestSConverter.DataType.Harmony && ((IEnumerable<QuestHarmonyLimitation>) MasterData.QuestHarmonyLimitationList).Any<QuestHarmonyLimitation>((Func<QuestHarmonyLimitation, bool>) (n => n.quest_s_id_QuestHarmonyS == convertQuest.questS.ID));
  }

  public static bool IsSeaLimitation(PlayerSeaQuestS seaQuest)
  {
    return ((IEnumerable<QuestStoryLimitation>) MasterData.QuestStoryLimitationList).Any<QuestStoryLimitation>((Func<QuestStoryLimitation, bool>) (n => n.quest_s_id_QuestStoryS == seaQuest.quest_sea_s.ID));
  }

  public static bool GetFriendPositionEnable(int stageID)
  {
    BattleStageGuest[] array = ((IEnumerable<BattleStageGuest>) MasterData.BattleStageGuestList).Where<BattleStageGuest>((Func<BattleStageGuest, bool>) (x => x.stage_BattleStage == stageID)).ToArray<BattleStageGuest>();
    return MasterData.BattleStagePlayer.Any<KeyValuePair<int, BattleStagePlayer>>((Func<KeyValuePair<int, BattleStagePlayer>, bool>) (x => x.Value.stage_BattleStage == stageID && x.Value.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND)) && !((IEnumerable<BattleStageGuest>) array).Any<BattleStageGuest>((Func<BattleStageGuest, bool>) (x => x.deck_position == Consts.GetInstance().DECK_POSITION_FRIEND));
  }

  private enum BtnType
  {
    BATTLE,
    REPAIR,
    EDIT,
  }

  protected class LimitedQuestData
  {
    private PlayerDeck[] deck;
    private QuestLimitationBase[] limitation;

    public PlayerDeck[] Deck
    {
      get
      {
        return this.deck;
      }
    }

    public QuestLimitationBase[] limitationParams
    {
      get
      {
        return this.limitation;
      }
    }

    public void SetInfo(PlayerDeck[] tmp, QuestLimitationBase[] limits)
    {
      this.deck = tmp;
      this.limitation = limits;
    }
  }
}
