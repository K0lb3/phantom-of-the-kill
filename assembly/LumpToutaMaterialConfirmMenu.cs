﻿// Decompiled with JetBrains decompiler
// Type: LumpToutaMaterialConfirmMenu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class LumpToutaMaterialConfirmMenu : UnitSelectMenuBase
{
  private List<PlayerUnit> selectedBasePlayerUnits = new List<PlayerUnit>();
  private List<List<UnitIconInfo>> selectedMaterialUnitIconInfos = new List<List<UnitIconInfo>>();
  private Dictionary<int, GameObject> cacheWidthLines = new Dictionary<int, GameObject>();
  private const int COLUM_COUNT = 5;
  [SerializeField]
  private GameObject baseWidthLine;
  [SerializeField]
  private UILabel useZeny;
  [SerializeField]
  private UILabel selectedCount;
  [SerializeField]
  private SpreadColorButton DecisionButton;

  public IEnumerator StartAsync(
    List<PlayerUnit> selectedBasePlayerUnits,
    Dictionary<PlayerUnit, List<PlayerUnit>> allSamePlayerUnits)
  {
    LumpToutaMaterialConfirmMenu materialConfirmMenu = this;
    materialConfirmMenu.isMaterial = true;
    IEnumerator e = materialConfirmMenu.Initialize();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    List<PlayerUnit> playerUnitList1 = new List<PlayerUnit>();
    float unityValueMax = (float) PlayerUnit.GetUnityValueMax();
    foreach (PlayerUnit selectedBasePlayerUnit in selectedBasePlayerUnits)
    {
      materialConfirmMenu.selectedBasePlayerUnits.Add(selectedBasePlayerUnit);
      float unityTotal = selectedBasePlayerUnit.unityTotal;
      float num = 0.0f;
      List<PlayerUnit> playerUnitList2 = new List<PlayerUnit>();
      bool flag = false;
      foreach (PlayerUnit playerUnit in (IEnumerable<PlayerUnit>) allSamePlayerUnits[selectedBasePlayerUnit].OrderBy<PlayerUnit, int>((Func<PlayerUnit, int>) (x => x.unit.rarity.index)))
      {
        if (selectedBasePlayerUnit.id != playerUnit.id)
        {
          if (!flag)
          {
            if (playerUnitList2.Count < 30)
            {
              if (!materialConfirmMenu.IsExclusionUnitForLumpToutaMaterial(playerUnit))
              {
                ++num;
                if ((double) num + (double) unityTotal >= (double) unityValueMax)
                  flag = true;
                playerUnitList2.Add(playerUnit);
              }
            }
            else
              break;
          }
          else
            break;
        }
      }
      playerUnitList1.AddRange((IEnumerable<PlayerUnit>) playerUnitList2);
    }
    materialConfirmMenu.SetIconType(UnitMenuBase.IconType.Normal);
    materialConfirmMenu.IconHeight = UnitIcon.HeightWithHpGauge;
    // ISSUE: reference to a compiler-generated method
    materialConfirmMenu.InitializeInfo((IEnumerable<PlayerUnit>) playerUnitList1.ToArray(), (IEnumerable<PlayerMaterialUnit>) null, (Persist<Persist.UnitSortAndFilterInfo>) null, false, false, true, true, false, new System.Action(materialConfirmMenu.\u003CStartAsync\u003Eb__8_0), 0);
    e = materialConfirmMenu.CreateUnitIcon();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    materialConfirmMenu.InitializeEnd();
    materialConfirmMenu.UpdateBottomInfo();
  }

  protected override IEnumerator CreateUnitIcon(
    int info_index,
    int unit_index,
    PlayerUnit baseUnit = null)
  {
    // ISSUE: reference to a compiler-generated field
    int num = this.\u003C\u003E1__state;
    LumpToutaMaterialConfirmMenu materialConfirmMenu = this;
    if (num != 0)
    {
      if (num != 1)
        return false;
      // ISSUE: reference to a compiler-generated field
      this.\u003C\u003E1__state = -1;
      materialConfirmMenu.allUnitIcons[unit_index].gameObject.transform.localScale = new Vector3(0.8f, 0.8f, 1f);
      return false;
    }
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = -1;
    // ISSUE: reference to a compiler-generated field
    // ISSUE: reference to a compiler-generated method
    this.\u003C\u003E2__current = (object) materialConfirmMenu.\u003C\u003En__0(info_index, unit_index, baseUnit);
    // ISSUE: reference to a compiler-generated field
    this.\u003C\u003E1__state = 1;
    return true;
  }

  private void ExInitializeInfo()
  {
    foreach (PlayerUnit selectedBasePlayerUnit in this.selectedBasePlayerUnits)
    {
      List<UnitIconInfo> unitIconInfoList = new List<UnitIconInfo>();
      foreach (UnitIconInfo allUnitInfo in this.allUnitInfos)
      {
        if (allUnitInfo.unit.same_character_id == selectedBasePlayerUnit.unit.same_character_id)
          unitIconInfoList.Add(allUnitInfo);
      }
      this.selectedMaterialUnitIconInfos.Add(unitIconInfoList);
    }
    foreach (UnitIconInfo allUnitInfo in this.allUnitInfos)
      allUnitInfo.select = 0;
  }

  private void ShowWidthLine(int index)
  {
    if (this.cacheWidthLines.Count < 3)
    {
      for (int count = this.cacheWidthLines.Count; count < 3; ++count)
      {
        if (!this.cacheWidthLines.ContainsKey(count * 5))
        {
          GameObject widthLine = createWidthLine(count * 5);
          this.cacheWidthLines.Add(count * 5, widthLine);
        }
      }
    }
    if (!this.cacheWidthLines.ContainsKey(index))
    {
      GameObject widthLine = createWidthLine(index);
      this.cacheWidthLines.Add(index, widthLine);
    }
    foreach (KeyValuePair<int, GameObject> cacheWidthLine in this.cacheWidthLines)
    {
      if (cacheWidthLine.Key <= index - this.IconMaxValue || cacheWidthLine.Key >= index + this.IconMaxValue)
        cacheWidthLine.Value.SetActive(false);
      else
        cacheWidthLine.Value.SetActive(true);
    }

    GameObject createWidthLine(int _index)
    {
      GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(this.baseWidthLine);
      gameObject.transform.parent = this.scroll.scrollView.transform;
      gameObject.transform.localPosition = new Vector3(0.0f, (float) -(_index / 5 * this.IconHeight + this.IconHeight / 2), 0.0f);
      gameObject.transform.localScale = Vector3.one;
      return gameObject;
    }
  }

  protected override void Select(UnitIconBase selectUnitIcon)
  {
    if (selectUnitIcon.Selected)
    {
      this.UnSelect(selectUnitIcon);
      PlayerUnit playerUnit = (PlayerUnit) null;
      UnitIconInfo unitIconInfo = (UnitIconInfo) null;
      foreach (PlayerUnit selectedBasePlayerUnit in this.selectedBasePlayerUnits)
      {
        if (selectedBasePlayerUnit.unit.same_character_id == selectUnitIcon.unit.same_character_id)
        {
          playerUnit = selectedBasePlayerUnit;
          unitIconInfo = this.allUnitInfos.First<UnitIconInfo>((Func<UnitIconInfo, bool>) (x => x.playerUnit == selectUnitIcon.PlayerUnit));
          break;
        }
      }
      this.selectedMaterialUnitIconInfos[this.selectedBasePlayerUnits.IndexOf(playerUnit)].Remove(unitIconInfo);
    }
    else
    {
      this.OnSelect(selectUnitIcon);
      PlayerUnit playerUnit = (PlayerUnit) null;
      UnitIconInfo unitIconInfo = (UnitIconInfo) null;
      foreach (PlayerUnit selectedBasePlayerUnit in this.selectedBasePlayerUnits)
      {
        if (selectedBasePlayerUnit.unit.same_character_id == selectUnitIcon.unit.same_character_id)
        {
          playerUnit = selectedBasePlayerUnit;
          unitIconInfo = this.allUnitInfos.First<UnitIconInfo>((Func<UnitIconInfo, bool>) (x => x.playerUnit == selectUnitIcon.PlayerUnit));
          break;
        }
      }
      this.selectedMaterialUnitIconInfos[this.selectedBasePlayerUnits.IndexOf(playerUnit)].Add(unitIconInfo);
    }
    this.UpdateBottomInfo();
  }

  public override void OnSelect(UnitIconBase unitIcon)
  {
    unitIcon.SelectByCheckIcon(false);
    this.UnitInfoUpdates(unitIcon);
  }

  public override void UpdateSelectIcon()
  {
    foreach (UnitIconInfo displayUnitInfo in this.displayUnitInfos)
    {
      if ((UnityEngine.Object) displayUnitInfo.icon != (UnityEngine.Object) null && displayUnitInfo.playerUnit.favorite)
        displayUnitInfo.icon.Gray = true;
    }
    foreach (UnitIconInfo selectedUnitIcon in this.selectedUnitIcons)
    {
      UnitIconInfo unitInfoDisplay = this.GetUnitInfoDisplay(selectedUnitIcon.playerUnit);
      if (unitInfoDisplay != null && (UnityEngine.Object) unitInfoDisplay.icon != (UnityEngine.Object) null)
        unitInfoDisplay.icon.SelectByCheckIcon(false);
    }
  }

  protected override void CreateUnitIconAction(int info_index, int unit_index)
  {
    UnitIconBase allUnitIcon = this.allUnitIcons[unit_index];
    UnitIconInfo info = this.displayUnitInfos[info_index];
    info.gray = false;
    if (info.select >= 0)
      info.icon.SelectByCheckIcon(false);
    if (info_index % 5 == 0)
      this.ShowWidthLine(info_index);
    allUnitIcon.onClick = (System.Action<UnitIconBase>) (ui => this.Select(ui));
    allUnitIcon.onLongPress = (System.Action<UnitIconBase>) (x => Unit0042Scene.changeSceneEvolutionUnit(true, info.playerUnit, this.getUnits(), true, false, true));
  }

  public override bool SelectedUnitIsMax()
  {
    return false;
  }

  private void UpdateBottomInfo()
  {
    long num1 = 0;
    foreach (PlayerUnit selectedBasePlayerUnit in this.selectedBasePlayerUnits)
    {
      PlayerUnit[] array = this.selectedMaterialUnitIconInfos[this.selectedBasePlayerUnits.IndexOf(selectedBasePlayerUnit)].Select<UnitIconInfo, PlayerUnit>((Func<UnitIconInfo, PlayerUnit>) (x => x.playerUnit)).ToArray<PlayerUnit>();
      num1 += CalcUnitCompose.priceCompose(selectedBasePlayerUnit, array);
    }
    this.useZeny.SetTextLocalize(num1.ToString());
    int num2 = 0;
    foreach (List<UnitIconInfo> materialUnitIconInfo in this.selectedMaterialUnitIconInfos)
      num2 += materialUnitIconInfo.Count<UnitIconInfo>();
    this.selectedCount.text = num2.ToString();
    if (num2 <= 0)
      this.DecisionButton.isEnabled = false;
    else
      this.DecisionButton.isEnabled = true;
  }

  public void OnCombineButton()
  {
    this.StartCoroutine(this.Combine());
  }

  private IEnumerator Combine()
  {
    LumpToutaMaterialConfirmMenu materialConfirmMenu = this;
    Singleton<PopupManager>.GetInstance().open((GameObject) null, false, false, false, false, false, false, "SE_1006");
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_004_8_3_2__anim_popup02.Load<GameObject>();
    IEnumerator e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Unit004832Menu component = Singleton<PopupManager>.GetInstance().open(popupPrefabF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Unit004832Menu>();
    materialConfirmMenu.IsPush = true;
    // ISSUE: reference to a compiler-generated method
    component.callbackNo = new System.Action(materialConfirmMenu.\u003CCombine\u003Eb__19_0);
    component.mode = Unit00468Scene.Mode.UnitLumpTouta;
    bool isAlert = false;
    foreach (List<UnitIconInfo> materialUnitIconInfo in materialConfirmMenu.selectedMaterialUnitIconInfos)
    {
      foreach (UnitIconInfo unitIconInfo in materialUnitIconInfo)
      {
        if (unitIconInfo.playerUnit.unit.rarity.index >= 2)
        {
          isAlert = true;
          break;
        }
      }
      if (isAlert)
        break;
    }
    bool isMemoryAlert = false;
    if (PlayerTransmigrateMemoryPlayerUnitIds.Current != null)
    {
      int?[] memoryPlayerUnitIds = PlayerTransmigrateMemoryPlayerUnitIds.Current.transmigrate_memory_player_unit_ids;
      foreach (List<UnitIconInfo> materialUnitIconInfo in materialConfirmMenu.selectedMaterialUnitIconInfos)
      {
        foreach (UnitIconInfo unitIconInfo in materialUnitIconInfo)
        {
          if (((IEnumerable<int?>) memoryPlayerUnitIds).Contains<int?>(new int?(unitIconInfo.playerUnit.id)))
          {
            isMemoryAlert = true;
            break;
          }
        }
        if (isMemoryAlert)
          break;
      }
    }
    List<List<PlayerUnit>> selectedMaterialPlayerUnits = new List<List<PlayerUnit>>();
    foreach (List<UnitIconInfo> materialUnitIconInfo in materialConfirmMenu.selectedMaterialUnitIconInfos)
    {
      List<PlayerUnit> playerUnitList = new List<PlayerUnit>();
      foreach (UnitIconInfo unitIconInfo in materialUnitIconInfo)
        playerUnitList.Add(unitIconInfo.playerUnit);
      selectedMaterialPlayerUnits.Add(playerUnitList);
    }
    component.Init(materialConfirmMenu.selectedBasePlayerUnits, selectedMaterialPlayerUnits, isAlert, isMemoryAlert);
  }
}
