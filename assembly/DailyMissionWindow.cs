﻿// Decompiled with JetBrains decompiler
// Type: DailyMissionWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using MissionData;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;
using UnityEngine;

public class DailyMissionWindow : MonoBehaviour
{
  [SerializeField]
  [Tooltip("objGuildNotAffiliationを表示する際に調整する")]
  private int objGuildNotAffiliationOffsetY = -274;
  private Dictionary<DailyMissionWindow.ScrollType, MissionType> scrollMissionType = new Dictionary<DailyMissionWindow.ScrollType, MissionType>()
  {
    {
      DailyMissionWindow.ScrollType.Daily,
      MissionType.daily
    },
    {
      DailyMissionWindow.ScrollType.Game,
      MissionType.game
    },
    {
      DailyMissionWindow.ScrollType.Period,
      MissionType.period
    },
    {
      DailyMissionWindow.ScrollType.Guild,
      MissionType.guild
    }
  };
  private bool[] isScrollInit = new bool[Enum.GetValues(typeof (DailyMissionWindow.ScrollType)).Length];
  [SerializeField]
  public DailyMissionList[] dirMissionListObject;
  [SerializeField]
  private GameObject dailyQuest;
  [SerializeField]
  private List<DailyMissionPointRewardItemController> dailyMissionPointRewardItems;
  [SerializeField]
  private UIButton weeelyMissionButton;
  [SerializeField]
  private GameObject weeklyMissionButtonBadge;
  [SerializeField]
  private UILabel pointAcquiredTodayText;
  [SerializeField]
  private UIProgressBar dailyMissionPointGauge;
  [SerializeField]
  private GameObject[] dirBadge;
  [SerializeField]
  private SpreadColorButton[] tabBtn;
  [SerializeField]
  [Tooltip("ギルドミッション表示時、ギルド未所属時に表示")]
  private GameObject objGuildNotAffiliation;
  [SerializeField]
  [Tooltip("objGuildNotAffiliationを表示する際に調整する")]
  private UIWidget widgetGuildMain;
  private int? originalGuildMainAnchorAbsolute;
  private HelpCategory helpCategory;
  private List<PointRewardBox> weeklyPointRewardBoxList;
  private int currentWeeklyPoint;
  private List<PointRewardBox> dailyPointRewardBoxList;
  private int currentDailyPoint;
  private bool isProcessingMissionList;
  private DailyMissionWindow.ScrollType scrollType;
  private Dictionary<int, IMissionAchievement[]> missionDic;
  private DailyMissionController controller;

  public bool isGuildNotAffiliation { get; private set; }

  public bool IsPush { get; set; }

  public bool IsPushAndSet()
  {
    if (this.IsPush)
      return true;
    this.IsPush = true;
    return false;
  }

  private IEnumerator DisplayMissionList(DailyMissionWindow.ScrollType type)
  {
    this.isProcessingMissionList = true;
    Singleton<CommonRoot>.GetInstance().ShowLoadingLayer(1, true);
    int index1 = (int) type;
    for (int index2 = 0; index2 < this.tabBtn.Length; ++index2)
      this.tabBtn[index2].isEnabled = index2 != index1;
    ((IEnumerable<DailyMissionList>) this.dirMissionListObject).ForEach<DailyMissionList>((System.Action<DailyMissionList>) (x => x.SetVisible(false)));
    this.dailyQuest.SetActive(type == DailyMissionWindow.ScrollType.Daily);
    this.weeelyMissionButton.gameObject.SetActive(type == DailyMissionWindow.ScrollType.Daily);
    DailyMissionList targetList = this.dirMissionListObject[index1];
    if (!targetList.IsCreated)
    {
      IEnumerator e = targetList.Create(this.controller);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
    }
    while (!targetList.IsCreated)
      yield return (object) null;
    targetList.SetVisible(true);
    targetList.scrollView.SetDragAmount(0.0f, targetList.scrollBar.value, true);
    if (!this.isScrollInit[(int) type])
    {
      this.isScrollInit[(int) type] = true;
      targetList.ResetPosition();
    }
    if ((UnityEngine.Object) this.objGuildNotAffiliation != (UnityEngine.Object) null)
      this.objGuildNotAffiliation.SetActive(type == DailyMissionWindow.ScrollType.Guild && this.isGuildNotAffiliation);
    Singleton<CommonRoot>.GetInstance().HideLoadingLayer();
    this.isProcessingMissionList = false;
  }

  public IEnumerator InitMissionList(
    IMissionAchievement[] playerDailyMissions,
    int[] types,
    bool isTabBtnDisplay)
  {
    DailyMissionWindow targetWindow = this;
    ((IEnumerable<bool>) targetWindow.isScrollInit).ForEach<bool>((System.Action<bool>) (x => x = false));
    HashSet<MissionType> missionTargets = new HashSet<MissionType>(((IEnumerable<int>) types).Select<int, MissionType>((Func<int, MissionType>) (i => this.scrollMissionType[(DailyMissionWindow.ScrollType) i])));
    Dictionary<MissionType, IMissionAchievement[]> dictionary = ((IEnumerable<IMissionAchievement>) playerDailyMissions).Where<IMissionAchievement>((Func<IMissionAchievement, bool>) (x => x.mission != null && missionTargets.Contains(x.mission.missionType) && x.isShow)).GroupBy<IMissionAchievement, MissionType>((Func<IMissionAchievement, MissionType>) (x => x.mission.missionType)).ToDictionary<IGrouping<MissionType, IMissionAchievement>, MissionType, IMissionAchievement[]>((Func<IGrouping<MissionType, IMissionAchievement>, MissionType>) (k => k.Key), (Func<IGrouping<MissionType, IMissionAchievement>, IMissionAchievement[]>) (v => this.sortMission(v.Key, (IEnumerable<IMissionAchievement>) v).ToArray<IMissionAchievement>()));
    if (targetWindow.missionDic == null)
      targetWindow.missionDic = new Dictionary<int, IMissionAchievement[]>();
    foreach (int type in types)
    {
      IMissionAchievement[] missionAchievementArray;
      if (!dictionary.TryGetValue(targetWindow.scrollMissionType[(DailyMissionWindow.ScrollType) type], out missionAchievementArray))
        missionAchievementArray = new IMissionAchievement[0];
      targetWindow.missionDic[type] = missionAchievementArray;
      targetWindow.dirBadge[type].SetActive(((IEnumerable<IMissionAchievement>) missionAchievementArray).Any<IMissionAchievement>((Func<IMissionAchievement, bool>) (x => x.isCleared && !x.isReceived)));
    }
    ((IEnumerable<DailyMissionList>) targetWindow.dirMissionListObject).ForEach<DailyMissionList>((System.Action<DailyMissionList>) (x => x.SetVisible(true)));
    int[] numArray = types;
    for (int index = 0; index < numArray.Length; ++index)
    {
      int listType = numArray[index];
      targetWindow.dirMissionListObject[listType].Initialize(targetWindow, listType, targetWindow.controller.panelPrefab, targetWindow.missionDic[listType]);
      if (isTabBtnDisplay)
        targetWindow.dirBadge[listType].SetActive(((IEnumerable<IMissionAchievement>) targetWindow.missionDic[listType]).Any<IMissionAchievement>((Func<IMissionAchievement, bool>) (x => x.isCleared && !x.isReceived)));
      if (!PerformanceConfig.GetInstance().IsTuningMissionInitialize)
      {
        IEnumerator e = targetWindow.dirMissionListObject[listType].Create(targetWindow.controller);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    numArray = (int[]) null;
    targetWindow.isProcessingMissionList = false;
    targetWindow.StartCoroutine(targetWindow.DisplayMissionList(targetWindow.scrollType));
  }

  private IEnumerable<IMissionAchievement> sortMission(
    MissionType missionType,
    IEnumerable<IMissionAchievement> sortDat)
  {
    return missionType != MissionType.guild ? sortDat : (IEnumerable<IMissionAchievement>) sortDat.OrderBy<IMissionAchievement, int>((Func<IMissionAchievement, int>) (x =>
    {
      if (x.isReceived)
        return 3;
      if (x.isCleared)
        return 0;
      return x.isOwnCleared ? 1 : 2;
    }));
  }

  public IEnumerator Init(DailyMissionController controller, bool resetCurrentTab = true)
  {
    DailyMissionWindow dailyMissionWindow1 = this;
    dailyMissionWindow1.controller = controller;
    dailyMissionWindow1.dailyQuest.SetActive(false);
    dailyMissionWindow1.weeelyMissionButton.gameObject.SetActive(false);
    if (dailyMissionWindow1.helpCategory == null)
      dailyMissionWindow1.helpCategory = Array.Find<HelpCategory>(MasterData.HelpCategoryList, (Predicate<HelpCategory>) (x => x.name == "ミッション"));
    foreach (DailyMissionList dailyMissionList in dailyMissionWindow1.dirMissionListObject)
      dailyMissionList.Clear();
    IEnumerator e1 = ServerTime.WaitSync();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    Future<WebAPI.Response.DailymissionIndex> future = WebAPI.DailymissionIndex((System.Action<WebAPI.Response.UserError>) (e =>
    {
      WebAPI.DefaultUserErrorCallback(e);
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    }));
    e1 = future.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (future.Result != null)
    {
      Future<WebAPI.Response.GuildmissionIndex> future2 = WebAPI.GuildmissionIndex((System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = future2.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (future2.Result != null)
      {
        DailyMissionWindow dailyMissionWindow2 = dailyMissionWindow1;
        PlayerAffiliation current = PlayerAffiliation.Current;
        int num = (current != null ? (current.isGuildMember() ? 1 : 0) : 0) == 0 ? 1 : 0;
        dailyMissionWindow2.isGuildNotAffiliation = num != 0;
        if ((UnityEngine.Object) dailyMissionWindow1.widgetGuildMain != (UnityEngine.Object) null)
        {
          if (!dailyMissionWindow1.originalGuildMainAnchorAbsolute.HasValue)
            dailyMissionWindow1.originalGuildMainAnchorAbsolute = new int?(dailyMissionWindow1.widgetGuildMain.topAnchor.absolute);
          dailyMissionWindow1.widgetGuildMain.topAnchor.absolute = dailyMissionWindow1.isGuildNotAffiliation ? dailyMissionWindow1.objGuildNotAffiliationOffsetY : dailyMissionWindow1.originalGuildMainAnchorAbsolute.Value;
        }
        List<IMissionAchievement> lstMission = ((IEnumerable<PlayerDailyMissionAchievement>) future.Result.player_daily_missions).Select<PlayerDailyMissionAchievement, IMissionAchievement>((Func<PlayerDailyMissionAchievement, IMissionAchievement>) (dm => Util.Create(dm))).ToList<IMissionAchievement>();
        GuildMissionInfo[] playerGuildMissions = future2.Result.player_guild_missions;
        if ((playerGuildMissions != null ? playerGuildMissions.Length : 0) > 0)
          lstMission.AddRange(((IEnumerable<GuildMissionInfo>) future2.Result.player_guild_missions).Select<GuildMissionInfo, IMissionAchievement>((Func<GuildMissionInfo, IMissionAchievement>) (gm => Util.Create(gm))));
        if (resetCurrentTab)
          dailyMissionWindow1.scrollType = DailyMissionWindow.ScrollType.Daily;
        int[] types = new int[4]{ 0, 1, 2, 3 };
        e1 = dailyMissionWindow1.InitMissionList(lstMission.ToArray(), types, true);
        while (e1.MoveNext())
          yield return e1.Current;
        e1 = (IEnumerator) null;
        dailyMissionWindow1.InitializePointRewardSection(lstMission.ToArray());
      }
    }
  }

  public void InitializePointRewardSection(IMissionAchievement[] playerDailyMissions)
  {
    DateTime nowTime = ServerTime.NowAppTimeAddDelta();
    PlayerDailyMissionPointDaily daily = SMManager.Get<PlayerDailyMissionPoint>().daily;
    this.currentDailyPoint = daily.point.Value;
    this.pointAcquiredTodayText.SetTextLocalize(this.currentDailyPoint);
    this.dailyPointRewardBoxList = ((IEnumerable<PointRewardBox>) MasterData.PointRewardBoxList).Where<PointRewardBox>((Func<PointRewardBox, bool>) (x => x.type == 1 && x.start_at.HasValue && (x.start_at.Value <= nowTime && x.end_at.HasValue) && x.end_at.Value >= nowTime)).ToList<PointRewardBox>();
    int num = ((IEnumerable<IMissionAchievement>) playerDailyMissions).Where<IMissionAchievement>((Func<IMissionAchievement, bool>) (x => x.mission.missionType == MissionType.daily)).Sum<IMissionAchievement>((Func<IMissionAchievement, int>) (x => x.mission.point));
    int?[] rewardPoints = new int?[this.dailyMissionPointRewardItems.Count + 2];
    rewardPoints[0] = new int?(0);
    rewardPoints[rewardPoints.Length - 1] = new int?(num);
    for (int i = 0; i < this.dailyMissionPointRewardItems.Count; i++)
    {
      PointRewardBox pointRewardBox = this.dailyPointRewardBoxList.FirstOrDefault<PointRewardBox>((Func<PointRewardBox, bool>) (x => x.box_type == i + 1));
      if (pointRewardBox != null)
      {
        this.dailyMissionPointRewardItems[i].gameObject.SetActive(true);
        this.dailyMissionPointRewardItems[i].Init(this.controller, pointRewardBox, this.currentDailyPoint, ((IEnumerable<int?>) daily.received_rewards).Contains<int?>(new int?(pointRewardBox.ID)), new System.Action(this.UpdateDailyPointRewardItems));
      }
      else
        this.dailyMissionPointRewardItems[i].gameObject.SetActive(false);
      rewardPoints[i + 1] = pointRewardBox?.point;
    }
    this.SetDailyMissionPointGauge(this.currentDailyPoint, rewardPoints);
    this.weeklyPointRewardBoxList = ((IEnumerable<PointRewardBox>) MasterData.PointRewardBoxList).Where<PointRewardBox>((Func<PointRewardBox, bool>) (x => x.type == 2 && x.start_at.HasValue && (x.start_at.Value <= nowTime && x.end_at.HasValue) && x.end_at.Value >= nowTime)).ToList<PointRewardBox>();
    PlayerDailyMissionPointWeekly weekly = SMManager.Get<PlayerDailyMissionPoint>().weekly;
    this.currentWeeklyPoint = weekly.point.Value;
    int?[] receivedWeeklyRewardBoxIDs = weekly.received_rewards;
    this.weeklyMissionButtonBadge.SetActive(this.weeklyPointRewardBoxList.Any<PointRewardBox>((Func<PointRewardBox, bool>) (x => this.currentWeeklyPoint >= x.point && !((IEnumerable<int?>) receivedWeeklyRewardBoxIDs).Contains<int?>(new int?(x.ID)))));
    this.weeelyMissionButton.isEnabled = this.weeklyPointRewardBoxList.Count > 0;
    this.UpdateDailyMissionTabBadge();
  }

  private void UpdateDailyPointRewardItems()
  {
    for (int i = 0; i < this.dailyMissionPointRewardItems.Count; i++)
    {
      PointRewardBox pointRewardBox = this.dailyPointRewardBoxList.FirstOrDefault<PointRewardBox>((Func<PointRewardBox, bool>) (x => x.box_type == i + 1));
      if (pointRewardBox != null)
      {
        this.dailyMissionPointRewardItems[i].gameObject.SetActive(true);
        this.dailyMissionPointRewardItems[i].Init(this.controller, pointRewardBox, this.currentDailyPoint, ((IEnumerable<int?>) SMManager.Get<PlayerDailyMissionPoint>().daily.received_rewards).Contains<int?>(new int?(pointRewardBox.ID)), new System.Action(this.UpdateDailyPointRewardItems));
      }
      else
        this.dailyMissionPointRewardItems[i].gameObject.SetActive(false);
    }
    this.UpdateDailyMissionTabBadge();
  }

  private void UpdateDailyMissionTabBadge()
  {
    bool flag1 = ((IEnumerable<IMissionAchievement>) this.missionDic[0]).Any<IMissionAchievement>((Func<IMissionAchievement, bool>) (x => x.isCleared && !x.isReceived));
    PlayerDailyMissionPointWeekly weeklyData = SMManager.Get<PlayerDailyMissionPoint>().weekly;
    bool flag2 = this.weeklyPointRewardBoxList.Any<PointRewardBox>((Func<PointRewardBox, bool>) (x =>
    {
      int? point1 = weeklyData.point;
      int point2 = x.point;
      return point1.GetValueOrDefault() >= point2 & point1.HasValue && !((IEnumerable<int?>) weeklyData.received_rewards).Contains<int?>(new int?(x.ID));
    }));
    PlayerDailyMissionPointDaily dailyData = SMManager.Get<PlayerDailyMissionPoint>().daily;
    bool flag3 = this.dailyPointRewardBoxList.Any<PointRewardBox>((Func<PointRewardBox, bool>) (x =>
    {
      int? point1 = dailyData.point;
      int point2 = x.point;
      return point1.GetValueOrDefault() >= point2 & point1.HasValue && !((IEnumerable<int?>) dailyData.received_rewards).Contains<int?>(new int?(x.ID));
    }));
    this.dirBadge[0].SetActive(flag1 | flag2 | flag3);
  }

  private void SetDailyMissionPointGauge(int currentPoint, int?[] rewardPoints)
  {
    float[] numArray = new float[7]
    {
      0.0f,
      0.08f,
      0.297f,
      0.502f,
      0.715f,
      0.928f,
      1f
    };
    int index1 = 1;
    for (int index2 = 1; index2 < rewardPoints.Length; ++index2)
    {
      if (rewardPoints[index2].HasValue && currentPoint <= rewardPoints[index2].Value)
      {
        index1 = index2;
        break;
      }
    }
    int index3 = 0;
    for (int index2 = index1 - 1; index2 >= 0; --index2)
    {
      if (rewardPoints[index2].HasValue)
      {
        index3 = index2;
        break;
      }
    }
    this.dailyMissionPointGauge.value = numArray[index3] + (float) (((double) numArray[index1] - (double) numArray[index3]) * ((double) currentPoint - (double) rewardPoints[index3].Value)) / (float) (rewardPoints[index1].Value - rewardPoints[index3].Value);
  }

  public void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().DailyMissionController.Hide();
  }

  public void IbtnDaily()
  {
    if (this.isProcessingMissionList || this.scrollType == DailyMissionWindow.ScrollType.Daily)
      return;
    this.scrollType = DailyMissionWindow.ScrollType.Daily;
    this.StartCoroutine(this.DisplayMissionList(this.scrollType));
  }

  public void IbtnGame()
  {
    if (this.isProcessingMissionList || this.scrollType == DailyMissionWindow.ScrollType.Game)
      return;
    this.scrollType = DailyMissionWindow.ScrollType.Game;
    this.StartCoroutine(this.DisplayMissionList(this.scrollType));
  }

  public void IbtnPeriod()
  {
    if (this.isProcessingMissionList || this.scrollType == DailyMissionWindow.ScrollType.Period)
      return;
    this.scrollType = DailyMissionWindow.ScrollType.Period;
    this.StartCoroutine(this.DisplayMissionList(this.scrollType));
  }

  public void IbtnGuild()
  {
    if (this.isProcessingMissionList || this.scrollType == DailyMissionWindow.ScrollType.Guild)
      return;
    this.scrollType = DailyMissionWindow.ScrollType.Guild;
    this.StartCoroutine(this.DisplayMissionList(this.scrollType));
  }

  public void onClickedHelp()
  {
    if (this.helpCategory == null || this.IsPushAndSet())
      return;
    Singleton<CommonRoot>.GetInstance().DailyMissionController.Hide();
    Help0152Scene.ChangeScene(true, this.helpCategory);
  }

  public void OnClickWeeklyMissionButton()
  {
    this.StartCoroutine(this.OpenWeeklyMissionPopup());
  }

  private IEnumerator OpenWeeklyMissionPopup()
  {
    DailyMissionWindow dailyMissionWindow = this;
    IEnumerator e = Singleton<PopupManager>.GetInstance().open(dailyMissionWindow.controller.weeklyMissionPointRewardPopupPrefab, false, false, false, true, false, false, "SE_1006").GetComponent<WeeklyMissionPointRewardPopupController>().Init(dailyMissionWindow.controller, dailyMissionWindow.currentWeeklyPoint, dailyMissionWindow.weeklyPointRewardBoxList, new System.Action(dailyMissionWindow.UpdateWeeklyMissionButtonBadge), new System.Action(dailyMissionWindow.OnClickWeeklyMissionButton));
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  private void UpdateWeeklyMissionButtonBadge()
  {
    this.weeklyMissionButtonBadge.SetActive(this.weeklyPointRewardBoxList.Any<PointRewardBox>((Func<PointRewardBox, bool>) (x => this.currentWeeklyPoint >= x.point && !((IEnumerable<int?>) SMManager.Get<PlayerDailyMissionPoint>().weekly.received_rewards).Contains<int?>(new int?(x.ID)))));
    this.UpdateDailyPointRewardItems();
  }

  private enum ScrollType
  {
    Daily,
    Game,
    Period,
    Guild,
  }
}
