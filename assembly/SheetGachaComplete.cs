﻿// Decompiled with JetBrains decompiler
// Type: SheetGachaComplete
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class SheetGachaComplete : MonoBehaviour
{
  [SerializeField]
  private SheetGachaAnim anim;

  public void Init(System.Action endAction)
  {
    NGSoundManager instance = Singleton<NGSoundManager>.GetInstance();
    if ((UnityEngine.Object) instance != (UnityEngine.Object) null)
      instance.playSE("SE_0554", false, 0.0f, -1);
    this.anim.Init(endAction);
  }
}
