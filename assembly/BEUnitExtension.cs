﻿// Decompiled with JetBrains decompiler
// Type: BEUnitExtension
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;

public static class BEUnitExtension
{
  public static void spawn(this BL.Unit self, BE env, bool resetDangerAria)
  {
    env.core.getUnitPosition(self).resetSpawnPosition(env.core, false, resetDangerAria);
    self.isEnable = true;
    if (self.isView)
      env.unitResource[self].unitParts_.spawn();
    foreach (BL.UnitPosition unitPosition in env.core.unitPositions.value)
    {
      if (unitPosition.hasPanelsCache)
        unitPosition.clearMovePanelCache();
    }
  }

  public static void rebirthBE(this BL.Unit self, BE env)
  {
    if (!self.isView)
      return;
    env.unitResource[self].unitParts_.rebirth();
  }
}
