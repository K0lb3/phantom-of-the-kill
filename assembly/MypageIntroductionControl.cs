﻿// Decompiled with JetBrains decompiler
// Type: MypageIntroductionControl
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using System.Collections;
using System.Collections.Generic;
using UniLinq;

public class MypageIntroductionControl
{
  private Queue<MypageIntroductionControl.Unit> queUnits_ = new Queue<MypageIntroductionControl.Unit>();
  private System.Action<int> onEnd_;

  public void addExecute(Func<int, bool> check, Func<int, IEnumerator> execute)
  {
    this.queUnits_.Enqueue(new MypageIntroductionControl.Unit(check, execute));
  }

  public void setEnd(System.Action<int> actEnd)
  {
    this.onEnd_ = actEnd;
  }

  public IEnumerator doExecute()
  {
    int count = 0;
    while (this.queUnits_.Any<MypageIntroductionControl.Unit>())
    {
      MypageIntroductionControl.Unit unit = this.queUnits_.Dequeue();
      if (unit.checkExec_(count))
      {
        IEnumerator e = unit.execute_(count);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        ++count;
      }
    }
    this.onEnd_(count);
  }

  private class Unit
  {
    public Func<int, bool> checkExec_ { get; private set; }

    public Func<int, IEnumerator> execute_ { get; private set; }

    public Unit(Func<int, bool> check, Func<int, IEnumerator> execute)
    {
      this.checkExec_ = check;
      this.execute_ = execute;
    }
  }
}
