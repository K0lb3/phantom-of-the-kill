﻿// Decompiled with JetBrains decompiler
// Type: Guild028NgWordPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using UnityEngine;

public class Guild028NgWordPopup : BackButtonMenuBase
{
  private System.Action callback;
  [SerializeField]
  private UILabel txtTitle;
  [SerializeField]
  private UILabel txtDesc;

  public void Initialize(System.Action ok = null)
  {
    if ((UnityEngine.Object) this.GetComponent<UIWidget>() != (UnityEngine.Object) null)
      this.GetComponent<UIWidget>().alpha = 0.0f;
    this.txtTitle.SetTextLocalize(Consts.GetInstance().GUILD_COMMON_NG_WORD_TITLE);
    this.txtDesc.SetTextLocalize(Consts.GetInstance().GUILD_COMMON_NG_WORD);
    this.callback = ok;
  }

  public override void onBackButton()
  {
    Singleton<PopupManager>.GetInstance().dismiss(false);
    if (this.callback == null)
      return;
    this.callback();
  }
}
