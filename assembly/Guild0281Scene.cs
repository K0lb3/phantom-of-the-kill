﻿// Decompiled with JetBrains decompiler
// Type: Guild0281Scene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using MasterDataTable;
using SM;
using System;
using System.Collections;
using UnityEngine;

public class Guild0281Scene : NGSceneBase
{
  private bool changeScene;
  [SerializeField]
  private Guild0281Menu guild0281Menu;
  private static Guild0281Menu menu;
  private bool isBackScene_;
  private bool? restartGvg;

  public void setFlagBackScene(bool flag = true)
  {
    this.isBackScene_ = flag;
  }

  public static void ChangeSceneBattleResultBackButton()
  {
    Singleton<NGSceneManager>.GetInstance().changeScene("guild028_1", false, (object) false, (object) Guild0281Menu.SceneType.GuildTop);
  }

  public static void ChangeSceneGuildTop(bool establishOrJoinGuild = false, Guild0281Menu menu = null, bool stack = true)
  {
    if (establishOrJoinGuild && Persist.guildSetting.Exists)
    {
      Persist.guildSetting.Data.reset();
      Persist.guildSetting.Flush();
    }
    if ((UnityEngine.Object) menu == (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) Guild0281Scene.menu != (UnityEngine.Object) null)
      {
        Guild0281Scene.menu.EndAnimation();
        Guild0281Scene.menu.sceneType = Guild0281Menu.SceneType.NONE;
      }
      Singleton<NGSceneManager>.GetInstance().changeScene("guild028_1", (stack ? 1 : 0) != 0, (object) establishOrJoinGuild, (object) Guild0281Menu.SceneType.GuildTop);
    }
    else
    {
      menu.PlayTweens(Guild0281Menu.HQTOP_TO_GUILDTOP, (EventDelegate.Callback) null);
      menu.InitializeGuildTop();
    }
  }

  public static void ChangeSceneHQTop(bool establishOrJoinGuild = false, Guild0281Menu menu = null)
  {
    if (!PlayerAffiliation.Current.guild.appearance.GuildHQOpen())
      Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, true);
    else if ((UnityEngine.Object) menu == (UnityEngine.Object) null)
    {
      if ((UnityEngine.Object) Guild0281Scene.menu != (UnityEngine.Object) null)
      {
        Guild0281Scene.menu.EndAnimation();
        Guild0281Scene.menu.sceneType = Guild0281Menu.SceneType.NONE;
      }
      Singleton<NGSceneManager>.GetInstance().changeScene("guild028_1", true, (object) establishOrJoinGuild, (object) Guild0281Menu.SceneType.HQTop);
    }
    else
    {
      menu.PlayTweens(Guild0281Menu.GUILDTOP_TO_HQTOP, (EventDelegate.Callback) null);
      menu.InitializeHQTop();
    }
  }

  public IEnumerator onStartSceneAsync(
    bool establishOrJoinGuild,
    Guild0281Menu.SceneType type)
  {
    Guild0281Scene.menu = this.guild0281Menu;
    if (this.guild0281Menu.sceneType != Guild0281Menu.SceneType.NONE)
      type = this.guild0281Menu.sceneType;
    this.changeScene = false;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    IEnumerator e1 = this.whetherResumeGvg();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (this.restartGvg.Value)
    {
      e1 = this.ResumeGvg();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      this.changeScene = true;
    }
    else
    {
      Future<WebAPI.Response.GuildTop> guildTop = WebAPI.GuildTop(-1, false, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }));
      e1 = guildTop.Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (guildTop.Result == null)
      {
        this.changeScene = true;
      }
      else
      {
        Guild0281Scene.menu.setRaidRankingResultFlag(guildTop.Result);
        if (guildTop.Result.display_transfer_popup)
          ModalWindow.Show(Consts.GetInstance().GUILD_APPLY_APPLICANT_TITLE_CHANGE_GUILD, Consts.GetInstance().GUILD_APPLY_APPLICANT_MESSAGE_CHANGE_GUILD, (System.Action) (() => Singleton<PopupManager>.GetInstance().onDismiss()));
        if (establishOrJoinGuild)
        {
          Future<WebAPI.Response.PlayerHelpers> helpers = WebAPI.PlayerHelpers(0, (System.Action<WebAPI.Response.UserError>) (e =>
          {
            WebAPI.DefaultUserErrorCallback(e);
            Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
          }));
          e1 = helpers.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          if (helpers.Result == null)
          {
            this.changeScene = true;
            yield break;
          }
          else
            helpers = (Future<WebAPI.Response.PlayerHelpers>) null;
        }
        if (!PlayerAffiliation.Current.isGuildMember())
        {
          this.changeScene = true;
          Guild02811Scene.ChangeScene();
        }
        else
        {
          guildTop = WebAPI.GuildTop(-1, false, (System.Action<WebAPI.Response.UserError>) (e =>
          {
            WebAPI.DefaultUserErrorCallback(e);
            Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
          }));
          e1 = guildTop.Wait();
          while (e1.MoveNext())
            yield return e1.Current;
          e1 = (IEnumerator) null;
          if (guildTop.Result == null)
          {
            this.changeScene = true;
          }
          else
          {
            Guild0281Scene.menu.setRaidRankingResultFlag(guildTop.Result);
            if (!establishOrJoinGuild && SM.GuildSignal.Current.existPlayershipEventType(GuildEventType.apply_applicant))
            {
              this.changeScene = true;
              Guild02811Scene.ChangeScene();
            }
            else
            {
              e1 = this.guild0281Menu.InitializeAsync(type, guildTop.Result);
              while (e1.MoveNext())
                yield return e1.Current;
              e1 = (IEnumerator) null;
            }
          }
        }
      }
    }
  }

  public void onStartScene(bool establishOrJoinGuild, Guild0281Menu.SceneType type)
  {
    if (this.guild0281Menu.sceneType != Guild0281Menu.SceneType.NONE)
      type = this.guild0281Menu.sceneType;
    if (this.changeScene)
      return;
    switch (type)
    {
      case Guild0281Menu.SceneType.GuildTop:
        this.guild0281Menu.PlayTweens(Guild0281Menu.OTHER_TO_GUILDTOP, new EventDelegate.Callback(((NGSceneBase) this).onTweenFinished));
        this.guild0281Menu.InitializeGuildTop();
        this.guild0281Menu.checkOpenBattleEntryPopup();
        break;
      case Guild0281Menu.SceneType.HQTop:
        this.guild0281Menu.PlayTweens(Guild0281Menu.OTHER_TO_HQTOP, new EventDelegate.Callback(((NGSceneBase) this).onTweenFinished));
        this.guild0281Menu.InitializeHQTop();
        break;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
    if (this.guild0281Menu.GVGResult != null)
      this.StartCoroutine(this.guild0281Menu.GuildBattleResult());
    else
      this.StartCoroutine(this.guild0281Menu.startRankingReward());
  }

  public override void onEndScene()
  {
    if (this.changeScene)
      return;
    this.guild0281Menu.onEndScene();
  }

  public override IEnumerator onEndSceneAsync()
  {
    Guild0281Scene guild0281Scene = this;
    if (guild0281Scene.changeScene)
    {
      guild0281Scene.tweenTimeoutTime = 0.0f;
    }
    else
    {
      float startTime = Time.time;
      while (!guild0281Scene.isTweenFinished && (double) Time.time - (double) startTime < (double) guild0281Scene.tweenTimeoutTime)
        yield return (object) null;
      Singleton<CommonRoot>.GetInstance().loadingMode = 0;
      if (!guild0281Scene.isBackScene_)
        Singleton<CommonRoot>.GetInstance().isLoading = true;
      guild0281Scene.isBackScene_ = false;
      guild0281Scene.isTweenFinished = true;
      yield return (object) null;
      // ISSUE: reference to a compiler-generated method
      yield return (object) guild0281Scene.\u003C\u003En__0();
    }
  }

  public override IEnumerator onDestroySceneAsync()
  {
    Guild0281Scene.menu = (Guild0281Menu) null;
    if (!this.changeScene)
    {
      this.guild0281Menu.onDestroySceneAsync();
      yield return (object) null;
    }
  }

  private IEnumerator whetherResumeGvg()
  {
    Guild0281Scene guild0281Scene1 = this;
    guild0281Scene1.restartGvg = new bool?();
    if (Persist.gvgBattleEnvironment.Exists && (PlayerAffiliation.Current.guild.gvg_status == GvgStatus.fighting || PlayerAffiliation.Current.guild.gvg_status == GvgStatus.aggregating))
    {
      Guild0281Scene guild0281Scene = guild0281Scene1;
      while (!guild0281Scene1.restartGvg.HasValue)
      {
        bool showPopup = true;
        ModalWindow.ShowYesNo(Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_TITLE, Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_DESC, (System.Action) (() =>
        {
          guild0281Scene.restartGvg = new bool?(true);
          guild0281Scene.changeScene = true;
          showPopup = false;
        }), (System.Action) (() => ModalWindow.ShowYesNo(Consts.GetInstance().POPUP_GUILD_BATTLE_NOT_RESUME_TITLE, Consts.GetInstance().POPUP_GUILD_BATTLE_NOT_RESUME_DESC, (System.Action) (() =>
        {
          guild0281Scene.restartGvg = new bool?(false);
          guild0281Scene.StartCoroutine(guild0281Scene.forceCloseGvg((System.Action) (() =>
          {
            showPopup = false;
            Persist.gvgBattleEnvironment.Delete();
          })));
        }), (System.Action) (() => showPopup = false))));
        while (showPopup)
          yield return (object) null;
      }
      yield return (object) null;
    }
    else
    {
      if (Persist.gvgBattleEnvironment.Exists)
        Persist.gvgBattleEnvironment.Delete();
      guild0281Scene1.restartGvg = new bool?(false);
    }
  }

  private IEnumerator ResumeGvg()
  {
    string errorCode = string.Empty;
    Future<WebAPI.Response.GvgBattleResume> ft = WebAPI.GvgBattleResume((System.Action<WebAPI.Response.UserError>) (e =>
    {
      if (e.Code.Equals("GLD014"))
        WebAPI.DefaultUserErrorCallback(e);
      errorCode = e.Code;
    }));
    IEnumerator e1 = ft.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (errorCode.Equals("GLD014"))
      Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
    if (ft.Result == null)
    {
      if (errorCode.Equals("GVG001"))
        ModalWindow.Show(Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_ERROR_TITLE, Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_ERROR_BATTLE_FINISH, (System.Action) (() =>
        {
          Persist.gvgBattleEnvironment.Delete();
          Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, true);
        }));
      else if (errorCode.Equals("GVG006"))
        ModalWindow.Show(Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_ERROR_TITLE, Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_ERROR_BATTLE_EXPIRED, (System.Action) (() =>
        {
          Persist.gvgBattleEnvironment.Delete();
          Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, true);
        }));
      else
        ModalWindow.Show(Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_ERROR_TITLE, Consts.GetInstance().POPUP_GUILD_BATTLE_RESUME_ERROR_MESSAGE, (System.Action) (() =>
        {
          Persist.gvgBattleEnvironment.Delete();
          Guild0281Scene.ChangeSceneGuildTop(false, (Guild0281Menu) null, true);
        }));
    }
    else
    {
      BattleInfo battleInfo = new BattleInfo();
      GuildUtil.gvgBattleIDServer = ft.Result.battle_uuid;
      battleInfo.gvg = true;
      battleInfo.pvp_restart = true;
      Singleton<NGBattleManager>.GetInstance().startBattle(battleInfo, 0);
    }
  }

  private IEnumerator getGvgBattleIDFromServer(System.Action<string> action)
  {
    Future<WebAPI.Response.GvgBattleResume> ftResume = WebAPI.GvgBattleResume((System.Action<WebAPI.Response.UserError>) (e =>
    {
      if (e.Code.Equals("GLD014"))
      {
        WebAPI.DefaultUserErrorCallback(e);
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      }
      if (action == null)
        return;
      action(e.Code);
    }));
    IEnumerator e1 = ftResume.Wait();
    while (e1.MoveNext())
      yield return e1.Current;
    e1 = (IEnumerator) null;
    if (ftResume.Result != null && action != null)
      action(ftResume.Result.battle_uuid);
  }

  private IEnumerator forceCloseGvg(System.Action action)
  {
    string response = string.Empty;
    IEnumerator e1;
    if (string.IsNullOrEmpty(GuildUtil.gvgBattleIDLocal))
    {
      e1 = this.getGvgBattleIDFromServer((System.Action<string>) (res => response = res));
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
    }
    else
      response = GuildUtil.gvgBattleIDLocal;
    if (response.Equals("GLD014"))
    {
      if (action != null)
        action();
    }
    else if (response.Equals(string.Empty))
      ModalWindow.Show(Consts.GetInstance().POPUP_GUILD_BATTLE_CLOSE_ERROR_TITLE, Consts.GetInstance().POPUP_GUILD_BATTLE_CLOSE_ERROR_MESSAGE, (System.Action) (() =>
      {
        if (action == null)
          return;
        action();
      }));
    else if (response.Equals("GVG006") || response.Equals("GVG001"))
    {
      if (action != null)
        action();
    }
    else
    {
      e1 = WebAPI.GvgBattleForceClose(response, (System.Action<WebAPI.Response.UserError>) (e =>
      {
        WebAPI.DefaultUserErrorCallback(e);
        if (!e.Code.Equals("GLD014"))
          return;
        Singleton<NGSceneManager>.GetInstance().changeScene("mypage", false, (object[]) Array.Empty<object>());
      })).Wait();
      while (e1.MoveNext())
        yield return e1.Current;
      e1 = (IEnumerator) null;
      if (action != null)
        action();
    }
  }
}
