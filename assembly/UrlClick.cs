﻿// Decompiled with JetBrains decompiler
// Type: UrlClick
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using UnityEngine;

public class UrlClick : MonoBehaviour
{
  [SerializeField]
  private UILabel label;

  private void OnClick()
  {
    string urlAtPosition = this.label.GetUrlAtPosition(UICamera.lastHit.point);
    if (urlAtPosition == null)
      return;
    Debug.Log((object) ("open url " + urlAtPosition));
    App.OpenUrl(urlAtPosition);
  }
}
