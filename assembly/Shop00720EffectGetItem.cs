﻿// Decompiled with JetBrains decompiler
// Type: Shop00720EffectGetItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class Shop00720EffectGetItem : MonoBehaviour
{
  [SerializeField]
  private Shop00720EffectRarity[] rarities_;

  public void getItemSoundPlay(int n)
  {
    this.rarities_[n - 1].getItemSoundPlay(n);
  }
}
