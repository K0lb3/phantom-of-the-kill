﻿// Decompiled with JetBrains decompiler
// Type: ResourceDownloader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using DeviceKit;
using GameCore;
using gu3.Device;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UniLinq;
using UnityEngine;

public static class ResourceDownloader
{
  private const int DELETE_MAX_YIELD_COUNT = 50;
  private static DLC assetBundle;
  private static DLC streamingAssets;
  private static DLC additions;
  private static string error;
  private static bool completed;

  private static IEnumerator DownloadJson(string url, Promise<string> promise)
  {
    using (WWW www = new WWW(url))
    {
      yield return (object) www;
      if (www.error != null)
      {
        Debug.LogError((object) www.error);
        promise.Exception = new Exception(www.error);
        yield break;
      }
      else
      {
        IEnumerator e = Singleton<ResourceManager>.GetInstance().SaveResourceInfo(www.bytes);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
      }
    }
    promise.Result = (string) null;
  }

  public static ResourceDownloader.ProgressInfo Progress
  {
    get
    {
      if (ResourceDownloader.assetBundle == null || ResourceDownloader.streamingAssets == null)
        return (ResourceDownloader.ProgressInfo) null;
      return new ResourceDownloader.ProgressInfo()
      {
        Numerator = ResourceDownloader.assetBundle.GetDownloadedSize(false) + ResourceDownloader.streamingAssets.GetDownloadedSize(false) + (ResourceDownloader.additions != null ? ResourceDownloader.additions.GetDownloadedSize(false) : 0L),
        Denominator = ResourceDownloader.assetBundle.GetDownloadSize(false) + ResourceDownloader.streamingAssets.GetDownloadSize(false) + (ResourceDownloader.additions != null ? ResourceDownloader.additions.GetDownloadSize(false) : 0L)
      };
    }
  }

  public static bool Completed
  {
    get
    {
      return ResourceDownloader.error != null || ResourceDownloader.completed;
    }
    set
    {
      ResourceDownloader.completed = value;
    }
  }

  public static string Error
  {
    get
    {
      return ResourceDownloader.error;
    }
  }

  public static void Restart()
  {
    ResourceDownloader.error = (string) null;
  }

  public static bool IsDlcVersionChange()
  {
    string str = "";
    try
    {
      if (File.Exists(ResourceManager.dlcVersionPath))
        str = File.ReadAllText(ResourceManager.dlcVersionPath, Encoding.UTF8);
    }
    catch (Exception ex)
    {
      Debug.LogWarning((object) ex.ToString());
    }
    return str != StartupDownLoad.GetLastestVersion() || !File.Exists(ResourceManager.pathsJsonPath);
  }

  private static IEnumerator InternalStart(
    MonoBehaviour mono,
    string urlBase,
    string dlcVersion,
    bool confirmDLC,
    bool appendFiles,
    System.Action actionAbort)
  {
    while (!Caching.ready)
      yield return (object) null;
    bool dlcVersionChanged = ResourceDownloader.IsDlcVersionChange();
    IEnumerator e;
    if (dlcVersionChanged)
    {
      string jsonUrl = !Persist.normalDLC.Data.IsSound ? string.Format("{0}{1}_{2}.json", (object) urlBase, (object) Revision.ApplicationVersion, (object) dlcVersion) : string.Format("{0}{1}_{2}.json", (object) urlBase, (object) Revision.ApplicationVersion, (object) (dlcVersion + "_normal"));
label_5:
      Future<string> future = new Future<string>((Func<Promise<string>, IEnumerator>) (promise => ResourceDownloader.DownloadJson(jsonUrl, promise)));
      e = future.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      if (future.Exception != null)
      {
        ResourceDownloader.error = future.Exception.ToString();
        Debug.LogError((object) ResourceDownloader.error);
        ResourceDownloader.error = Consts.GetInstance().dlc_fail_download_text;
        while (ResourceDownloader.error != null)
          yield return (object) null;
        goto label_5;
      }
    }
    e = Singleton<ResourceManager>.GetInstance().InitResourceInfo();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    ResourceInfo resourceInfo = Singleton<ResourceManager>.GetInstance().ResourceInfo;
    int num = 0;
    string dlcResourceDirectory = DLC.ResourceDirectory;
    if (!Directory.Exists(dlcResourceDirectory))
      Directory.CreateDirectory(dlcResourceDirectory);
    List<string> assetBundlePaths = new List<string>();
    List<string> streamingAssetPaths = new List<string>();
    HashSet<string> files = new HashSet<string>((IEnumerable<string>) FileManager.GetEntries(dlcResourceDirectory));
    if (dlcVersionChanged)
    {
      ResourceInfo.Resource resource1 = resourceInfo.FirstOrDefault<ResourceInfo.Resource>((Func<ResourceInfo.Resource, bool>) (x => x._key == Singleton<NGSoundManager>.GetInstance().platform + "/VO_9999_acb"));
      bool isDLCMobVoice = resource1._key != null && resource1._value.Exists;
      bool isDLCOpMovie = !Persist.tutorial.Data.IsFinishTutorial();
      foreach (ResourceInfo.Resource resource2 in resourceInfo)
      {
        ResourceInfo.Value obj = resource2._value;
        switch (obj._path_type)
        {
          case ResourceInfo.PathType.AssetBundle:
            if (files.Contains(obj._file_name))
            {
              CachedFile.Add(dlcResourceDirectory + obj._file_name);
              files.Remove(obj._file_name);
              break;
            }
            if (ResourceManager.IsInitialDLCTarget(resource2._key, isDLCMobVoice, isDLCOpMovie))
            {
              assetBundlePaths.Add(resource2._key);
              break;
            }
            break;
          case ResourceInfo.PathType.StreamingAssets:
            if (files.Contains(obj._file_name))
            {
              CachedFile.Add(dlcResourceDirectory + obj._file_name);
              files.Remove(obj._file_name);
              break;
            }
            if (ResourceManager.IsInitialDLCTarget(resource2._key, isDLCMobVoice, isDLCOpMovie))
            {
              streamingAssetPaths.Add(resource2._key);
              break;
            }
            break;
        }
        if (++num >= 1000)
        {
          yield return (object) null;
          num = 0;
        }
      }
      foreach (string str in files)
      {
        try
        {
          File.Delete(dlcResourceDirectory + "/" + str);
        }
        catch (Exception ex)
        {
        }
        if (++num >= 200)
        {
          yield return (object) null;
          num = 0;
        }
      }
      ResourceDownloader.assetBundle = new DLC(resourceInfo, assetBundlePaths.ToArray(), true);
      ResourceDownloader.streamingAssets = new DLC(resourceInfo, streamingAssetPaths.ToArray(), true);
      ResourceDownloader.additions = (DLC) null;
      if (appendFiles)
        yield return (object) ResourceDownloader.appendDownload(resourceInfo, (System.Action<DLC>) (r => ResourceDownloader.additions = r));
      if (confirmDLC)
      {
        DLC[] dlcArray1;
        if (ResourceDownloader.additions == null)
          dlcArray1 = new DLC[2]
          {
            ResourceDownloader.assetBundle,
            ResourceDownloader.streamingAssets
          };
        else
          dlcArray1 = new DLC[3]
          {
            ResourceDownloader.assetBundle,
            ResourceDownloader.streamingAssets,
            ResourceDownloader.additions
          };
        DLC[] dlcArray2 = dlcArray1;
        bool toNext = false;
        yield return (object) ModalDownloadWindow.Show((IEnumerable<DLC>) dlcArray2, (System.Action) (() => toNext = true), string.Empty);
        if (!toNext)
        {
          actionAbort();
          yield break;
        }
      }
label_49:
      long requiredSize = ResourceDownloader.assetBundle.GetStoreSize(false) + ResourceDownloader.streamingAssets.GetStoreSize(false) + (ResourceDownloader.additions != null ? ResourceDownloader.additions.GetStoreSize(false) : 0L);
      long int64 = Convert.ToInt64(Sys.GetAvailableStorageBytes());
      if (requiredSize > int64)
      {
        ResourceDownloader.error = Consts.GetInstance().dlcNotEnoughDiskSpace(requiredSize);
        while (ResourceDownloader.error != null)
          yield return (object) null;
        goto label_49;
      }
      else
      {
label_54:
        e = ResourceDownloader.assetBundle.Start(mono);
        while (e.MoveNext())
          yield return e.Current;
        e = (IEnumerator) null;
        if (ResourceDownloader.assetBundle.Error != null)
        {
          ResourceDownloader.error = ResourceDownloader.assetBundle.Error;
          while (ResourceDownloader.error != null)
            yield return (object) null;
          goto label_54;
        }
        else
        {
label_61:
          e = ResourceDownloader.streamingAssets.Start(mono);
          while (e.MoveNext())
            yield return e.Current;
          e = (IEnumerator) null;
          if (ResourceDownloader.streamingAssets.Error != null)
          {
            ResourceDownloader.error = ResourceDownloader.streamingAssets.Error;
            while (ResourceDownloader.error != null)
              yield return (object) null;
            goto label_61;
          }
          else
          {
            if (ResourceDownloader.additions != null)
            {
label_69:
              e = ResourceDownloader.additions.Start(mono);
              while (e.MoveNext())
                yield return e.Current;
              e = (IEnumerator) null;
              if (ResourceDownloader.additions.Error != null)
              {
                ResourceDownloader.error = ResourceDownloader.additions.Error;
                while (ResourceDownloader.error != null)
                  yield return (object) null;
                goto label_69;
              }
            }
            File.WriteAllText(ResourceManager.dlcVersionPath, dlcVersion, Encoding.UTF8);
            ResourceManager.DLCVersion = dlcVersion;
          }
        }
      }
    }
    else
    {
      foreach (ResourceInfo.Resource resource in resourceInfo)
      {
        ResourceInfo.Value obj = resource._value;
        switch (obj._path_type)
        {
          case ResourceInfo.PathType.AssetBundle:
            if (files.Contains(obj._file_name))
            {
              CachedFile.Add(dlcResourceDirectory + obj._file_name);
              continue;
            }
            continue;
          case ResourceInfo.PathType.StreamingAssets:
            if (files.Contains(obj._file_name))
            {
              CachedFile.Add(dlcResourceDirectory + obj._file_name);
              continue;
            }
            continue;
          default:
            continue;
        }
      }
    }
    ResourceDownloader.completed = true;
  }

  private static IEnumerator appendDownload(
    ResourceInfo resourceInfo,
    System.Action<DLC> result)
  {
    string path = System.IO.Path.Combine(Application.streamingAssetsPath, "first_dlc");
    string str1 = (string) null;
    if (File.Exists(path))
      str1 = File.ReadAllText(path, Encoding.UTF8);
    List<string> source = new List<string>();
    if (!string.IsNullOrEmpty(str1))
    {
      string str2 = "~/";
      string str3 = Singleton<NGSoundManager>.GetInstance().platform + "/";
      char[] chArray = new char[1]{ '\xFEFF' };
      using (StringReader stringReader = new StringReader(str1.TrimStart(chArray)))
      {
        while (stringReader.Peek() > -1)
        {
          string str4 = stringReader.ReadLine();
          if (!string.IsNullOrEmpty(str4))
          {
            if (str4.StartsWith(str2))
              source.Add(str3 + str4.Substring(str2.Length));
            else
              source.Add(str4);
          }
          else
            break;
        }
      }
    }
    result(source.Any<string>() ? new DLC(resourceInfo, source.ToArray(), false) : (DLC) null);
    yield break;
  }

  public static Coroutine Start(
    MonoBehaviour mono,
    string urlBase,
    string dlcVersion,
    bool confirmDLC,
    bool appendFiles,
    System.Action actionAbort)
  {
    ResourceDownloader.completed = false;
    return mono.StartCoroutine(ResourceDownloader.InternalStart(mono, urlBase, dlcVersion, confirmDLC, appendFiles, actionAbort));
  }

  public static IEnumerator CleanCache(System.Action<int, int> progress)
  {
    if (Directory.Exists(DLC.ResourceDirectory))
    {
      ResourceDownloader.ClearDLC();
      CachedFile.Clear();
      Caching.ClearCache();
      IEnumerator e = ResourceDownloader.DeleteContents(progress);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      File.Delete(ResourceManager.dlcVersionPath);
      File.Delete(ResourceManager.pathsJsonPath);
    }
  }

  public static void ClearDLC()
  {
    ResourceDownloader.assetBundle = (DLC) null;
    ResourceDownloader.streamingAssets = (DLC) null;
  }

  public static IEnumerator DeleteContents(System.Action<int, int> progress)
  {
    int num = 0;
    int count = 0;
    string[] files;
    try
    {
      files = Directory.GetFiles(DLC.ResourceDirectory);
    }
    catch (Exception ex)
    {
      Debug.LogException(ex);
      yield break;
    }
    string[] strArray = files;
    for (int index = 0; index < strArray.Length; ++index)
    {
      string path = strArray[index];
      try
      {
        File.Delete(path);
      }
      catch (Exception ex)
      {
        Debug.LogException(ex);
      }
      if (++num >= 50)
      {
        yield return (object) null;
        num = 0;
      }
      progress(++count, files.Length);
    }
    strArray = (string[]) null;
  }

  public static IEnumerator DeleteContents(List<string> fileNames)
  {
    int num = 0;
    foreach (string fileName in fileNames)
    {
      try
      {
        File.Delete(System.IO.Path.Combine(DLC.ResourceDirectory, fileName));
      }
      catch (Exception ex)
      {
        Debug.LogException(ex);
      }
      if (++num >= 50)
      {
        yield return (object) null;
        num = 0;
      }
    }
  }

  public class ProgressInfo
  {
    public long Numerator;
    public long Denominator;
  }
}
