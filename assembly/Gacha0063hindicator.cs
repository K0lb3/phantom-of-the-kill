﻿// Decompiled with JetBrains decompiler
// Type: Gacha0063hindicator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gacha0063hindicator : NGMenuBase
{
  [SerializeField]
  protected UILabel TxtGachaPt;
  [SerializeField]
  protected UILabel TxtGachaTicket;
  [SerializeField]
  private GameObject newIcon;
  public List<GachaButton> gachaButton;
  public List<GachaButton> compensationGachaButton;
  public List<GameObject> dirNormalObject;
  public List<GameObject> dirCompensationObject;
  public GachaButton singleGachaButton;
  public GachaButton singleCompensationGachaButton;
  public GachaButton singleGachaButtonEx;
  public UI2DSprite singleGachaButtonExIcon;
  public GachaButton singleCompensationGachaButtonEx;
  public GameObject singleCompensationGachaButtonExObject;
  private GachaModule gachaModule;

  public GachaModule GachaModule
  {
    get
    {
      return this.gachaModule;
    }
    set
    {
      this.gachaModule = value;
    }
  }

  public Gacha0063Menu Menu { get; set; }

  public int GachaNumber
  {
    get
    {
      return this.gachaModule == null ? -1 : this.gachaModule.number;
    }
  }

  public virtual void InitGachaModuleGacha(
    Gacha0063Menu gacha0063Menu,
    GachaModule gachaModule,
    DateTime serverTime,
    UIScrollView scrollView)
  {
  }

  public virtual void InitGachaModuleGacha(
    Gacha0063Menu menu,
    GachaModuleGacha gacha,
    GachaModule gachaModule)
  {
  }

  public virtual IEnumerator Set(GameObject detailPopup)
  {
    yield break;
  }

  public virtual void PlayAnim()
  {
  }

  public virtual void EndAnim()
  {
  }

  public void SetNewIconVisibility(bool isVisible)
  {
    this.newIcon.SetActive(isVisible);
  }

  public virtual void IbtnBuyKiseki()
  {
  }

  public virtual void IbtnGachaCharge()
  {
  }

  public virtual void IbtnGachaPt01()
  {
  }

  public virtual void IbtnGachaPt02()
  {
  }

  public virtual void IbtnGachaTicket01()
  {
  }

  public virtual void IbtnGachaTicket02()
  {
  }

  public virtual void IbtnGetList01()
  {
  }

  public virtual void IbtnGetList02()
  {
  }

  public virtual void IbtnUnitlist()
  {
  }
}
