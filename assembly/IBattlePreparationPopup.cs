﻿// Decompiled with JetBrains decompiler
// Type: IBattlePreparationPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;

public interface IBattlePreparationPopup
{
  PlayerUnit[] GetPopupDeck();

  PlayerUnit GetPopupFriend();

  PlayerItem[] GetPopupSupply();

  int[] GetPopupGrayUnitIds();

  void OnPopupClose();

  void OnPopupSortie();

  void OnPopupUnitDetailOpen(PlayerUnit unit, PlayerUnit[] units, bool isFriend);

  void OnPopupDeckEditOpen(PlayerUnit[] deck);

  void OnPopupAutoDeckEdit();

  void OnPopupGearEquipOpen();

  void OnPopupGearRepairOpen();

  void OnPopupSupplyEquipOpen();

  void OnPopupBattleConfigOpen();
}
