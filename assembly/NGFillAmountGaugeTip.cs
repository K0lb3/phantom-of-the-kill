﻿// Decompiled with JetBrains decompiler
// Type: NGFillAmountGaugeTip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class NGFillAmountGaugeTip : MonoBehaviour
{
  public float OffSetX;
  [SerializeField]
  private UISprite mParentSprite;

  private void Update()
  {
    float x = (float) this.mParentSprite.width * this.mParentSprite.fillAmount + this.OffSetX;
    Vector3 localPosition = this.transform.localPosition;
    if ((double) x == (double) localPosition.x)
      return;
    this.transform.localPosition = new Vector3(x, localPosition.y, localPosition.z);
  }
}
