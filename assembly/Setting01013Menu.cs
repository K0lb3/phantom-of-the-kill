﻿// Decompiled with JetBrains decompiler
// Type: Setting01013Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting01013Menu : BackButtonMenuBase
{
  [SerializeField]
  protected UILabel InpId01;
  [SerializeField]
  protected UILabel README;
  [SerializeField]
  protected UILabel TxtDescription;
  [SerializeField]
  protected UILabel TxtPopuptitle;
  [SerializeField]
  protected UILabel TxtTitle;
  public TutorialAdvice advice;

  private void Start()
  {
    this.InpId01.GetComponent<UIInput>().caretColor = Color.black;
  }

  public void Initialize()
  {
    Player player = SMManager.Get<Player>();
    this.InpId01.SetTextLocalize(player.name);
    this.InpId01.GetComponent<UIInput>().value = player.name;
    this.InpId01.GetComponent<UIInput>().value = player.name;
    this.InpId01.GetComponent<UIInput>().defaultText = player.name;
    this.InpId01.GetComponent<UIInput>().onValidate = new UIInput.OnValidate(this.onValidate);
    this.advice.gameObject.SetActive(false);
  }

  private char onValidate(string text, int charIndex, char addedChar)
  {
    bool flag = char.IsControl(addedChar) || addedChar >= '\xE000' && addedChar <= '\xF8FF';
    Debug.Log((object) (((int) addedChar).ToString() + ":" + flag.ToString()));
    return flag ? char.MinValue : addedChar;
  }

  public void onChangeInput()
  {
    int num = this.IsPush ? 1 : 0;
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }

  public virtual void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<NGSceneManager>.GetInstance().backScene();
  }

  private IEnumerator NameEdit()
  {
    Setting01013Menu menu = this;
    menu.InpId01.SetText(menu.InpId01.text.ToConverter());
    Future<GameObject> popupPrefabF = Res.Prefabs.popup.popup_010_1_5__anim_popup01.Load<GameObject>();
    IEnumerator e = popupPrefabF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Popup01015Menu component = Singleton<PopupManager>.GetInstance().open(popupPrefabF.Result, false, false, false, true, false, false, "SE_1006").GetComponent<Popup01015Menu>();
    menu.StartCoroutine(component.Init(menu, menu.InpId01.text));
  }

  public virtual void IbtnPopupOk()
  {
    if (this.IsPushAndSet())
      return;
    this.StartCoroutine(this.NameEdit());
  }

  public void ErrDialog()
  {
    this.advice.gameObject.SetActive(true);
    this.alert(Consts.GetInstance().tutorial_fail_player_name);
  }

  private void alert(string message)
  {
    this.advice.SetMessage("#background black\n" + message, (Dictionary<string, Func<Transform, UIButton>>) null);
    this.advice.Show();
  }
}
