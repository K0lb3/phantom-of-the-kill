﻿// Decompiled with JetBrains decompiler
// Type: AnchorTargetAdjustment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class AnchorTargetAdjustment : MonoBehaviour
{
  public string targetParentName = "MainPanel";

  private void Start()
  {
    UIWidget component = this.GetComponent<UIWidget>();
    if ((Object) component == (Object) null)
      return;
    AnchorAdjustmentController.AdjustAnchor(component, this.targetParentName, (string) null);
  }

  private void OnDisable()
  {
    UIWidget component = this.GetComponent<UIWidget>();
    if ((Object) component == (Object) null)
      return;
    Transform parentInFind = this.transform.GetParentInFind(this.targetParentName);
    if ((Object) parentInFind == (Object) null)
      return;
    component.leftAnchor.target = parentInFind;
    component.rightAnchor.target = parentInFind;
    component.topAnchor.target = parentInFind;
    component.bottomAnchor.target = parentInFind;
    component.ResetAnchors();
    component.Update();
  }
}
