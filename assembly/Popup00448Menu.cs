﻿// Decompiled with JetBrains decompiler
// Type: Popup00448Menu
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;

public class Popup00448Menu : NGMenuBase
{
  public UILabel TxtDescription;

  public IEnumerator Init(PlayerItem item)
  {
    this.TxtDescription.SetTextLocalize(Consts.Format(Consts.GetInstance().popup_004_4_8, (IDictionary) new Hashtable()
    {
      {
        (object) "Gear",
        (object) item.gear.kind.name
      },
      {
        (object) "Proficiencies",
        (object) MasterData.UnitProficiency[item.gear.rarity.index].proficiency
      }
    }));
    yield break;
  }

  public virtual void IbtnOk()
  {
    Singleton<PopupManager>.GetInstance().onDismiss();
  }
}
