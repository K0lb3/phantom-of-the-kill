﻿// Decompiled with JetBrains decompiler
// Type: PopupHelperSelector
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class PopupHelperSelector : Quest00282Menu
{
  private object questS_;
  private System.Action onClose_;

  public static Future<GameObject> loadPrefab()
  {
    return new ResourceObject("Prefabs/battle/popup_Re_sortie_frend_selection__anim_popup01").Load<GameObject>();
  }

  public static void open(
    GameObject prefab,
    object questS,
    System.Action<PlayerHelper> eventSetHelper,
    System.Action eventClose)
  {
    GameObject gameObject = Singleton<PopupManager>.GetInstance().open(prefab, false, false, false, true, true, true, "SE_1006");
    gameObject.GetComponent<PopupHelperSelector>().initialize(questS, eventSetHelper, eventClose);
    gameObject.SetActive(true);
  }

  private void initialize(object questS, System.Action<PlayerHelper> eventSetHelper, System.Action eventClose)
  {
    this.setEventSetHelper((System.Action<PlayerHelper>) (helper =>
    {
      if (eventSetHelper != null)
        eventSetHelper(helper);
      if (eventClose != null)
        eventClose();
      Singleton<PopupManager>.GetInstance().dismiss(false);
    }));
    this.questS_ = questS;
    this.onClose_ = eventClose;
  }

  private IEnumerator Start()
  {
    PopupHelperSelector popupHelperSelector = this;
    PlayerStoryQuestS story_quest = (PlayerStoryQuestS) null;
    PlayerExtraQuestS extra_quest = (PlayerExtraQuestS) null;
    PlayerCharacterQuestS char_quest = (PlayerCharacterQuestS) null;
    PlayerQuestSConverter harmony_quest = (PlayerQuestSConverter) null;
    PlayerSeaQuestS sea_quest = (PlayerSeaQuestS) null;
    if (popupHelperSelector.questS_ is PlayerStoryQuestS)
      story_quest = popupHelperSelector.questS_ as PlayerStoryQuestS;
    else if (popupHelperSelector.questS_ is PlayerExtraQuestS)
      extra_quest = popupHelperSelector.questS_ as PlayerExtraQuestS;
    else if (popupHelperSelector.questS_ is PlayerQuestSConverter)
      harmony_quest = popupHelperSelector.questS_ as PlayerQuestSConverter;
    else if (popupHelperSelector.questS_ is PlayerCharacterQuestS)
      char_quest = popupHelperSelector.questS_ as PlayerCharacterQuestS;
    else if (popupHelperSelector.questS_ is PlayerHarmonyQuestS)
      harmony_quest = new PlayerQuestSConverter(popupHelperSelector.questS_ as PlayerHarmonyQuestS);
    else
      sea_quest = popupHelperSelector.questS_ as PlayerSeaQuestS;
    IEnumerator e = popupHelperSelector.InitPlayerDecks(story_quest, extra_quest, char_quest, harmony_quest, sea_quest, false, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    Singleton<PopupManager>.GetInstance().startOpenAnime(popupHelperSelector.gameObject, false);
  }

  public override void IbtnBack()
  {
    if (this.IsPushAndSet())
      return;
    if (this.onClose_ != null)
      this.onClose_();
    Singleton<PopupManager>.GetInstance().dismiss(false);
  }

  public override void onBackButton()
  {
    this.IbtnBack();
  }
}
