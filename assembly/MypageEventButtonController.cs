﻿// Decompiled with JetBrains decompiler
// Type: MypageEventButtonController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using System;
using UnityEngine;

public class MypageEventButtonController : MonoBehaviour
{
  [SerializeField]
  private UIGrid mGrid;
  private MypageEventButton[] mEventButtons;

  public MypageEventButton GetButton<T>()
  {
    return this.mEventButtons == null ? (MypageEventButton) null : Array.Find<MypageEventButton>(this.mEventButtons, (Predicate<MypageEventButton>) (x => x is T));
  }

  public void UpdateButtonState()
  {
    this.mEventButtons = this.GetComponentsInChildren<MypageEventButton>(true);
    int num = 0;
    foreach (MypageEventButton mEventButton in this.mEventButtons)
    {
      if (mEventButton.IsActive())
      {
        mEventButton.SetActive(true);
        if (mEventButton.IsBadge())
          mEventButton.SetBadgeActive(true);
        else
          mEventButton.SetBadgeActive(false);
        ++num;
      }
      else
        mEventButton.SetActive(false);
    }
    this.mGrid.repositionNow = true;
  }
}
