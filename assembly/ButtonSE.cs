﻿// Decompiled with JetBrains decompiler
// Type: ButtonSE
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class ButtonSE : MonoBehaviour
{
  public string SE_name = "SE_1002";
  private NGSoundManager sm;
  private int channel;

  private void Start()
  {
    this.sm = Singleton<NGSoundManager>.GetInstance();
  }

  public void playSound()
  {
    this.sm.playSE(this.SE_name, false, 0.0f, -1);
    Debug.Log((object) "[CRI] ButtonSE playSound");
  }
}
