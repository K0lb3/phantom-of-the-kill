﻿// Decompiled with JetBrains decompiler
// Type: ExploreDeckEditPopup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using SM;
using System.Collections;
using UnityEngine;

public class ExploreDeckEditPopup : BackButtonMenuBase
{
  [SerializeField]
  private ExploreDeckIndicator mExploreDeckIndicator;
  [SerializeField]
  private ExploreDeckIndicator mChallengaDeckIndicator;

  public IEnumerator InitializeAsync()
  {
    ExploreDataManager data = Singleton<ExploreDataManager>.GetInstance();
    data.SetReopenPopupStateDeckEdit();
    IEnumerator e = this.mExploreDeckIndicator.InitializeAsync(data.GetExploreDeck(), data.GetWinRate());
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    PlayerChallenge playerChallenge = SMManager.Get<PlayerChallenge>();
    int winRate = data.GetWinRate(playerChallenge.win_count, playerChallenge.lose_count);
    e = this.mChallengaDeckIndicator.InitializeAsync(data.GetChallengeDeck(), winRate);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  public void onEditExploreDeckButton()
  {
    if (this.IsPushAndSet())
      return;
    Explore033DeckEditScene.ChangeSceneExploreDeckEdit();
  }

  public void onEditChallengeDeckButton()
  {
    if (this.IsPushAndSet())
      return;
    Explore033DeckEditScene.ChangeSceneChallengeDeckEdit();
  }

  public void onCloseButton()
  {
    if (this.IsPushAndSet())
      return;
    Singleton<PopupManager>.GetInstance().dismiss(false);
    Singleton<ExploreDataManager>.GetInstance().InitReopenPopupState();
  }

  public override void onBackButton()
  {
    this.onCloseButton();
  }
}
