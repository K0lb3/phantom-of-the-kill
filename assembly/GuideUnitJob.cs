﻿// Decompiled with JetBrains decompiler
// Type: GuideUnitJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using UnityEngine;

public class GuideUnitJob : MonoBehaviour
{
  private bool isEnable = true;
  [SerializeField]
  private GameObject selectObj;
  [SerializeField]
  private UIButton button;
  private int job_id;
  private Guide01122Menu menu;
  private GuideRaritySelectDialog dialog;

  public Guide01122Menu Menu
  {
    get
    {
      return this.menu;
    }
  }

  public void pressButton()
  {
    this.dialog.onJobButton(this.job_id);
  }

  public void Set(
    Guide01122Menu menu01122,
    GuideRaritySelectDialog dialog,
    bool isEnable,
    int job_id)
  {
    this.menu = menu01122;
    this.dialog = dialog;
    this.isEnable = isEnable;
    this.job_id = job_id;
  }

  public void SetSelect(int select_job_id)
  {
    bool flag = this.job_id == select_job_id;
    this.selectObj.SetActive(flag && this.isEnable);
    if (!(bool) (Object) this.button)
      return;
    this.button.isEnabled = this.isEnable;
    this.button.enabled = !flag;
  }
}
