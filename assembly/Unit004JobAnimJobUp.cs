﻿// Decompiled with JetBrains decompiler
// Type: Unit004JobAnimJobUp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: C18A5813-F680-45BB-9FED-E835B4A3AC46
// Assembly location: S:\Program Files (x86)\DMMGamePlayer\games\PoK\PotK_Data\Managed\Assembly-CSharp.dll

using GameCore;
using SM;
using System.Collections;
using UnityEngine;

public class Unit004JobAnimJobUp : BackButtonMenuBase
{
  [SerializeField]
  private Transform dirJobAfter;
  private PlayerUnit _playerUnit;
  private PlayerUnitJob_abilities _jobAbility;
  private float clickTimer;
  private bool isOpening;
  private System.Action onUpdatedJobAbility_;

  public IEnumerator Init(
    PlayerUnit unit,
    PlayerUnitJob_abilities jobAbility,
    PlayerUnitJob_abilities beforeAbility,
    System.Action eventUpdatedJobAbility)
  {
    this._playerUnit = unit;
    this._jobAbility = jobAbility;
    this.onUpdatedJobAbility_ = eventUpdatedJobAbility;
    this.clickTimer = Time.time + 0.5f;
    Future<GameObject> JobAfterPanelF = Singleton<NGGameDataManager>.GetInstance().IsSea ? Res.Prefabs.unit004_Job.Unit_job_after_sea.Load<GameObject>() : Res.Prefabs.unit004_Job.Unit_job_after.Load<GameObject>();
    IEnumerator e = JobAfterPanelF.Wait();
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
    e = JobAfterPanelF.Result.Clone(this.dirJobAfter).GetComponent<Unit004JobAfter>().Init(true, jobAbility, beforeAbility, false);
    while (e.MoveNext())
      yield return e.Current;
    e = (IEnumerator) null;
  }

  protected override void Update()
  {
    if ((double) this.clickTimer >= (double) Time.time || !Input.GetMouseButtonDown(0) || this.isOpening)
      return;
    this.OpenDialogAndQuitAnim();
    this.isOpening = true;
  }

  public override void onBackButton()
  {
    if (this.IsPushAndSet())
      return;
    this.OpenDialogAndQuitAnim();
  }

  private void OpenDialogAndQuitAnim()
  {
    Singleton<PopupManager>.GetInstance().monitorCoroutine(this.OpenJobDialogPanel());
  }

  private IEnumerator OpenJobDialogPanel()
  {
    Singleton<PopupManager>.GetInstance().closeAllWithoutAnim(false);
    while (Singleton<PopupManager>.GetInstance().isOpenNoFinish)
      yield return (object) null;
    Singleton<CommonRoot>.GetInstance().loadingMode = 1;
    Singleton<CommonRoot>.GetInstance().isLoading = true;
    yield return (object) new WaitForSeconds(0.5f);
    GameObject popup;
    Future<GameObject> prefab;
    IEnumerator e;
    if (this._jobAbility.current_levelup_pattern != null)
    {
      prefab = (Future<GameObject>) null;
      prefab = Singleton<NGGameDataManager>.GetInstance().IsSea ? Res.Prefabs.unit004_Job.Unit_JobCharacteristic_UP_Dialog_sea.Load<GameObject>() : Res.Prefabs.unit004_Job.Unit_JobCharacteristic_UP_Dialog.Load<GameObject>();
      e = prefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, true, true, "SE_1006");
      e = popup.GetComponent<Unit004JobDialogUp>().Init(this._playerUnit, this._jobAbility, this.onUpdatedJobAbility_, false);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      Singleton<PopupManager>.GetInstance().startOpenAnime(popup, false);
      prefab = (Future<GameObject>) null;
    }
    else
    {
      prefab = Res.Prefabs.unit004_Job.Unit_JobCharacteristic_Dialog.Load<GameObject>();
      e = prefab.Wait();
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup = Singleton<PopupManager>.GetInstance().open(prefab.Result, false, false, false, true, false, false, "SE_1006");
      popup.SetActive(false);
      e = popup.GetComponent<Unit004JobDialog>().Init(this._jobAbility);
      while (e.MoveNext())
        yield return e.Current;
      e = (IEnumerator) null;
      popup.SetActive(true);
      prefab = (Future<GameObject>) null;
    }
    Singleton<CommonRoot>.GetInstance().isLoading = false;
    Singleton<CommonRoot>.GetInstance().loadingMode = 0;
  }
}
